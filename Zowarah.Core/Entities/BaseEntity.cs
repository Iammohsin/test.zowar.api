﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Core.Entities
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
