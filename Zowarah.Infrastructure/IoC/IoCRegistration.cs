﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Application.Interfaces.Invoices;
using Zowarah.Application.Interfaces.Payments;
using Zowarah.Application.Interfaces.Security;
using Zowarah.Application.Interfaces.Transactions;
using Zowarah.Application.Interfaces.Users;
using Zowarah.Application.Services.Common;
using Zowarah.Application.Services.Hotels;
using Zowarah.Application.Services.Invoices;
using Zowarah.Application.Services.Payments;
using Zowarah.Application.Services.Transactions;
using Zowarah.Application.Services.Users;
using Zowarah.Data;
using Zowarah.Data.Contexts;
using Zowarah.Data.Repositories;
using Zowarah.Domain.Interfaces;

namespace Zowarah.Infrastructure.IoC
{
    public class IoCRegistration
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddTransient<DbInitializer>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(ILookupRepository<>), typeof(LookupRepository<>));
            services.AddScoped(typeof(IUnitOfWork), typeof(UnitOfWork));
            services.AddScoped(typeof(IPhotoService), typeof(PhotoService));
            services.AddScoped(typeof(ICityService), typeof(CityService));
            services.AddScoped(typeof(IBookingService), typeof(BookingService));
            services.AddScoped(typeof(ICalendarService), typeof(CalendarService));
            services.AddScoped(typeof(IHotelService), typeof(HotelService));
            services.AddScoped(typeof(IRoomService), typeof(RoomService));
            services.AddScoped(typeof(IPhotoService), typeof(PhotoService));
            // services.AddScoped(typeof(IUserSecurityService), typeof(UserSecurityService));
            services.AddScoped(typeof(IUserService), typeof(UserService));
            services.AddScoped(typeof(ITransactionService), typeof(TransactionService));
            services.AddScoped(typeof(IPaymentService), typeof(PaymentService));
            services.AddScoped(typeof(IInvoiceService), typeof(InvoiceService));
            services.AddScoped(typeof(IPaymentService), typeof(PaymentService));
            services.AddScoped<IServiceProfileService, ServiceProfileService>();
            services.AddScoped<IServiceNearbyService, ServiceNearbyService>();
            services.AddScoped<IReviewService, ReviewService>();
            services.AddScoped<IReviewService, ReviewService>();
            services.AddScoped<IReportingService, ReportingService>();
            services.AddScoped<IServiceService, ServiceService>();
            services.AddScoped<IPromotionService, PromotionService>();
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IMessagingService, MessagingService>();
            services.AddScoped(typeof(INotificationService), typeof(NotificationService));
            services.AddSingleton(typeof(IHttpContextAccessor), typeof(HttpContextAccessor));
            services.AddScoped<IMessagingReportService, MessagingReportService>();
            services.AddScoped<ICreditCardService, CreditCardService>();
        }
    }
}