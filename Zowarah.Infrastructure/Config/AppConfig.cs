﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Infrastructure.Config
{
    public class CustomException : Exception
    {
        public CustomException(string message) : base(message)
        {
        }
    }
}
