﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Queries
{
    public class ClientsOverview
    {
        public int CountryId { get; set; }
        public int NoOfClients { get; set; }
        public int NoOfActiveClients { get; set; }

    }
}
