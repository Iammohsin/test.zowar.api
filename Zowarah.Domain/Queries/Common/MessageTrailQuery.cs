﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.Domain.Queries.Common
{
    public class MessageTrailQuery
    {
        public string SenderName { get; set; }
        public DateTime Date { get; set; }
        public string Body { get; set; }
        public Boolean IsRead { get; set; }
       
    }
}
