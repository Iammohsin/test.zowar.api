﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.Domain.Queries.Common
{
    public class MessageQuery
    {
        public string Id { get; set; }
        public long SenderId { get; set; }
        public string SenderName { get; set; }
        public string RecieverName { get; set; }
        public RoleTypes SenderRole { get; set; }
        public RoleTypes ReceiverRole { get; set; }
        public int MessageCategoryId { get; set; }
        public DateTime Date { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public Boolean IsRead { get; set; }
        public int TransactionTypeId { get; set; }
        public string ReferenceId { get; set; }        
        public ICollection<MessageTrailQuery> MessageTrails { get; set; }
       
    }
}