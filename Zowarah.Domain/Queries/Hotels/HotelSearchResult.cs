﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.Domain.Queries.Hotels
{
    public class HotelSearchResult
    {

        public SearchBasicInfo SearchBasicInfo { get; set; }
        public HotelSearchParameters HotelSearchParameters { get; set; }
        public RoomSearchParameters RoomSearchParameters { get; set; }
        public Promotion RoomPromotion { get; set; }
    }
}
