﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.Domain.Queries.Hotels
{
    public class SearchBasicInfo
    {
       
        public long ServiceId { get; set; }
        public string Name { get; set; }

        public long ProviderId { get; set; }

        public long RoomId { get; set; }
        public Boolean IsCreditCardAccepted { get; set; }
        public  IEnumerable<int> Facilities { get; set; }
        public float ReviewScore { get; set; }
        public decimal TotalReviews { get; set; }
        public decimal RatePerNight { get; set; }
        public decimal TotalRatePerNight { get; set; }

        public int Rating { get; set; }
        public decimal TaxP { get; set; } //mohsin 23 
        public decimal Vat { get; set; }
        public string Address { get; set; }
        public Boolean IsPrePayment { get; set; }
        public HotelSearchPromotion Promotion { get; set; }
        public Boolean IsDeleted { get; set; }
        public string Photo { get; set; }
    }
}
