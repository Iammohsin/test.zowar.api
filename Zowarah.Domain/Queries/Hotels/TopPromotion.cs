﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.Domain.Queries.Hotels
{
    public class TopPromotion
    {
        public PropertyBasicInfo PropertyBasicInfo { get; set; }
        public Promotion Promotion { get; set; }
    }
}
