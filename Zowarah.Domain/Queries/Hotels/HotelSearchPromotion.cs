﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.Domain.Queries.Hotels
{
    public class HotelSearchPromotion
    {
        public decimal Discount { get; set; }
        public PromotionType PromotionType { get; set; }
        public int FreeNights { get; set; }
        public int MinimumStay { get; set; }
    }
}
