﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Queries.Hotels
{
    public class HotelSearchParameters
    {
        public int LastReservedRooms { get; set; }
    }
}
