﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Queries.Hotels
{
    public class PropertyDisplayInfo
    {
        public string Id { get; set; }
        public String Name { get; set; }
        public int PropertyTypeId { get; set; }
        public int SmokingOptionId { get; set; }
        public int Rating { get; set; }
        public String Location { get; set; }
        public int NoOfRooms { get; set; }
        public String ContactName { get; set; }
        public String Address { get; set; }
        public int CityId { get; set; }
        public String Contact1 { get; set; }
        public String Contact2 { get; set; }
        public Boolean IsBasicInfoConfigured { get; set; }
        public string ProviderId { get; set; }
        public decimal LocLng { get; set; }
        public decimal LocLat { get; set; }
        public string MainPhoto { get; set; }
    }
}
