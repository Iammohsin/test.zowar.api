﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Common;

namespace Zowarah.Domain.Queries.Hotels
{
    public class ReviewsDetails
    {
        public decimal TotalReviews { get; set; }
        public float ReviewsScore { get; set; }
        public ICollection<ServiceReviewFeaturesScore> Features { get; set; } = new HashSet<ServiceReviewFeaturesScore>();
        public ICollection<ServiceReviewAveragesScore> Averages { get; set; } = new HashSet<ServiceReviewAveragesScore>();
    }
}
