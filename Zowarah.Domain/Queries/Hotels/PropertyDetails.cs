﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Queries.Hotels
{
    public class PropertyDetails
    {

        public long Id { get; set; }
        public int checkInFrom { get; set; }
        public int checkInTo { get; set; }
        public int checkOutFrom { get; set; }
        public int checkOutTo { get; set; }
        public Boolean isPetsAllowed { get; set; }
        public Boolean isChildrenAllowed { get; set; }
        public int internetOptionId { get; set; }
        public int parkingOptionId { get; set; }
        public ICollection<int> Facilities { get; set; }
    }
}
