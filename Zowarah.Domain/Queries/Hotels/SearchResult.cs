﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.Domain.Queries.Hotels
{
    public class SearchResult
    {
        public PropertyBasicInfo PropertyBasicInfo { get; set; }
    }
}
