﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.Domain.Queries.Hotels
{
    public class Promotion
    {
        public long PromotionId {get;set;}
        public long RoomId { get; set; }
        public decimal Discount { get; set; }
        public PromotionType PromotionType { get; set; }
        public int FreeNights { get; set; }
        public int MinimumStay { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
