﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Enums
{
    public enum PeriodEnum
    {
        last7Days = 1,
        last30Days =2 ,
        thisYear = 3,
        custom = 10
    }
}
