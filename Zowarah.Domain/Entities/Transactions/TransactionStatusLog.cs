﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Transactions
{
    public class TransactionStatusLog: MasterStringBaseEntity
    {
        public string TransactionId { get; set; }
        public string Comment { get; set; }
        public virtual Transaction Transaction { get; set; }
    }
}
