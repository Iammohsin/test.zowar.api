﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Invoices;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.Domain.Entities.Transactions
{
    public enum TransactionStatus {
        Confirmed = 1,
        NoShow = 2,
        Cancelled = 3,
        Pending = 4
    }
    public class Transaction : MasterStringBaseEntity
    {
        public Transaction()
        {
            Transactionlogs = new HashSet<TransactionStatusLog>();
        }
        public long ServiceId { get; set; }
        public long ClientId { get; set; }
        public TransactionStatus Status { get; set; }
        public string ProviderCurrency { get; set; }
        public string ClientCurrency { get; set; }
        public decimal ProviderAmount { get; set; }
        public decimal ClientAmount { get; set; }
        public decimal ExchangeRate { get; set; }
        public int CountryId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public int CreditCardId { get; set; }
        public string CardNo { get; set; }
        public byte CardExpiryMonth { get; set; }
        public int CardExpiryYear { get; set; }
        public int Cvv { get; set; }  //mohsin27-2-2020

        public string  InvoiceId { get; set; }
        public DateTime Date { get; set; }
        public TransactionType TransactionType { get; set; }
        public string OwnerId { get; set; }
        public decimal Commission { get; set; }
        public decimal BaseAmount { get; set; }
        public ICollection<TransactionStatusLog> Transactionlogs { get; set; }
        public virtual Service Service { get; set; }
        public virtual AppUser Client { get; set; }
        public virtual Invoice Invoice { get; set; }
        public string STCPayRefNum { get; set; }
    }
}
