﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities
{
    public class LookupBaseEntity : BaseEntity
    {
        public int Id { get; set; }
        public String Description { get; set; }
    }
}
