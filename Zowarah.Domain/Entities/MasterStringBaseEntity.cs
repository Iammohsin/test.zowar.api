﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Zowarah.Domain.Entities
{
    public class MasterStringBaseEntity : BaseEntity
    {
        public string Id { get; set; }
    }
}