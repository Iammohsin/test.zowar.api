﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities
{
    public class LookupStringBaseEntity : BaseEntity
    {
        public string Id { get; set; }
        public String Description { get; set; }
    }
}
