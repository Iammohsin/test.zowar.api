﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Users
{
    public class ProfilePhoto : MasterBaseEntity
    {
        public string FileName { get; set; }
        public long UserId { get; set; }
        public AppUser User { get; set; }
    }
}
