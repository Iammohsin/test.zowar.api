﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Users
{
    public enum RoleTypes
    {
        Administrator = 1,
        Provider = 2,
        Agent = 3,
        Client = 4
    }


    public class AppRole : IdentityRole<long>
    {

    }
    public class AppUserRole : IdentityUserRole<long>
    {

    }

    public class AppUser : IdentityUser<long>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public int? CityId { get; set; }
        public string PostalCode { get; set; }
        public int? CountryId { get; set; }
        public string PassportNo { get; set; }
        public DateTime? PassportExpiryDate { get; set; }
        public string MobilePhoneNo { get; set; }
        public int? CitizenshipCountryId { get; set; }
        public int? Gender { get; set; }
        public int? Title { get; set; }
        public DateTime? Birthday { get; set; }
        public int? DisplayCountryId { get; set; }
        public string DisplayName { get; set; }
        public ProfilePhoto ProfilePhoto { get; set; }
        public string CurrencyId { get; set; }
        public int SmokingOption { get; set; }
        public int Rating { get; set; }
        public string CultureId { get; set; }
        public Boolean IsActive { get; set; }
        public DateTime JoinDate { get; set; }
        public ICollection<UserPermission> UserPermissions { get; set; }

        public void AttachPermission(UserPermission userPermission)
        {
            if (UserPermissions == null)
                UserPermissions = new List<UserPermission>();

            UserPermissions.Add(userPermission);
        }

        public void DetachPermission(UserPermission userPermission)
        {
            if (UserPermissions == null)
                throw new Exception("No permissions assigned to user yet");

            UserPermissions.Remove(userPermission);
        }

    }
}
