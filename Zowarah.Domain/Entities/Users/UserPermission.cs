﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Users
{
    public class UserPermission
    {
        public long UserId { get; set; }
        public AppUser User { get; set; }
        public long PermissionId { get; set; }
        public AppPermission Permission { get; set; }
        
    }
}
