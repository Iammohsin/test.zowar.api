﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Users
{
    public class AppPermission
    {
        public long Id { get; set; }
        public string Description { get; set; }
        public ICollection<UserPermission> UserPermissions { get; set; }
    }
}
