﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Common;

namespace Zowarah.Domain.Entities.Users
{
    public class CreditCardInfo : MasterBaseEntity
    {
        public long UserId { get; set; }
        public int CreditCardId { get; set; }
        public string CardNumber { get; set; }
        public int ExpiryMonth { get; set; }
        public int ExpiryYear { get; set; }
        public string NameOnCard { get; set; }
        public string SecurityCode { get; set; }
        public virtual CreditCard CreditCard { get; set; }
    }
}