﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Hotels
{
    public class HotelFacility 
    {
        public long HotelId { get; set; }
        public int FacilityId { get; set; }
        public virtual Hotel Hotel { get; set; }
        public virtual Facility Facility { get; set; }
    }
}
