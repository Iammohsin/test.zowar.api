﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Common;

namespace Zowarah.Domain.Entities.Hotels
{
    public class HotelCreditCard 
    {
        public long HotelId { get; set; }
        public Hotel Hotel { get; set; }

        public int CreditCardId { get; set; }
        public CreditCard CreditCard { get; set; }
    }
}
