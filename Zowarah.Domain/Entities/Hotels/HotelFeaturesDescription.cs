﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Hotels
{
    public class HotelFeaturesDescription : LookupBaseEntity
    {
        public string Feature { get; set; }
        public int Order { get; set; }
    }
}
