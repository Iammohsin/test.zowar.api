﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Hotels
{
    public enum AudienceEnum
    {
        Public=1,
        Members=2        
    }
    public enum PromotionType {
        Basic=1,
        LastMinute=2,
        Earlybooked=3,
        FreeNights=4
    }


    public class HotelPromotion:MasterBaseEntity
    {
        public HotelPromotion()
        {
            HotelPromotionRooms = new List<HotelPromotionRoom>();
        }

        public long ServiceId { get; set; }
        public string Name { get; set; }
        public PromotionType PromotionType { get; set; }
        public DateTime Date { get; set; }
        public Boolean IsActive { get; set; }
        public AudienceEnum AudienceId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        // used for two cases
        // 1. last minute promotion: can guest booked before (n) days from  check-in date
        // 2. early booked promotion:  guest can booked before (n) days from check-in date
        public int BookingDaysCondition { get; set; }        
        public int FreeNights { get; set; }
        public int MinimumStay { get; set; }
        public decimal Discount { get; set; }
        public ICollection<HotelPromotionRoom> HotelPromotionRooms { get; set; }


    }
}
