﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Hotels
{
    public class Facility : LookupBaseEntity
    {
        public int FacilityCategoryId { get; set; }
        public virtual FacilityCategory FacilityCategory { get; set; }
        public string Icon { get; set; }
    }
}
