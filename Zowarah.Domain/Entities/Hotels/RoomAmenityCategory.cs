﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Hotels
{
    public class RoomAmenityCategory : LookupBaseEntity
    {
        
        public IEnumerable<RoomAmenity> RoomAmenities { get; set; }
    }
}
