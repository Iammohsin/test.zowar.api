﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Zowarah.Domain.Entities.Hotels
{
    public class Hotel : MasterBaseEntity
    {
        public Hotel()
        {
            IsBasicInfoConfigured = true;
            IsPropertyDetailsConfigured = false;
            IsRoomTypesConfigured = false;
            IsRoomAmenitiesConfigured = false;
            IsPaymentPolicyConfigured = false;
            IsSubmitted = false;
            IsProfileConfigured = false;
            IsNearbyConfigured = false;
            //Status = (int)EServiceStatus.InProgress;
            Facilities = new List<HotelFacility>();
            RoomAmenities = new List<HotelRoomAmenity>();
            Rooms = new List<Room>();
            Photos = new List<HotelPhoto>();
            CreditCards = new List<HotelCreditCard>();
            LocLat = 24.774265m;
            LocLng = 46.738586m;
            Status = 0;
           
        }


        // Basic Info
        public string Name { get; set; }
        public string CurrencyId { get; set; }
//        public string ServiceId { get; set; }
        public long ProviderId { get; set; }
        public int PropertyTypeId { get; set; }
        public virtual PropertyType PropertyType { get; set; }
        public int Rating { get; set; }
        [Column(TypeName = "decimal(25, 23)")]
        //mohsin
        public decimal TaxP { get; set; }
        [Column(TypeName = "decimal(8, 2)")]

        public decimal Vat { get; set; }
        [Column(TypeName = "decimal(8, 2)")]

        public decimal  LocLng { get; set; }
        [Column(TypeName = "decimal(25, 23)")]
        public decimal LocLat { get; set; }
        public int? Room { get; set; }
        public string ContactName { get; set; }
        public string Address { get; set; }
        public int? CityId { get; set; }
        public string Contact1 { get; set; }
        public string Contact2 { get; set; }
        public int SmokingOptionId { get; set; }
        public int noOfRooms { get; set; }
        public int Status { get; set; }
       
        // Hotel Details
        public byte CheckInFrom { get; set; }
        public byte CheckInTo { get; set; }
        public byte CheckOutFrom { get; set; }
        public byte CheckOutTo { get; set; }

        public int? InternetOptionId { get; set; }
        public int? ParkingOptionId { get; set; }
        public int? ParkingTypeId { get; set; }
        public int? ParkingSiteId { get; set; }
        public int? ParkingReservationId { get; set; }
        public int? BreakFastOptionId { get; set; }
        public Boolean IsPetsAllowed { get; set; }
        public Boolean IsChildrenAllowed { get; set; }
        public Boolean IsPrePayment { get; set; }
        public virtual ICollection<HotelFacility> Facilities { get; set; }
        public virtual ICollection<HotelRoomAmenity> RoomAmenities { get; set; }
        public  HotelMeals HotelMeals { get; set; }
        public virtual ICollection<HotelLanguage> Languages { get; set; }



        // Rooms
        public ICollection<Room> Rooms { get; set; }

        public bool IsBasicInfoConfigured { get; set; }
        public bool IsPropertyDetailsConfigured { get; set; }
        public bool IsRoomTypesConfigured { get; set; }
        public bool IsRoomAmenitiesConfigured { get; set; }
        public bool IsPaymentPolicyConfigured { get; set; }
        public bool IsSubmitted { get; set; }
        public bool IsPhotosConfigured { get; set; }
        public bool IsFacilitiesConfigured { get; set; }
        public bool IsFoodServed { get; set; }
        public bool IsProfileConfigured { get; set; }
        public bool IsNearbyConfigured { get; set; }

        public virtual SmokingOption smokingOption { get; set; }
        public virtual InternetOption internetOption { get; set; }
        public virtual ParkingOption parkingOption { get; set; }
        public virtual ParkingSite parkingSite { get; set; }
        public virtual ParkingType parkingType { get; set; }
        public virtual ParkingReservation parkingReservation { get; set; }
        public virtual BreakFastOption breakFastOption { get; set; }
        

        // Payment Policies
        public bool IsCreditCardAccepted { get; set; }
        public ICollection<HotelCreditCard> CreditCards { get; set; }
        public bool IsInvoicingHotelAddress { get; set; }
        public string InvoicingRecipientName { get; set; }
        public int InvoicingCountryId { get; set; }
        public int InvoicingCityId { get; set; }
        public string InvoicingAddress { get; set; }
        public string InvoicingPostalAddress { get; set; }

        public int? CancellationPolicyId { get; set; }
        public CancellationPolicy CancellationPolicy { get; set; }
        public int? CancellationFeeId { get; set; }
        public CancellationFee CancellationFee { get; set; }
        // Photos
        public IEnumerable<HotelPhoto> Photos { get; set; }

        //reviews
        
    }

}
