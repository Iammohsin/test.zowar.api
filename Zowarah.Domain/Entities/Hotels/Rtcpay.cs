﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Hotels
{
    public class Rtcpay

    { 
        public long Id { get; set; }
        public string BookingId { get; set; }
        public DateTime TransactionDate { get; set; }
        public string BranchID { get; set; }
        public string RefNum { get; set; }
        public string BillNumber { get; set; }
        public decimal Amount { get; set; }
    }

}
