﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Hotels
{
    public class HotelMeals 
    {
        public long HotelId { get; set; }
        public Boolean BreakFast { get; set; }
        public Boolean Dinner { get; set; }
        public Boolean Lunch { get; set; }
        public decimal BreakFastRate { get; set; }
        public decimal LunchRate { get; set; }
        public decimal DinnerRate { get; set; }
        public  Hotel Hotel { get; set; }
    }
}
