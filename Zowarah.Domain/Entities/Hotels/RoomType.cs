﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Hotels
{
    public class RoomType : LookupBaseEntity
    {
        public IEnumerable<RoomTypeName> RoomTypeNames { get; set; }
    }
}
