﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Zowarah.Domain.Entities.Transactions;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.Domain.Entities.Hotels
{

    public enum BookingFilterEnum
    {
        All = 0,
        Upcoming = 1,
        Cancelled = 2
    }
    public class Booking : MasterStringBaseEntity
    {
        public long RoomId { get; set; }
        public int Rooms { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public Boolean IsPersonal { get; set; }
        public string RefereeName { get; set; }
        public string RefereeEmail { get; set; }
        public string SpecialRequest { get; set; }
        public DateTime ArrivalDate { get; set; }
        public string TransactionId { get; set; }
        public decimal RatePerNight { get; set; }
        public long PromotionId { get; set; }
        public decimal BaseRate { get; set; }
        public decimal Discount { get; set; }
        public int FreeNights { get; set; }
        public virtual Transaction Transaction { get; set; }
        public virtual Room Room { get; set; }
        public Boolean isPaymentHotel { get; set; }

    }

}
