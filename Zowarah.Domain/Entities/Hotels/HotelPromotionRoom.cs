﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Hotels
{
   public class HotelPromotionRoom
    {
        public long HotelPromotionId { get; set; }
        public long RoomId { get; set; }
        public virtual HotelPromotion HotelPromotion { get; set; }
        public virtual Room Room { get; set; }

    }
}
