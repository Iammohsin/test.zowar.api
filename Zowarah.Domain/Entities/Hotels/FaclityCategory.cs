﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Hotels
{
    public class FacilityCategory : LookupBaseEntity
    {
        public string Icon { get; set; }
    }
}
