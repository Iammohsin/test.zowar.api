﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Zowarah.Domain.Entities.Hotels
{
    public class Calendar:MasterBaseEntity
    {
        public long HotelId { get; set; }
        public DateTime OpenFrom { get; set; }
        public DateTime OpenTo { get; set; }
        public DateTime CloseFrom { get; set; }
        public DateTime CloseTo { get; set; }
        public Boolean Fri { get; set; }
        public Boolean Sat { get; set; }
        public Boolean Sun { get; set; }
        public Boolean Mon { get; set; }
        public Boolean Tue { get; set; }
        public Boolean Wed { get; set; }
        public Boolean Thr { get; set; }
        public Hotel Hotel { get; set; }
    }
}
