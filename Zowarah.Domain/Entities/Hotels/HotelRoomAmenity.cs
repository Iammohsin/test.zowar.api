﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Hotels
{
    public class HotelRoomAmenity 
    {
        public int RoomAmenityId { get; set; }
        public long HotelId { get; set; }
        public long RoomId { get; set; }
        public RoomAmenity RoomAmenity { get; set; }
        public Hotel Hotel { get; set; }
    }
}
