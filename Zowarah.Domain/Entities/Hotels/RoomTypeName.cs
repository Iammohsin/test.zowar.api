﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Hotels
{
    public class RoomTypeName : LookupBaseEntity
    {
        public int RoomTypeId { get; set; }
        public RoomType RoomType { get; set; }
    }
}
