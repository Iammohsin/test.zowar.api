﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Hotels
{
    public class HotelPhoto : MasterStringBaseEntity
    {
        public long HotelId { get; set; }
        public string FileName { get; set; }
        public Boolean IsMain { get; set; }
        public Hotel Hotel { get; set; }
    }
}
