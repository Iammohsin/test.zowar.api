﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Zowarah.Domain.Entities.Hotels
{
    public class CalendarManual:MasterBaseEntity
    {
        public long HotelId { get; set; }
        public long RoomId { get; set; }
        public DateTime Date { get; set; }
        public int? Rooms { get; set; }

        public decimal RatePerNight { get; set; } //Mohsin
        public Boolean? Status { get; set; }
        public Hotel Hotel { get; set; }
        public Room Room { get; set; }
    }
}
