﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Hotels
{
    public class CancellationPolicy : LookupBaseEntity
    {
        public int NumberOfDays { get; set; }
        public int NumberOfHours { get; set; }
    }
}
