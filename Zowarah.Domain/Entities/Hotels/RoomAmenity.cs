﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Hotels
{
    public class RoomAmenity : LookupBaseEntity
    {
        public int RoomAmenityCategoryId { get; set; }
        public string Icon { get; set; }
        public RoomAmenityCategory RoomAmenityCategory { get; set; }
    }
}
