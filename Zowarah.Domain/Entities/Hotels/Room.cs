﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Hotels
{
    public class Room : MasterBaseEntity
    {
        public int SmokingOptionId { get; set; }
        public int NoOfRooms { get; set; }
        public bool IsLivingRoom { get; set; }
        public bool IsKitchen { get; set; }
        public int NoOfBathrooms { get; set; }
        public int NoOfPersons { get; set; }
        public decimal RatePerNight { get; set; }

        public decimal TotalRatePerNight { get; set; }

        public decimal TaxOnRate { get; set; }
        public decimal VatOnRate { get; set; }

       
        public long HotelId { get; set; }
        public Hotel Hotel { get; set; }

        //public int RoomTypeId { get; set; }
        //public RoomType RoomType { get; set; }
        public int RoomTypeNameId { get; set; }
        //public RoomTypeName RoomTypeName { get; set; }
        public virtual RoomTypeName RoomTypeName { get; set; }

    }
}
