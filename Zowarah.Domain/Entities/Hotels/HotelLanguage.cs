﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Common;

namespace Zowarah.Domain.Entities.Hotels
{
    public class HotelLanguage :MasterBaseEntity
    {
        public long HotelId { get; set; }
        public string LanguageId { get; set; }
        public virtual Hotel Hotel { get; set; }
        public virtual Language Language { get; set; }

    }
}
