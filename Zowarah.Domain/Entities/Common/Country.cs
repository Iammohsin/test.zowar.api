﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Common
{
    public class Country : LookupBaseEntity
    {
        public string DialingCode { get; set; }
        public string IsoCode { get; set; }
        public string CurrencyId { get; set; }
        public virtual Currency Currency { get; set; }
    }
}
