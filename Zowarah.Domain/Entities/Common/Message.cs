﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.Domain.Entities.Common
{

    public enum MessagingTransTypes
    {
        General = 1,
        Booking = 2, 
        Invoicing = 3
    }

    public class Message : MasterStringBaseEntity
    {
        public long SenderId { get; set; }
        public long RecieverId { get; set; }        
        public RoleTypes SenderRole { get; set; }
        public RoleTypes ReceiverRole { get; set; }
        public int TransactionTypeId { get; set; }
        public string ReferenceId { get; set; }
        public DateTime Date { get; set; }
        public int MessageCategoryId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public Boolean IsClosed { get; set; }
        public DateTime CloseDate { get; set; }
        public Boolean IsRead { get; set; }
        public Boolean IsDeletedBySender { get; set; }
        public Boolean IsDeletedByReciever { get; set; }
        public virtual ICollection<MessageTrail> MessageTrails { get; set; }
        public virtual MessageCategory MessageCategory { get; set; }
    }
}