﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Common
{
    public class City : LookupBaseEntity
    {
        public string Name { get; set; }
    }
}