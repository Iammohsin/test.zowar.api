﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Common
{
    public class Culture : MasterBaseEntity
    {
        public string Description { get; set; }
    }
}