﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Common
{
    public  class ServiceNearbyCategory:LookupBaseEntity
    {
        public int ServiceNearbyTypeId { get; set; }

        public virtual ServiceNearbyType ServiceNearbyType { get; set; }

    }
}
