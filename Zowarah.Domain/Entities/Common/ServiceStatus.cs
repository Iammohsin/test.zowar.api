﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Common
{
    public enum ServiceStatusEnum
    {
        All = -1,
        InProgress = 0,
        Submitted = 1,
        UnderReview = 2,
        Approved = 3,
        Rejected = 4,
        Suspended = 5
    }
    public class ServiceStatus : LookupBaseEntity
    {
        public static ServiceStatusEnum Submitted { get; set; }
    }
}