﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Common
{
    public class LookUpTable : MasterStringBaseEntity
    {
        public string Description { get; set; }
        public int LookUpModelId { get; set; }
        public virtual LookUpModel LookUpModel { get; set; }
    }
}
