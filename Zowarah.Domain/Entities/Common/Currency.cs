﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Common
{
    public class Currency : MasterStringBaseEntity
    {
        public string Symbol { get; set; }
        public string Name { get; set; }
        public decimal ExchangeRate { get; set; }
    }
}
