﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Common
{
    public class Language:MasterStringBaseEntity 
    {
        public string Name { get; set; }
        public string NativeName { get; set; }
    }
}
