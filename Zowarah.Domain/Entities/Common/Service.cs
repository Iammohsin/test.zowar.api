﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.Domain.Entities.Common
{
    public class Service : MasterBaseEntity
    {
        public Service()
        {
            ServiceProfiles = new HashSet<ServiceProfile>();
        }
        public string Name { get; set; }
        public long ProviderId { get; set; }
        public ServiceTypeEnum ServiceType { get; set; }
        public int CityId { get; set; }
        public ServiceStatusEnum Status { get; set; }
        public List<ServiceStatusLog> Logs { get; set; }
        public virtual City City { get; set; }
        public Boolean IsActive { get; set; }
        public float ReviewScore { get; set; }
        public decimal TotalScore { get; set; }
        public decimal TotalReviews { get; set; }
        public virtual AppUser Provider { get; set; }

        public void AddServiceStatusLog(ServiceStatusLog serviceStatusLog)
        {
            if (Logs == null)
                Logs = new List<ServiceStatusLog>();
            Logs.Add(serviceStatusLog);
        }
        public virtual ICollection<ServiceProfile> ServiceProfiles { get; set; }
        public ICollection<ServiceReviewFeaturesScore> ServiceReviewFeaturesScores { get; set; }
    }
}