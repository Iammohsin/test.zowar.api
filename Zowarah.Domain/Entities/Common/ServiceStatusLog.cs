﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Common
{
    public class ServiceStatusLog : MasterBaseEntity
    {
        public ServiceStatusEnum Status { get; set; }
        public string Comments { get; set; }
    }
}