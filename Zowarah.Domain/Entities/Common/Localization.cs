﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Zowarah.Domain.Entities.Common
{
    public class Localization : MasterStringBaseEntity
    {
        public string LanguageId { get; set; }
        public string LocText { get; set; }
        public virtual Language Language { get; set; }
    }
}