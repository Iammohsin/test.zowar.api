﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Common
{
    public class ServiceNearby:MasterBaseEntity
    {
        public long ServiceId { get; set; }
        public string Name { get; set; }
        public int Distance { get; set; }
        public string Unit { get; set; }
        public int ServiceNearbyCategoryId { get; set; }
        public virtual Service Service { get; set; }
        public virtual ServiceNearbyCategory ServiceNearbyCategory { get; set; }
    }
}
