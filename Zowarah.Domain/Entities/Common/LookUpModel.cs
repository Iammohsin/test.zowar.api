﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Common
{
    public class LookUpModel:LookupBaseEntity
    {
        public LookUpModel()
        {
            LookUpTables = new HashSet<LookUpTable>();
        }
        public virtual ICollection<LookUpTable>  LookUpTables{ get; set; }
    }
}
