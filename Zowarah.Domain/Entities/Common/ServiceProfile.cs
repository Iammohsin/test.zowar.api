﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Common
{
    public class ServiceProfile : MasterBaseEntity
    {
        public long ServiceId { get; set; }
        public string Language { get; set; }
        public string PropertyName { get; set; }
        public string AboutProperty { get; set; }
        public string AboutStaff { get; set; }
        public string AboutNeighborhood { get; set; }
        public virtual Service Service { get; set; }

    }
}
