﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Common
{
    public class Setting : MasterStringBaseEntity
    {
        public string Description { get; set; }
        public string Value { get; set; }
    }
}