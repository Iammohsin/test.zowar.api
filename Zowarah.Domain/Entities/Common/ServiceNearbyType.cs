﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Common
{
    public class ServiceNearbyType:LookupBaseEntity
    {
        public virtual ICollection<ServiceNearbyCategory> ServiceNearbyCategories { get; set; } = new HashSet<ServiceNearbyCategory>();
    }
}
