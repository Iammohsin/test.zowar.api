﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Common
{
    public class MessageTrail : MasterStringBaseEntity
    {
        public MessageTrail(){
            MessageId = "";
            SenderId = 0;
            Date = DateTime.Now;
            Body = "welcome world";
        }
        public string MessageId { get; set; }
        public long  SenderId { get; set; }
        public DateTime Date { get; set; }
        public string Body { get; set; }
    }
}