﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Zowarah.Domain.Entities.Common {
    public class DimCalendar {
        [Key]
        public DateTime Date { get; set; }
    }
}
