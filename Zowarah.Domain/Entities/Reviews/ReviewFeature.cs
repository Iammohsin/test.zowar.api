﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Reviews
{
    public class ReviewFeature : MasterBaseEntity
    {
        public string ReviewId { get; set; }
        public int ReviewFeatureTypeId { get; set; }
        public float Score { get; set; }
        public ReviewFeatureType ReviewFeatureType { get; set; }
    }
}
