﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Common
{
    public class ServiceReviewFeaturesScore
    {
        public long ServiceId { get; set; }
        public int ReviewFeatureTypeId { get; set; }
        public float Score { get; set; }
        public decimal TotalScore { get; set; }
        public decimal TotalReviews { get; set; }
    }

    public class ServiceReviewAveragesScore
    {
        public int Average { get; set; }
        public int TotalReviews { get; set; }
    }
}
