﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Transactions;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.Domain.Entities.Reviews
{
    public class ReviewsScoreDescription : LookupBaseEntity
    {
        public float Score { get; set; }
    }
}
