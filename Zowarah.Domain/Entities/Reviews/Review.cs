﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Transactions;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.Domain.Entities.Reviews
{
    public class Review : MasterStringBaseEntity
    {
        public Review()
        {
            ReviewFeatures = new HashSet<ReviewFeature>();
        }
        public float Score { get; set; }
        public string Title { get; set; }
        public string Comment { get; set; }
        public string ReplyComment { get; set; }
        public DateTime ReplyDate { get; set; }
        public string TransactionId { get; set; }
        public long ServiceId { get; set; }
        public long ClientId { get; set; }
        public DateTime Date { get; set; }

        // aggregation from booking person count 
        // if persons count ==1 => add solo travelers
        //    persons count ==2 => add couples
        //    persons count > 2 =>  add Groups
        //    persons count > 2 with childern =>  add family
        public string Tags { get; set; }

        public bool IsRead { get; set; }
        public bool IsPuplished { get; set; }

        public virtual Transaction Transaction { get; set; }
        public Service Service { get; set; }
        public AppUser Client { get; set; }

        public ICollection<ReviewFeature> ReviewFeatures { get; set; }

    }
}
