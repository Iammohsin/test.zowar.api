﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Common;

namespace Zowarah.Domain.Entities.Reviews
{
    public class ReviewFeatureType:LookupBaseEntity
    {
        public ServiceTypeEnum ServiceTypeId { get; set; }
    }
}
