﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Zowarah.Domain.Entities
{
    public class MasterBaseEntity : BaseEntity
    {
        public long Id { get; set; }
    }
}