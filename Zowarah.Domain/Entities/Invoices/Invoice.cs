﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Transactions;

namespace Zowarah.Domain.Entities.Invoices
{
    public enum InvoiceStatus
    {
        Pending = 1,
        OverDue = 2,
        Collected = 3,
    }
    public  class Invoice: MasterStringBaseEntity
    {
        public Invoice() {
            Transactions = new HashSet<Transaction>();
        }
        public long ServiceId { get; set; }
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
        public DateTime DueDate { get; set; }
        public InvoiceStatus Status { get; set; }
        public virtual Service Service { get; set; }
        public ICollection<InvoiceStatusLog> Logs { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; }
        public void AddServiceStatusLog(InvoiceStatusLog serviceStatusLog)
        {
            if (Logs == null)
                Logs = new List<InvoiceStatusLog>();
            Logs.Add(serviceStatusLog);
        }
    }
}
