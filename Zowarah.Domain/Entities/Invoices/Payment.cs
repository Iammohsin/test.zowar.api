﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Invoices
{
    public class Payment:MasterStringBaseEntity
    {
        public string InvoiceId { get; set; }
        public string Reference { get; set; }
        public string Bank { get; set; }
        public DateTime Date { get; set; } 
        public string AccountNo { get; set; }
        public virtual Invoice Invoice { get; set; }
    }
}
