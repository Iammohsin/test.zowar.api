﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Domain.Entities.Invoices
{
    public class InvoiceStatusLog: MasterStringBaseEntity
    {
        public string InvoiceId { get; set; }
        public string Comment { get; set; }

        public virtual Invoice Invoice { get; set; }
    }
}
