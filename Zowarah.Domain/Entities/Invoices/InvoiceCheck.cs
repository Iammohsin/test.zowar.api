﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.Domain.Entities.Invoices
{
    public class InvoiceCheck : MasterStringBaseEntity
    {
        public string Month { get; set; }
        public string Year  { get; set;  }
        public int NoOfInvoices { get; set; }
        public decimal Amount { get; set; }
    }
}
