﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Domain.Entities;

namespace Zowarah.Domain.Interfaces
{
    public interface ILookupRepository<Entity>:IRepository<Entity> where Entity:LookupBaseEntity 
    {
    }
}
