﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Zowarah.Domain.Interfaces
{
    public interface IRepository<Entity> where Entity : class
    {
        Task<ICollection<Entity>> GetAll();
        Task<Entity> GetById(object id );
        IQueryable<Entity> FindBy(System.Linq.Expressions.Expression<Func<Entity, bool>> predicate, params Expression<Func<Entity, object>>[] includes);
          void Create(Entity entity);
        void Update(Entity entity);
        void Delete(Entity entity);
        void RealDelete(Entity entity);
        Task ExecSql(string sql);
    }
}
