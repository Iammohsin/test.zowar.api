﻿

namespace Zowarah.Application.Enums.Hotels
{
    public enum InternetOptions
    {
        No = 1,
        Free = 2,
        Paid = 3
    }
}