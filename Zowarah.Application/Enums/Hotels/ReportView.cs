﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Application.Enums.Hotels
{
    public enum ReportView
    {
        days,
        weeks,
        months,
        years
    }
}
