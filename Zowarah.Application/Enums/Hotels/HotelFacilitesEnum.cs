﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Application.Enums.Hotels
{
    public enum HotelFacilitesEnum
    {
        AirConditioning = 1,
        NonSmokingRooms = 2,
        Playground = 3,
        BBQ = 4,
        Sauna = 5,
        FamilyRooms = 6,
        AllSpacesNonSmoking = 7,
        AirportShuttleAdditionalCharge = 8,
        Elevator = 9,
        BaggageStorage = 10,
        AirportShuttle = 11,
        Restaurant = 12,
        FitnessCenter = 13,
        OutdoorPool = 14,
        Grounds = 15,
        DailyHousekeeping = 16

    }
}
