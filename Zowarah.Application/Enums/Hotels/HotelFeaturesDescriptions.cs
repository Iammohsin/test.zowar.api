﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Application.Enums.Hotels
{
    public enum HotelFeaturesDescriptionsEnum
    {
        FreeWifi = 1,
        AccommodationsOffer = 2,
        Sauna = 3,
        BBQ = 4,
        Restaurant = 5,
        FreeParking = 6,
        PaidParking = 7,
        Room = 8,
        FrontdeskandBaggageStorage = 9,
        Frontdesk = 10,
        GreatFacilities = 11,
        FavoriteInCity = 12,
        TopRatedLocation = 13,
        Location = 14,
        Promotion = 15,
        Language = 16
    }
}
