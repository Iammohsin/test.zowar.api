﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zowarah.Application.Config
{
    public class CustomException : Exception
    {
        public CustomException(string message) : base(message)
        {
        }
    }
}
