﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Enums.Hotels;
using Zowarah.Application.Services.Transactions;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Transactions;
using Zowarah.Domain.Interfaces;

namespace Zowarah.Application.Interfaces.Transactions
{
    public interface ITransactionService :IRepository<Transaction>    
    {
        Task<List<GetServiceTransaction>> GetAllClientServiceTranactions(long ClientId, BookingFilterEnum bookingFilter);
        Task<List<GetServiceTransaction>> SearchClientServiceTranactions(long ClientId, ServiceTypeEnum ServiceType, TransactionStatus Status , DateTime From ,DateTime To);
        Task<List<GetServiceTransaction>> GetAllProviderServiceTranactions(long providerId);
        Task<List<GetServiceTransaction>> SearchProviderServiceTranactions(long providerId, ServiceTypeEnum ServiceType, TransactionStatus Status , DateTime From ,DateTime To);
        Task<(int, List<BasicTransactionInfo>)> GetAllTranactionsByFilter(int page,int pageSize, string invoiceId,long? serviceId, long? providerId,long? clientId, ServiceTypeEnum ServiceType, TransactionStatus Status, DateTime? From, DateTime? To);
        Task<ICollection<TransactionReport>> GetTranactionsReportByFilter(int page, int pageSize, string invoiceId, long? serviceId, long? providerId, long? clientId, ServiceTypeEnum ServiceType, TransactionStatus Status, DateTime? From, DateTime? To);
        Task<ICollection<BookingTransaction>> GetAllBooking(
           string InvoiceId,
           long serviceId,
           long clientId,
           int? roomTypeId,
           int? serviceCity,
           TransactionStatus Status,
           bool? isShowUp,
           DateTime? From,
           DateTime? To);
        Task<decimal> GetTotalConfirmed(long ServiceId);
        Task<decimal> GetTotalCancelled(long ServiceId);
        Task<int> GetTotalConfirmed();
        Task<int> GetTotalCancelled();
        Task<int> GetTotalNoShow();
        Task<ICollection<SaleReporting>> GetSaleReporting(long ClientId, DateTime from, DateTime to, ServiceTypeEnum serviceType, int city, ReportView view);
        Task<SaleReporting> GetSaleReportingDetails(long ClientId, DateTime date, DateTime from, DateTime to, ServiceTypeEnum serviceType, int city, ReportView view);
    }
}
