﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Services.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;

namespace Zowarah.Application.Interfaces.Common
{
    public interface IServiceService : IRepository<Service>
    {
        Task<ServicePage> GetServices(int page, int pageSize, int? city, ServiceTypeEnum serviceType, ServiceStatusEnum status, DateTime? from, DateTime? to, long serviceId, long providerId);
        Task<ICollection<ServiceReport>> GetServiceReport(int? city, ServiceTypeEnum serviceType, ServiceStatusEnum status, DateTime? from, DateTime? to, long serviceId, long providerId);

        Task<ProviderPage> GetProviders(int pageNumber, int PageSize, DateTime From, DateTime To);
    }


}
