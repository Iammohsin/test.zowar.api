﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;
using Microsoft.Extensions.Options;

namespace Zowarah.Application.Interfaces.Common
{
    public interface INotificationService
    {
        Task SendBySmtp(string To, string Subject, string DisplayName,string Content);
        String GetTemplate(string templateId);
    }

}
