﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Queries.Common;

namespace Zowarah.Application.Interfaces.Common
{
    public interface IMessagingReportService
    {
        IEnumerable<MessageQuery> GetMessages(RoleTypes role);

        IEnumerable<MessageQuery> GetAll();
    }
}