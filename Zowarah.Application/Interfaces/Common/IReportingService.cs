﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Enums.Hotels;
using Zowarah.Application.Services.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;

namespace Zowarah.Application.Interfaces.Common
{
    public interface IReportingService : IRepository<Service>
    {
        Task<int> GetTotalServiceRegistered();
        Task<int> GetTotalServiceActived();
        Task<int> GetTotalServicePendding();
    }
}
