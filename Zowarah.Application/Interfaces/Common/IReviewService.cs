﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Services.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Reviews;
using Zowarah.Domain.Interfaces;

namespace Zowarah.Application.Interfaces.Common
{
    public interface IReviewService : IRepository<Review>
    {
        Task<ICollection<ReviewFeatureType>> GetServiceFeatureTypes(ServiceTypeEnum ServiceTypeId);
        Task<float> TotalReviewFeatureTypesScore();
        Task UpdateServiceReviewRate(long serviceId,float Score);
        Task UpdateServiceReviewFeaturesRate(long ServiceId, ICollection<ReviewFeature> ReviewFeatures);
        Task<ICollection<ReviewQuery>> GetByServiceId(long serviceId, int Page, int Count, ReviewOrderbyEnum OrderBy);
        Task<string> GetReviewTags(string TransactionId);
        Task<ICollection<ServiceReviewFeaturesScore>> GetServiceFeaturesScore(long serviceId);
        Task<ICollection<ServiceReviewAveragesScore>> GetServiceAveragesScore(long serviceId);
        Task<bool> IsExist(long ClientId, string TransactionId);
        Task<decimal> GetTodayReview(long serviceId);
    }
}
