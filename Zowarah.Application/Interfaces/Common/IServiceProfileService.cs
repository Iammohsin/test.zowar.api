﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Services.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;

namespace Zowarah.Application.Interfaces.Common
{
    public interface IServiceProfileService : IRepository<ServiceProfile>
    {
        Task<ICollection<GetServiceProfile>> GetAllByServiceId(long ServiceId);
        Task<GetServiceProfile> GetByServiceIdAndLanguage(long ServiceId, string Language);

    }
}
