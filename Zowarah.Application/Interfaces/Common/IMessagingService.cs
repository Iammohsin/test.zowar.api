﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Queries.Common;

namespace Zowarah.Application.Interfaces.Common
{

    public interface IMessagingService: IRepository<Message>
    {
        Task<int> AdminNewMessageCount();
        Task<int> AdminActiveMessageCount(long UserId);
        Task<ICollection<MessageQuery>> GetAllByFilter(bool? isClosed,long? receiveId, RoleTypes receiverRole, bool isIncludeMessageTrails=false);
        Task<MessageQuery> GetMessage(String messageId,long UserId);
        Task<string> Compose (Message message);
        Task<IList<MessageQuery>> GetMessages (Boolean IsAll,
            DateTime? fromDate, DateTime? toDate, RoleTypes roleType, long? userId, long? serviceId,
            bool isSend, bool isRecieve,
            bool? isClosed, bool isIncludeMessageTrails = false);
        Task<string> Reply(MessageTrail messageTrail);
        Task<Boolean> CloseMessage(string messageId);
        Task<Boolean> StartProcessing(string messageId, long userId);
        Task<Boolean> DeleteMessage(long userId, string messageId);
        Task<int> GetNoPendingMessages(long userId, RoleTypes roleType, long serviceId);
        Task<long> IsValidTransaction (long userId, RoleTypes roleType, MessagingTransTypes transType , string transId);
        Task MarkRead(long userId, string messageId);
    }

}
