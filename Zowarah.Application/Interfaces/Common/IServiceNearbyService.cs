﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Services.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;

namespace Zowarah.Application.Interfaces.Common
{
    public interface IServiceNearbyService : IRepository<ServiceNearby>
    {
        Task<ServiceNearby> GetById(long ServiceId, bool v);
        Task<ServiceNearby> GetById(long ServiceId);
        Task<ICollection<ServiceNearbyCategoryQuery>> GetServiceNearbyTypes();
        Task<ICollection<ServiceNearbyQuery>> GetAllByServiceId(long ServiceId);
    }
}
