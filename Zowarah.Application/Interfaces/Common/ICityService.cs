﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;

namespace Zowarah.Application.Interfaces.Common
{
    public interface ICityService:ILookupRepository<City>
    {
        Task<IList<String>> Search(String Term);
    }

}
