﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Services.Hotels;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Transactions;
using Zowarah.Domain.Interfaces;

namespace Zowarah.Application.Interfaces.Hotels
{
    public interface IBookingService : IRepository<Booking>
    {

        Task<IQueryable<NetBooking>> GetNetBooking(long RoomId,
                             DateTime fromDate,
                             DateTime toDate
                      );
        Task<int> GetMaxBooked(long RoomId,
                     DateTime fromDate,
                     DateTime toDate
              );
        Task<int> GetLastReservedRooms(long HotelId, int days);
   
        Task<BookingDetails> GetBookingDetails(string Id);
        Task<bool> IsClientShowUp(string BookingId);
        Task<decimal> GetTodayBooking(long HotelId);
        Task<decimal> GetTotalBooking(long HotelId);
        Task<decimal> GetTodayArrival(long HotelId);
        Task<decimal> GetTodayDeparture(long HotelId);


    }
}
