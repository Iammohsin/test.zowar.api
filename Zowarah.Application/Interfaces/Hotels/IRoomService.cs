﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Services;
using Zowarah.Application.Services.Hotels;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Interfaces;

namespace Zowarah.Application.Interfaces.Hotels
{
    public interface IRoomService:IRepository<Room>
    {       
        Task<Room> GetById(long roomId);
        Task<Room> GetById(long roomId,bool isIncludeHotel);
        Task<RoomQuery> GetDetailsById(long id);
        Task<IQueryable<RoomQuery>> GetByHotelId(long hotelId);
    }
}
