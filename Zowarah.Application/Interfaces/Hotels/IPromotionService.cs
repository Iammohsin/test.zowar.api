﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Services.Hotels;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Transactions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Queries.Hotels;

namespace Zowarah.Application.Interfaces.Hotels
{
    public interface IPromotionService : IRepository<HotelPromotion>
    {
        bool IsValidPromotionTypePraramer(PromotionType promotionType, int bookingDaysCondition, int freeNights, int minimunStay);
        Task<HotelPromotion> GetById(long id, params Expression<Func<HotelPromotion, object>>[] includes);
        Task<ICollection<Promotion>> GetRoomsPromotions(long ServiceId, Boolean isMember, DateTime checkInDate, DateTime checkOutDate);
        Task<Promotion> GetRoomPromotion( long RoomId, Boolean isMember, DateTime checkInDate, DateTime checkOutDate);
        Task<ICollection<TopPromotion>> GetTopPromotions(int noOfPromotions);
    }
}
