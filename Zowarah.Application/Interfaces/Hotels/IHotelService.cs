﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Enums.Hotels;
using Zowarah.Application.Services;
using Zowarah.Application.Services.Hotels;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Transactions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Queries.Hotels;

namespace Zowarah.Application.Interfaces.Hotels {
    public interface IHotelService : IRepository<Hotel> {
        Task<Hotel> GetById(long hotelId, params Expression<Func<Hotel, object>>[] includes);
        Task<HotelQuery> GetHotelById(long id);
        Task<IQueryable<HotelQuery>> GetByProviderId(long providerId);
        Task<Service> SubmitHotel(long hotelId,long UserId);
        Task<IQueryable<HotelRoomAmenityQuery>> GetRoomAmenities(long hotelId);
       

        //Task<IQueryable<Language>> GetHotelLanguages(long HotelId);
        Task<ICollection<HotelSearchResult>> Search(
            int page,
            int pageSize,
                                      Boolean IsMember,
                                      DateTime checkInTime,
                                      DateTime checkOutTime,
                                      int noOfPersons,
                                      int noOfChildren,
                                      int noOfRooms,
                                      string city,
                                      int orderBy,
                                      int[] rating,
                                       //decimal taxP,
                                       int[] propertyTypes,
                                      int[] facilities,
                                      int[] roomAmenities
                                      );
        Task<ICollection<AvailableRoom>> AvailableRooms(long hotelId, DateTime checkIn, DateTime checkOut,int noOfRooms, int noOfPersons, int noOfChildren);
        Task<HotelRoomQuery> GetHotelRoom(long roomId);
        Task<int> GetAllocatedRooms(long hotelId, int noOfRooms = 0);
        Task<ICollection<HotelBooking>> GetActiveBookings(long serviceId);
        Task<ICollection<HotelFeaturesDescription>> GetHotelFeaturesDescriptions();
        Task<ICollection<HotelBooking>> SearchBooking(long serviceId,long userId, DateTime fromDate, DateTime toDate, TransactionStatus status);
        Task<ICollection<HotelLanguage>> GetSpokenLanguages(long serviceId);
        Task<RoomSearchParameters> GetRoomSearchParameters(long roomId, DateTime fromDate, DateTime toDate);
        Task<PropertyBasicInfo> GetBasicInfo(long id);
        Task<PropertyDetails> GetPropertyDetails(long id);
        Task<HotelSearchParameters> GetHotelSearchParameters(long hotelId, DateTime fromDate, DateTime toDate);
        Task<ICollection<HotelRoomSaleReporting>> GetHotelRoomSaleReporting(long hotelId, long userId, DateTime from, DateTime to, int room, ReportView view);
        Task<HotelRoomSaleDetails> GetHotelRoomSaleDetails(long hotelId, long userId, DateTime From, DateTime To, int room, ReportView view);
        Task<ICollection<SearchResult>> SearchHotel();

        Task<Hotel> GetHotelWithPhotos(long id);

        Task<RtcQuery> GetRtcpayById();
    }
}
