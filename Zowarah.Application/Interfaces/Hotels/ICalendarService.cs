﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Services;
using Zowarah.Application.Services.Hotels;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.Application.Interfaces.Hotels {
    public interface ICalendarService {
        Task SetCalendar(long Id, long userId, DateTime openFrom, DateTime openTo);
        Task CloseRooms(long Id,            
                        long userId,                        
                        DateTime closeFrom,
                        DateTime closeTo,
                        Boolean fri,
                        Boolean sat,
                        Boolean sun,
                        Boolean mon,
                        Boolean tue,
                        Boolean wed,
                        Boolean thr
                        );
        Task<CalendarQuery> GetCalendar(long hotelId,long userId);

        Task SetCalendarStatus(
                    long hotelId,
                    long userId,
                    long roomId,
                    long rooms,
                    decimal ratePerNight, //Mohsin
                    DateTime Date,
                    Boolean Status
                            );

        Task SetCalendarRooms(
              long hotelId,
              long UserId,
              long roomId,
              DateTime Date,
              int Rooms,
              decimal RatePerNight //Mohsin
                      );
        Task<ICollection<CalendarManual>> GetCalendarManual(long roomId,long userId);

        Task<bool> IsHotelCalenderConfigured(long hotelId,long userId);
    }
}
