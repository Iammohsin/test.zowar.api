﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Enums.Hotels;
using Zowarah.Application.Services.Invoices;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Invoices;
using Zowarah.Domain.Interfaces;

namespace Zowarah.Application.Interfaces.Invoices {
    public interface IInvoiceService : IRepository<Invoice> {
        Task GenerateInvoices();
        Task RemoveInvoices();
        Task<ICollection<InvoiceQuery>> GetAllByFilter(string InvoiceId, long ProviderId, long ServiceId, DateTime from, DateTime to, int? City, ServiceTypeEnum ServiceType, InvoiceStatus? Status);
        
        Task<InvoiceQuery> GetInvoiceDetails(string InvoiceId);
        Task<ICollection<InvoiceTransaction>> GetAllInvoiceTransactions(string InoviceId);
        Task<InvoiceTransactionDetails> GetTransactionInvoiceDetails(string TransactionId);
        Task<decimal> GetPenddingInvoices(long serviceId);
        Task<ICollection<InvoiceReport>> GetInvoicesReport(long ProviderId, long ServiceId
               , DateTime from, DateTime to, ServiceTypeEnum ServiceType, int? City, InvoiceStatus? Status, ReportView view);
        Task<InvoiceReport> GetInvoicesReportDetails(long ProviderId, long ServiceId,
             DateTime date, DateTime from, DateTime to, ServiceTypeEnum ServiceType, int? City, InvoiceStatus? Status, ReportView view);

       
    }
}
