﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Invoices;
using Zowarah.Domain.Interfaces;

namespace Zowarah.Application.Interfaces.Payments
{
    public interface IPaymentService : IRepository<Payment>
    {
        Task<Payment> GetByInvoiceId(string InvoiceId);
    }
}
