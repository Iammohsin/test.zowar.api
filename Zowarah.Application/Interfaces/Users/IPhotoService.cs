﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.Application.Interfaces.Users
{
    public interface IPhotoService : IRepository<ProfilePhoto>
    {
        Task<ProfilePhoto> GetByUserId(long userId);
    }
}
