﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Interfaces;

namespace Zowarah.Application.Interfaces.Users
{
    public interface ICreditCardService : IRepository<CreditCardInfo>
    {
        List<CreditCardInfo> GetByUserId(long UserId);

        CreditCardInfo GetByCardId(int cardId);

        Task Add(CreditCardInfo creditCardInfo);
    }
}