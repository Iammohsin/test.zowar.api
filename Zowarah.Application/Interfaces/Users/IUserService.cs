﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.Application.Interfaces.Users
{
    public interface IUserService
    {
        Task<bool> CreateUser(AppUser user);
        void UpdateUser(AppUser user);
        Task<AppUser> GetUserById(long Id);

        Task<int> TotalUserByRoleName(string roleName, bool? isActive = null, DateTime? From = null, DateTime? To = null);
    }
}
