﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Enums;
using Zowarah.Domain.Queries;

namespace Zowarah.Application.Interfaces.Users
{
    public interface IClientService
    {
        Task<ICollection<ClientsOverview>> GetOverview(PeriodEnum period, DateTime fromDate, DateTime toDate, int country);
    }
}
