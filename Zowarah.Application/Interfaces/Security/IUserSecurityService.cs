﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Zowarah.Application.Interfaces.Security
{
    public interface IUserSecurityService
    {
        Task<IReadOnlyCollection<long>> GetPermissions();
        Task<bool> AttachUserPermission(long userId, List<long> permissions);
        Task<bool> DetachUserPermission(long userId, List<long> permissions);
    }
}
