﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Application.Interfaces.Security;
using Zowarah.Data.Contexts;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.Application.Services.Security
{
    public class UserSecurityService : IUserSecurityService
    {
        private readonly ApplicationDbContext _dbContext;
        public UserSecurityService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> AttachUserPermission(long userId, List<long> permissions)
        {
            var user = _dbContext.Users.FirstOrDefault(x => x.Id == userId);
            foreach (var permission in permissions)
            {
                user.AttachPermission(new UserPermission {UserId=userId,PermissionId= permission });
            }
            var result = await _dbContext.SaveChangesAsync();

            return result >= 1;
        }

        public async Task<bool> DetachUserPermission(long userId, List<long> permissions)
        {
            var user = _dbContext.Users.Include(x=>x.UserPermissions).FirstOrDefault(x => x.Id == userId);

            foreach (var permission in permissions)
            {
                var toBeRemovedPermission = user.UserPermissions.FirstOrDefault(x => x.PermissionId == permission);
                user.DetachPermission(toBeRemovedPermission);
            }
            var result = await _dbContext.SaveChangesAsync();

            return result >= 1;
        }
        public async Task<IReadOnlyCollection<long>> GetPermissions()
        {
            var permissions= await _dbContext.Permissions.ToListAsync();
            return permissions.Select(x => x.Id).ToList().AsReadOnly();
        }


    }
}
