﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Payments;
using Zowarah.Data.Contexts;
using Zowarah.Data.Repositories;
using Zowarah.Domain.Entities.Invoices;

namespace Zowarah.Application.Services.Payments
{
    public class PaymentService : Repository<Payment>, IPaymentService
    {
        public PaymentService(ApplicationDbContext db) : base(db)
        {

        }
        public async Task<Payment> GetByInvoiceId(string InvoiceId)
        {
            var query = _dbContext.Payments
                .Where(p => p.InvoiceId == InvoiceId);
            return await query.FirstOrDefaultAsync();

        }
    }
}
