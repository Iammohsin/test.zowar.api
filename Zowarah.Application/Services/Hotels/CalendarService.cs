﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Zowarah.Application.Config;
using Zowarah.Application.Interfaces;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Interfaces;

namespace Zowarah.Application.Services.Hotels {
    public class CalendarQuery {
        public long Id { get; set; }
        public DateTime OpenFrom { get; set; }
        public DateTime OpenTo { get; set; }
        public DateTime CloseFrom { get; set; }
        public DateTime CloseTo { get; set; }
        public Boolean Fri { get; set; }
        public Boolean Sat { get; set; }
        public Boolean Sun { get; set; }
        public Boolean Mon { get; set; }
        public Boolean Tue { get; set; }
        public Boolean Wed { get; set; }
        public Boolean Thr { get; set; }
    }
    public class CalendarService : ICalendarService {
        private readonly ApplicationDbContext _dbContext;
        private readonly DbSet<Calendar> _calendar;
        private readonly DbSet<CalendarManual> _calendarManual;
        private readonly IUnitOfWork _uow;




        public CalendarService(ApplicationDbContext dbContext, IUnitOfWork uow) {
            // _repoCalendar = repoCalendar;
            // _repoCalendarStatus = repCalendarStatus;

            _dbContext = dbContext;
            _calendar = dbContext.Calendar;
            _calendarManual = dbContext.CalendarManual;
            _uow = uow;
        }

        public async Task SetCalendarStatus(
                    long hotelId,
                    long userId,
                    long roomId,
                    long rooms,
                    decimal ratePerNight,  //Mohsin
                    DateTime date,
                    Boolean status
                            ) {
            var hotel = await _dbContext.Hotels.FindAsync(hotelId);
            if (hotel != null && hotel.ProviderId == userId) {
                var result = _calendarManual.Where(p => p.HotelId == hotelId && p.RoomId == roomId && p.Date.ToString("MMddYYYY") == date.ToString("MMddYYYY")).FirstOrDefault();
                if (result == null) // New Record
                {
                    if ( status == false)
                    {
                        await _calendarManual.AddAsync(new CalendarManual { HotelId = hotelId, RoomId = roomId, Date = date, Status = status, Rooms = 0, RatePerNight = 0 });
                    }
                    else
                    {
                        await _calendarManual.AddAsync(new CalendarManual { HotelId = hotelId, RoomId = roomId, Date = date, Status = status, Rooms = Convert.ToInt32(rooms), RatePerNight = ratePerNight
                    });
                    }
                  
                } else {
                    if(status == false)
                    {
                        result.Status = status;
                        result.Rooms = 0;

                    }
                    else
                    {
                        result.Status = status;
                        result.Rooms = Convert.ToInt32(rooms);
                        result.RatePerNight = ratePerNight;  //Mohsin
                    }
                    _calendarManual.Update(result);
                }
                await _uow.SaveChangesAsync();
            } else
                throw new CustomException("Invalid Hotel!");
        }

        public async Task SetCalendarRooms(
            long hotelId,
            long userId,
            long roomId,
            DateTime date,
            int rooms,
            decimal ratePerNight
                    ) {
            var hotel = await _dbContext.Hotels.FindAsync(hotelId);
            if (hotel != null && hotel.ProviderId == userId) {
                var result = await _calendarManual.Where(p => p.HotelId == hotelId && p.RoomId == roomId && p.Date.Date == date.Date).FirstOrDefaultAsync();
                if (result == null) // New Record
                {
                    await _calendarManual.AddAsync(new CalendarManual { HotelId = hotelId, RoomId = roomId, Date = date, Status = true, Rooms = rooms, RatePerNight = ratePerNight });
                } else {
                    
                  
                    _calendarManual.Update(result);
                }
                await _uow.SaveChangesAsync();
            } else
                throw new CustomException("Invalid Hotel!");
        }



        public async Task SetCalendar(long hotelId,
                                long userId,
                                DateTime openFrom,
                                DateTime openTo
                                ) {
            var hotel = await _dbContext.Hotels.FindAsync(hotelId);
            if (hotel != null && hotel.ProviderId == userId) {
                var calendar = await _calendar.Where(p => p.HotelId == hotelId).FirstOrDefaultAsync();
                if (calendar == null) // New Entry For Hotel
                {
                    _calendar.Add(new Calendar {
                        HotelId = hotelId,
                        OpenFrom = openFrom,
                        OpenTo = openTo
                    });
                } else {
                    calendar.OpenFrom = openFrom;
                    calendar.OpenTo = openTo;
                    _calendar.Update(calendar);
                }
                await _uow.SaveChangesAsync();
            } else
                throw new CustomException("Invalid Hotel!");
        }

        public async Task CloseRooms(long hotelId,
                                long userId,
                              DateTime closeFrom,
                              DateTime closeTo,
                              Boolean fri,
                              Boolean sat,
                              Boolean sun,
                              Boolean mon,
                              Boolean tue,
                              Boolean wed,
                              Boolean thr
                              ) {

            var calendar = await _calendar
                .Include(s => s.Hotel)
                .Where(p => p.HotelId == hotelId
                && p.Hotel.ProviderId == userId
                ).FirstOrDefaultAsync();
            if (calendar != null) // New Entry For Hotel
            {
                calendar.CloseFrom = closeFrom;
                calendar.CloseTo = closeTo;
                calendar.Fri = fri;
                calendar.Sat = sat;
                calendar.Sun = sun;
                calendar.Mon = mon;
                calendar.Tue = tue;
                calendar.Wed = wed;
                calendar.Thr = thr;
                _calendar.Update(calendar);
                await _uow.SaveChangesAsync();
            } else
                throw new CustomException("Invalid Hotel!");
        }

        public async Task<CalendarQuery> GetCalendar(long hotelId, long userId) {
            var calendar = await _calendar.Include(s => s.Hotel)
                .Where(p => p.HotelId == hotelId && p.Hotel.ProviderId == userId).FirstOrDefaultAsync();
            if (calendar != null)
                return new CalendarQuery {
                    Id = hotelId,
                    OpenFrom = calendar.OpenFrom,
                    OpenTo = calendar.OpenTo,
                    CloseFrom = calendar.CloseFrom,
                    CloseTo = calendar.CloseTo,
                    Fri = calendar.Fri,
                    Sat = calendar.Sat,
                    Sun = calendar.Sun,
                    Mon = calendar.Mon,
                    Tue = calendar.Tue,
                    Wed = calendar.Wed,
                    Thr = calendar.Thr
                };
            return null;
        }

        public async Task<ICollection<CalendarManual>> GetCalendarManual(long roomId, long userId) {
            return await (from p in _calendarManual
                          join h in _dbContext.Hotels
                          on p.HotelId equals h.Id
                          where p.RoomId == roomId
                          && h.ProviderId == userId
                          select p
                          ).ToListAsync();
        }

        public async Task<bool> IsHotelCalenderConfigured(long HotelId, long userId) {
            return await _dbContext.Calendar.Include(s => s.Hotel).Where(s => s.HotelId == HotelId && s.Hotel.ProviderId == userId).AnyAsync();
        }
    }
}
