﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Zowarah.Application.Interfaces;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Data.Contexts;
using Zowarah.Data.Repositories;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Transactions;
using Zowarah.Domain.Queries.Hotels;

namespace Zowarah.Application.Services.Hotels
{

    public class PromotionService : Repository<HotelPromotion>, IPromotionService
    {
        public PromotionService(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public bool IsValidPromotionTypePraramer(PromotionType promotionType, int bookingDaysCondition, int freeNights, int minimunStay)
        {
            return ((promotionType == PromotionType.LastMinute || promotionType == PromotionType.Earlybooked) && bookingDaysCondition > 0) ||
                (promotionType == PromotionType.FreeNights && freeNights > 0 && minimunStay > 0) || (promotionType == PromotionType.Basic);
        }
        public async Task<ICollection<Promotion>> GetRoomsPromotions(long serviceId, Boolean isMember, DateTime checkInDate, DateTime checkOutDate)
        {
            var stay = (checkOutDate - checkInDate).Days;
            var bookedBefore = Math.Abs(DateTime.Now.Subtract(checkInDate).Days);
            var query = _dbContext.HotelPromotionRooms
                        .Include(s => s.HotelPromotion)
                        .Where(s => s.HotelPromotion.ServiceId == serviceId
                        && s.HotelPromotion.IsActive
                        && ((s.HotelPromotion.AudienceId == AudienceEnum.Members && isMember) || (s.HotelPromotion.AudienceId == AudienceEnum.Public))
                          && checkInDate.Date >= s.HotelPromotion.FromDate.Date && checkOutDate.Date <= s.HotelPromotion.ToDate.Date && stay >= s.HotelPromotion.MinimumStay
                          && ((s.HotelPromotion.PromotionType == PromotionType.LastMinute && bookedBefore <= s.HotelPromotion.BookingDaysCondition) ||
                              (s.HotelPromotion.PromotionType == PromotionType.Earlybooked && bookedBefore >= s.HotelPromotion.BookingDaysCondition) ||
                              (s.HotelPromotion.PromotionType == PromotionType.FreeNights) ||
                              (s.HotelPromotion.PromotionType == PromotionType.Basic))
                              )
                           .OrderByDescending(s => s.HotelPromotion.FreeNights)
                           .OrderByDescending(s => s.HotelPromotion.Discount)
                           .OrderByDescending(s => s.HotelPromotion.PromotionType)
                            .Select(s => new
                            {
                                s.RoomId,
                                s.HotelPromotion
                            })
                            .GroupBy(g => g.RoomId)
                            .Select(gr => new
                            {
                                RoomId = gr.Key,
                                Promotion = gr.Select(s => s.HotelPromotion).FirstOrDefault()
                            }).Select(s => new Promotion
                            {
                                RoomId = s.RoomId,
                                Discount = s.Promotion.Discount,
                                FreeNights = s.Promotion.FreeNights,
                                MinimumStay = s.Promotion.MinimumStay,
                                PromotionType = s.Promotion.PromotionType,
                                PromotionId = s.Promotion.Id
                            });
            return await query.ToListAsync();
        }

        public async Task<HotelPromotion> GetById(long promotionId, params Expression<Func<HotelPromotion, object>>[] includes)
        {
            return await (from m in FindBy(x => x.Id == promotionId, includes)
                          join s in _dbContext.Services
                          on m.ServiceId equals s.Id
                          //where s.providerid == userid
                          select m).FirstOrDefaultAsync();
        }

        public async Task<Promotion> GetRoomPromotion(long roomId, Boolean isMember, DateTime checkInDate, DateTime checkOutDate)
        {
            var stay = (checkOutDate - checkInDate).Days;
            var bookedBefore = Math.Abs(DateTime.Now.Subtract(checkInDate).Days);
            return await _dbContext.HotelPromotionRooms
                        .Include(s => s.HotelPromotion)
                        .Where(s => s.RoomId == roomId
                            && ((s.HotelPromotion.AudienceId == AudienceEnum.Public && isMember == false) || (isMember))
                            && s.HotelPromotion.IsActive
                            && checkInDate.Date >= s.HotelPromotion.FromDate.Date && checkOutDate.Date <= s.HotelPromotion.ToDate.Date && stay >= s.HotelPromotion.MinimumStay
                            && ((s.HotelPromotion.PromotionType == PromotionType.LastMinute && bookedBefore <= s.HotelPromotion.BookingDaysCondition) ||
                              (s.HotelPromotion.PromotionType == PromotionType.Earlybooked && bookedBefore >= s.HotelPromotion.BookingDaysCondition) ||
                              (s.HotelPromotion.PromotionType == PromotionType.FreeNights) ||
                              (s.HotelPromotion.PromotionType == PromotionType.Basic))
                              )
                              .OrderByDescending(s => s.HotelPromotion.FreeNights)
                              .OrderByDescending(s => s.HotelPromotion.Discount)
                              .OrderByDescending(s => s.HotelPromotion.PromotionType)
                              .Select(s => new Promotion
                              {
                                  RoomId = s.RoomId,
                                  Discount = s.HotelPromotion.Discount,
                                  FreeNights = s.HotelPromotion.FreeNights,
                                  MinimumStay = s.HotelPromotion.MinimumStay,
                                  PromotionType = s.HotelPromotion.PromotionType,
                                  PromotionId = s.HotelPromotion.Id
                              }).FirstOrDefaultAsync();
        }

        public async Task<ICollection<TopPromotion>> GetTopPromotions(int noOfPromotions)
        {
            // To Get the best deals for the next 24 hours 
            var query = await (from p in _dbContext.HotelPromotions
                               join s in _dbContext.Services
                               on p.ServiceId equals s.Id
                               join h in _dbContext.Hotels
                               on p.ServiceId equals h.Id
                               join u in _dbContext.Users
                               on s.ProviderId equals u.Id
                               where DateTime.Now.Date >= p.FromDate && DateTime.Now.Date <= p.ToDate && p.IsActive
                               orderby p.FreeNights descending, p.Discount descending, p.PromotionType descending
                               select new TopPromotion
                               {
                                   PropertyBasicInfo = new PropertyBasicInfo()
                                   {
                                       Id = p.ServiceId,
                                       Address = h.Address,
                                       Name = s.Name,
                                       Rating = h.Rating,
                                   //    TaxP = h.TaxP,
                                       CityId = s.CityId,
                                       ProviderId = s.ProviderId,
                                       CurrencyId = u.CurrencyId,
                                       MainPhoto = (from ph in _dbContext.HotelPhotos where ph.HotelId == h.Id && ph.IsMain == true select ph.FileName).FirstOrDefault()
                                   },
                                   Promotion = new Promotion
                                   {
                                       PromotionId = p.Id,
                                       Discount = p.Discount,
                                       FreeNights = p.FreeNights,
                                       MinimumStay = p.MinimumStay,
                                       PromotionType = p.PromotionType,
                                       RoomId = p.Id,
                                       FromDate = p.FromDate,
                                       ToDate = p.ToDate
                                   }
                               }
                )
               .Take(noOfPromotions)
                .ToListAsync();
            return query;
        }

    }
}
