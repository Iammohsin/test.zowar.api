﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Data.Contexts;
using Zowarah.Data.Repositories;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Transactions;

namespace Zowarah.Application.Services.Hotels
{
    public class BookingDetails
    {
        public string Id { get; set; }
        public long ServiceId { get; set; }
        public string ServiceName { get; set; }
        public long ClientId { get; set; }
        public long RoomId { get; set; }
        public int Rooms { get; set; }
        public DateTime Date { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public TransactionStatus Status { get; set; }
        public decimal ProviderAmount { get; set; }
        public decimal ClientAmount { get; set; }
        public decimal ExchangeRate { get; set; }
        public string ProviderCurrency { get; set; }
        public string ClientCurrency { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Boolean IsPersonal { get; set; }
        public string RefereeName { get; set; }
        public string RefereeEmail { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string Mobile { get; set; }
        public int CreditCardId { get; set; }
        public string CardNo { get; set; }
        public byte CardExpiryMonth { get; set; }
        public int CardExpiryYear { get; set; }
        public string SpecialRequest { get; set; }
        public DateTime ArrivalDate { get; set; }
        public string TransactionId { get; set; }
        public string RoomName { get; set; }
        public Boolean IsInCheckInDate { get; set; }
        public Boolean IsInCheckOutDate { get; set; }
        public bool HasReview { get; set; }
        public Boolean isPaymentHotel { get; set; }
    }

    public class NetBooking
    {
        public long RoomId { get; set; }
        public DateTime Date { get; set; }
        public int Rooms { get; set; }
    }

    public class BookingService : Repository<Booking>, IBookingService
    {



        public BookingService(ApplicationDbContext db) : base(db)
        {
        }

        public async Task<IQueryable<NetBooking>> GetNetBooking(long roomId, DateTime fromDate, DateTime toDate)
        {
            IQueryable<NetBooking> query = from c in _dbSet.Include(b => b.Transaction)
                                           where c.RoomId == roomId && (c.Transaction.Date >= fromDate && c.Transaction.Date <= toDate)
                                           group c by new { c.RoomId, c.Transaction.Date }
                        into grp
                                           select new NetBooking
                                           {
                                               RoomId = grp.Key.RoomId,
                                               Date = grp.Key.Date,
                                               Rooms = grp.Sum(t => t.Rooms)
                                           };
            return await Task.FromResult(query);
        }

        public async Task<int> GetMaxBooked(long roomId, DateTime fromDate, DateTime toDate)
        {
            var result = await GetNetBooking(roomId, fromDate, toDate).Result.ToListAsync();

            if (result == null)
            {
                return 0;
            }

            if (result.Count == 0)
            {
                return 0;
            }
            else
            {
                return result.Max(x => x.Rooms);
            }
        }

        public async Task<BookingDetails> GetBookingDetails(string Id)
        {

            var query = (from t in _dbContext.Booking.Include(t => t.Transaction).Include(t => t.Transaction.Service)
                         join r in _dbContext.Rooms
                         on t.RoomId equals r.Id
                         join rt in _dbContext.RoomTypeNames
                         on r.RoomTypeNameId equals rt.Id
                         join rev in _dbContext.Reviews
                         on t.Transaction.Id equals rev.TransactionId into revs
                         from review in revs.DefaultIfEmpty()
                         where t.Id == Id
                         select new BookingDetails()
                         {
                             Id = t.Id,
                             ArrivalDate = t.ArrivalDate,
                             CardExpiryMonth = t.Transaction.CardExpiryMonth,
                             CardExpiryYear = t.Transaction.CardExpiryYear,
                             CardNo = t.Transaction.CardNo,
                             CreditCardId = t.Transaction.CreditCardId,
                             CheckInDate = t.CheckInDate,
                             CheckOutDate = t.CheckOutDate,
                             ClientAmount = t.Transaction.ClientAmount,
                             ClientCurrency = t.Transaction.ClientCurrency,
                             ClientId = t.Transaction.ClientId,
                             CountryId = t.Transaction.CountryId,
                             //  CountryName = c.Name,
                             Date = t.Transaction.Date,
                             Email = t.Transaction.Email,
                             ExchangeRate = t.Transaction.ExchangeRate,
                             Title = t.Transaction.Title,
                             FirstName = t.Transaction.FirstName,
                             LastName = t.Transaction.LastName,
                             IsPersonal = t.IsPersonal,
                             isPaymentHotel = t.isPaymentHotel,
                             Mobile = t.Transaction.Mobile,
                             ProviderAmount = t.Transaction.ProviderAmount,
                             ProviderCurrency = t.Transaction.ProviderCurrency,
                             RefereeEmail = t.RefereeEmail,
                             RefereeName = t.RefereeName,
                             RoomId = t.RoomId,
                             Rooms = t.Rooms,
                             ServiceId = t.Transaction.ServiceId,
                             ServiceName = t.Transaction.Service.Name,
                             SpecialRequest = t.SpecialRequest,
                             Status = t.Transaction.Status,
                             TransactionId = t.TransactionId,
                             RoomName = rt.Description,
                             IsInCheckInDate = t.CheckInDate > DateTime.Now,
                             IsInCheckOutDate = t.CheckOutDate <= DateTime.Now,
                             HasReview = review != null ? true : false
                         });

            return await query.FirstOrDefaultAsync();
        }
        public Task<int> GetLastReservedRooms(long hotelId, int days)
        {
            var query = (from m in _dbSet
                         join t in _dbContext.Transactions
                         on m.TransactionId equals t.Id
                         where t.ServiceId == hotelId && (DateTime.Now - t.Date).TotalHours <= 24
                         select t
                         ).Count();
            return Task.FromResult(query);
        }

        public async Task<bool> IsClientShowUp(string BookingId)
        {
            return await _dbContext.Booking.Include(s => s.Transaction).Where(s => s.Id == BookingId && s.CheckOutDate < DateTime.Now && s.Transaction.Status == TransactionStatus.Confirmed).AnyAsync();
        }


        public async Task<decimal> GetTodayBooking(long hotelId)
        {
            return await _dbContext.Booking.Include(t => t.Transaction).Where(s => s.Transaction.ServiceId == hotelId && s.Transaction.Date.Date == DateTime.Now.Date).CountAsync();
        }
        public async Task<decimal> GetTotalBooking(long hotelId)
        {
            return await _dbContext.Booking.Include(t => t.Transaction).Where(s => s.Transaction.ServiceId == hotelId).CountAsync();
        }
        public async Task<decimal> GetTodayArrival(long hotelId)
        {
            return await _dbContext.Booking.Include(t => t.Transaction)
                .Where(s => s.Transaction.ServiceId == hotelId &&
                s.CheckInDate.Date == DateTime.Now.Date
       //         &&  s.Transaction.Date.Date == DateTime.Now.Date
                )
                .CountAsync();
        }
        public async Task<decimal> GetTodayDeparture(long HotelId)
        {
            return await _dbContext.Booking.Include(t => t.Transaction)
                .Where(s => s.Transaction.ServiceId == HotelId &&
                s.CheckOutDate.Date == DateTime.Now.Date
             //   && s.Transaction.Date.Date == DateTime.Now.Date
                )
                .CountAsync();
        }

    }
}
