﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Zowarah.Application.Interfaces;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Data.Contexts;
using Zowarah.Data.Repositories;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.Application.Services.Hotels
{
    public class RoomQuery
    {
        public long Id { get; set; }
        public long HotelId { get; set; }
        public int RoomTypeId { get; set; }
        public int RoomTypeNameId { get; set; }
        public int SmokingOptionId { get; set; }
        public int NoOfRooms { get; set; }
        public Boolean IsLivingRoom { get; set; }
        public Boolean IsKitchen { get; set; }
        public int NoOfPathrooms { get; set; }
        public int NoOfPersons { get; set; }
        public Decimal RatePerNight { get; set; }

        public Decimal TotalRatePerNight { get; set; }
        public Decimal TaxOnRate { get; set; }

        public Decimal VatOnRate { get; set; }
       


        public String RoomName { get; set; }
    }

    public class RtcQuery
    {
        public string Id { get; set; }
        public string BookingId { get; set; }
        public DateTime TransactionDate { get; set; }
        public string BranchID { get; set; }
        public string RefNum { get; set; }
        public string BillNumber { get; set; }
        public decimal Amount { get; set; }
    }
    public class RoomService : Repository<Room>, IRoomService
    {  
        public RoomService(ApplicationDbContext db) : base(db)
        {
     
        }

       
        public async Task<RoomQuery> GetDetailsById(long id)
        {
            var query = (from m in _dbSet
                         join d in _dbContext.RoomTypeNames
                         on m.RoomTypeNameId equals d.Id
                         where m.Id==id
                         select new RoomQuery
                         {
                             Id = id,
                             HotelId = m.HotelId,
                             RoomTypeNameId = m.RoomTypeNameId,
                             RoomTypeId = d.RoomTypeId,
                             NoOfRooms = m.NoOfRooms,
                             NoOfPersons = m.NoOfPersons,
                             SmokingOptionId = m.SmokingOptionId,
                             RoomName = d.Description,
                             RatePerNight = m.RatePerNight,
                             TotalRatePerNight = m.TotalRatePerNight,
                             TaxOnRate = m.TaxOnRate,
                             VatOnRate = m.VatOnRate

                         });
            return await query.FirstOrDefaultAsync();
        }

        public async Task<Room> GetById(long roomId)
        {
            return await _dbSet.FindAsync(roomId);            
        }
        public async Task<Room> GetById(long roomId,bool isIncludeHotel) {
            return await _dbSet.Include(s=>s.Hotel).Where(d=>d.Id==roomId).SingleOrDefaultAsync();          
        }

        public async Task<IQueryable<RoomQuery>> GetByHotelId(long hotelId)
        {
            IQueryable<RoomQuery> query =  (from p in _dbContext.Rooms
                          join c in _dbContext.RoomTypeNames
                          on p.RoomTypeNameId equals c.Id
                          where p.HotelId == hotelId && p.IsDeleted==false
                          select new RoomQuery
                          {
                              Id = p.Id,
                              HotelId = p.HotelId,
                              RoomTypeId=c.RoomTypeId,
                              RoomTypeNameId = p.RoomTypeNameId,
                              SmokingOptionId = p.SmokingOptionId,
                              NoOfPersons=p.NoOfPersons,
                              NoOfRooms=p.NoOfRooms,
                              RoomName=c.Description,
                              RatePerNight=p.RatePerNight,
                              TotalRatePerNight = p.TotalRatePerNight,
                              TaxOnRate = p.TaxOnRate,
                              VatOnRate = p.VatOnRate

                          }
                               );
                          return await Task.FromResult(query);
        }

      

    }
}
