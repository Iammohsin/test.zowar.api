using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Zowarah.Application.Enums.Hotels;
using Zowarah.Application.Interfaces;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Data.Contexts;
using Zowarah.Data.Repositories;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Transactions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Queries.Hotels;

namespace Zowarah.Application.Services.Hotels
{
    public class HotelBooking
    {
        public string Id { get; set; }
        public string TransactionId { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RoomName { get; set; }
        public DateTime Date { get; set; }
        public TransactionStatus Status { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public bool IsInCheckInDate { get; set; }
        public bool IsInCheckOutDate { get; set; }
    }

    public class HotelQuery
    {
        public long Id { get; set; }
        public long ProviderId { get; set; }
        public string Name { get; set; }
        public int Rating { get; set; }

        public decimal TaxP { get; set; }

        public decimal Vat { get; set; }

        public int PropertyTypeId { get; set; }
        public int NoOfRooms { get; set; }
        public string ContactName { get; set; }
        public string Address { get; set; }
        public int CityId { get; set; }
        public ServiceStatusEnum Status { get; set; }
        public int CheckInFrom { get; set; }
        public int CheckInTo { get; set; }
        public int CheckOutFrom { get; set; }
        public int CheckOutTo { get; set; }
        public int InternetOptionId { get; set; }
        public int ParkingOptionId { get; set; }
        public int ParkingTypeId { get; set; }
        public int ParkingSiteId { get; set; }
        public int ParkingReservationId { get; set; }
        public int BreakfastOptionId { get; set; }
        public Boolean IsPetsAllowed { get; set; }
        public Boolean IsChildrenAllowed { get; set; }
        public Boolean IsCreditCardAccepted { get; set; }
        public ICollection<int> CreditCards { get; set; }
        public ICollection<int> CancellationPolicyId { get; set; }
        public ICollection<int> CancellationFeeId { get; set; }
        public String InvoicingRecipientName { get; set; }
        public int InvoicingCountryId { get; set; }
        public int InvoicingCityId { get; set; }
        public string InvoicingAddress { get; set; }
        public string InvoicingPostalAddress { get; set; }
        public string IsInvoicingPropertyAddress { get; set; }
        public String CityName { get; set; }
        public String StatusName { get; set; }
        public ICollection<int> RoomAmenities { get; set; }
        public ICollection<int> Facilities { get; set; }
        public decimal LocLng { get; set; }
        public decimal LocLat { get; set; }

        // Indicators 
        public Boolean IsBookable { get; set; }
        public int Arrivals { get; set; }
        public int Depatures { get; set; }
    }

    public class HotelRoomQuery
    {
        public long Id { get; set; }

        /* Hotel Details*/
        public long ServiceId { get; set; }
        public long HotelId { get; set; }
        public long ProviderId { get; set; }
        public string Name { get; set; }
        public int? PropertyTypeId { get; set; }
        public int? Rating { get; set; }
        //mohsin
        //    public decimal? TaxP { get; set; }
        public decimal LocLng { get; set; }
        public decimal LocLat { get; set; }
        public string ContactName { get; set; }
        public string Address { get; set; }
        public int? CityId { get; set; }
        public string Contact1 { get; set; }
        public string Contact2 { get; set; }
        public byte CheckInFrom { get; set; }
        public byte CheckInTo { get; set; }
        public byte CheckOutFrom { get; set; }
        public byte CheckOutTo { get; set; }
        public int? InternetOptionId { get; set; }
        public int? ParkingOptionId { get; set; }
        public int? ParkingTypeId { get; set; }
        public int? ParkingSiteId { get; set; }
        public int? ParkingReservationId { get; set; }
        public int? BreakFastOptionId { get; set; }
        public Boolean IsPetsAllowed { get; set; }
        public Boolean IsChildrenAllowed { get; set; }
        public IEnumerable<int> Facilities { get; set; }
        public IEnumerable<int> RoomAmenities { get; set; }

        /* Room Details */
        public int? SmokingOptionId { get; set; }
        public int NoOfRooms { get; set; }
        public bool IsLivingRoom { get; set; }
        public bool IsKitchen { get; set; }
        public int NoOfBathrooms { get; set; }
        public int NoOfPersons { get; set; }
        public decimal RatePerNight { get; set; }
        public decimal TotalRatePerNight { get; set; }

        public decimal TaxOnRate { get; set; }

        public decimal VatOnRate { get; set; }

        public Boolean isCreditCardAccepted { get; set; }
        public int LastReservedRooms { get; set; }
        public int? CancellationPolicyId { get; set; }
        public Boolean IsFreeCancellation { get; set; }
        public int RemainingRooms { get; set; }
        public string CurrencyId { get; set; }
        public string MainPhoto { get; set; }
        public string RoomName { get; set; }
        public float ReviewsScore { get; set; }
        public decimal TotalReviews { get; set; }
    }
    public class AvailableRoom
    {
        public long Id { get; set; }
        public int Rooms { get; set; }

        public decimal RatePerNight { get; set; }
        public DateTime Date { get; set; }


    }
    public class HotelRoomAmenityQuery
    {
        public int RoomAmenityId;
        public long RoomId;
    }
    public class HotelRoomSaleReporting
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int PerNight { get; set; }
        public decimal Revenue { get; set; }
        public decimal AverageRate { get; set; }
    }
    public class HotelRoomSale
    {
        public DateTime Date { get; set; }
        public int Rooms { get; set; }
        public decimal Amount { get; set; }
        public string BookingId { get; set; }

    }
    public class HotelRoomSaleDetails
    {
        public HotelRoomSaleDetails()
        {
            HotelRoomSales = new HashSet<HotelRoomSale>();
        }
        public HotelRoomSaleReporting HotelRoomSaleReporting { get; set; }
        public ICollection<HotelRoomSale> HotelRoomSales { get; set; }

    }
    public class HotelService : Repository<Hotel>, IHotelService
    {
        private readonly IBookingService bookingRepo;
        private readonly IReviewService reviewService;
        private readonly IPromotionService promoService;
        private readonly IRepository<Hotel> hotelRepo;
        private readonly IRepository<Service> serviceRepo;
        private readonly IUnitOfWork uow;
        private readonly IHttpContextAccessor httpContextAccessor;

        #region Search Compiled Query
        //static readonly Func<ApplicationDbContext, DateTime, DateTime, IOrderedQueryable<(long,decimal)>> _hotelPromotionsCompiledQuery = EF.CompileQuery((ApplicationDbContext db, DateTime _checkInTime, DateTime _checkOutDate) => db.HotelPromotions
        //                             .Where(s => _checkInTime.Date >= s.FromDate.Date && _checkOutDate <= s.ToDate.Date)
        //                             .Select(s => new {( s.Id, s.Discount) })
        //                            .OrderByDescending(p => p.Discount));
        #endregion
        public HotelService(ApplicationDbContext _db,
                            IHttpContextAccessor _httpContextAccessor,
                            IPromotionService _promoService,
                            IBookingService _bookingRepo,
                            IRepository<Hotel> _hotelRepo,
                            IRepository<Service> _serviceRepo,
                            IReviewService _reviewService,
                            IUnitOfWork _uow
                            ) : base(_db)
        {

            bookingRepo = _bookingRepo;
            hotelRepo = _hotelRepo;
            promoService = _promoService;
            serviceRepo = _serviceRepo;
            reviewService = _reviewService;
            uow = _uow;
            httpContextAccessor = _httpContextAccessor;
        }

        public async Task<Hotel> GetById(long hotelId, params Expression<Func<Hotel, object>>[] includes)
        {
            return await (from m in FindBy(x => x.Id == hotelId, includes)
                          join s in _dbContext.Services
                          on m.Id equals s.Id
                          // where s.ProviderId == long.Parse(httpContextAccessor.HttpContext.User.Claims.Where(x=> x.Type == "id").FirstOrDefault().Value)
                          select m).FirstOrDefaultAsync();
        }


        public async Task<PropertyBasicInfo> GetBasicInfo(long id)
        {
            var query = (from h in _dbContext.Hotels
                         join m in _dbContext.Services
                         on h.Id equals m.Id
                         where h.Id == id
                         select new PropertyBasicInfo
                         {
                             Id = m.Id,
                             Name = m.Name,
                             Address = h.Address,
                             Contact1 = h.Contact1,
                             Contact2 = h.Contact2,
                             CityId = m.CityId,
                             ContactName = h.ContactName,
                             NoOfRooms = h.noOfRooms,
                             Rating = h.Rating,
                             //      TaxP = h.TaxP,
                             LocLat = h.LocLat,
                             LocLng = h.LocLng,
                             SmokingOptionId = h.SmokingOptionId,
                             PropertyTypeId = h.PropertyTypeId,
                             ProviderId = m.ProviderId,
                             MainPhoto = (from ph in _dbContext.HotelPhotos where ph.HotelId == id && ph.IsMain == true select ph.FileName).FirstOrDefault()
                         }
                         ).FirstOrDefault();
            return await Task.FromResult(query);
        }


        public async Task<ICollection<SearchResult>> SearchHotel()
        {
            var query = await (from m in _dbContext.Hotels
                               join s in _dbContext.Services
                               on m.Id equals s.Id
                               select new
                               {
                                   HotelId = m.Id,
                               }).ToListAsync();
            var query1 = query
                        .Select(x => new SearchResult
                        {
                            PropertyBasicInfo = GetBasicInfo(x.HotelId).Result
                        }).ToList();
            return query1;
        }

        public async Task<PropertyDetails> GetPropertyDetails(long id)
        {
            var query = (from h in _dbContext.Hotels
                         join m in _dbContext.Services
                         on h.Id equals m.Id
                         where m.Id == id
                         select new PropertyDetails
                         {
                             Id = m.Id,
                             checkInFrom = h.CheckInFrom,
                             checkInTo = h.CheckInTo,
                             checkOutFrom = h.CheckOutFrom,
                             checkOutTo = h.CheckOutTo,
                             isChildrenAllowed = h.IsChildrenAllowed,
                             isPetsAllowed = h.IsPetsAllowed,
                             internetOptionId = h.InternetOptionId != null ? h.InternetOptionId.Value : 0
                         }).FirstOrDefault();
            return await Task.FromResult(query);
        }

        public async Task<HotelSearchParameters> GetHotelSearchParameters(long hotelId, DateTime fromDate, DateTime toDate)
        {
            return await Task.FromResult(new HotelSearchParameters
            {
                LastReservedRooms = (from t in _dbContext.Transactions join b in _dbContext.Booking on t.Id equals b.TransactionId join d in _dbContext.Rooms on b.RoomId equals d.Id where (t.Date > DateTime.Now.AddDays(-1) && t.Date <= DateTime.Now) && d.HotelId == hotelId select b.Rooms).Sum(),
            });
        }

        public async Task<RoomSearchParameters> GetRoomSearchParameters(long roomId, DateTime fromDate, DateTime toDate)
        {
            var booked = _dbContext.Booking.Select(s => new { s.RoomId, s.Rooms, s.CheckInDate, s.CheckOutDate }).Where(c => c.RoomId == roomId && c.CheckInDate.Date >= fromDate && c.CheckOutDate.Date < toDate).GroupBy(s => s.RoomId).Select(s => new { RoomId = s.Key, Rooms = s.Sum(r => r.Rooms) });
            var rooms = await (from r in _dbContext.Rooms.Where(s => s.Id == roomId)
                               join c in booked
                                  on r.Id equals c.RoomId
                               select
                                    c != null ? r.NoOfRooms - c.Rooms : r.NoOfRooms
                        ).FirstOrDefaultAsync();
            return new RoomSearchParameters
            {
                RemainingRooms = rooms
            };
        }

        public async Task<ReviewsDetails> GetReviewsDetails(long serviceId)
        {
            var service = await serviceRepo.GetById(serviceId);
            return new ReviewsDetails
            {
                //RemainingRooms = room.NoOfRooms - bookingRepo.GetMaxBooked(room.Id, fromDate, toDate).Result,
                ReviewsScore = service.ReviewScore,
                TotalReviews = service.TotalReviews,
                Features = await reviewService.GetServiceFeaturesScore(serviceId),
                Averages = await reviewService.GetServiceAveragesScore(serviceId)
            };
        }

        /*   public async Task<Promotion> GetPromotionDetails(long RoomId, DateTime fromDate, DateTime toDate)
           {
               //var room = await _dbContext.Rooms.FindAsync(roomId);
               return await Task.FromResult(new PromotionDetails
               {
                   //  RemainingRooms = room.NoOfRooms - bookingRepo.GetMaxBooked(room.Id, fromDate, toDate).Result,
                   PromotionId = "",
                   Percentage = 5
               });
           }*/


        /*     public async Task<ICollection<HotelSearchResult>> SearchHotel(DateTime fromDate, DateTime toDate)
             {
                 // To Get The Seed Records , Fill them with Sub Queries 
                 ApplicationDbContext dbContext = _dbContext;
                 var query = await (from h in dbContext.Hotels

                              join s in dbContext.Services
                              on h.Id equals s.Id
                              join m in dbContext.Rooms
                              on h.Id equals m.HotelId
                              select new HotelSearchResult
                              {
                                    HotelId = h.Id,
                                    RoomId = m.Id
                              }
                              ).ToListAsync();

                 foreach(var item in query)
                 {
                     item.propertyBasicInfo = await GetBasicInfo(item.HotelId);
                     item.propertyDetails = await GetPropertyDetails(item.HotelId);
                     item.ReviewsDetails = await GetReviewsDetails(item.HotelId);
                     item.PromotionDetails = await GetPromotionDetails(item.RoomId, fromDate, toDate);
                     item.HotelSearchParameters = await GetHotelSearchParameters(item.HotelId,fromDate,toDate);
                     item.RoomSearchParameters = await GetRoomSearchParameters(item.RoomId, fromDate, toDate);
                 }
                 return await Task.FromResult(query);
             }
             */

        /*      public async Task<Hotel> GetById(long id, string includeProperties = null)
              {
                  var query = _dbSet.Where(p => p.Id == id);
                  if (includeProperties != null) {
                      foreach (var includeProperty in includeProperties.Split
                      (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)) {
                          query = query.Include(includeProperty);
                      }
                  }
                  return await query.FirstOrDefaultAsync();
              }*/

        public async Task<Service> SubmitHotel(long hotelId, long UserId)
        {
            var hotel = await hotelRepo.GetById(hotelId);
            var service = await serviceRepo.GetById(hotel.Id);
            if (hotel != null && service != null && service.ProviderId == UserId)
            {
                if (hotel.IsBasicInfoConfigured &&
                    hotel.IsPropertyDetailsConfigured &&
                    hotel.IsRoomTypesConfigured &&
                    hotel.IsPhotosConfigured &&
                    hotel.IsPaymentPolicyConfigured)
                {
                    hotel.IsSubmitted = true;
                    hotel.UpdatedOn = DateTime.Now;
                    service.Status = ServiceStatusEnum.Submitted;
                    hotelRepo.Update(hotel);
                    serviceRepo.Update(service);
                    var result = await uow.SaveChangesAsync();
                }
                else
                    throw new Exception("Incomplete Configuration!, Please complete the configuration before submitting.");
                return service;
            }
            else
            {
                throw new Exception("Invalid Hotel !!");
            }
        }
        public async Task<HotelQuery> GetHotelById(long id)
        {
            var query =
                (from p in _dbContext.Hotels.Include(p => p.Facilities)
                 join c in _dbContext.Cities
                 on p.CityId equals c.Id
                 join s in _dbContext.Services
                 on p.Id equals s.Id
                 where p.Id == id
                 select new HotelQuery
                 {
                     Id = p.Id,
                     ProviderId = p.ProviderId,
                     Name = p.Name,
                     Rating = p.Rating,
                     //       TaxP = p.TaxP,
                     PropertyTypeId = p.PropertyTypeId,
                     NoOfRooms = p.noOfRooms,
                     ContactName = p.ContactName,
                     Address = p.Address,
                     CityId = p.CityId.GetValueOrDefault(0),
                     Status = s.Status,
                     CheckInFrom = p.CheckInFrom,
                     CheckInTo = p.CheckInTo,
                     CheckOutFrom = p.CheckOutFrom,
                     CheckOutTo = p.CheckOutTo,
                     InternetOptionId = p.InternetOptionId.GetValueOrDefault(0),
                     ParkingOptionId = p.ParkingOptionId.GetValueOrDefault(0),
                     ParkingTypeId = p.ParkingTypeId.GetValueOrDefault(0),
                     ParkingSiteId = p.ParkingSiteId.GetValueOrDefault(0),
                     ParkingReservationId = p.ParkingReservationId.GetValueOrDefault(0),
                     BreakfastOptionId = p.BreakFastOptionId.GetValueOrDefault(0),
                     IsPetsAllowed = p.IsPetsAllowed,
                     IsChildrenAllowed = p.IsChildrenAllowed,
                     IsCreditCardAccepted = p.IsCreditCardAccepted,
                     InvoicingRecipientName = p.InvoicingRecipientName,
                     InvoicingCountryId = p.InvoicingCountryId,
                     InvoicingCityId = p.InvoicingCityId,
                     InvoicingAddress = p.InvoicingAddress,
                     InvoicingPostalAddress = p.InvoicingPostalAddress,
                     Facilities = p.Facilities.Select(e => e.FacilityId).ToList(),
                     // how to get this property
                     //IsInvoicingPropertyAddress = "",
                     CityName = c.Name,
                     //StatusName = s.Description,
                     LocLng = p.LocLat,
                     LocLat = p.LocLng
                 });

            return await query.FirstOrDefaultAsync();
        }

        public async Task<IQueryable<HotelQuery>> GetByProviderId(long providerId)
        {
            IQueryable<HotelQuery> query = (from p in _dbContext.Hotels
                                            join s in _dbContext.Services
                                            on p.Id equals s.Id
                                            join c in _dbContext.Cities
                                            on s.CityId equals c.Id
                                            where s.ProviderId == providerId
                                            select new HotelQuery
                                            {
                                                Id = p.Id,
                                                ProviderId = p.ProviderId,
                                                CityName = c.Description,
                                                Name = p.Name,
                                                //        TaxP = p.TaxP,
                                                Status = s.Status,
                                                IsBookable = (from cl in _dbContext.Calendar where cl.HotelId == p.Id select cl.Id).Count() > 0 && p.IsSubmitted && p.IsProfileConfigured && p.IsNearbyConfigured,
                                                Arrivals = (from b in _dbContext.Booking
                                                            join tr in _dbContext.Transactions on b.TransactionId equals tr.Id
                                                            join sr in _dbContext.Services on tr.ServiceId equals sr.Id
                                                            where sr.ProviderId == providerId && b.CheckInDate == DateTime.Now.Date
                                                            select b.Id
                                                          ).Count(),
                                                Depatures = (from b in _dbContext.Booking
                                                             join tr in _dbContext.Transactions on b.TransactionId equals tr.Id
                                                             join sr in _dbContext.Services on tr.ServiceId equals sr.Id
                                                             where sr.ProviderId == providerId && b.CheckOutDate == DateTime.Now.Date
                                                             select b.Id
                                                          ).Count(),
                                            }
                               );

            return await Task.FromResult(query);

        }

        public Task<HotelRoomQuery> GetHotelRoom(long roomId)
        {
            var hotelRoom = (from d in _dbSet
                             .Include(x => x.Facilities)
                             join r in _dbContext.Rooms
                             on d.Id equals r.HotelId
                             join c in _dbContext.RoomTypeNames
                             on r.RoomTypeNameId equals c.Id
                             join ct in _dbContext.Cities
                             on d.CityId equals ct.Id
                             where r.Id == roomId
                             select new HotelRoomQuery
                             {
                                 Id = r.Id,
                                 HotelId = d.Id,
                                 ServiceId = d.Id,
                                 ProviderId = d.ProviderId,
                                 Name = d.Name,
                                 Address = d.Address,
                                 IsChildrenAllowed = d.IsChildrenAllowed,
                                 isCreditCardAccepted = d.IsCreditCardAccepted,
                                 IsKitchen = r.IsKitchen,
                                 IsLivingRoom = r.IsLivingRoom,
                                 IsPetsAllowed = d.IsPetsAllowed,
                                 SmokingOptionId = d.SmokingOptionId,
                                 BreakFastOptionId = d.BreakFastOptionId,
                                 CancellationPolicyId = d.CancellationPolicyId,
                                 ParkingOptionId = d.ParkingOptionId,
                                 ParkingReservationId = d.ParkingReservationId,
                                 CheckInFrom = d.CheckInFrom,
                                 CheckOutFrom = d.CheckOutFrom,
                                 CheckInTo = d.CheckInTo,
                                 CheckOutTo = d.CheckOutTo,
                                 CityId = d.CityId,
                                 ContactName = d.ContactName,
                                 ParkingSiteId = d.ParkingSiteId,
                                 RatePerNight = r.RatePerNight,
                                 //TotalRatePerNight = r.TotalRatePerNight,
                                 //TaxOnRate = r.TaxOnRate,
                                 //VatOnRate = r.VatOnRate,
                                 InternetOptionId = d.InternetOptionId,
                                 Contact1 = d.Contact1,
                                 Contact2 = d.Contact2,
                                 NoOfRooms = r.NoOfRooms,
                                 NoOfPersons = r.NoOfPersons,
                                 Rating = d.Rating,
                                 //      TaxP = d.TaxP,
                                 RoomName = c.Description,
                                 LastReservedRooms = (from b in _dbContext.Booking join rm in _dbContext.Rooms on b.RoomId equals rm.Id where rm.HotelId == d.Id select b.Rooms).Sum(),
                                 MainPhoto = (from ph in _dbContext.HotelPhotos where ph.HotelId == d.Id && ph.IsMain == true select ph.FileName).FirstOrDefault(),
                                 RoomAmenities = (from ra in _dbContext.HotelRoomAmenities where ra.HotelId == d.Id && (ra.RoomId == r.Id || ra.RoomId == 0) select ra.RoomAmenityId),
                                 Facilities = d.Facilities.Select(x => x.FacilityId)
                             }
                             ).FirstOrDefaultAsync();
            return hotelRoom;
        }

        public async Task<IQueryable<HotelRoomAmenityQuery>> GetRoomAmenities(long hotelId)
        {
            IQueryable<HotelRoomAmenityQuery> query = (from m in _dbContext.HotelRoomAmenities
                                                       where m.HotelId == hotelId
                                                       select new HotelRoomAmenityQuery
                                                       {
                                                           RoomId = m.RoomId,
                                                           RoomAmenityId = m.RoomAmenityId
                                                       }
                                                    );
            return await Task.FromResult(query);
        }

        /*public async Task<IQueryable<Language>> GetHotelLanguages(long HotelId)
        {
            IQueryable<Language> query = (from m in _dbContext.HotelLanguages
                                          join l in _dbContext.Languages
                                          on m.LanguageId equals l.Id
                                          where m.HotelId == hotelId
                                                       select new Language
                                                       {
                                                           Id = m.LanguageId,
                                                           Description = l.Description
                                                       }
                                                    );
            return await Task.FromResult(query);
        }*/

        public async Task<int> GetAllocatedRooms(long hotelId, int roomRooms = 0)
        {
            var hotelRooms = _dbContext.Rooms.Where(x => x.HotelId == hotelId).Select(x => x.NoOfRooms).Sum();
            var allocatedRooms = hotelRooms - roomRooms;
            return await Task.FromResult(allocatedRooms);
        }


        public async Task<ICollection<HotelSearchResult>> Search(
            int page,
            int pageSize,
            Boolean isMember,
            DateTime checkInTime,
            DateTime checkOutTime,
            int noOfPersons,
            int noOfChildren,
            int noOfRooms,
            string city,
            int orderBy,
            int[] rating,
            //       decimal[] taxP,
            int[] propertyTypes,
            int[] facilities,
            int[] roomAmenties
            )
        {

            var bookedBefore = Math.Abs(DateTime.Now.Subtract(checkInTime).Days);
            var stay = checkOutTime.Subtract(checkInTime).Days;
            var allDays = GetAllDays(checkInTime, checkOutTime);
            var topRoomsQuery = EF.CompileQuery((ApplicationDbContext db, int _bookedBefore, int _stay, DateTime _checkInTime, DateTime _checkOutDate) =>
                                        (from r in db.Rooms.AsNoTracking()
                                                            .Where(rm => rm.NoOfPersons >= Math.Round((Decimal)(noOfPersons / noOfRooms), 0)
                              && rm.IsDeleted == false 
                       //  M T3 & T9
                                                            ).Select(s => new
                                                            {
                                                                s.Id,
                                                                s.HotelId,
                                                                s.RatePerNight,
                                                                s.TotalRatePerNight,
                                                                s.IsDeleted //mohsin9/3
                                                            })
                                         join rp in (from hp in (from s in db.HotelPromotions
                                                                 where _checkInTime.Date >= s.FromDate.Date && _checkOutDate.Date <= s.ToDate.Date && _stay <= s.MinimumStay
                                                                 //Mohsin Revoked MARCH FWTno 8 { stay should be like  _stay >= s.MinimumStay }
                                                                 && s.IsActive
                                                                 && ((s.PromotionType == PromotionType.LastMinute && _bookedBefore <= s.BookingDaysCondition) ||
                                                                     (s.PromotionType == PromotionType.Earlybooked && _bookedBefore >= s.BookingDaysCondition) ||
                                                                     (s.PromotionType == PromotionType.FreeNights
                                                                     //&& (_checkInTime.Date > s.FromDate && _checkOutDate.Date < s.ToDate && _stay >= s.MinimumStay)
                                                                     //Mohsin Revoked MARCH FWTno 8
                                                                     ) ||
                                                                     (s.PromotionType == PromotionType.Basic))
                                                                 orderby s.FreeNights descending, s.Discount descending
                                                                 select new { s.Id, s.Discount, s.PromotionType, s.FreeNights, s.MinimumStay })
                                                     join rp in db.HotelPromotionRooms
                                          on hp.Id equals rp.HotelPromotionId
                                                     orderby hp.Discount descending, hp.FreeNights descending
                                                     select new
                                                     {
                                                         rp.RoomId,
                                                         Promotion = new HotelSearchPromotion
                                                         {
                                                             Discount = hp.Discount,
                                                             PromotionType = hp.PromotionType,
                                                             FreeNights = hp.FreeNights,
                                                             MinimumStay = hp.MinimumStay
                                                         }
                                                     })
                                         on r.Id equals rp.RoomId into rp_grp
                                         from rpr in rp_grp.DefaultIfEmpty()
                                         select new
                                         {
                                             r.IsDeleted, //mohsin9/3
                                             r.HotelId,
                                             RoomId = r.Id,
                                             r.RatePerNight,
                                             r.TotalRatePerNight,
                                             Promotion = rpr != null ? rpr.Promotion : null
                                         }));

            var servicesQuery = EF.CompileQuery((ApplicationDbContext db, string _city) =>
                                    (from s in db.Services
                                     join ct in db.Cities on s.CityId equals ct.Id
                                     where s.Status == ServiceStatusEnum.Approved && s.IsDeleted == false &&
                                            ct.Description.ToLower().Contains(_city.ToLower())
                                     select new
                                     {
                                         s.Id,
                                         s.Name,
                                         s.ProviderId,
                                         s.Status,
                                         s.ReviewScore,
                                         s.TotalReviews
                                     }));

            //mohsin 
                      




            // mohsin end 

            var calanderQuery = EF.CompileQuery((ApplicationDbContext db, List<DateTime> _allDays, DateTime _checkInTime, DateTime _checkOutTime) =>
                                        db.Calendar.Where(cl => _allDays.Any(s => s.Date >= cl.CloseFrom || s.Date <= cl.CloseTo ||
                                   ((s.DayOfWeek == DayOfWeek.Sunday && cl.Sun) ||
                                    (s.DayOfWeek == DayOfWeek.Monday && cl.Mon) ||
                                    (s.DayOfWeek == DayOfWeek.Tuesday && cl.Tue) ||
                                    (s.DayOfWeek == DayOfWeek.Wednesday && cl.Wed) ||
                                    (s.DayOfWeek == DayOfWeek.Thursday && cl.Thr) ||
                                    (s.DayOfWeek == DayOfWeek.Friday && cl.Fri) ||
                                    (s.DayOfWeek == DayOfWeek.Saturday && cl.Sat))) &&
                        (_checkInTime >= cl.OpenFrom && _checkOutTime <= cl.OpenTo))
                                               .Select(s => s.HotelId));

            /////////////////////////////////////////////////////////////////////
            //Mohsin : To hide hotel if close date range is set 

            //var calanderQuery = EF.CompileQuery((ApplicationDbContext db, List<DateTime> _allDays, DateTime _checkInTime, DateTime _checkOutTime) =>
            //                             db.Calendar.Where(cl => _allDays.Any(s => (_checkInTime.Date >= cl.CloseFrom.Date && _checkInTime.Date >= cl.CloseTo.Date) ||
            //                             (_checkInTime.Date <= cl.CloseFrom.Date && _checkOutTime.Date <= cl.CloseFrom.Date) ||
            //                        ((s.DayOfWeek == DayOfWeek.Sunday && cl.Sun) ||
            //                         (s.DayOfWeek == DayOfWeek.Monday && cl.Mon) ||
            //                         (s.DayOfWeek == DayOfWeek.Tuesday && cl.Tue) ||
            //                         (s.DayOfWeek == DayOfWeek.Wednesday && cl.Wed) ||
            //                         (s.DayOfWeek == DayOfWeek.Thursday && cl.Thr) ||
            //                         (s.DayOfWeek == DayOfWeek.Friday && cl.Fri) ||
            //                         (s.DayOfWeek == DayOfWeek.Saturday && cl.Sat))) &&
            //             (_checkInTime >= cl.OpenFrom && _checkOutTime <= cl.OpenTo))
            //                                    .Select(s => s.HotelId));
            var hotelQuery = EF.CompileQuery((ApplicationDbContext db, int _noOfChildren, int[] _rating,
                //  decimal[] _taxP,
                int[] _propertyTypes, int[] _facilities) =>
                                         db.Hotels.Include(s => s.Facilities)
                                          .Where(h =>
                                                    ((_noOfChildren > 0 && h.IsChildrenAllowed == true) || (_noOfChildren == 0)) &&
                                                    (_rating.Length > 0 ? _rating.Contains(h.Rating) : true) &&
                                                    //  ( _taxP.Length > 0 ? _taxP.Contains(h.TaxP) : true) &&
                                                    (_propertyTypes.Length > 0 ? _propertyTypes.Contains(h.PropertyTypeId) : true) &&
                                                    (_facilities.Length > 0 ? h.Facilities.Any(x => _facilities.Contains(x.FacilityId)) : true))
                                                    .Select(h => new
                                                    {
                                                        h.Id,
                                                        Facilities = h.Facilities.Select(x => x.FacilityId),
                                                        h.Address,
                                                        h.IsCreditCardAccepted,
                                                        h.IsPrePayment,
                                                        h.Rating,
                                                        //       h.TaxP,
                                                    }
                                               ));
            var finalHotelsQuery = (from s in servicesQuery(_dbContext, city)
                                    join h in hotelQuery(_dbContext, noOfChildren, rating,
                                    //      taxP, 
                                    propertyTypes, facilities)
                                    on s.Id equals h.Id
                                    join cl in calanderQuery(_dbContext, allDays, checkInTime, checkOutTime)
                                    on s.Id equals cl
                                    select new
                                    {
                                        s.Id,
                                        s.Name,
                                        s.ProviderId,
                                        s.Status,
                                        h.Facilities,
                                        h.Address,
                                        h.IsCreditCardAccepted,
                                        h.IsPrePayment,
                                        h.Rating,
                                        //     h.TaxP,
                                        s.ReviewScore,
                                        s.TotalReviews,


                                    })
                                    .Skip((page - 1) * pageSize).Take(pageSize);


            var roomQuery = (from rm in topRoomsQuery(_dbContext, bookedBefore, stay, checkInTime, checkOutTime)
                             join h in finalHotelsQuery
                             on rm.HotelId equals h.Id
                             select new
                             {
                                 h.Id,
                                 h.Name,
                                 h.ProviderId,
                                 rm.RoomId,
                                 rm.RatePerNight,
                                 rm.TotalRatePerNight,
                                 rm.Promotion,
                                 rm.IsDeleted, //mohsin9/3
                                 h.Facilities,
                                 h.Address,
                                 h.IsCreditCardAccepted,
                                 h.IsPrePayment,
                                 h.Rating,
                                 //h.TaxP,
                                 h.ReviewScore,
                                 h.TotalReviews,                                
                                  netPerNight = rm.Promotion?.Discount > 0 ? rm.TotalRatePerNight - (rm.TotalRatePerNight * (rm.Promotion.Discount / 100)) : rm.TotalRatePerNight
                                 //Mohsin Revoked MARCH FWTno 7 
                          // netPerNight = rm.Promotion?.Discount > 0 ? rm.RatePerNight * (rm.Promotion.Discount) : rm.RatePerNight
                                 //          Photo = ph.ImageId

                             }).ToList();


            var selectTopRoom = (from r in roomQuery
                                 group r by r.Id into grp
                                 select grp.OrderBy(r => r.netPerNight).FirstOrDefault()).OrderBy(s => s.netPerNight).ToList();

            //shoaib // 9 March Merge

            //List<HotelSearchResult> hotels2 = (from h in selectTopRoom
            //                                   from cm in _dbContext.CalendarRateManual
            //                                   where cm.RoomId == h.RoomId && cm.Date.Date == checkInTime.Date
            //                                   select new HotelSearchResult
            //                                   {
            //                                       SearchBasicInfo = new SearchBasicInfo()
            //                                       {
            //                                           ServiceId = h.Id,
            //                                           Name = h.Name,
            //                                           RoomId = h.RoomId,
            //                                           RatePerNight = (cm.RatePerNight > 0 ? cm.RatePerNight : h.RatePerNight),
            //                                           TotalRatePerNight = (cm.TotalRate > 0 ? cm.TotalRate : h.TotalRatePerNight),
            //                                           Promotion = h.Promotion,
            //                                           Facilities = h.Facilities,
            //                                           Address = h.Address,
            //                                           IsCreditCardAccepted = h.IsCreditCardAccepted,
            //                                           IsPrePayment = h.IsPrePayment,
            //                                           Rating = h.Rating,
            //                                           //TaxP = h.TaxP,
            //                                           ReviewScore = h.ReviewScore,
            //                                           TotalReviews = h.TotalReviews,
            //                                           //Photo = ph.ImageId
            //                                       }
            //                                   }).ToList();

            //List<HotelSearchResult> hotels1 = (from h in selectTopRoom

            //                                   select new HotelSearchResult
            //                                   {
            //                                       SearchBasicInfo = new SearchBasicInfo()
            //                                       {
            //                                           ServiceId = h.Id,
            //                                           Name = h.Name,
            //                                           RoomId = h.RoomId,
            //                                           RatePerNight = h.RatePerNight,
            //                                           TotalRatePerNight = h.TotalRatePerNight,
            //                                           Promotion = h.Promotion,
            //                                           Facilities = h.Facilities,
            //                                           Address = h.Address,
            //                                           IsCreditCardAccepted = h.IsCreditCardAccepted,
            //                                           IsPrePayment = h.IsPrePayment,
            //                                           Rating = h.Rating,
            //                                           //TaxP = h.TaxP,
            //                                           ReviewScore = h.ReviewScore,
            //                                           TotalReviews = h.TotalReviews,
            //                                           //Photo = ph.ImageId
            //                                       }
            //                                   }).ToList();
            ////shoaib
            ////  List<HotelSearchResult> hotels12 = hotels1.Where(p => !hotels2.Any(e => e.SearchBasicInfo.ServiceId == p.SearchBasicInfo.ServiceId)).ToList();
            //foreach (var ls1 in hotels1.ToList())
            //{
            //    foreach (var ls2 in hotels2.ToList())
            //    {
            //        if (ls1.SearchBasicInfo.ServiceId == ls2.SearchBasicInfo.ServiceId)
            //        {
            //            hotels1.RemoveAll(x => x.SearchBasicInfo.ServiceId == ls2.SearchBasicInfo.ServiceId);
            //        }
            //    }
            //}



            //List<HotelSearchResult> hotels = (hotels1.Union(hotels2).Distinct()).ToList();

            ////shoaib  9 March Merge

          //  9 March Merge
            List<HotelSearchResult> hotels = (from h in selectTopRoom
                                              select new HotelSearchResult
                                              {
                                                  SearchBasicInfo = new SearchBasicInfo()
                                                  {
                                                      ServiceId = h.Id,
                                                      Name = h.Name,
                                                      ProviderId = h.ProviderId,
                                                      RoomId = h.RoomId,
                                                      RatePerNight = h.RatePerNight,
                                                      TotalRatePerNight = h.TotalRatePerNight,
                                                      Promotion = h.Promotion,
                                                      Facilities = h.Facilities,
                                                      Address = h.Address,
                                                      IsCreditCardAccepted = h.IsCreditCardAccepted,
                                                      IsPrePayment = h.IsPrePayment,
                                                      Rating = h.Rating,
                                                      //TaxP = h.TaxP,
                                                      ReviewScore = h.ReviewScore,
                                                      TotalReviews = h.TotalReviews,
                                                      IsDeleted = h.IsDeleted //mohsin9/3
                                                      //Photo = ph.ImageId
                                                  }
                                              }).ToList();

            var photoQuery = EF.CompileAsyncQuery((ApplicationDbContext db, long _serviceId) =>
                                         db.HotelPhotos.AsNoTracking()
                                         .Where(s => s.IsMain && s.HotelId == _serviceId)
                                         .Select(s => s.FileName).FirstOrDefault());
            var lastReservedRoomsQuery = EF.CompileAsyncQuery((ApplicationDbContext db, long _serviceId) =>
                                                      db.Booking.Include(s => s.Transaction).Include(s => s.Room)
                                                      .Where(b => b.Transaction.Date.Date >= DateTime.Now.AddDays(-1).Date && b.Transaction.ServiceId == _serviceId)
                                                      .Sum(b => b.Rooms));
            var bookedRoomsQuery = EF.CompileAsyncQuery((ApplicationDbContext db, long _roomId, DateTime _CheckInDate, DateTime _CheckOutDate) =>
                                          (from d in db.DimCalendars
                                           where d.Date >= _CheckInDate && d.Date <= _CheckOutDate.Date
                                           select new
                                           {
                                               Rooms = (from c in db.Booking
                                                        join
                          t in db.Transactions on c.Id equals t.Id
                                                        //      where c.RoomId == _roomId && t.Status == TransactionStatus.Confirmed &&
                                                        //c.CheckInDate.Date >= d.Date.Date && c.CheckOutDate.Date <= d.Date.Date
                                                     
                                                        where c.RoomId == _roomId && t.Status == TransactionStatus.Confirmed && (c.CheckInDate.Date <= _CheckInDate.Date) && (c.CheckOutDate.Date >= _CheckOutDate.Date || c.CheckOutDate.Date >= _CheckInDate.Date)

                                                        select c.Rooms
).Sum()
                                           }).Max(s => s.Rooms)
                                        );
           

            var roomsQuery = EF.CompileAsyncQuery((ApplicationDbContext db, long _roomId) =>
                                            (from r in db.Rooms
                                             where r.Id == _roomId
                                             select r.NoOfRooms).FirstOrDefault());

            foreach (var result in hotels)
            {
                result.SearchBasicInfo.Photo = await photoQuery(_dbContext, result.SearchBasicInfo.ServiceId);
                
                result.HotelSearchParameters = new HotelSearchParameters { LastReservedRooms = await lastReservedRoomsQuery(_dbContext, result.SearchBasicInfo.ServiceId) };
              
                result.RoomSearchParameters = new RoomSearchParameters { RemainingRooms = await roomsQuery(_dbContext, result.SearchBasicInfo.RoomId) - await bookedRoomsQuery(_dbContext, result.SearchBasicInfo.RoomId, checkInTime, checkOutTime) };
            }

            return hotels;

        }

        private async Task<string> GetHotelImage(long serviceId)
        {
            return await _dbContext.HotelPhotos.AsNoTracking()
                                         .Where(s => s.IsMain && s.HotelId == serviceId)
                                         .Select(s => s.FileName).FirstOrDefaultAsync();
        }


        public async Task<ICollection<AvailableRoom>> AvailableRooms(long hotelId, DateTime checkIn, DateTime checkOut, int noOfRooms, int noOfPersons, int noOfChildren)
        {
            var validCalenderQuery = EF.CompileAsyncQuery((ApplicationDbContext db, long _hotelId, DateTime _checkIn, DateTime _checkOut) =>
                                                        (from d in db.DimCalendars
                                                         where d.Date >= _checkIn && d.Date <= _checkOut.Date
                                                         select new
                                                         {
                                                             hasCanlender = db.Calendar.Where(s => s.HotelId == _hotelId
                                                                 && d.Date >= s.OpenFrom.Date && d.Date <= s.OpenTo.Date
                                                                 && !(d.Date >= s.CloseFrom.Date && d.Date <= s.CloseTo.Date))
.Any()
                                                         }).Any()
                                                       );
            // var days = GetAllDays(checkIn, checkOut);
            var validCalender = await validCalenderQuery(_dbContext, hotelId, checkIn, checkOut);
            if (validCalender)
            {
                var bookedRooms = await (from d in _dbContext.DimCalendars
                                         where d.Date >= checkIn.Date && d.Date <= checkOut.Date
                                         select new
                                         {
                                             Rooms = (from c in _dbContext.Booking
                                                      join t in _dbContext.Transactions on c.Id equals t.Id
                                                  
                                                      where t.ServiceId == hotelId && t.Status == TransactionStatus.Confirmed
                                                       &&
                                                      //((c.CheckInDate.Date >= d.Date.Date && d.Date.Date <= c.CheckOutDate.Date && checkOut <= c.CheckOutDate.Date && checkOut >= c.CheckInDate.Date))
                                                      // Mohsin Revoked MARCH FWTno 1

                                                      (
                                                          (checkIn.Date >= c.CheckInDate.Date && checkIn.Date <= c.CheckOutDate.Date) || (checkOut.Date >= c.CheckInDate.Date && checkOut.Date <= c.CheckOutDate.Date)
                                                      || (checkIn.Date <= c.CheckInDate.Date && checkOut.Date >= c.CheckOutDate.Date)
                                                      || (checkIn.Date <= c.CheckInDate.Date && checkOut.Date >= c.CheckInDate.Date)

                                                      )
                                                      // Mohsin Revoked MARCH FWTno 1

                                                      group c by c.RoomId into grpRoom
                                                      select new
                                                      {
                                                          id = grpRoom.Key,
                                                          total = grpRoom.Sum(r => r.Rooms)
                                                      })
                                         })
                                         .SelectMany(s => s.Rooms)
                                         .GroupBy(s => s.id)
                                        .Select(s => new { id = s.Key, booked = s.Max(r => r.total) })
                                        .ToListAsync();

                var customRoom = await (from d in _dbContext.DimCalendars
                                        from cl in _dbContext.CalendarManual
                                        where d.Date >= checkIn.Date && d.Date < checkOut.Date
                                              && cl.HotelId == hotelId && cl.Status.Value && cl.Date.Date == d.Date.Date
                                         //     orderby cl.Rooms
                                        orderby cl.Date
                                        select new { cl.RoomId, cl.Rooms, cl.RatePerNight, cl.Date.Date })                                        
                                          //.GroupBy(s => s.RoomId).Select(s => new
                                          //{
                                          //    RoomId = s.Key,
                                          //    Rooms = s.Min(r => r.Rooms),
                                          //    RatePerNight = s.Min(r => r.RatePerNight)
                                          //})

                                          .ToListAsync();
             

                var rooms = await (_dbContext.Rooms.Where(r => r.HotelId == hotelId && (r.NoOfPersons * noOfRooms) >= (noOfPersons + noOfChildren))
                                .AsNoTracking()
                                .Select(s => new { s.Id, s.NoOfRooms, s.NoOfPersons, s.RatePerNight }))
                                .ToListAsync();
                var closedRoom = await (from d in _dbContext.DimCalendars
                                        from cl in _dbContext.CalendarManual
                                        where d.Date >= checkIn.Date && d.Date <= checkOut.Date
                                              && cl.HotelId == hotelId
                                             && !cl.Status.Value
                                              && cl.Date.Date == d.Date.Date
                                        select cl.RoomId).Distinct()
                                  .ToListAsync();
              
             
                var availableRoom = from r in rooms
                                    join br in customRoom on
                                    r.Id equals br.RoomId into joinRoom
                                    from jr in joinRoom.DefaultIfEmpty()
                                    where !closedRoom.Contains(r.Id)
                                    select new { r.Id, noOfRooms = ((jr != null) ? jr.Rooms : r.NoOfRooms)
                                   ,ratePerNight = ((jr != null) ? jr.RatePerNight : r.RatePerNight)
                                   ,date =( (jr != null) ? jr.Date : DateTime.Today.AddDays(365243))
                                   
                                 // , r.RatePerNight
                                    };

                var query = from r in availableRoom
                            join br in bookedRooms on
                            r.Id equals br.id into joinRoom
                            from br in joinRoom.DefaultIfEmpty()
                            select new AvailableRoom
                            {
                                Id = r.Id,
                                Rooms = r.noOfRooms.GetValueOrDefault(0) - ((br != null) ? br.booked : 0),
                                RatePerNight = r.ratePerNight,
                                Date = r.date                               
                            };
                return query.
                    ToList();
            }
            return new List<AvailableRoom>();
        }

        private static List<DateTime> GetAllDays(DateTime checkIn, DateTime checkOut)
        {
            var days = new List<DateTime>();
            do
            {
                days.Add(checkIn.Date);
                checkIn = checkIn.AddDays(1);
            } while (checkIn <= checkOut);
            return days;
        }

        private static List<DateTime> GetAllDays(DateTime fromDate, DateTime toDate,
            Boolean Sun, Boolean Mon, Boolean Tue,
            Boolean Wed, Boolean Thr, Boolean Fri, Boolean Sat)
        {
            var days = new List<DateTime>();
            do
            {
                if (fromDate.DayOfWeek == DayOfWeek.Sunday ? Sun : false ||
                    fromDate.DayOfWeek == DayOfWeek.Monday ? Mon : false ||
                    fromDate.DayOfWeek == DayOfWeek.Tuesday ? Tue : false ||
                    fromDate.DayOfWeek == DayOfWeek.Wednesday ? Wed : false ||
                    fromDate.DayOfWeek == DayOfWeek.Thursday ? Thr : false ||
                    fromDate.DayOfWeek == DayOfWeek.Friday ? Fri : false ||
                    fromDate.DayOfWeek == DayOfWeek.Saturday ? Sat : false
                    )
                    days.Add(fromDate.Date);
                fromDate = fromDate.AddDays(1);
            } while (fromDate <= toDate);
            return days;

        }
        //Mohsin Today booking 
        public async Task<ICollection<HotelBooking>> GetTodayBookings(long ServiceId)
        {
            var query = (from tr in _dbContext.Transactions
                         .Include(c => c.Client)
                         .Include(c => c.Service)
                         join b in _dbContext.Booking on
                         tr.Id equals b.TransactionId
                         join ro in _dbContext.Rooms
                         on b.RoomId equals ro.Id
                         join c in _dbContext.RoomTypeNames
                         on ro.RoomTypeNameId equals c.Id
                         where (tr.ServiceId == ServiceId
                         && tr.Status == TransactionStatus.Confirmed
                         && b.CheckOutDate >= DateTime.Now
                         && b.Transaction.Date == DateTime.Now)
                         select new HotelBooking()
                         {
                             Id = b.Id,
                             CheckInDate = b.CheckInDate,
                             CheckOutDate = b.CheckOutDate,
                             Date = b.Transaction.Date,
                             FirstName = b.Transaction.FirstName,
                             LastName = b.Transaction.LastName,
                             Status = tr.Status,
                             TransactionId = b.TransactionId,
                             Amount = b.Transaction.ProviderAmount,
                             Currency = b.Transaction.ProviderCurrency,
                             RoomName = c.Description,
                             IsInCheckInDate = b.CheckInDate > DateTime.Now,
                             IsInCheckOutDate = DateTime.Now >= b.CheckInDate && DateTime.Now <= b.CheckOutDate
                         });
            return await query.ToListAsync();
        }
        //mohsin today booking END
        public async Task<ICollection<HotelBooking>> GetActiveBookings(long ServiceId)
        {
            var query = (from tr in _dbContext.Transactions
                         .Include(c => c.Client)
                         .Include(c => c.Service)
                         join b in _dbContext.Booking on
                         tr.Id equals b.TransactionId
                         join ro in _dbContext.Rooms
                         on b.RoomId equals ro.Id
                         join c in _dbContext.RoomTypeNames
                         on ro.RoomTypeNameId equals c.Id
                         where (tr.ServiceId == ServiceId
                         && tr.Status == TransactionStatus.Confirmed
                         && b.CheckOutDate >= DateTime.Now)
                         select new HotelBooking()
                         {
                             Id = b.Id,
                             CheckInDate = b.CheckInDate,
                             CheckOutDate = b.CheckOutDate,
                             Date = b.Transaction.Date,
                             FirstName = b.Transaction.FirstName,
                             LastName = b.Transaction.LastName,
                             Status = tr.Status,
                             TransactionId = b.TransactionId,
                             Amount = b.Transaction.ProviderAmount,
                             Currency = b.Transaction.ProviderCurrency,
                             RoomName = c.Description,
                             IsInCheckInDate = b.CheckInDate > DateTime.Now,
                             IsInCheckOutDate = DateTime.Now >= b.CheckInDate && DateTime.Now <= b.CheckOutDate
                         });
            return await query.ToListAsync();
        }
        public async Task<ICollection<HotelBooking>> SearchBooking(long ServiceId, long userId, DateTime fromDate, DateTime toDate, TransactionStatus status)
        {
            var _status = Enum.Parse<TransactionStatus>(status.ToString());
            var query = (from tr in _dbContext.Transactions
                         join b in _dbContext.Booking on
                         tr.Id equals b.TransactionId
                         join ro in _dbContext.Rooms
                         on b.RoomId equals ro.Id
                         join c in _dbContext.RoomTypeNames
                         on ro.RoomTypeNameId equals c.Id
                         join s in _dbContext.Services
                         on tr.ServiceId equals s.Id
                         where (
                         s.ProviderId == userId
                         && tr.ServiceId == ServiceId
                         && ((status != 0) ? tr.Status == _status : tr.Status > _status)
                         && tr.Date.Date >= fromDate.Date
                         && tr.Date.Date <= toDate.Date)
                         select new HotelBooking()
                         {
                             Id = b.Id,
                             CheckInDate = b.CheckInDate,
                             CheckOutDate = b.CheckOutDate,
                             Date = b.Transaction.Date,
                             FirstName = b.Transaction.FirstName,
                             LastName = b.Transaction.LastName,
                             Status = tr.Status,
                             TransactionId = b.TransactionId,
                             Amount = b.Transaction.ProviderAmount,
                             Currency = b.Transaction.ProviderCurrency,
                             RoomName = c.Description,
                             IsInCheckInDate = b.CheckInDate > DateTime.Now,
                             IsInCheckOutDate = DateTime.Now.Date >= b.CheckInDate.Date && DateTime.Now.Date <= b.CheckOutDate.Date
                         });
            return await query.ToListAsync();
        }

        public async Task<ICollection<HotelFeaturesDescription>> GetHotelFeaturesDescriptions()
        {
            return await _dbContext.HotelFeaturesDescriptions.ToListAsync();
        }

        public async Task<ICollection<HotelLanguage>> GetSpokenLanguages(long ServiceId)
        {
            return await _dbContext.HotelLanguages
                //.Include(d => d.Language)
                .Where(s => s.HotelId == ServiceId).ToListAsync();
        }

        public async Task<ICollection<HotelRoomSaleReporting>> GetHotelRoomSaleReporting(long hotelId, long userId, DateTime From, DateTime To, int room, ReportView view)
        {
            var query = _dbContext.Booking
                        .Include(s => s.Transaction)
                        .Include(s => s.Transaction.Service)
                        .Include(s => s.Room)
                        .Where(b => b.Transaction.Service.ProviderId == userId &&
                        b.Transaction.ServiceId == hotelId &&
                        b.Transaction.Status == TransactionStatus.Confirmed &&
                        b.CheckOutDate.Date <= DateTime.Now.Date &&
                        b.Transaction.Date.Date >= From.Date &&
                        b.Transaction.Date.Date <= To.Date)
                        .Select(b => new
                        {
                            b.Id,
                            b.Transaction.Date,
                            b.Transaction.ProviderAmount,
                            b.Room.RoomTypeNameId,
                            b.CheckOutDate,
                            b.Rooms
                        });
            if (room != 0)
                query = query.Where(s => s.RoomTypeNameId == room);



            switch (view)
            {
                case ReportView.years:
                    return await (from gr in query
                                  group gr by new
                                  {
                                      gr.RoomTypeNameId,
                                      gr.Date.Year
                                  } into groupRoom
                                  select new HotelRoomSaleReporting
                                  {
                                      Id = groupRoom.Key.RoomTypeNameId,
                                      Date = new DateTime(groupRoom.Key.Year, 1, 1),
                                      Revenue = groupRoom.Sum(s => s.ProviderAmount),
                                      AverageRate = groupRoom.Sum(s => s.ProviderAmount) / groupRoom.Sum(s => s.Rooms),
                                      PerNight = groupRoom.Sum(s => s.Rooms)
                                  }).ToListAsync();
                case ReportView.weeks:
                    return await (from gr in query
                                  group gr by new
                                  {
                                      gr.RoomTypeNameId,
                                      gr.Date.Year,
                                      week = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(gr.Date, CalendarWeekRule.FirstDay, DayOfWeek.Monday)
                                  } into groupRoom
                                  select new HotelRoomSaleReporting
                                  {
                                      Id = groupRoom.Key.RoomTypeNameId,
                                      Date = new DateTime(groupRoom.Key.Year, 1, 1).AddDays((groupRoom.Key.week - 1) * 7),
                                      Revenue = groupRoom.Sum(s => s.ProviderAmount),
                                      AverageRate = groupRoom.Sum(s => s.ProviderAmount) / groupRoom.Sum(s => s.Rooms),
                                      PerNight = groupRoom.Sum(s => s.Rooms)
                                  }).ToListAsync();
                case ReportView.months:
                    return await (from gr in query
                                  group gr by new
                                  {
                                      gr.RoomTypeNameId,
                                      gr.Date.Month,
                                      gr.Date.Year
                                  } into groupRoom
                                  select new HotelRoomSaleReporting
                                  {
                                      Id = groupRoom.Key.RoomTypeNameId,
                                      Date = new DateTime(groupRoom.Key.Year, groupRoom.Key.Month, 1),
                                      Revenue = groupRoom.Sum(s => s.ProviderAmount),
                                      AverageRate = groupRoom.Sum(s => s.ProviderAmount) / groupRoom.Sum(s => s.Rooms),
                                      PerNight = groupRoom.Sum(s => s.Rooms)
                                  }).ToListAsync();
                case ReportView.days:
                default:
                    return await (from gr in query
                                  group gr by new { gr.RoomTypeNameId, gr.Date.Date } into groupRoom
                                  select new HotelRoomSaleReporting
                                  {
                                      Id = groupRoom.Key.RoomTypeNameId,
                                      Date = groupRoom.Key.Date,
                                      Revenue = groupRoom.Sum(s => s.ProviderAmount),
                                      AverageRate = groupRoom.Sum(s => s.ProviderAmount) / groupRoom.Sum(s => s.Rooms),
                                      PerNight = groupRoom.Sum(s => s.Rooms)
                                  }).ToListAsync();
            }
        }
        public async Task<HotelRoomSaleDetails> GetHotelRoomSaleDetails(long hotelId, long userId, DateTime From, DateTime To, int room, ReportView view)
        {

            var query = _dbContext.Booking
                        .Include(s => s.Transaction)
                        .Include(s => s.Transaction.Service)
                        .Include(s => s.Room)
                        .Where(b => b.Transaction.Service.ProviderId == userId &&
                        b.Transaction.ServiceId == hotelId &&
                        b.Transaction.Status == TransactionStatus.Confirmed &&
                        b.CheckOutDate.Date <= DateTime.Now.Date &&
                        b.Transaction.Date.Date >= From.Date &&
                        b.Transaction.Date.Date <= To.Date)
                        .Select(b => new
                        {
                            b.Id,
                            b.Transaction.Date,
                            b.Transaction.ProviderAmount,
                            b.Room.RoomTypeNameId,
                            b.CheckOutDate,
                            b.Rooms
                        });
            if (room != 0)
                query = query.Where(s => s.RoomTypeNameId == room);
            var selectedQuery = await query.ToListAsync();
            switch (view)
            {
                case ReportView.years:
                    return (from gr in selectedQuery
                            group gr by new
                            {
                                gr.RoomTypeNameId,
                                gr.Date.Year
                            } into groupRoom
                            select new HotelRoomSaleDetails
                            {
                                HotelRoomSaleReporting = new HotelRoomSaleReporting
                                {
                                    Id = groupRoom.Key.RoomTypeNameId,
                                    Date = new DateTime(groupRoom.Key.Year, 1, 1),
                                    Revenue = groupRoom.Sum(s => s.ProviderAmount),
                                    AverageRate = groupRoom.Sum(s => s.ProviderAmount) / groupRoom.Sum(s => s.Rooms),
                                    PerNight = groupRoom.Sum(s => s.Rooms)
                                },
                                HotelRoomSales = groupRoom.Select(g => new HotelRoomSale
                                {
                                    Amount = g.ProviderAmount,
                                    BookingId = g.Id,
                                    Date = g.Date,
                                    Rooms = g.Rooms
                                }).ToList()
                            }).FirstOrDefault();
                case ReportView.months:
                    return (from gr in selectedQuery
                            group gr by new
                            {
                                gr.RoomTypeNameId,
                                gr.Date.Month,
                                gr.Date.Year
                            } into groupRoom
                            select new HotelRoomSaleDetails
                            {
                                HotelRoomSaleReporting = new HotelRoomSaleReporting
                                {
                                    Id = groupRoom.Key.RoomTypeNameId,
                                    Date = new DateTime(groupRoom.Key.Year, groupRoom.Key.Month, 1),
                                    Revenue = groupRoom.Sum(s => s.ProviderAmount),
                                    AverageRate = groupRoom.Sum(s => s.ProviderAmount) / groupRoom.Sum(s => s.Rooms),
                                    PerNight = groupRoom.Sum(s => s.Rooms)
                                },
                                HotelRoomSales = groupRoom.Select(g => new HotelRoomSale
                                {
                                    Amount = g.ProviderAmount,
                                    BookingId = g.Id,
                                    Date = g.Date,
                                    Rooms = g.Rooms
                                }).ToList()
                            }).FirstOrDefault();
                case ReportView.weeks:
                    return (from gr in selectedQuery
                            group gr by new
                            {
                                gr.RoomTypeNameId,
                                gr.Date.Year,
                                week = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(gr.Date, CalendarWeekRule.FirstDay, DayOfWeek.Monday)
                            } into groupRoom
                            select new HotelRoomSaleDetails
                            {
                                HotelRoomSaleReporting = new HotelRoomSaleReporting
                                {
                                    Id = groupRoom.Key.RoomTypeNameId,
                                    Date = new DateTime(groupRoom.Key.Year, 1, 1).AddDays((groupRoom.Key.week - 1) * 7),
                                    Revenue = groupRoom.Sum(s => s.ProviderAmount),
                                    AverageRate = groupRoom.Sum(s => s.ProviderAmount) / groupRoom.Sum(s => s.Rooms),
                                    PerNight = groupRoom.Sum(s => s.Rooms)
                                },
                                HotelRoomSales = groupRoom.Select(g => new HotelRoomSale
                                {
                                    Amount = g.ProviderAmount,
                                    BookingId = g.Id,
                                    Date = g.Date,
                                    Rooms = g.Rooms
                                }).ToList()
                            }).FirstOrDefault();
                case ReportView.days:
                default:
                    return (from gr in selectedQuery
                            group gr by new { gr.RoomTypeNameId, gr.Date.Date } into groupRoom
                            select new HotelRoomSaleDetails
                            {
                                HotelRoomSaleReporting = new HotelRoomSaleReporting
                                {
                                    Id = groupRoom.Key.RoomTypeNameId,
                                    Date = groupRoom.Key.Date,
                                    Revenue = groupRoom.Sum(s => s.ProviderAmount),
                                    AverageRate = groupRoom.Sum(s => s.ProviderAmount) / groupRoom.Sum(s => s.Rooms),
                                    PerNight = groupRoom.Sum(s => s.Rooms)
                                },
                                HotelRoomSales = groupRoom.Select(g => new HotelRoomSale
                                {
                                    Amount = g.ProviderAmount,
                                    BookingId = g.Id,
                                    Date = g.Date,
                                    Rooms = g.Rooms
                                }).ToList()
                            }).FirstOrDefault();

            }
        }

        public async Task<Hotel> GetHotelWithPhotos(long id)
        {
            return await _dbContext.Hotels.Include(s => s.Photos).Where(s => s.Id == id).SingleOrDefaultAsync();
        }

        public async Task<RtcQuery> GetRtcpayById()
        {
            var query =
                (from p in _dbContext.Rtcpay
                 orderby p.Id descending
                 select new RtcQuery
                 {
                     RefNum = p.RefNum,
                     BillNumber = p.BillNumber
                 });
            return await query.FirstOrDefaultAsync();
        }
    }
}
