﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Data.Contexts;
using Zowarah.Data.Repositories;
using Zowarah.Domain.Entities.Common;

namespace Zowarah.Application.Services.Common
{
    public class ServiceNearbyCategoryQuery
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int TypeId { get; set; }
        public string TypeName { get; set; }
    }
    public class ServiceNearbyQuery
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Distance { get; set; }
        public string Unit { get; set; }
        public long ServiceId { get; set; }
        public int ServiceNearbyCategoryId { get; set; }
    }
    public class ServiceNearbyService : Repository<ServiceNearby>, IServiceNearbyService
    {
        public ServiceNearbyService(ApplicationDbContext db) : base(db)
        {

        }

        public async Task<ICollection<ServiceNearbyCategoryQuery>> GetServiceNearbyTypes()
        {
            return await _dbContext.ServiceNearbyCategories.Include(s => s.ServiceNearbyType)
                .Select(s => new ServiceNearbyCategoryQuery
                {
                    Id = s.Id,
                    Description = s.Description,
                    TypeId = s.ServiceNearbyTypeId,
                    TypeName = s.ServiceNearbyType.Description                    
                })
                .ToListAsync();
        }
        public async Task<ICollection<ServiceNearbyQuery>> GetAllByServiceId(long ServiceId)
        {
            return await _dbContext.ServiceNearbies
                .Where(d => d.ServiceId == ServiceId)
                .Select(s => new ServiceNearbyQuery
                {
                    Id = s.Id,
                    Name = s.Name,
                    Distance = s.Distance,
                    Unit = s.Unit,
                    ServiceId=s.ServiceId,
                    ServiceNearbyCategoryId = s.ServiceNearbyCategoryId,
                }).ToListAsync();
        }
        public async Task<ServiceNearby> GetById(long ServiceId)
        {
            //return await _dbSet.FindAsync(ServiceId);
            return await _dbSet.Where(d => d.Id == ServiceId).SingleOrDefaultAsync();
            
        }

        public Task<ServiceNearby> GetById(long ServiceId, bool v)
        {
            throw new NotImplementedException();
        }
    }
}
