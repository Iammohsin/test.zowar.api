﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Data.Contexts;
using Zowarah.Data.Repositories;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Reviews;

namespace Zowarah.Application.Services.Common {
    public enum ReviewOrderbyEnum {
        Most_recent = 1,
        Rating_high_to_low = 2,
        Rating_low_to_high = 3
    }
    public class ReviewQuery {
        public string Id { get; set; }
        public float Score { get; set; }
        public string Title { get; set; }
        public string Comment { get; set; }
        public string ReplyComment { get; set; }
        public DateTime ReplyDate { get; set; }
        public string ClientId { get; set; }
        public string ClientCountry { get; set; }
        public string ClientName { get; set; }
        public DateTime Date { get; set; }
        public string Tags { get; set; }
        public string ServiceName { get; set; }
        public int NoOfNight { get; set; }
    }

    public class ReviewService : Repository<Review>, IReviewService {
        public ReviewService(ApplicationDbContext db) : base(db) {

        }
        public new void Create(Review entity) {
            entity.Date = DateTime.Now;
            entity.IsRead = false;
            entity.IsPuplished = false;
            entity.CreatedDate = DateTime.Now.Date;
            _dbContext.AddAsync<Review>(entity);
        }

        public async Task<ICollection<ReviewFeatureType>> GetServiceFeatureTypes(ServiceTypeEnum ServiceTypeId) {
            return await _dbContext.ReviewFeatureTypes.Where(x => x.ServiceTypeId == 0 || x.ServiceTypeId == ServiceTypeId).ToListAsync();
        }
        public async Task<float> TotalReviewFeatureTypesScore() {
            return await _dbContext.ReviewFeatureTypes.CountAsync();
        }
        public async Task UpdateServiceReviewRate(long ServiceId, float Score) {
            string sql = $@"update [dbo].[Services]
                        set  [ReviewScore] = ([ReviewScore] +  @Score  ) / ( [TotalReviews] + 1)
                            ,[TotalScore] = [TotalScore] +  @Score 
                            ,[TotalReviews] = [TotalReviews] + 1
                where Id = '@ServiceId'";
            var exec = await _dbContext.Database.ExecuteSqlCommandAsync(sql, new SqlParameter("@Score", Score), new SqlParameter("@ServiceId", ServiceId));
        }
        public async Task UpdateServiceReviewFeaturesRate(long ServiceId, ICollection<ReviewFeature> ReviewFeatures) {
            foreach (var feature in ReviewFeatures) {
                var score = new SqlParameter("@Score", feature.Score);
                var serviceId = new SqlParameter("@ServiceId", ServiceId);
                var featureTypeId = new SqlParameter("@FeatureTypeId", feature.ReviewFeatureTypeId);
                await _dbContext.Database.ExecuteSqlCommandAsync(
                    $@"exec [dbo].[UpdateServiceReview]
		                        @Score,
		                        @ServiceId,
		                        @FeatureTypeId"
                                          , score, serviceId, featureTypeId);


                //$@"UPDATE [dbo].[HotelReviewFeaturesRates]
                //           SET [Score] = ([TotalScore] + {feature.Rate} )/([TotalReviewer] +1)
                //              ,[TotalScore] = [TotalScore] + {feature.Rate}
                //              ,[TotalReviewer] = [TotalReviewer] + 1
                //      WHERE [ReviewFeatureTypeId]='{feature.ReviewFeatureTypeId}' and [HotelId] ='{ ServiceId }'"
            }
        }
        public async Task<ICollection<ReviewQuery>> GetByServiceId(long ServiceId, int Page, int Count, ReviewOrderbyEnum OrderBy) {
            var query = _dbContext.Reviews
                .Include(s => s.Client)
                .Where(d => d.ServiceId == ServiceId)
                .Skip((Page - 1) * Count)
                .Take(Count);
            switch (OrderBy) {
                case ReviewOrderbyEnum.Rating_high_to_low:
                    query = query.OrderByDescending(d => d.Score);
                    break;
                case ReviewOrderbyEnum.Rating_low_to_high:
                    query = query.OrderBy(d => d.Score);
                    break;
                case ReviewOrderbyEnum.Most_recent:
                default:
                    query = query.OrderByDescending(d => d.Date);
                    break;
            }
            var selectQuery = from d in query
                              join b in _dbContext.Booking on
                              d.TransactionId equals b.Id
                              select new ReviewQuery {
                                  Id = d.Id,
                                  Score = d.Score,
                                  Title = d.Title,
                                  Comment = d.Comment,
                                  ReplyComment = d.ReplyComment,
                                  ReplyDate = d.ReplyDate,
                                  Date = d.Date,
                                  Tags = d.Tags,
                                  ClientName = d.Client.FirstName,
                                  ClientCountry = d.Client.CountryId + "",
                                  NoOfNight = b.CheckOutDate.Subtract(b.CheckInDate).Days
                              };

            return await selectQuery.ToListAsync();
        }
        public async Task<ICollection<ServiceReviewFeaturesScore>> GetServiceFeaturesScore(long ServiceId) {
            return await _dbContext.ServiceReviewFeaturesScores.Where(d => d.ServiceId == ServiceId).ToListAsync();
        }

        public async Task<ICollection<ServiceReviewAveragesScore>> GetServiceAveragesScore(long ServiceId) {
            return await _dbContext.Reviews.Where(d => d.ServiceId == ServiceId).GroupBy(
                x => Convert.ToInt16(Math.Round(x.Score, 0)),
                (key, g) => new ServiceReviewAveragesScore {
                    Average = key,
                    TotalReviews = g.Count()
                }
            ).OrderBy(x => x.Average).ToListAsync();
        }
        public async Task<string> GetReviewTags(string TransactionId) {
            var trans = await _dbContext.Booking.Include(s => s.Room.RoomTypeName).Where(d => d.Id == TransactionId).FirstOrDefaultAsync();
            string tag = "";

            if (trans.Rooms == 1) {
                switch (trans.Room.RoomTypeName.Description) {
                    case "Budget Single Room":
                    case "Economy Single Room":
                    case "Delux Single Room": tag = "Solo traveler"; break;
                    case "Budget Double Room":
                    case "Delux Double Room":
                    case "Economy Double Room": tag = "Couple"; break;
                    case "Budget Twin Room":
                    case "Delux Twin Room":
                    case "Economy Twin Room": tag = "Family"; break;
                }
            } else {
                tag = "Group";
            }
            return tag;
        }
        public async Task<bool> IsExist(long ClientId, string TransactionId) {
            return await _dbContext.Reviews.AnyAsync(r => r.ClientId == ClientId && r.TransactionId == TransactionId);
        }
        public async Task<decimal> GetTodayReview(long serviceId) {
            return await _dbContext.Reviews.Where(s => s.ServiceId == serviceId && s.CreatedDate.Date == DateTime.Now.Date).CountAsync();
        }
    }
}
