﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Data.Contexts;
using Zowarah.Data.Repositories;
using Zowarah.Domain.Entities.Common;

namespace Zowarah.Application.Services.Common
{
    public static class AppSettings
    {
        public static MailSettings MailSettings { get; set; }
        public static SMSSettings SMSSettings { get; set; }

    }
    public class MailSettings
    {
        public static string MailApi => "Gate123456#";
        public static string EmailAddress => "info.zowar@nooridev.in";
        public static string DisplayName => "zowar.com.sa";
        public static string MainTemplateId => "mail";
        public static string UserName => "info.zowar@nooridev.in";
        public static string Password => "RNm{_b[R_t42";
        public static string Host => "mail.nooridev.in";
        public static int Port => 587;
        public static Boolean EnableSSL => false;

    }

    public class SMSSettings
    {
        public static string ApiLink => "https://platform.clickatell.com/messages";
        public static string ApiKey => "Iv-6vwdnQeW0V7gvcyHuOQ==";
    }
    public class NotificationService : INotificationService
    {


        private readonly IHostingEnvironment _hostingEnvironment;

        public NotificationService(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public String GetTemplate(string templateId)
        {
            var template = File.ReadAllTextAsync($"{this._hostingEnvironment.WebRootPath }/Template/{ templateId }.html");
            return template.Result;
        }

        public async Task SendBySmtp(string To, string Subject, string DisplayName, string Content)
        {
            /// Godaddy smtp by office365
            /// we use email info@zowarah.com
            try
            {
                var from = new MailAddress(MailSettings.EmailAddress, MailSettings.DisplayName);
                var subject = Subject;
                var to = new MailAddress(To, DisplayName);
                var plainTextContent = Content;
                var template = GetTemplate(MailSettings.MainTemplateId);
                var htmlContent = template.Replace("$$title$$", Subject).Replace("$$planText$$", Content).Replace("$$mail$$", To);

                MailMessage mail = new MailMessage();
                mail.To.Add(to);
                mail.From = from;
                mail.Subject = Subject;
                mail.Body = htmlContent;
                mail.IsBodyHtml = true;
                SmtpClient smtpClient = new SmtpClient();
                smtpClient.Host = MailSettings.Host;
                // set timeout to 10 minuts
                smtpClient.Timeout = 600000;
                smtpClient.EnableSsl = MailSettings.EnableSSL;
                smtpClient.Port = MailSettings.Port;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(MailSettings.UserName, MailSettings.Password);
                smtpClient.Send(mail);
            }
            catch
            {
            }
        }
    }
}
