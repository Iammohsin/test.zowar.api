﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Data.Contexts;
using Zowarah.Data.Repositories;
using Zowarah.Domain.Entities.Common;

namespace Zowarah.Application.Services.Common
{
    public class GetServiceProfile
    {
        public long Id { get; set; }
        public long ServiceId { get; set; }
        public string Language { get; set; }
        public string PropertyName { get; set; }
        public string AboutProperty { get; set; }
        public string AboutStaff { get; set; }
        public string AboutNeighborhood { get; set; }
    }

    public class ServiceProfileService : Repository<ServiceProfile>, IServiceProfileService
    {
        public ServiceProfileService(ApplicationDbContext _db) : base(_db) { }
        public async Task<ICollection<GetServiceProfile>> GetAllByServiceId(long ServiceId)
        {
            return await _dbContext.ServiceProfiles.Where(s => s.ServiceId == ServiceId)
                .Select(s => new GetServiceProfile
                {
                    Id = s.Id,
                    Language = s.Language,
                    PropertyName = s.PropertyName,
                    ServiceId = s.ServiceId,
                    AboutProperty = s.AboutProperty,
                    AboutStaff = s.AboutStaff,
                    AboutNeighborhood = s.AboutNeighborhood
                }).ToListAsync();
        }

        public async Task<GetServiceProfile> GetByServiceIdAndLanguage(long ServiceId,string Language)
        {
            return await _dbContext.ServiceProfiles.Where(s => s.ServiceId == ServiceId && s.Language==Language)
                .Select(s => new GetServiceProfile
                {
                    Id = s.Id,
                    Language = s.Language,
                    PropertyName = s.PropertyName,
                    ServiceId = s.ServiceId,
                    AboutProperty = s.AboutProperty,
                    AboutStaff = s.AboutStaff,
                    AboutNeighborhood = s.AboutNeighborhood
                }).FirstOrDefaultAsync();
        }
    }
}
