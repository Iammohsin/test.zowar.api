﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Common;
using System.Collections;
using Zowarah.Domain.Queries.Common;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.Application.Services.Common
{
    public class MessagingReportService : IMessagingReportService
    {
        private readonly ApplicationDbContext db;

        public MessagingReportService(ApplicationDbContext _db)
        {
            db = _db;
        }

        public IEnumerable<MessageQuery> GetAll()
        {
            var query = (from m in db.Messages
                         join u in db.Users on m.SenderId equals u.Id
                         join uu in db.Users on m.RecieverId equals uu.Id
                         join c in db.MessageCategories on m.MessageCategoryId equals c.Id
                         select new MessageQuery
                         {
                             SenderId = u.Id,
                             SenderName = u.FirstName,
                             RecieverName = uu.FirstName,
                             MessageCategoryId = c.Id,
                             Title = m.Title,
                             Body = m.Body,
                             Date = m.CreatedDate,
                             IsRead = m.IsRead,
                             TransactionTypeId = m.TransactionTypeId,
                             Id = m.Id
                         }).ToList();
            return query;
        }

        public IEnumerable<MessageQuery> GetMessages(RoleTypes role)
        {
            var query = (from m in db.Messages
                         join u in db.Users on m.SenderId equals u.Id
                         join uu in db.Users on m.RecieverId equals uu.Id
                         join c in db.MessageCategories on m.MessageCategoryId equals c.Id
                         where m.SenderRole == role
                         select new MessageQuery
                         {
                             SenderId = u.Id,
                             SenderName = u.FirstName,
                             RecieverName = uu.FirstName,
                             MessageCategoryId = c.Id,
                             Title = m.Title,
                             Body = m.Body,
                             Date = m.CreatedDate,
                             IsRead = m.IsRead,
                             TransactionTypeId = m.TransactionTypeId,
                             Id = m.Id
                         }).ToList();
            return query;
        }
    }
}