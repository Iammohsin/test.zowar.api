﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zowarah.Application.Config;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Data.Contexts;
using Zowarah.Data.Repositories;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Queries.Common;

namespace Zowarah.Application.Services.Common
{
    public class MessagingService : Repository<Message>, IMessagingService
    {
        public MessagingService(ApplicationDbContext dbContext
            ) : base(dbContext)
        {
        }

        public Task Close(string Term)
        {
            throw new NotImplementedException();
        }

        public async Task<string> Compose(
                      Message message
                      )
        {
            _dbContext.Messages.Add(message);
            await _dbContext.SaveChangesAsync();
            return message.Id;
        }
        public async Task<int> AdminNewMessageCount()
        {
            return await _dbContext.Messages.Where(s => !s.IsClosed && s.RecieverId == 0 && s.ReceiverRole == RoleTypes.Administrator).CountAsync();
        }

        public async Task<int> AdminActiveMessageCount(long UserId)
        {
            return await _dbContext.Messages.Where(s => !s.IsClosed && s.RecieverId == UserId && s.ReceiverRole == RoleTypes.Administrator).CountAsync();
        }

        public async Task<ICollection<MessageQuery>> GetAllByFilter(bool? isClosed, long? receiveId, RoleTypes receiverRole, bool isIncludeMessageTrails = false)
        {
            IQueryable<Message> query = _dbContext.Messages.AsQueryable();
            if (isIncludeMessageTrails)
            {
                query.Include(s => s.MessageTrails);
            }

            if (isClosed.HasValue)
            {
                query = query.Where(s => s.IsClosed == isClosed.Value);
            }

            if (receiveId.HasValue)
            {
                query = query.Where(s => s.RecieverId == receiveId.Value);
            }

            if (receiverRole > 0)
            {
                query = query.Where(s => s.ReceiverRole == receiverRole);
            }

            return await (from m in query
                          join ss in _dbContext.Services on m.SenderId equals ss.Id into ssj
                          from ssa in ssj.DefaultIfEmpty()
                          join sr in _dbContext.Services on m.RecieverId equals sr.Id into srj
                          from sra in srj.DefaultIfEmpty()
                          join us in _dbContext.Users on m.SenderId equals us.Id into usj
                          from sender in usj.DefaultIfEmpty()
                          join ur in _dbContext.Users on m.RecieverId equals ur.Id into urj
                          from reciever in urj.DefaultIfEmpty()
                          select new MessageQuery
                          {
                              Id = m.Id,
                              MessageCategoryId = m.MessageCategoryId,
                              SenderId = m.SenderId,
                              ReferenceId = m.ReferenceId,
                              TransactionTypeId = m.TransactionTypeId,
                              SenderName = sender == null ? "Zowar" : sender.FirstName,
                              RecieverName = reciever == null ? "Zowar" : reciever.FirstName,
                              Date = m.Date,
                              Title = m.Title,
                              Body = m.Body,
                              MessageTrails = m.MessageTrails.Select(s => new MessageTrailQuery
                              {
                                  Body = s.Body,
                                  Date = s.Date,
                                  SenderName = s.SenderId == m.SenderId ? sender.FirstName : reciever.FirstName
                              }).ToList()
                          }).ToListAsync();
        }


        public async Task<MessageQuery> GetMessage(string MessageId, long userId)
        {

            MessageQuery result = await (from m in _dbContext.Messages
                         .Include(x => x.MessageTrails)
                                         join ss in _dbContext.Services on m.SenderId equals ss.Id into ssj
                                         from ssa in ssj.DefaultIfEmpty()
                                         join sr in _dbContext.Services on m.RecieverId equals sr.Id into srj
                                         from sra in srj.DefaultIfEmpty()
                                         join us in _dbContext.Users on m.SenderId equals us.Id into usj
                                         from usa in usj.DefaultIfEmpty()
                                         join ur in _dbContext.Users on m.RecieverId equals ur.Id into urj
                                         from ura in urj.DefaultIfEmpty()
                                         where m.Id == MessageId
                                         select new MessageQuery
                                         {
                                             Id = m.Id,
                                             MessageCategoryId = m.MessageCategoryId,
                                             SenderId = m.SenderId,
                                             SenderName = m.SenderId == userId ? "You" : m.SenderRole == RoleTypes.Administrator ? "Zowar" : m.SenderRole == RoleTypes.Provider ? ssa.Name : usa.DisplayName,
                                             RecieverName = m.RecieverId == userId ? "You" : m.ReceiverRole == RoleTypes.Administrator ? "Zowar" : m.ReceiverRole == RoleTypes.Provider ? sra.Name : ura.DisplayName,
                                             Date = m.Date,
                                             Title = m.Title,
                                             Body = m.Body,
                                             MessageTrails = (from mt in m.MessageTrails
                                                              join u in _dbContext.Users
                                                              on mt.SenderId equals u.Id
                                                              select new MessageTrailQuery
                                                              {
                                                                  SenderName = u.FirstName,
                                                                  Body = mt.Body,
                                                                  Date = mt.Date
                                                              }).ToList()
                                         }).FirstOrDefaultAsync();

            return await Task.FromResult(result);

        }

        public async Task<string> Reply(MessageTrail messageTrail)
        {
            Message message = await _dbContext.Messages.Where(x => x.Id == messageTrail.MessageId).FirstOrDefaultAsync();
            message.IsRead = false;
            _dbContext.MessageTrails.Add(messageTrail);
            await _dbContext.SaveChangesAsync();
            return messageTrail.Id;
        }

        public async Task<Boolean> CloseMessage(string messageId)
        {
            Message message = await _dbSet.FindAsync(messageId);
            if (message == null)
            {
                return false;
            }
            else
            {
                message.IsClosed = true;
                message.CloseDate = DateTime.Now.Date;
                await _dbContext.SaveChangesAsync();
                return true;
            }
        }


        public async Task<Boolean> StartProcessing(string messageId, long userId)
        {
            Message message = await _dbSet.FindAsync(messageId);
            if (message == null)
            {
                return false;
            }
            else
            {
                if (message.RecieverId != 0)
                {
                    throw new CustomException("Already Processed By Another Person  !!");
                }

                message.RecieverId = userId;
                await _dbContext.SaveChangesAsync();
                return true;
            }
        }

        [HttpGet]
        public async Task<int> GetNoPendingMessages(long userId, RoleTypes roleType, long serviceId)
        {
            IList<MessageQuery> result = await GetMessages(true, DateTime.Now, DateTime.Now, roleType, userId, serviceId, true, true, null, true);
            int cnt = result.Where(x => x.IsRead == false).Count();
            return cnt;
        }

        public async Task<Boolean> DeleteMessage(long userId, string messageId)
        {
            Message message = await _dbContext.Messages.Where(x => x.Id == messageId && (x.SenderId == userId || x.RecieverId == userId)).FirstOrDefaultAsync();
            if (message == null)
            {
                return false;
            }

            if (message.SenderId == userId)
            {
                message.IsDeletedBySender = true;
            }
            else if (message.RecieverId == userId)
            {
                message.IsDeletedByReciever = true;
            }

            _dbContext.Messages.Update(message);
            int result = await _dbContext.SaveChangesAsync();
            if (result > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<IList<MessageQuery>> GetMessages(Boolean IsAll,
            DateTime? fromDate, DateTime? toDate, RoleTypes roleType, long? userId, long? serviceId,
            bool isSend, bool isRecieve,
            bool? isClosed, bool isIncludeMessageTrails = false)
        {

            DbSet<Message> query = _dbContext.Messages;
            if (isIncludeMessageTrails)
            {
                query.Include(s => s.MessageTrails);
            }

            Boolean isClosedValue = isClosed.HasValue ? isClosed.Value : true;
            List<MessageQuery> messages = await (from m in _dbContext.Messages
                                  .Include(x => x.MessageTrails)
                                                 join ss in _dbContext.Services on m.SenderId equals ss.Id into ssj
                                                 from ssa in ssj.DefaultIfEmpty()
                                                 join sr in _dbContext.Services on m.RecieverId equals sr.Id into srj
                                                 from sra in srj.DefaultIfEmpty()
                                                 join us in _dbContext.Users on m.SenderId equals us.Id into usj
                                                 from usa in usj.DefaultIfEmpty()
                                                 join ur in _dbContext.Users on m.RecieverId equals ur.Id into urj
                                                 from ura in urj.DefaultIfEmpty()
                                                 where
                                                 ((isClosed.HasValue == true) ? (m.IsClosed == isClosedValue) : true) &&
                                                 (IsAll == true ? true : (m.Date >= fromDate && m.Date <= toDate)) &&
                                                 (
                                                       (isSend == true ? ((m.SenderRole == RoleTypes.Provider? (m.SenderId == userId || m.SenderId == serviceId && m.SenderRole == roleType && m.IsDeletedBySender == false):(m.SenderId == userId && m.SenderRole == roleType && m.IsDeletedBySender == false))) : false) ||
                                                       (isRecieve == true ? (m.RecieverId == userId && m.ReceiverRole == roleType && m.IsDeletedByReciever == false) : false)
                                                 )
                                                 select new MessageQuery()
                                                 {
                                                     Id = m.Id,                                                     
                                                     MessageCategoryId = m.MessageCategoryId,
                                                     SenderId = m.SenderId,
                                                     SenderName = m.SenderId == userId || m.SenderId == serviceId ? "You" : m.SenderRole == RoleTypes.Administrator ? "Zowar" : m.SenderRole == RoleTypes.Provider ? ssa.Name : usa.DisplayName,
                                                     RecieverName = m.RecieverId == userId ? "You" : m.ReceiverRole == RoleTypes.Administrator ? "Zowar" : m.ReceiverRole == RoleTypes.Provider ? sra.Name : ura.DisplayName,
                                                     SenderRole = m.SenderRole,
                                                     ReceiverRole = m.ReceiverRole,
                                                     TransactionTypeId = m.TransactionTypeId,
                                                     ReferenceId = m.ReferenceId,
                                                     Date = m.Date,
                                                     Title = m.Title,
                                                     Body = m.Body,
                                                     IsRead = (m.IsRead == false && ((m.MessageTrails.Count > 0 && m.MessageTrails.OrderByDescending(x => x.Date).FirstOrDefault().SenderId != userId) || m.SenderId != userId)) ? false : true,
                                                     MessageTrails = (from mt in m.MessageTrails
                                                                      join ss in _dbContext.Services on mt.SenderId equals ss.Id into ssj
                                                                      from ssa in ssj.DefaultIfEmpty()
                                                                      join sr in _dbContext.Services on mt.SenderId equals sr.Id into srj
                                                                      from sra in srj.DefaultIfEmpty()
                                                                      join us in _dbContext.Users on m.SenderId equals us.Id into usj
                                                                      from usa in usj.DefaultIfEmpty()
                                                                      orderby m.Date
                                                                      select new MessageTrailQuery
                                                                      {
                                                                          SenderName = mt.SenderId == userId || mt.SenderId==serviceId ? "You" :
                                                                          (mt.SenderId == m.SenderId && m.SenderRole == RoleTypes.Provider) ? ssa.Name :
                                                                          (mt.SenderId == m.SenderId && m.SenderRole == RoleTypes.Administrator) ? "Zowar" :
                                                                          (mt.SenderId == m.RecieverId && m.ReceiverRole == RoleTypes.Provider) ? sra.Name :
                                                                          (mt.SenderId == m.RecieverId && m.ReceiverRole == RoleTypes.Administrator) ? "Zowar" : sra.Name,
                                                                          Body = mt.Body,
                                                                          Date = mt.Date
                                                                         
                                                                      })
                                                                      .OrderBy(x => x.Date)
                                                                      .ToList()
                                                 }).ToListAsync();
            return messages;
        }

        public async Task<long> IsValidTransaction(long userId, RoleTypes roleType, MessagingTransTypes transType, string transId)
        {
            if (transType == MessagingTransTypes.Booking) // Booking Transaction
            {
                var result = await (from m in _dbContext.Transactions
                                    where m.Id == transId
                                    && (roleType == RoleTypes.Client ? m.ClientId == userId : (roleType == RoleTypes.Provider ? m.ServiceId == userId : true))
                                    select new
                                    {
                                        serviceId = m.ServiceId,
                                        clientId = m.ClientId
                                    }
                            ).FirstOrDefaultAsync();
                if (result != null)
                {
                    if (roleType == RoleTypes.Client) // GetProvider 
                    {
                        return result.serviceId;
                    }
                    else if (roleType == RoleTypes.Provider)
                    {
                        return result.clientId;
                    }
                    else
                    {
                        return 0;
                    }
                }

            }
            else if (transType == MessagingTransTypes.Invoicing) // Invoicing
            {
                var result = await (from m in _dbContext.Invoices
                                    join s in _dbContext.Services on m.ServiceId equals s.Id
                                    where m.Id == transId
                                    &&
                                    (roleType == RoleTypes.Provider ? (roleType == RoleTypes.Provider ? s.Id == userId : true) : true)
                                    select new
                                    {
                                        providerId = s.ProviderId
                                    }
                                 ).FirstOrDefaultAsync();
                if (result != null)
                {
                    if (roleType == RoleTypes.Administrator) // GetProvider 
                    {
                        return result.providerId;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            return -1;
        }

        public async Task MarkRead(long userId, string messageId)
        {
            //var result = await _dbContext.Messages.Where(x => x.Id == messageId).FirstOrDefaultAsync();
            MessageTrail result = await _dbContext.MessageTrails.Where(x => x.MessageId == messageId).LastOrDefaultAsync();
            if (result == null || result.SenderId != userId) // The Last message sent by the other party
            {
                Message message = await _dbContext.Messages.Where(x => x.Id == messageId).FirstOrDefaultAsync();
                if (result == null && message.SenderId == userId)
                {
                    return;
                }

                if (message.IsRead == false)
                {
                    message.IsRead = true;
                    _dbContext.Messages.Update(message);
                    int saveResult = await _dbContext.SaveChangesAsync();
                    if (saveResult <= 0)
                    {
                        throw new CustomException("Unexpected error occurred ...");
                    }
                }
            }
        }
    }
}
