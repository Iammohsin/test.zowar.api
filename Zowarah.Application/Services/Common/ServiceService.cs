﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Data.Contexts;
using Zowarah.Data.Repositories;
using Zowarah.Domain.Entities.Common;

namespace Zowarah.Application.Services.Common {
    public class ServiceItem {
        public long Id { get; set; }
        public ServiceTypeEnum ServiceType { get; set; }
        public int CityId { get; set; }
        public string Name { get; set; }
        public long ProviderId { get; set; }
        public string ProviderName { get; set; }
        public DateTime Date { get; set; }
        public ServiceStatusEnum Status { get; set; }
    }
    public class ServicePage {
        public int Total { get; set; } = 0;
        public ICollection<ServiceItem> Services { get; set; } = new HashSet<ServiceItem>();
    }

    public class ProviderItem {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Country { get; set; }
        public int Services { get; set; }
        public bool Status { get; set; }
        // ToDo: add Credibility and JoinDate to user profile  
        public bool Credibility { get; set; }
        public DateTime JoinDate { get; set; }
    }
    public class ProviderPage {
        public int Total { get; set; } = 0;
        public ICollection<ProviderItem> providers { get; set; } = new HashSet<ProviderItem>();
    }

    public class ServiceReport {
        public ServiceTypeEnum ServiceType { get; set; }
        public int CityId { get; set; }
        public ServiceStatusEnum Status { get; set; }
        public int Providers { get; set; }
        public int Services { get; set; }
    }
    public class ServiceService : Repository<Service>, IServiceService {
        public ServiceService(ApplicationDbContext dbContext
            ) : base(dbContext) {

        }

        public async Task<ServicePage> GetServices(int page, int pageSize, int? city, ServiceTypeEnum serviceType, ServiceStatusEnum status, DateTime? from, DateTime? to, long serviceId, long providerId) {
            var servicePage = new ServicePage();
            var query = _dbContext.Services.Include(s => s.Provider).AsQueryable();
            if (city.HasValue && city.Value != 0)
                query = query.Where(s => s.CityId == city.Value);
            if (serviceType != 0)
                query = query.Where(s => s.ServiceType == serviceType);
            if (status != ServiceStatusEnum.All)
                query = query.Where(s => s.Status == status);
            if (from.HasValue)
                query = query.Where(s => s.CreatedDate.Date >= from.Value.Date);
            if (to.HasValue)
                query = query.Where(s => s.CreatedDate.Date <= to.Value.Date);
            if (serviceId != 0)
                query = query.Where(s => s.Id == serviceId);
            if (providerId != 0)
                query = query.Where(s => s.ProviderId == providerId);

            servicePage.Total = await query.Select(s => s.Id).CountAsync();
            servicePage.Services = await query         
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .Select(s => new ServiceItem {
                    Id = s.Id,
                    Name = s.Name,
                    ProviderId = s.ProviderId,
                    ProviderName = s.Provider.FirstName + " " + s.Provider.LastName,
                    CityId = s.CityId,
                    Date = s.CreatedDate,
                    ServiceType = s.ServiceType,
                    Status = s.Status
                }).ToListAsync();
            return servicePage;
        }

        public async Task<ICollection<ServiceReport>> GetServiceReport(int? city, ServiceTypeEnum serviceType, ServiceStatusEnum status, DateTime? from, DateTime? to, long serviceId, long providerId) {
            var servicePage = new ServicePage();
            var query = _dbContext.Services.AsQueryable();
            if (city.HasValue && city.Value != 0)
                query = query.Where(s => s.CityId == city.Value);
            if (serviceType != 0)
                query = query.Where(s => s.ServiceType == serviceType);
            if (status != ServiceStatusEnum.All)
                query = query.Where(s => s.Status == status);
            if (from.HasValue)
                query = query.Where(s => s.CreatedDate.Date >= from.Value.Date);
            if (to.HasValue)
                query = query.Where(s => s.CreatedDate.Date <= to.Value.Date);
            if (serviceId != 0)
                query = query.Where(s => s.Id == serviceId);
            if (providerId != 0)
                query = query.Where(s => s.ProviderId == providerId);


            return await query.GroupBy(s => new { s.ServiceType, s.CityId, s.Status }).Select(s => new ServiceReport {
                ServiceType = s.Key.ServiceType,
                CityId = s.Key.CityId,
                Status = s.Key.Status,
                Providers = s.Select(p => p.ProviderId).Distinct().Count(),
                Services = s.Count(),

            }).ToListAsync();
        }

        public async Task<ProviderPage> GetProviders(int pageNumber, int PageSize, DateTime From, DateTime To) {
            var providerPage = new ProviderPage();
            var providers = (from ur in _dbContext.UserRoles
                             join role in _dbContext.Roles on ur.RoleId equals role.Id
                             join user in _dbContext.Users on ur.UserId equals user.Id
                             where role.Name.Equals("Provider") &&
                              user.JoinDate.Date >= From.Date &&
                            user.JoinDate.Date <= To.Date
                             select user);

            providerPage.Total = await providers.CountAsync();
            providerPage.providers = await providers
                .Skip((pageNumber - 1) * PageSize)
                .Take(PageSize)
                .Select(p => new ProviderItem {
                    Id = p.Id,
                    Name = $"{ p.FirstName} { p.LastName}",
                    Country = p.CountryId.GetValueOrDefault(),
                    Status = p.IsActive,
                    Services = _dbContext.Services.Where(s => s.ProviderId == p.Id).Count()
                }).ToListAsync();

            return providerPage;

        }
    }
}
