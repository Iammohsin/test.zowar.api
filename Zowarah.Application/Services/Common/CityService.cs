﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Data.Contexts;
using Zowarah.Data.Repositories;
using Zowarah.Domain.Entities.Common;

namespace Zowarah.Application.Services.Common
{
    public class CityService : Repository<City>, ICityService
    {
        public CityService(ApplicationDbContext dbContext
            ) :base(dbContext)
        {
        }
        public async Task<IList<string>> Search(string Term)
        { //.getall(x => x.Description.ToLower().Contains(Term.ToLower())).Select(x => x.Description).ToListAsync();
            var query = (from m in _dbSet
                       //  join l in _dbContext.Localization
                     //    on m.Description equals l.Id
                         // where l.LocText.ToLower().Contains(Term.ToLower())
                         where m.Description.ToLower().Contains(Term.ToLower())
                         select m.Description
                //l.LocText
                         );
            return await query.ToListAsync();
        }
    }
}
