﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Enums.Hotels;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Data.Contexts;
using Zowarah.Data.Repositories;
using Zowarah.Domain.Entities.Common;

namespace Zowarah.Application.Services.Common
{
   
    public class ReportingService : Repository<Service>, IReportingService
    {
        public ReportingService(ApplicationDbContext dbContext
            ) : base(dbContext)
        {
        }

        public async Task<int> GetTotalServiceActived()
        {
            return await _dbContext.Services.Where(s => s.Status == ServiceStatusEnum.Approved).CountAsync();
        }

        public async Task<int> GetTotalServicePendding()
        {
            return await _dbContext.Services.Where(s => s.Status == ServiceStatusEnum.InProgress).CountAsync();
        }

        public async Task<int> GetTotalServiceRegistered()
        {
             return await _dbContext.Services.CountAsync();
        }
    }
}
