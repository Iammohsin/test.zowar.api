﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Users;
using Zowarah.Data.Contexts;
using System.Linq;
using Zowarah.Domain.Entities.Users;
using Zowarah.Data.Repositories;

namespace Zowarah.Application.Services.Users
{
    public class CreditCardService : Repository<CreditCardInfo>, ICreditCardService
    {
        public CreditCardService(ApplicationDbContext db) : base(db)
        {
        }

        public async Task Add(CreditCardInfo creditCardInfo)
        {
            await _dbContext.CreditCardInfos.AddAsync(creditCardInfo);
        }

        //public void Update(CreditCardInfo creditCardInfo)
        //{
        //    _dbContext.CreditCardInfos.Update(creditCardInfo);
        //}

        //public void Delete(CreditCardInfo creditCardInfo)
        //{
        //    _dbContext.CreditCardInfos.Update(creditCardInfo);
        //}
        public List<CreditCardInfo> GetByUserId(long UserId)
        {
            return _dbContext.CreditCardInfos.Where(w => w.IsDeleted == false && w.UserId.Equals(UserId)).OrderByDescending(o => o.UpdatedOn).ToList();
        }

        CreditCardInfo ICreditCardService.GetByCardId(int cardId)
        {
            return _dbContext.CreditCardInfos.Where(w => w.Id.Equals(cardId)).FirstOrDefault();
        }
    }
}