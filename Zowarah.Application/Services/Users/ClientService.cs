﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Users;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Enums;
using Zowarah.Domain.Queries;


namespace Zowarah.Application.Services.Users
{
    public class ClientService : IClientService
    {
        private readonly ApplicationDbContext _dbContext;
        public ClientService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ICollection<ClientsOverview>> GetOverview(PeriodEnum period, DateTime fromDate, DateTime toDate, int country)
        {

            //if (period == PeriodEnum.last7Days)
            // {
            //     fromDate = DateTime.Now.Date.AddDays(-7);
            //     toDate = DateTime.Now.Date;
            // }
            // else if (period == PeriodEnum.last30Days)
            // {
            //     fromDate = DateTime.Now.Date.AddDays(-30);
            //     toDate = DateTime.Now.Date;
            // }
          
                var clients = await (from u in _dbContext.Users
                                     join ur in _dbContext.UserRoles on u.Id equals ur.UserId
                                     join r in _dbContext.Roles on ur.RoleId equals r.Id
                                     join t in _dbContext.Transactions.GroupBy(x => x.ClientId).Select(g => new { ClientId = g.Key }) on u.Id equals t.ClientId into j1
                                     where
                                     r.Id == (int)RoleTypes.Client && u.JoinDate >= fromDate && u.JoinDate <= toDate && u.CountryId !=null
                                     //((period == PeriodEnum.thisYear) ?  (u.JoinDate.Year == DateTime.Now.Year) : true)

                                     from j2 in j1.DefaultIfEmpty()
                                     group j2 by u.CountryId into grp
                                     select new ClientsOverview
                                     {
                                         CountryId = grp.Key.Value,
                                         NoOfClients = grp.Count(),
                                         NoOfActiveClients = grp.Where(x => x != null).Count()
                                     }
                           ).ToListAsync();
                return await Task.FromResult(clients);
          

        }
    }
}


