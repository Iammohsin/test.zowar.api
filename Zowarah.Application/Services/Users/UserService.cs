﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Users;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.Application.Services.Users
{
    public class UserService : IUserService
    {
        private readonly ApplicationDbContext _dbContext;
        public UserService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> CreateUser(AppUser user)
        {
            await _dbContext.Users.AddAsync(user);
            var result = await _dbContext.SaveChangesAsync();
            return result >= 1;
        }

        public void UpdateUser(AppUser user)
        {
            _dbContext.Users.Update(user);
        }
        public async Task<AppUser> GetUserById(long Id)
        {
            return await _dbContext.Users.FindAsync(Id);
        }

        public async Task<int> TotalUserByRoleName(string roleName, bool? isActive = null, DateTime? From=null,DateTime? To=null)
        {
            var query = (from ur in _dbContext.UserRoles
                         join role in _dbContext.Roles on ur.RoleId equals role.Id
                         join user in _dbContext.Users on ur.UserId equals user.Id
                         where role.Name.Equals(roleName)
                         select new { user.IsActive, user.JoinDate}
                          );

            if (isActive.HasValue)
                query = query.Where(s => s.IsActive == isActive.Value);

            if(From.HasValue)
                query = query.Where(s => s.JoinDate.Date >= From.Value.Date);

            if (To.HasValue)
                query = query.Where(s => s.JoinDate.Date <= To.Value.Date);
            return await query.CountAsync();
        }

        public Task<int> TotalClientsRegistered()
        {
            throw new NotImplementedException();
        }
    }
}
