﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces;
using Zowarah.Application.Interfaces.Users;
using Zowarah.Data.Contexts;
using Zowarah.Data.Repositories;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.Application.Services.Users
{
    public class PhotoService : Repository<ProfilePhoto>, IPhotoService
    {
        private new readonly ApplicationDbContext _dbContext;
       // private readonly DbSet<ProfilePhoto> _photo;
 
        public PhotoService(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
           // _photo = _dbContext.ProfilePhotos;
        }

        public async Task<ProfilePhoto> GetByUserId(long userId)
        {
            //return await _dbSet.Where(x=>x.UserId == userId).FirstOrDefaultAsync();
            //return await _dbSet.Where(x => x.Name.ToLower().Contains(Term.ToLower())).Select(x => x.Name).ToListAsync();
            return await _dbContext.ProfilePhotos.Where(c => c.UserId == userId).FirstOrDefaultAsync();            
        }
    }
}
