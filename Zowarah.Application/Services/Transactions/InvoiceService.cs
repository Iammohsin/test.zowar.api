﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Interfaces.Transactions;
using Zowarah.Application.Services.Hotels;
using Zowarah.Data;
using Zowarah.Data.Contexts;
using Zowarah.Data.Repositories;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Transactions;
using Hangfire;
using Zowarah.Domain.Entities.Invoices;

namespace Zowarah.Application.Services.Transactions
{

    public class InvoiceNotification
    {
        public static string TemplateId => "invoiceCreated";
        public static string Subject => "Invoice Generated";
        public static string SMSMessage => "Your Montly invoice has been generated, please visit Zowarah.com for ore details";

    }

    public class SuspendNotification
    {
        public static string TemplateId => "ServiceSuspended";
        public static string Subject => "Service Suspended";
        public static string SMSMessage => "Your Service has been suspended due to an over due Invoice, please visit Zowarah.com";

    }

    public class InvoiceQuery
    {
        public string InvoiceId { get; set; }
        public string ServiceId { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string ProviderName { get; set; }
        public string ProviderId { get; set; }
        public ServiceTypeEnum ServiceType { get; set; }
        public string ServiceName { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public InvoiceStatus Status { get; set; }
        public DateTime DueDate { get; set; }
        public string PaymentMethod { get; set; }
    }


    public class InvoiceTransaction
    {
        public string InvoiceId { get; set; }
        public string ServiceId { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string ProviderName { get; set; }
        public string ProviderId { get; set; }
        public ServiceTypeEnum ServiceType { get; set; }
        public string ServiceName { get; set; }
        public string TransactionId { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public InvoiceStatus Status { get; set; }

    }
    public class InvoiceTransactionDetails : InvoiceTransaction
    {
        public DateTime TransactionDate { get; set; }
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public int PaymentMethod { get; set; }
        public int CountryId { get; set; }
        public DateTime DueDate { get; set; }
    }
    public class InvoicingService : Repository<Invoice>, IInvoicingService
    {
        INotificationService notificationService;
        //UnitOfWork uow;
        public InvoicingService(ApplicationDbContext db,INotificationService _notificationService) : base(db)
        {
            notificationService = _notificationService;
            //uow = _uow;
        }

        public async Task RemoveInvoices()
        {
             _dbContext.Database.ExecuteSqlCommand("update transactions set invoiceId=null " +
                " delete from invoices " +
                " delete from invoiceChecks "
                );
        }
        public async Task GenerateInvoices()
        {
            // ToDo : ToBe Converted Into A stored Procedure [Enhancements]
            // To Do : YearMonth Check Constraint MonthYear,NoOfInvoices [Done]
            // Get Previous Month Transactions based on the CheckOut Date
            // The Invoicing Currency USD **ToDo** to add exchange in the transactions

                // Check If Already Invoiced 
                var query = _dbContext.InvoiceChecks.Where(x => x.Month == DateTime.Now.Month.ToString() && x.Year == DateTime.Now.Year.ToString()).FirstOrDefault();
                if (query != null)
                    throw new Exception("This Month invoices are already generated !!");

                // Get Commission
                var commission = decimal.Parse(_dbContext.Settings.Where(x => x.Id == "hotels_commission").Select(x => x.Value).FirstOrDefault()) / 100;//Convert To 0.x

                // Get Due Grace Duration
                var dueDuration = double.Parse(_dbContext.Settings.Where(x => x.Id == "invoicing_due_duration").Select(x => x.Value).FirstOrDefault());


                // Added 48 Ours to the checkout of the pervious month before checking
                var previousMonth = DateTime.Now.AddMonths(-1);
            var transactions = (
                        from t in _dbContext.Transactions
                        join b in _dbContext.Booking
                        on t.Id equals b.TransactionId
                        join c in _dbContext.Currencies
                        on t.ProviderCurrency equals c.Id
                        where
                        b.CheckOutDate.AddDays(2).Month == previousMonth.Month &&
                        b.CheckOutDate.AddDays(2).Year == previousMonth.Year &&
                        t.Status == TransactionStatus.Confirmed
                        select new
                        {
                            ServiceId = t.ServiceId,
                            TransactionId = t.Id,
                            Amount = (c.ExchangeRate > 1 ? Math.Round(t.ProviderAmount / c.ExchangeRate, 2) : Math.Round(t.ProviderAmount * c.ExchangeRate, 2)) * commission
                        }).ToList();

            var invoices =  (from d in transactions
                                  group d by d.ServiceId into g
                                  select new
                                  {
                                      ServiceId = g.Key,
                                      Amount = g.Sum(x => x.Amount)
                                  }).ToList();


                var noOfInvoices = invoices.Count();
                var totalAmount = (from d in invoices
                                   select d.Amount).Sum();


                _dbContext.InvoiceChecks.Add(new InvoiceCheck { Month = DateTime.Now.Month.ToString(), Year = DateTime.Now.Year.ToString(), NoOfInvoices = noOfInvoices, Amount = totalAmount });
                IList<String> serviceLst = new List<String>();
                foreach (var invoice in invoices)
                {
                    serviceLst.Add(invoice.ServiceId);
                    var invoiceId = Guid.NewGuid().ToString();
                    _dbContext.Invoices.Add(new Invoice { Id = invoiceId, ServiceId = invoice.ServiceId, Date = DateTime.Now.Date, Amount = invoice.Amount, DueDate = DateTime.Now.AddDays(dueDuration), Status = InvoiceStatus.Pending });

                //UpdateInvoices 
                var invoiceTransList = transactions.Where(x => x.ServiceId == invoice.ServiceId).Select(x=> x.TransactionId).ToList();
                var invoiceTransactions = _dbContext.Transactions.Where(x=> invoiceTransList.Contains(x.Id));
                   foreach(var tran in invoiceTransactions)
                    {
                        tran.InvoiceId = invoiceId;
                        _dbContext.Transactions.Update(tran);
                    }
                }

            await _dbContext.SaveChangesAsync();


                // Send Notification
                var services = (from m in _dbContext.Services
                                join u in _dbContext.Users
                                on m.ProviderId equals u.Id
                                where serviceLst.Contains(m.Id)
                                select new
                                {
                                    ServiceId = m.Id,
                                    ServiceName = m.Name,
                                    ProviderName = u.FirstName,
                                    Email = u.Email
                                }).ToList();

                // Send Bulk Emails
                DateTimeFormatInfo mfi = new DateTimeFormatInfo();
                var template = notificationService.GetTemplate(InvoiceNotification.TemplateId);
                foreach (var service in services)
                {
                    var htmlContent = template
                    .Replace("$$ServiceName$$", service.ServiceName)
                    .Replace("$$Month$$", mfi.GetMonthName(DateTime.Now.Month))
                    .Replace("$$Year$$", DateTime.Now.Year.ToString());
                    BackgroundJob.Enqueue(()=> notificationService.SendBySmtp(service.Email, InvoiceNotification.Subject, service.ProviderName, htmlContent));
                }
        }

        public async Task CheckOverDue()
        {
            var toSuspend = (from m in _dbContext.Invoices
                             join s in _dbContext.Services.Include(x=> x.Logs)
                             on m.ServiceId equals s.Id
                             where DateTime.Now.Date > m.DueDate
                             select s
                            ).ToList();
                            
            foreach(var service in toSuspend)
            {
                // Change The Status, Add Log , Send Email 
                service.IsActive = false;
                service.AddServiceStatusLog(new ServiceStatusLog { Status = ServiceStatusEnum.Suspended, Comments = "Service Suspended Due To an Overdue Invoice." });
                _dbContext.Services.Update(service);
            }
            await _dbContext.SaveChangesAsync();

            // Send Emails
            var toSend = (from m in toSuspend
                          join u in _dbContext.Users
                          on m.ProviderId equals u.Id
                          select new
                          {
                              ServiceId = m.Id,
                              ServiceName = m.Name,
                              ProviderName = u.FirstName,
                              Email = u.Email
                          }
                          ).ToList();

            var template = notificationService.GetTemplate(SuspendNotification.TemplateId);
            foreach (var service in toSend)
            {
                var htmlContent = template
                .Replace("$$ServiceName$$", service.ServiceName);
                await notificationService.SendBySmtp(service.Email, InvoiceNotification.Subject, service.ProviderName, htmlContent);
            }
        }
    }
}
