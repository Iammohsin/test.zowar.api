﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Application.Enums.Hotels;
using Zowarah.Application.Interfaces.Transactions;
using Zowarah.Application.Services.Hotels;
using Zowarah.Data.Contexts;
using Zowarah.Data.Repositories;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Transactions;

namespace Zowarah.Application.Services.Transactions {
    public class BasicTransactionInfo {
        public string Id { get; set; }
        public long ServiceId { get; set; }
        public string ServiceName { get; set; }
        public long ClientId { get; set; }
        public string ClientName { get; set; }
        public long ProviderId { get; set; }
        public string ProviderName { get; set; }
        public int ServiceType { get; set; }
        public TransactionType TransactionType { get; set; }
        public DateTime Date { get; set; }
        public TransactionStatus Status { get; set; }
        public string ProviderCurrency { get; set; }
        public decimal ProviderAmount { get; set; }
        public string ClientCurrency { get; set; }
        public decimal ClientAmount { get; set; }
        public string InvoiceId { get; set; }

    }
    public class BookingTransaction : BasicTransactionInfo {
        public string BookingId { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public bool IsShowUp { get; set; }
        public bool IsInCheckInDate { get; set; }
        public bool IsInCheckOutDate { get; set; }
        public string RoomName { get; internal set; }
    }

    public class GetServiceTransaction {
        public string Id { get; set; }
        public long ServiceId { get; set; }
        public string ServiceName { get; set; }
        public long ClientId { get; set; }
        public long ProviderId { get; set; }
        public int ServiceType { get; set; }
        public string OwnerId { get; set; }
        public TransactionType TransactionType { get; set; }
        public DateTime Date { get; set; }
        public TransactionStatus Status { get; set; }
        public string CurrencyCode { get; set; }
        public decimal Amount { get; set; }
        public bool IsInCheckInDate { get; set; }
        public bool IsInCheckOutDate { get; set; }
    }
    public class SaleReporting {
        public DateTime Date { get; set; }
        public ServiceTypeEnum ServiceType { get; set; }
        public int City { get; set; }
        public int Providers { get; set; }
        public int Transactions { get; set; }
        public decimal BaseAmount { get; set; }
        public decimal ProviderAmount { get; set; }
        public decimal ClientAmount { get; set; }
        public decimal Commission { get; set; }
    }

    public class TransactionReport {
        public ServiceTypeEnum ServiceType { get; set; }
        public TransactionStatus Status { get; set; }
        public int Transactions { get; set; }
    }

    public class TransactionService : Repository<Transaction>, ITransactionService {
        private readonly IHttpContextAccessor httpContextAccessor;
        public TransactionService(ApplicationDbContext db, IHttpContextAccessor _httpContextAccessor) : base(db) {
            httpContextAccessor = _httpContextAccessor;
        }
        public new void Create(Transaction entity) {
            entity.CreatedDate = DateTime.Now.Date;
            entity.Commission = decimal.Parse(_dbContext.Settings.Find("hotels_commission").Value);
            _dbContext.AddAsync<Transaction>(entity);
        }
        public async Task<ICollection<BookingTransaction>> GetAllBooking(
            string InvoiceId,
            long ServiceId,
            long ClientId,
            int? roomTypeId,
             int? serviceCity,
            TransactionStatus Status,
            bool? isShowUp,
            DateTime? From,
            DateTime? To) {
            var query = _dbContext.Booking
                       .Include(b => b.Transaction)
                       .Include(b => b.Transaction.Service)
                       .Include(b => b.Transaction.Client)
                       .Include(b => b.Transaction.Service.Provider)
                       .Include(b => b.Room.RoomTypeName)
                        .AsQueryable();
            if (!string.IsNullOrEmpty(InvoiceId))
                query = query.Where(tr => tr.Transaction.InvoiceId == InvoiceId);
            if (ClientId != 0)
                query = query.Where(tr => tr.Transaction.ClientId == ClientId);
            if (ServiceId != 0)
                query = query.Where(tr => tr.Transaction.ServiceId == ServiceId);
            if (Status > 0 && !isShowUp.HasValue)
                query = query.Where(tr => tr.Transaction.Status == Status);
            if (isShowUp.HasValue)
                query = query.Where(tr =>
                tr.CheckOutDate.Date <= DateTime.Now.Date &&
                tr.Transaction.Status == TransactionStatus.Confirmed);
            if (From.HasValue && From != DateTime.MinValue)
                query = query.Where(tr => tr.Transaction.Date.Date >= From.Value.Date);
            if (To.HasValue && To != DateTime.MinValue)
                query = query.Where(tr => tr.Transaction.Date.Date <= To.Value.Date);
            if (serviceCity.HasValue && serviceCity.Value != 0)
                query = query.Where(tr => tr.Transaction.Service.CityId == serviceCity.Value);
            if (roomTypeId.HasValue && roomTypeId.Value != 0)
                query = query.Where(tr => tr.Room.RoomTypeNameId == roomTypeId.Value);

            var selectQuery = query.Select(tr => new BookingTransaction() {
                Id = tr.TransactionId,
                ClientAmount = tr.Transaction.ClientAmount,
                ClientCurrency = tr.Transaction.ClientCurrency,
                ProviderAmount = tr.Transaction.ProviderAmount,
                ProviderCurrency = tr.Transaction.ProviderCurrency,
                BookingId = tr.Id,
                ClientId = tr.Transaction.ClientId,
                ClientName = tr.Transaction.Client.FirstName + " " + tr.Transaction.Client.LastName,
                Date = tr.Transaction.Date,
                ProviderId = tr.Transaction.Service.ProviderId,
                ProviderName = tr.Transaction.Service.Provider.FirstName + " " + tr.Transaction.Service.Provider.LastName,
                Status = tr.Transaction.Status,
                ServiceId = tr.Transaction.ServiceId,
                ServiceName = tr.Transaction.Service.Name,
                ServiceType = (int)tr.Transaction.Service.ServiceType,
                TransactionType = tr.Transaction.TransactionType,
                CheckInDate = tr.CheckInDate,
                CheckOutDate = tr.CheckOutDate,
                InvoiceId = tr.Transaction.InvoiceId,
                RoomName = tr.Room.RoomTypeName.Description,
                IsShowUp = tr.CheckOutDate < DateTime.Now && tr.Transaction.Status == TransactionStatus.Confirmed,
                IsInCheckInDate = tr.CheckInDate >= DateTime.Now,
                IsInCheckOutDate = tr.CheckOutDate >= DateTime.Now
            });
            return await selectQuery.ToListAsync();
        }

        public async Task<List<GetServiceTransaction>> GetAllClientServiceTranactions(long ClientId, BookingFilterEnum bookingFilter) {
            //GetAllClientServiceTranactions
            var query = (from tr in _dbContext.Transactions
                        .Include(c => c.Client)
                        .Include(c => c.Service)
                         join b in _dbContext.Booking on
                         tr.Id equals b.TransactionId
                         where 
                         tr.ClientId == ClientId &&
                         (
                          (bookingFilter == BookingFilterEnum.Upcoming) ? b.CheckOutDate >= DateTime.Now && tr.Status<TransactionStatus.NoShow : 
                          (bookingFilter == BookingFilterEnum.Cancelled) ? tr.Status == TransactionStatus.Cancelled : 
                          (bookingFilter == BookingFilterEnum.All) 
                         )
                         
                         select new GetServiceTransaction() {
                             Id = tr.Id,
                             Amount = tr.ClientAmount,
                             OwnerId = tr.OwnerId,
                             ClientId = tr.ClientId,
                             Date = tr.Date,
                             CurrencyCode = tr.ClientCurrency,
                             ProviderId = tr.Service.ProviderId,
                             Status = tr.Status,
                             ServiceId = tr.ServiceId,
                             ServiceName = tr.Service.Name,
                             ServiceType = (int)tr.Service.ServiceType,
                             TransactionType = tr.TransactionType,
                             IsInCheckInDate = b.CheckInDate > DateTime.Now
                         });
            return await query.ToListAsync();
        }

        public async Task<List<GetServiceTransaction>> SearchClientServiceTranactions(
            long ClientId, ServiceTypeEnum ServiceType, TransactionStatus Status, DateTime From, DateTime To) {
            var query = (from tr in _dbContext.Transactions
                        .Include(c => c.Client)
                        .Include(c => c.Service)
                         join b in _dbContext.Booking on
                         tr.Id equals b.Id
                         where (tr.ClientId == ClientId
                         && tr.Service.ServiceType == ServiceType
                         && (Status != 0 ? tr.Status == Enum.Parse<TransactionStatus>(Status.ToString()) : tr.Status != 0)
                         && tr.Date >= From
                         && tr.Date <= To)
                         select new GetServiceTransaction() {
                             Id = tr.Id,
                             Amount = tr.ClientAmount,
                             OwnerId = tr.Id,
                             ClientId = tr.ClientId,
                             Date = tr.Date,
                             CurrencyCode = tr.ClientCurrency,
                             ProviderId = tr.Service.ProviderId,
                             Status = tr.Status,
                             ServiceId = tr.ServiceId,
                             ServiceName = tr.Service.Name,
                             ServiceType = (int)tr.Service.ServiceType,
                             TransactionType = tr.TransactionType,
                             IsInCheckInDate = b.CheckInDate > DateTime.Now
                         });
            return await query.ToListAsync();
        }

        public async Task<List<GetServiceTransaction>> GetAllProviderServiceTranactions(long providerId) {
            var query = (from tr in _dbContext.Transactions
                         .Include(c => c.Client)
                         .Include(c => c.Service)
                         join b in _dbContext.Booking on
                         tr.Id equals b.Id
                         where (tr.Service.ProviderId == providerId
                         && (tr.Status < TransactionStatus.NoShow
                         || tr.Status == TransactionStatus.Cancelled)
                         && b.CheckOutDate >= DateTime.Now)
                         select new GetServiceTransaction() {
                             Id = tr.Id,
                             Amount = tr.ClientAmount,
                             OwnerId = tr.OwnerId,
                             ClientId = tr.ClientId,
                             Date = tr.Date,
                             CurrencyCode = tr.ClientCurrency,
                             ProviderId = tr.Service.ProviderId,
                             Status = tr.Status,
                             ServiceId = tr.ServiceId,
                             ServiceName = tr.Service.Name,
                             ServiceType = (int)tr.Service.ServiceType,
                             TransactionType = tr.TransactionType,
                             IsInCheckInDate = b.CheckInDate > DateTime.Now,
                             IsInCheckOutDate = DateTime.Now >= b.CheckInDate && DateTime.Now <= b.CheckOutDate
                         });
            return await query.ToListAsync();
        }

        public async Task<Transaction> GetById(long id) {
            var userId = long.Parse(httpContextAccessor.HttpContext.User.Claims.Where(x => x.Type == "id").FirstOrDefault().Value);
            return await (from m in _dbSet
                          join s in _dbContext.Services
                          on m.ServiceId equals s.Id
                          where (s.ProviderId == userId || m.ClientId == userId)
                          select m).FirstOrDefaultAsync();
        }
        public async Task<List<GetServiceTransaction>> SearchProviderServiceTranactions(long providerId, ServiceTypeEnum ServiceType, TransactionStatus Status, DateTime From, DateTime To) {
            var _status = Enum.Parse<TransactionStatus>(Status.ToString());
            var query = (from tr in _dbContext.Transactions
                       .Include(c => c.Client)
                       .Include(c => c.Service)
                         join b in _dbContext.Booking on
                         tr.OwnerId equals b.Id
                         where (tr.Service.ProviderId == providerId
                         && tr.Service.ServiceType == ServiceType
                        && (Status != 0) ? tr.Status == _status : tr.Status > _status
                         && tr.Date >= From
                         && tr.Date <= To)
                         select new GetServiceTransaction() {
                             Id = tr.Id,
                             Amount = tr.ProviderAmount,
                             OwnerId = tr.OwnerId,
                             ClientId = tr.ClientId,
                             Date = tr.Date,
                             CurrencyCode = tr.ProviderCurrency,
                             ProviderId = tr.Service.ProviderId,
                             Status = tr.Status,
                             ServiceId = tr.ServiceId,
                             ServiceName = tr.Service.Name,
                             ServiceType = (int)tr.Service.ServiceType,
                             TransactionType = tr.TransactionType,
                             IsInCheckInDate = b.CheckInDate > DateTime.Now,
                             IsInCheckOutDate = DateTime.Now >= b.CheckInDate && DateTime.Now <= b.CheckOutDate
                         });
            return await query.ToListAsync();
        }

        public Task<List<GetServiceTransaction>> SearchProviderServiceTranactions(string providerId, ServiceTypeEnum ServiceType, int Status, DateTime From, DateTime To) {
            throw new NotImplementedException();
        }

        public async Task<(int, List<BasicTransactionInfo>)> GetAllTranactionsByFilter(int page, int pageSize, string invoiceId, long? serviceId, long? providerId, long? clientId, ServiceTypeEnum ServiceType, TransactionStatus Status, DateTime? From, DateTime? To) {
            var query = _dbContext.Transactions
                                     .Include(s => s.Service)
                                     .Include(s => s.Service.Provider)
                                     .Include(s => s.Client).AsQueryable();
            if (!string.IsNullOrEmpty(invoiceId))
                query = query.Where(s => s.InvoiceId == invoiceId);
            if (serviceId.HasValue && serviceId.Value != 0)
                query = query.Where(s => s.ServiceId == serviceId);
            if (clientId.HasValue && clientId.Value != 0)
                query = query.Where(s => s.ClientId == clientId);
            if (providerId.HasValue && providerId.Value != 0)
                query = query.Where(s => s.Service.ProviderId == providerId);
            if (ServiceType != 0)
                query = query.Where(s => s.Service.ServiceType == ServiceType);
            if (Status != 0)
                query = query.Where(s => s.Status == Status);
            if (From.HasValue && From != DateTime.MinValue)
                query = query.Where(s => s.Date.Date >= From.Value.Date);
            if (To.HasValue && To != DateTime.MinValue)
                query = query.Where(s => s.Date.Date <= To.Value.Date);

            var total = await query.CountAsync();
            var list = await query
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .Select(s => new BasicTransactionInfo {
                    Id = s.Id,
                    ClientAmount = s.ClientAmount,
                    ClientCurrency = s.ClientCurrency,
                    ClientId = s.ClientId,
                    ClientName = s.Client.FirstName + " " + s.Client.LastName,
                    Date = s.Date,
                    InvoiceId = s.InvoiceId,
                    ProviderAmount = s.ProviderAmount,
                    ProviderCurrency = s.ProviderCurrency,
                    ProviderName = s.Service.Provider.FirstName + " " + s.Service.Provider.LastName,
                    ProviderId = s.Service.ProviderId,
                    ServiceId = s.ServiceId,
                    ServiceName = s.Service.Name,
                    ServiceType = (int)s.Service.ServiceType,
                    Status = s.Status
                }).ToListAsync();
            return (total, list);
        }
        public async Task<ICollection<TransactionReport>> GetTranactionsReportByFilter(int page, int pageSize, string invoiceId, long? serviceId, long? providerId, long? clientId, ServiceTypeEnum ServiceType, TransactionStatus Status, DateTime? From, DateTime? To) {
            var query = _dbContext.Transactions
                          .Include(s => s.Service)
                          .Include(s => s.Service.Provider)
                          .Include(s => s.Client).AsQueryable();
            if (!string.IsNullOrEmpty(invoiceId))
                query = query.Where(s => s.InvoiceId == invoiceId);
            if (serviceId.HasValue && serviceId.Value != 0)
                query = query.Where(s => s.ServiceId == serviceId);
            if (clientId.HasValue && clientId.Value != 0)
                query = query.Where(s => s.ClientId == clientId);
            if (providerId.HasValue && providerId.Value != 0)
                query = query.Where(s => s.Service.ProviderId == providerId);
            if (ServiceType != 0)
                query = query.Where(s => s.Service.ServiceType == ServiceType);
            if (Status != 0)
                query = query.Where(s => s.Status == Status);
            if (From.HasValue && From != DateTime.MinValue)
                query = query.Where(s => s.Date.Date >= From.Value.Date);
            if (To.HasValue && To != DateTime.MinValue)
                query = query.Where(s => s.Date.Date <= To.Value.Date);
            return await query.Select(s => new { s.Service.ServiceType, s.Status }).GroupBy(s => new { s.ServiceType, s.Status }).Select(s => new TransactionReport {
                ServiceType = s.Key.ServiceType,
                Status = s.Key.Status,
                Transactions = s.Count()
            }).ToListAsync();
        }
        public async Task<decimal> GetTotalConfirmed(long serviceId) {
            return await _dbContext.Transactions
                .Where(s => s.ServiceId == serviceId &&
                s.Status == TransactionStatus.Confirmed).CountAsync();
        }
        public async Task<decimal> GetTotalCancelled(long serviceId) {
            return await _dbContext.Transactions
                .Where(s => s.ServiceId == serviceId &&
                s.Status == TransactionStatus.Cancelled).CountAsync();
        }

        public async Task<ICollection<SaleReporting>> GetSaleReporting(long ClientId, DateTime from, DateTime to, ServiceTypeEnum serviceType, int city, ReportView view) {
            var query = _dbContext
                .Booking
                .Include(t => t.Transaction)
                .Include(s => s.Transaction.Service)
                .Where(e =>
                e.Transaction.Status == TransactionStatus.Confirmed &&
                e.CheckOutDate.Date <= DateTime.Now.Date &&
                e.Transaction.Date.Date >= from.Date &&
                e.Transaction.Date.Date <= to.Date);

            if (ClientId != 0)
                query = query.Where(e => e.Transaction.ClientId == ClientId);
            if (serviceType != 0)
                query = query.Where(e => e.Transaction.Service.ServiceType == serviceType);
            if (city != 0)
                query = query.Where(e => e.Transaction.Service.CityId == city);
            var selectedQuery = await query.Select(tr => new {
                tr.Transaction.Service.ServiceType,
                tr.Transaction.Service.CityId,
                tr.Transaction.Date,
                tr.Transaction.Service.ProviderId,
                tr.Transaction.BaseAmount,
                tr.Transaction.ProviderAmount,
                tr.Transaction.ClientAmount,
                tr.Transaction.Commission
            }).ToListAsync();
            switch (view) {
                case ReportView.years:
                    return selectedQuery.GroupBy(s => new { s.ServiceType, s.CityId, s.Date.Year })
                             .Select(s => new SaleReporting {
                                 ServiceType = s.Key.ServiceType,
                                 Date = new DateTime(s.Key.Year, 1, 1),
                                 City = s.Key.CityId,
                                 Providers = s.Select(p => p.ProviderId).Distinct().Count(),
                                 Transactions = s.Count(),
                                 BaseAmount = s.Sum(m => m.BaseAmount),
                                 ProviderAmount = s.Sum(m => m.ProviderAmount),
                                 ClientAmount = s.Sum(m => m.ClientAmount),
                                 Commission = s.Sum(m => m.BaseAmount * m.Commission)
                             }).ToList();
                case ReportView.months:
                    return selectedQuery.GroupBy(g => new { g.ServiceType, g.CityId, g.Date.Year, g.Date.Month })
                           .Select(gr => new SaleReporting {
                               ServiceType = gr.Key.ServiceType,
                               Date = new DateTime(gr.Key.Year, gr.Key.Month, 1),
                               City = gr.Key.CityId,
                               Providers = gr.Select(p => p.ProviderId).Distinct().Count(),
                               Transactions = gr.Count(),
                               BaseAmount = gr.Sum(m => m.BaseAmount),
                               ProviderAmount = gr.Sum(m => m.ProviderAmount),
                               ClientAmount = gr.Sum(m => m.ClientAmount),
                               Commission = gr.Sum(m => m.BaseAmount * m.Commission)
                           }).ToList();
                case ReportView.weeks:
                    return selectedQuery.GroupBy(g => new {
                        g.ServiceType,
                        g.CityId,
                        g.Date.Year,
                        //SqlFunctions.DatePart("week", s.Date)
                        week = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(g.Date, CalendarWeekRule.FirstDay, DayOfWeek.Monday)
                    }).Select(gr => new SaleReporting {
                        ServiceType = gr.Key.ServiceType,
                        Date = new DateTime(gr.Key.Year, 1, 1).AddDays((gr.Key.week - 1) * 7),
                        City = gr.Key.CityId,
                        Providers = gr.Select(p => p.ProviderId).Distinct().Count(),
                        Transactions = gr.Count(),
                        BaseAmount = gr.Sum(m => m.BaseAmount),
                        ProviderAmount = gr.Sum(m => m.ProviderAmount),
                        ClientAmount = gr.Sum(m => m.ClientAmount),
                        Commission = gr.Sum(m => m.BaseAmount * m.Commission)
                    }).ToList();
                case ReportView.days:
                default:
                    return  selectedQuery.GroupBy(g => new { g.ServiceType, g.CityId, g.Date.Date })
                           .Select(gr => new SaleReporting {
                               ServiceType = gr.Key.ServiceType,
                               Date = gr.Key.Date,
                               City = gr.Key.CityId,
                               Providers = gr.Select(p => p.ProviderId).Distinct().Count(),
                               Transactions = gr.Count(),
                               BaseAmount = gr.Sum(m => m.BaseAmount),
                               ProviderAmount = gr.Sum(m => m.ProviderAmount),
                               ClientAmount = gr.Sum(m => m.ClientAmount),
                               Commission = gr.Sum(m => m.BaseAmount * m.Commission)
                           }).ToList();
            }
        }

        public async Task<SaleReporting> GetSaleReportingDetails(long ClientId, DateTime date, DateTime from, DateTime to, ServiceTypeEnum serviceType, int city, ReportView view) {
            var query = _dbContext.Booking
                 .Include(s => s.Transaction)
                 .Include(s => s.Transaction.Service)
                 .Where(s =>
                 s.Transaction.Status == TransactionStatus.Confirmed &&
                 s.CheckOutDate.Date <= DateTime.Now.Date &&
                 s.Transaction.Date.Date >= from.Date &&
                 s.Transaction.Date.Date <= to.Date &&
                 s.Transaction.Service.ServiceType == serviceType &&
                 s.Transaction.Service.CityId == city);

            if (ClientId != 0)
                query = query.Where(e => e.Transaction.ClientId == ClientId);
            var selectedQuery =await query.Select(s => new {
                s.Transaction.Service.ServiceType,
                s.Transaction.Service.CityId,
                s.Transaction.Date,
                s.Transaction.Service.ProviderId,
                s.Transaction.BaseAmount,
                s.Transaction.ProviderAmount,
                s.Transaction.ClientAmount,
                s.Transaction.Commission
            })
            // to solve EF core bug we grab the result to list the use Group by 
            // just follow EF if the solve the following error
            // A column has been specified more than once in the order by list. Columns in the order by list must be unique.
            // problem: group by generate order by with repeated fields
            .ToListAsync();
            switch (view) {
                case ReportView.years:
                    return  selectedQuery
                        .Where(d => d.Date.Year == date.Year)
                        .GroupBy(s => new { s.ServiceType, s.CityId, s.Date.Year })
                           .Select(s => new SaleReporting {
                               ServiceType = s.Key.ServiceType,
                               Date = new DateTime(s.Key.Year, 1, 1),
                               City = s.Key.CityId,
                               Providers = s.Select(p => p.ProviderId).Distinct().Count(),
                               Transactions = s.Count(),
                               BaseAmount = s.Sum(m => m.BaseAmount),
                               ProviderAmount = s.Sum(m => m.ProviderAmount),
                               ClientAmount = s.Sum(m => m.ClientAmount),
                               Commission = s.Sum(m => m.BaseAmount * m.Commission)
                           }).FirstOrDefault();
                case ReportView.months:
                    return  selectedQuery
                        .Where(d => d.Date.Year == date.Year && d.Date.Month == date.Month)
                        .GroupBy(s => new { s.ServiceType, s.CityId, s.Date.Year, s.Date.Month })
                           .Select(s => new SaleReporting {
                               ServiceType = s.Key.ServiceType,
                               Date = new DateTime(s.Key.Year, s.Key.Month, 1),
                               City = s.Key.CityId,
                               Providers = s.Select(p => p.ProviderId).Distinct().Count(),
                               Transactions = s.Count(),
                               BaseAmount = s.Sum(m => m.BaseAmount),
                               ProviderAmount = s.Sum(m => m.ProviderAmount),
                               ClientAmount = s.Sum(m => m.ClientAmount),
                               Commission = s.Sum(m => m.BaseAmount * m.Commission)
                           }).FirstOrDefault();
                case ReportView.weeks:
                    return  selectedQuery
                         .Where(d => d.Date.Date >= date.Date && d.Date.Date <= date.AddDays(7))
                        .GroupBy(s => new {
                            s.ServiceType,
                            s.CityId,
                            s.Date.Year,
                            //SqlFunctions.DatePart("week", s.Date)
                            week = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(s.Date, CalendarWeekRule.FirstDay, DayOfWeek.Monday)
                        })
                           .Select(s => new SaleReporting {
                               ServiceType = s.Key.ServiceType,
                               Date = new DateTime(s.Key.Year, 1, 1).AddDays((s.Key.week - 1) * 7),
                               City = s.Key.CityId,
                               Providers = s.Select(p => p.ProviderId).Distinct().Count(),
                               Transactions = s.Count(),
                               BaseAmount = s.Sum(m => m.BaseAmount),
                               ProviderAmount = s.Sum(m => m.ProviderAmount),
                               ClientAmount = s.Sum(m => m.ClientAmount),
                               Commission = s.Sum(m => m.BaseAmount * m.Commission)
                           }).FirstOrDefault();
                case ReportView.days:
                default:
                    return  selectedQuery
                         .Where(d => d.Date.Date == date.Date)
                        .GroupBy(s => new { s.ServiceType, s.CityId, s.Date.Date })
                           .Select(s => new SaleReporting {
                               ServiceType = s.Key.ServiceType,
                               Date = s.Key.Date,
                               City = s.Key.CityId,
                               Providers = s.Select(p => p.ProviderId).Distinct().Count(),
                               Transactions = s.Count(),
                               BaseAmount = s.Sum(m => m.BaseAmount),
                               ProviderAmount = s.Sum(m => m.ProviderAmount),
                               ClientAmount = s.Sum(m => m.ClientAmount),
                               Commission = s.Sum(m => m.BaseAmount * m.Commission)
                           }).FirstOrDefault();
            }
        }

        public async Task<int> GetTotalConfirmed() {
            return await _dbContext.Transactions
               .Where(s => s.Status == TransactionStatus.Confirmed).CountAsync();
        }

        public async Task<int> GetTotalCancelled() {
            return await _dbContext.Transactions
               .Where(s => s.Status == TransactionStatus.Cancelled).CountAsync();
        }

        public async Task<int> GetTotalNoShow() {
            return await _dbContext.Transactions
               .Where(s => s.Status == TransactionStatus.NoShow).CountAsync();
        }
    }

}
