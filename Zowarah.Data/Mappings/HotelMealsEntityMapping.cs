﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.Data.Mappings
{
    public class HotelMealsEntityMapping : IEntityTypeConfiguration<HotelMeals>
    {
        public void Configure(EntityTypeBuilder<HotelMeals> builder)
        {
              builder.HasKey(HotelMeals => new { HotelMeals.HotelId });
        }
    }
}
