﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.Data.Mappings
{
    public class HotelPromotionRoomEntityMapping : IEntityTypeConfiguration<HotelPromotionRoom>
    {
        public void Configure(EntityTypeBuilder<HotelPromotionRoom> builder)
        {
            builder.HasKey(HotelPromotionRoom => new { HotelPromotionRoom.HotelPromotionId, HotelPromotionRoom.RoomId });
        }
    }
}
