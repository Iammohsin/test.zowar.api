﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.Data.Mappings
{
    public class LocalizationEntityMapping : IEntityTypeConfiguration<Localization>
    {
        public void Configure(EntityTypeBuilder<Localization> builder)
        {
            builder.HasKey(Localization => new { Localization.LanguageId, Localization.Id });
        }
    }
}
