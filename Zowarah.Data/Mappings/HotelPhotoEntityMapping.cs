﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.Data.Mappings
{
    public class HotelPhotoEntityMapping : IEntityTypeConfiguration<HotelPhoto>
    {
        public void Configure(EntityTypeBuilder<HotelPhoto> builder)
        {
            builder.HasKey(HotelPhoto => new {  HotelPhoto.Id });
        }
    }
}
