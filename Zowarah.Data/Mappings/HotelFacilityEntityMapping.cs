﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.Data.Mappings
{
    public class HotelFacilityEntityMapping : IEntityTypeConfiguration<HotelFacility>
    {
        public void Configure(EntityTypeBuilder<HotelFacility> builder)
        {
              builder.HasKey(HotelFacility => new { HotelFacility.HotelId, HotelFacility.FacilityId});
        }
    }
}
