﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.Data.Mappings
{
    class HotelRoomAmenityEntityMapping : IEntityTypeConfiguration<HotelRoomAmenity>
    {
        public void Configure(EntityTypeBuilder<HotelRoomAmenity> builder)
        {
            builder.HasKey(HotelRoomAmenity => new { HotelRoomAmenity.HotelId, HotelRoomAmenity.RoomAmenityId, HotelRoomAmenity.RoomId });
        }
    }
}
