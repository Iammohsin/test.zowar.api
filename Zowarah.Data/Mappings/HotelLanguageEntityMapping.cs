﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.Data.Mappings
{
    public class HotelLanguageEntityMapping : IEntityTypeConfiguration<HotelLanguage>
    {
        public void Configure(EntityTypeBuilder<HotelLanguage> builder)
        {
            builder.HasKey(e => new { e.LanguageId, e.HotelId });
        }
    }
}
