﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.Data.Mappings
{
    class HotelReviewFeatureRateEntityMapping : IEntityTypeConfiguration<ServiceReviewFeaturesScore>
    {
        public void Configure(EntityTypeBuilder<ServiceReviewFeaturesScore> builder)
        {
            builder.HasKey(e => new { e.ReviewFeatureTypeId, e.ServiceId });
        }
    }
}
