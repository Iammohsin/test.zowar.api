﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.Data.Mappings
{
    public class HotelCreditCardEntityMapping : IEntityTypeConfiguration<HotelCreditCard>
    {
        public void Configure(EntityTypeBuilder<HotelCreditCard> builder)
        {
            builder.HasKey(HotelCreditCard => new { HotelCreditCard.HotelId, HotelCreditCard.CreditCardId });
        }
    }
}
