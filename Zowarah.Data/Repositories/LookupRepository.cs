﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities;
using Zowarah.Domain.Interfaces;
using Microsoft.AspNetCore.Http;
using Zowarah.Domain.Entities.Common;

namespace Zowarah.Data.Repositories
{
    public class LookupRepository<Entity> : Repository<Entity>,ILookupRepository<Entity> where Entity : LookupBaseEntity, new()
    {
        private new readonly ApplicationDbContext _dbContext;
       // public  readonly HttpContext _httpContext;
        public new readonly DbSet<Entity> _dbSet;
        public string culture;
        public LookupRepository(
            ApplicationDbContext dbContext,
            IHttpContextAccessor _httpContextAccessor
                                    ) :base(dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<Entity>();
            var val = _httpContextAccessor.HttpContext.Session.GetString("culture");
            culture = val != null ? val : "en";
        }

        public new async Task<IList<Entity>> GetAll()
        {
            return await _dbSet.ToListAsync();
           // ToDo: Fix localization problem
            //var query = (from m in _dbSet
            //             join l in _dbContext.Localization
            //             on m.Description equals l.Id
            //             where l.LanguageId == culture
            //             select new Entity
            //             {
            //                 Id = m.Id,
            //                 Description = l.LocText
            //             }
            //            );
            //return await query.ToListAsync();
        }

         public new Task<Entity> GetById(object id)
        {
            //throw new NotImplementedException();
            return null;
        }
   
    }
}
