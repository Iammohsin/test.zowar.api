﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities;
using Zowarah.Domain.Interfaces;

namespace Zowarah.Data.Repositories
{
    public class Repository<Entity> : IRepository<Entity> where Entity : BaseEntity
    {
        public readonly ApplicationDbContext _dbContext;
        public readonly DbSet<Entity> _dbSet;

        public Repository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<Entity>();
        }

        public void Create(Entity entity)
        {
            entity.CreatedDate = DateTime.Now.Date;
            _dbContext.AddAsync<Entity>(entity);
        }

        public void Delete(Entity entity)
        {
            entity.IsDeleted = true;
            entity.DeletedOn = DateTime.Now.Date;
        }

        public void RealDelete(Entity entity) {
            _dbContext.Remove(entity);            
        }

        public IQueryable<Entity> FindBy(System.Linq.Expressions.Expression<Func<Entity, bool>> predicate, params Expression<Func<Entity, object>>[] includes)
        {
            IQueryable<Entity> query = _dbSet.Where(predicate);
            /*foreach (var property in includes)
            {
                query =  query.Include(property);
            }*/
            return includes.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }

        public async Task<ICollection<Entity>> GetAll()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<Entity> GetById(object id)
        {
            return await _dbSet.FindAsync(id);
           // return includes.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }

        public void Update(Entity entity)
        {
            _dbContext.Update(entity);
        }

        public async Task ExecSql(string sql)
        {
            await _dbContext.Database.ExecuteSqlCommandAsync(sql);
        }
    }
}
