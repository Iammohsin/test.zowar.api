﻿IF OBJECT_ID(N'__EFMigrationsHistory') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [LookUpModels] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_LookUpModels] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [LookUpTables] (
    [Id] nvarchar(450) NOT NULL,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [LookUpModelId] int NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_LookUpTables] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_LookUpTables_LookUpModels_LookUpModelId] FOREIGN KEY ([LookUpModelId]) REFERENCES [LookUpModels] ([Id]) ON DELETE CASCADE
);

GO

CREATE INDEX [IX_LookUpTables_LookUpModelId] ON [LookUpTables] ([LookUpModelId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20180509010431_lookUpTables', N'2.0.1-rtm-125');

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20180509014319_lookUpsTablesName', N'2.0.1-rtm-125');

GO

CREATE TABLE [AppPermissions] (
    [Id] bigint NOT NULL IDENTITY,
    CONSTRAINT [PK_AppPermissions] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [AspNetRoles] (
    [Id] bigint NOT NULL IDENTITY,
    [ConcurrencyStamp] nvarchar(max) NULL,
    [Name] nvarchar(256) NULL,
    [NormalizedName] nvarchar(256) NULL,
    CONSTRAINT [PK_AspNetRoles] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [AspNetUsers] (
    [Id] bigint NOT NULL IDENTITY,
    [AccessFailedCount] int NOT NULL,
    [Address] nvarchar(max) NULL,
    [Birthday] datetime2 NULL,
    [CitizenshipCountryId] int NULL,
    [CityId] int NULL,
    [ConcurrencyStamp] nvarchar(max) NULL,
    [CountryId] int NULL,
    [CultureId] nvarchar(max) NULL,
    [CurrencyId] nvarchar(max) NULL,
    [DisplayCountryId] int NULL,
    [DisplayName] nvarchar(max) NULL,
    [Email] nvarchar(256) NULL,
    [EmailConfirmed] bit NOT NULL,
    [FirstName] nvarchar(max) NULL,
    [Gender] int NULL,
    [IsActive] bit NOT NULL,
    [JoinDate] datetime2 NOT NULL,
    [LastName] nvarchar(max) NULL,
    [LockoutEnabled] bit NOT NULL,
    [LockoutEnd] datetimeoffset NULL,
    [MobilePhoneNo] nvarchar(max) NULL,
    [NormalizedEmail] nvarchar(256) NULL,
    [NormalizedUserName] nvarchar(256) NULL,
    [PassportExpiryDate] datetime2 NULL,
    [PassportNo] nvarchar(max) NULL,
    [PasswordHash] nvarchar(max) NULL,
    [PhoneNumber] nvarchar(max) NULL,
    [PhoneNumberConfirmed] bit NOT NULL,
    [PostalCode] nvarchar(max) NULL,
    [Rating] int NOT NULL,
    [SecurityStamp] nvarchar(max) NULL,
    [SmokingOption] int NOT NULL,
    [Title] int NULL,
    [TwoFactorEnabled] bit NOT NULL,
    [UserName] nvarchar(256) NULL,
    CONSTRAINT [PK_AspNetUsers] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [BreakfastOptions] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_BreakfastOptions] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [BreakfastTypes] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_BreakfastTypes] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [CancellationFees] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_CancellationFees] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [CancellationPolicies] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [NumberOfDays] int NOT NULL,
    [NumberOfHours] int NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_CancellationPolicies] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Cities] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [Name] nvarchar(max) NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_Cities] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [CreditCards] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_CreditCards] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Currencies] (
    [Id] nvarchar(450) NOT NULL,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [ExchangeRate] decimal(18, 2) NOT NULL,
    [IsDeleted] bit NOT NULL,
    [Name] nvarchar(max) NULL,
    [Symbol] nvarchar(max) NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_Currencies] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [FacilityCategories] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [Icon] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_FacilityCategories] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [HotelFeaturesDescriptions] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [Feature] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [Order] int NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_HotelFeaturesDescriptions] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [HotelPromotions] (
    [Id] bigint NOT NULL IDENTITY,
    [AudienceId] int NOT NULL,
    [CreatedDate] datetime2 NOT NULL,
    [Date] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Discount] real NOT NULL,
    [FromDate] datetime2 NOT NULL,
    [IsActive] bit NOT NULL,
    [IsDeleted] bit NOT NULL,
    [MinimumStay] int NOT NULL,
    [Name] nvarchar(max) NULL,
    [ServiceId] bigint NOT NULL,
    [ToDate] datetime2 NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_HotelPromotions] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [InternetOptions] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_InternetOptions] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [InvoiceChecks] (
    [Id] nvarchar(450) NOT NULL,
    [Amount] decimal(18, 2) NOT NULL,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [IsDeleted] bit NOT NULL,
    [Month] nvarchar(max) NULL,
    [NoOfInvoices] int NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    [Year] nvarchar(max) NULL,
    CONSTRAINT [PK_InvoiceChecks] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Languages] (
    [Id] nvarchar(450) NOT NULL,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [Name] nvarchar(max) NULL,
    [NativeName] nvarchar(max) NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_Languages] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Localization] (
    [CultureId] nvarchar(450) NOT NULL,
    [LocKey] nvarchar(450) NOT NULL,
    [LocText] nvarchar(max) NULL,
    CONSTRAINT [PK_Localization] PRIMARY KEY ([CultureId], [LocKey])
);

GO

CREATE TABLE [ParkingOptions] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_ParkingOptions] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [ParkingReservations] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_ParkingReservations] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [ParkingSite] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_ParkingSite] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [ParkingTypes] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_ParkingTypes] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [PropertyTypes] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_PropertyTypes] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [ReviewFeatureTypes] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [ServiceTypeId] int NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_ReviewFeatureTypes] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [RoomAmenityCategories] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_RoomAmenityCategories] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [RoomTypes] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_RoomTypes] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [ServiceNearbyTypes] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_ServiceNearbyTypes] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [ServiceStatuses] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_ServiceStatuses] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [ServiceTypes] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_ServiceTypes] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Settings] (
    [Id] nvarchar(450) NOT NULL,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    [Value] nvarchar(max) NULL,
    CONSTRAINT [PK_Settings] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [SmokingOptions] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_SmokingOptions] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Tests] (
    [Id] bigint NOT NULL IDENTITY,
    CONSTRAINT [PK_Tests] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [AspNetRoleClaims] (
    [Id] int NOT NULL IDENTITY,
    [ClaimType] nvarchar(max) NULL,
    [ClaimValue] nvarchar(max) NULL,
    [RoleId] bigint NOT NULL,
    CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [AspNetUserClaims] (
    [Id] int NOT NULL IDENTITY,
    [ClaimType] nvarchar(max) NULL,
    [ClaimValue] nvarchar(max) NULL,
    [UserId] bigint NOT NULL,
    CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [AspNetUserLogins] (
    [LoginProvider] nvarchar(450) NOT NULL,
    [ProviderKey] nvarchar(450) NOT NULL,
    [ProviderDisplayName] nvarchar(max) NULL,
    [UserId] bigint NOT NULL,
    CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY ([LoginProvider], [ProviderKey]),
    CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [AspNetUserRoles] (
    [UserId] bigint NOT NULL,
    [RoleId] bigint NOT NULL,
    CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY ([UserId], [RoleId]),
    CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [AspNetUserTokens] (
    [UserId] bigint NOT NULL,
    [LoginProvider] nvarchar(450) NOT NULL,
    [Name] nvarchar(450) NOT NULL,
    [Value] nvarchar(max) NULL,
    CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY ([UserId], [LoginProvider], [Name]),
    CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [ProfilePhotos] (
    [Id] bigint NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [FileName] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    [UserId] bigint NOT NULL,
    CONSTRAINT [PK_ProfilePhotos] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_ProfilePhotos_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [UsersPermissions] (
    [UserId] bigint NOT NULL,
    [PermissionId] bigint NOT NULL,
    CONSTRAINT [PK_UsersPermissions] PRIMARY KEY ([UserId], [PermissionId]),
    CONSTRAINT [FK_UsersPermissions_AppPermissions_PermissionId] FOREIGN KEY ([PermissionId]) REFERENCES [AppPermissions] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UsersPermissions_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Services] (
    [Id] bigint NOT NULL IDENTITY,
    [CityId] int NOT NULL,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [IsActive] bit NOT NULL,
    [IsDeleted] bit NOT NULL,
    [Name] nvarchar(max) NULL,
    [ProviderId] bigint NOT NULL,
    [ReviewScore] real NOT NULL,
    [ServiceType] int NOT NULL,
    [Status] int NOT NULL,
    [TotalReviews] decimal(18, 2) NOT NULL,
    [TotalScore] decimal(18, 2) NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_Services] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Services_Cities_CityId] FOREIGN KEY ([CityId]) REFERENCES [Cities] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Services_AspNetUsers_ProviderId] FOREIGN KEY ([ProviderId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [Countries] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [CurrencyId] nvarchar(450) NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [DialingCode] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [IsoCode] nvarchar(max) NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_Countries] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Countries_Currencies_CurrencyId] FOREIGN KEY ([CurrencyId]) REFERENCES [Currencies] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Facilities] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [FacilityCategoryId] int NOT NULL,
    [Icon] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_Facilities] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Facilities_FacilityCategories_FacilityCategoryId] FOREIGN KEY ([FacilityCategoryId]) REFERENCES [FacilityCategories] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [RoomAmenities] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [Icon] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [RoomAmenityCategoryId] int NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_RoomAmenities] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_RoomAmenities_RoomAmenityCategories_RoomAmenityCategoryId] FOREIGN KEY ([RoomAmenityCategoryId]) REFERENCES [RoomAmenityCategories] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [RoomTypeNames] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [RoomTypeId] int NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_RoomTypeNames] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_RoomTypeNames_RoomTypes_RoomTypeId] FOREIGN KEY ([RoomTypeId]) REFERENCES [RoomTypes] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [ServiceNearbyCategories] (
    [Id] int NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Description] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [ServiceNearbyTypeId] int NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_ServiceNearbyCategories] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_ServiceNearbyCategories_ServiceNearbyTypes_ServiceNearbyTypeId] FOREIGN KEY ([ServiceNearbyTypeId]) REFERENCES [ServiceNearbyTypes] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [Hotels] (
    [Id] bigint NOT NULL IDENTITY,
    [Address] nvarchar(max) NULL,
    [BreakFastOptionId] int NULL,
    [CancellationFeeId] int NULL,
    [CancellationPolicyId] int NULL,
    [CheckInFrom] tinyint NOT NULL,
    [CheckInTo] tinyint NOT NULL,
    [CheckOutFrom] tinyint NOT NULL,
    [CheckOutTo] tinyint NOT NULL,
    [CityId] int NULL,
    [Contact1] nvarchar(max) NULL,
    [Contact2] nvarchar(max) NULL,
    [ContactName] nvarchar(max) NULL,
    [CreatedDate] datetime2 NOT NULL,
    [CurrencyId] nvarchar(max) NULL,
    [DeletedOn] datetime2 NOT NULL,
    [InternetOptionId] int NULL,
    [InvoicingAddress] nvarchar(max) NULL,
    [InvoicingCityId] int NOT NULL,
    [InvoicingCountryId] int NOT NULL,
    [InvoicingPostalAddress] nvarchar(max) NULL,
    [InvoicingRecipientName] nvarchar(max) NULL,
    [IsBasicInfoConfigured] bit NOT NULL,
    [IsChildrenAllowed] bit NOT NULL,
    [IsCreditCardAccepted] bit NOT NULL,
    [IsDeleted] bit NOT NULL,
    [IsFacilitiesConfigured] bit NOT NULL,
    [IsFoodServed] bit NOT NULL,
    [IsInvoicingHotelAddress] bit NOT NULL,
    [IsNearbyConfigured] bit NOT NULL,
    [IsPaymentPolicyConfigured] bit NOT NULL,
    [IsPetsAllowed] bit NOT NULL,
    [IsPhotosConfigured] bit NOT NULL,
    [IsPrePayment] bit NOT NULL,
    [IsProfileConfigured] bit NOT NULL,
    [IsPropertyDetailsConfigured] bit NOT NULL,
    [IsRoomAmenitiesConfigured] bit NOT NULL,
    [IsRoomTypesConfigured] bit NOT NULL,
    [IsSubmitted] bit NOT NULL,
    [LocLat] decimal(25, 23) NOT NULL,
    [LocLng] decimal(25, 23) NOT NULL,
    [Name] nvarchar(max) NULL,
    [ParkingOptionId] int NULL,
    [ParkingReservationId] int NULL,
    [ParkingSiteId] int NULL,
    [ParkingTypeId] int NULL,
    [PropertyTypeId] int NOT NULL,
    [ProviderId] bigint NOT NULL,
    [Rating] int NOT NULL,
    [Room] int NULL,
    [SmokingOptionId] int NOT NULL,
    [Status] int NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    [noOfRooms] int NOT NULL,
    CONSTRAINT [PK_Hotels] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Hotels_BreakfastOptions_BreakFastOptionId] FOREIGN KEY ([BreakFastOptionId]) REFERENCES [BreakfastOptions] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Hotels_CancellationFees_CancellationFeeId] FOREIGN KEY ([CancellationFeeId]) REFERENCES [CancellationFees] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Hotels_CancellationPolicies_CancellationPolicyId] FOREIGN KEY ([CancellationPolicyId]) REFERENCES [CancellationPolicies] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Hotels_InternetOptions_InternetOptionId] FOREIGN KEY ([InternetOptionId]) REFERENCES [InternetOptions] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Hotels_ParkingOptions_ParkingOptionId] FOREIGN KEY ([ParkingOptionId]) REFERENCES [ParkingOptions] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Hotels_ParkingReservations_ParkingReservationId] FOREIGN KEY ([ParkingReservationId]) REFERENCES [ParkingReservations] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Hotels_ParkingSite_ParkingSiteId] FOREIGN KEY ([ParkingSiteId]) REFERENCES [ParkingSite] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Hotels_ParkingTypes_ParkingTypeId] FOREIGN KEY ([ParkingTypeId]) REFERENCES [ParkingTypes] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Hotels_PropertyTypes_PropertyTypeId] FOREIGN KEY ([PropertyTypeId]) REFERENCES [PropertyTypes] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Hotels_SmokingOptions_SmokingOptionId] FOREIGN KEY ([SmokingOptionId]) REFERENCES [SmokingOptions] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [Invoices] (
    [Id] nvarchar(450) NOT NULL,
    [Amount] decimal(18, 2) NOT NULL,
    [CreatedDate] datetime2 NOT NULL,
    [Date] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [DueDate] datetime2 NOT NULL,
    [IsDeleted] bit NOT NULL,
    [ServiceId] bigint NOT NULL,
    [Status] int NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_Invoices] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Invoices_Services_ServiceId] FOREIGN KEY ([ServiceId]) REFERENCES [Services] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [ServiceProfiles] (
    [Id] bigint NOT NULL IDENTITY,
    [AboutNeighborhood] nvarchar(max) NULL,
    [AboutProperty] nvarchar(max) NULL,
    [AboutStaff] nvarchar(max) NULL,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [IsDeleted] bit NOT NULL,
    [Language] nvarchar(max) NULL,
    [PropertyName] nvarchar(max) NULL,
    [ServiceId] bigint NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_ServiceProfiles] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_ServiceProfiles_Services_ServiceId] FOREIGN KEY ([ServiceId]) REFERENCES [Services] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [ServiceReviewFeaturesScores] (
    [ReviewFeatureTypeId] int NOT NULL,
    [ServiceId] bigint NOT NULL,
    [Score] real NOT NULL,
    [TotalReviews] decimal(18, 2) NOT NULL,
    [TotalScore] decimal(18, 2) NOT NULL,
    CONSTRAINT [PK_ServiceReviewFeaturesScores] PRIMARY KEY ([ReviewFeatureTypeId], [ServiceId]),
    CONSTRAINT [FK_ServiceReviewFeaturesScores_Services_ServiceId] FOREIGN KEY ([ServiceId]) REFERENCES [Services] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [ServiceStatusLog] (
    [Id] bigint NOT NULL IDENTITY,
    [Comments] nvarchar(max) NULL,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [IsDeleted] bit NOT NULL,
    [ServiceId] bigint NULL,
    [Status] int NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_ServiceStatusLog] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_ServiceStatusLog_Services_ServiceId] FOREIGN KEY ([ServiceId]) REFERENCES [Services] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [ServiceNearbies] (
    [Id] bigint NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Distance] int NOT NULL,
    [IsDeleted] bit NOT NULL,
    [Name] nvarchar(max) NULL,
    [ServiceId] bigint NOT NULL,
    [ServiceNearbyCategoryId] int NOT NULL,
    [Unit] nvarchar(max) NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_ServiceNearbies] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_ServiceNearbies_Services_ServiceId] FOREIGN KEY ([ServiceId]) REFERENCES [Services] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_ServiceNearbies_ServiceNearbyCategories_ServiceNearbyCategoryId] FOREIGN KEY ([ServiceNearbyCategoryId]) REFERENCES [ServiceNearbyCategories] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [Calendar] (
    [Id] bigint NOT NULL IDENTITY,
    [CloseFrom] datetime2 NOT NULL,
    [CloseTo] datetime2 NOT NULL,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Fri] bit NOT NULL,
    [HotelId] bigint NOT NULL,
    [IsDeleted] bit NOT NULL,
    [Mon] bit NOT NULL,
    [OpenFrom] datetime2 NOT NULL,
    [OpenTo] datetime2 NOT NULL,
    [Sat] bit NOT NULL,
    [Sun] bit NOT NULL,
    [Thr] bit NOT NULL,
    [Tue] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    [Wed] bit NOT NULL,
    CONSTRAINT [PK_Calendar] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Calendar_Hotels_HotelId] FOREIGN KEY ([HotelId]) REFERENCES [Hotels] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [HotelCreditCards] (
    [HotelId] bigint NOT NULL,
    [CreditCardId] int NOT NULL,
    CONSTRAINT [PK_HotelCreditCards] PRIMARY KEY ([HotelId], [CreditCardId]),
    CONSTRAINT [FK_HotelCreditCards_CreditCards_CreditCardId] FOREIGN KEY ([CreditCardId]) REFERENCES [CreditCards] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_HotelCreditCards_Hotels_HotelId] FOREIGN KEY ([HotelId]) REFERENCES [Hotels] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [HotelFacilities] (
    [HotelId] bigint NOT NULL,
    [FacilityId] int NOT NULL,
    CONSTRAINT [PK_HotelFacilities] PRIMARY KEY ([HotelId], [FacilityId]),
    CONSTRAINT [FK_HotelFacilities_Facilities_FacilityId] FOREIGN KEY ([FacilityId]) REFERENCES [Facilities] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_HotelFacilities_Hotels_HotelId] FOREIGN KEY ([HotelId]) REFERENCES [Hotels] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [HotelLanguages] (
    [LanguageId] nvarchar(450) NOT NULL,
    [HotelId] bigint NOT NULL,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Id] bigint NOT NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_HotelLanguages] PRIMARY KEY ([LanguageId], [HotelId]),
    CONSTRAINT [FK_HotelLanguages_Hotels_HotelId] FOREIGN KEY ([HotelId]) REFERENCES [Hotels] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_HotelLanguages_Languages_LanguageId] FOREIGN KEY ([LanguageId]) REFERENCES [Languages] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [HotelMeals] (
    [HotelId] bigint NOT NULL,
    [BreakFast] bit NOT NULL,
    [BreakFastRate] decimal(18, 2) NOT NULL,
    [Dinner] bit NOT NULL,
    [DinnerRate] decimal(18, 2) NOT NULL,
    [Lunch] bit NOT NULL,
    [LunchRate] decimal(18, 2) NOT NULL,
    CONSTRAINT [PK_HotelMeals] PRIMARY KEY ([HotelId]),
    CONSTRAINT [FK_HotelMeals_Hotels_HotelId] FOREIGN KEY ([HotelId]) REFERENCES [Hotels] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [HotelPhotos] (
    [Id] bigint NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [FileName] nvarchar(max) NULL,
    [HotelId] bigint NOT NULL,
    [IsDeleted] bit NOT NULL,
    [IsMain] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_HotelPhotos] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_HotelPhotos_Hotels_HotelId] FOREIGN KEY ([HotelId]) REFERENCES [Hotels] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [HotelRoomAmenities] (
    [HotelId] bigint NOT NULL,
    [RoomAmenityId] int NOT NULL,
    [RoomId] bigint NOT NULL,
    CONSTRAINT [PK_HotelRoomAmenities] PRIMARY KEY ([HotelId], [RoomAmenityId], [RoomId]),
    CONSTRAINT [FK_HotelRoomAmenities_Hotels_HotelId] FOREIGN KEY ([HotelId]) REFERENCES [Hotels] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_HotelRoomAmenities_RoomAmenities_RoomAmenityId] FOREIGN KEY ([RoomAmenityId]) REFERENCES [RoomAmenities] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [Rooms] (
    [Id] bigint NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [HotelId] bigint NOT NULL,
    [IsDeleted] bit NOT NULL,
    [IsKitchen] bit NOT NULL,
    [IsLivingRoom] bit NOT NULL,
    [NoOfBathrooms] int NOT NULL,
    [NoOfPersons] int NOT NULL,
    [NoOfRooms] int NOT NULL,
    [RatePerNight] decimal(18, 2) NOT NULL,
    [RoomTypeNameId] int NOT NULL,
    [SmokingOptionId] int NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_Rooms] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Rooms_Hotels_HotelId] FOREIGN KEY ([HotelId]) REFERENCES [Hotels] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Rooms_RoomTypeNames_RoomTypeNameId] FOREIGN KEY ([RoomTypeNameId]) REFERENCES [RoomTypeNames] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [InvoiceStatusLog] (
    [Id] nvarchar(450) NOT NULL,
    [Comment] nvarchar(max) NULL,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [InvoiceId] nvarchar(450) NULL,
    [IsDeleted] bit NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_InvoiceStatusLog] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_InvoiceStatusLog_Invoices_InvoiceId] FOREIGN KEY ([InvoiceId]) REFERENCES [Invoices] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Payments] (
    [Id] nvarchar(450) NOT NULL,
    [AccountNo] nvarchar(max) NULL,
    [Bank] nvarchar(max) NULL,
    [CreatedDate] datetime2 NOT NULL,
    [Date] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [InvoiceId] nvarchar(450) NULL,
    [IsDeleted] bit NOT NULL,
    [Reference] nvarchar(max) NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_Payments] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Payments_Invoices_InvoiceId] FOREIGN KEY ([InvoiceId]) REFERENCES [Invoices] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Transactions] (
    [Id] nvarchar(450) NOT NULL,
    [CardExpiryMonth] tinyint NOT NULL,
    [CardExpiryYear] int NOT NULL,
    [CardNo] nvarchar(max) NULL,
    [ClientAmount] decimal(18, 2) NOT NULL,
    [ClientCurrency] nvarchar(max) NULL,
    [ClientId] bigint NOT NULL,
    [Commission] decimal(18, 2) NOT NULL,
    [CountryId] int NOT NULL,
    [CreatedDate] datetime2 NOT NULL,
    [CreditCardId] int NOT NULL,
    [Date] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Email] nvarchar(max) NULL,
    [ExchangeRate] decimal(18, 2) NOT NULL,
    [FirstName] nvarchar(max) NULL,
    [InvoiceId] nvarchar(450) NULL,
    [IsDeleted] bit NOT NULL,
    [LastName] nvarchar(max) NULL,
    [Mobile] nvarchar(max) NULL,
    [OwnerId] nvarchar(max) NULL,
    [ProviderAmount] decimal(18, 2) NOT NULL,
    [ProviderCurrency] nvarchar(max) NULL,
    [ServiceId] bigint NOT NULL,
    [Status] int NOT NULL,
    [Title] nvarchar(max) NULL,
    [TransactionType] int NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_Transactions] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Transactions_AspNetUsers_ClientId] FOREIGN KEY ([ClientId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Transactions_Invoices_InvoiceId] FOREIGN KEY ([InvoiceId]) REFERENCES [Invoices] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Transactions_Services_ServiceId] FOREIGN KEY ([ServiceId]) REFERENCES [Services] ([Id])
);

GO

CREATE TABLE [CalendarManual] (
    [Id] bigint NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [Date] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [HotelId] bigint NOT NULL,
    [IsDeleted] bit NOT NULL,
    [RoomId] bigint NOT NULL,
    [Rooms] int NULL,
    [Status] bit NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_CalendarManual] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_CalendarManual_Hotels_HotelId] FOREIGN KEY ([HotelId]) REFERENCES [Hotels] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_CalendarManual_Rooms_RoomId] FOREIGN KEY ([RoomId]) REFERENCES [Rooms] ([Id])
);

GO

CREATE TABLE [HotelPromotionRooms] (
    [HotelPromotionId] bigint NOT NULL,
    [RoomId] bigint NOT NULL,
    CONSTRAINT [PK_HotelPromotionRooms] PRIMARY KEY ([HotelPromotionId], [RoomId]),
    CONSTRAINT [FK_HotelPromotionRooms_HotelPromotions_HotelPromotionId] FOREIGN KEY ([HotelPromotionId]) REFERENCES [HotelPromotions] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_HotelPromotionRooms_Rooms_RoomId] FOREIGN KEY ([RoomId]) REFERENCES [Rooms] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [Booking] (
    [Id] nvarchar(450) NOT NULL,
    [ArrivalDate] datetime2 NOT NULL,
    [BaseRate] decimal(18, 2) NOT NULL,
    [CheckInDate] datetime2 NOT NULL,
    [CheckOutDate] datetime2 NOT NULL,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [Discount] decimal(18, 2) NOT NULL,
    [IsDeleted] bit NOT NULL,
    [IsPersonal] bit NOT NULL,
    [PromotionId] bigint NOT NULL,
    [RatePerNight] decimal(18, 2) NOT NULL,
    [RefereeEmail] nvarchar(max) NULL,
    [RefereeName] nvarchar(max) NULL,
    [RoomId] bigint NOT NULL,
    [Rooms] int NOT NULL,
    [SpecialRequest] nvarchar(max) NULL,
    [TransactionId] nvarchar(450) NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_Booking] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Booking_Rooms_RoomId] FOREIGN KEY ([RoomId]) REFERENCES [Rooms] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Booking_Transactions_TransactionId] FOREIGN KEY ([TransactionId]) REFERENCES [Transactions] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Reviews] (
    [Id] nvarchar(450) NOT NULL,
    [ClientId] nvarchar(max) NULL,
    [ClientId1] bigint NULL,
    [Comment] nvarchar(max) NULL,
    [CreatedDate] datetime2 NOT NULL,
    [Date] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [IsDeleted] bit NOT NULL,
    [IsPuplished] bit NOT NULL,
    [IsRead] bit NOT NULL,
    [ReplyComment] nvarchar(max) NULL,
    [ReplyDate] datetime2 NOT NULL,
    [Score] real NOT NULL,
    [ServiceId] bigint NOT NULL,
    [Tags] nvarchar(max) NULL,
    [Title] nvarchar(max) NULL,
    [TransactionId] nvarchar(450) NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_Reviews] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Reviews_AspNetUsers_ClientId1] FOREIGN KEY ([ClientId1]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Reviews_Services_ServiceId] FOREIGN KEY ([ServiceId]) REFERENCES [Services] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Reviews_Transactions_TransactionId] FOREIGN KEY ([TransactionId]) REFERENCES [Transactions] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [TransactionStatusLog] (
    [Id] nvarchar(450) NOT NULL,
    [Comment] nvarchar(max) NULL,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [IsDeleted] bit NOT NULL,
    [TransactionId] nvarchar(450) NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_TransactionStatusLog] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_TransactionStatusLog_Transactions_TransactionId] FOREIGN KEY ([TransactionId]) REFERENCES [Transactions] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [ReviewFeatures] (
    [Id] bigint NOT NULL IDENTITY,
    [CreatedDate] datetime2 NOT NULL,
    [DeletedOn] datetime2 NOT NULL,
    [IsDeleted] bit NOT NULL,
    [ReviewFeatureTypeId] int NOT NULL,
    [ReviewId] nvarchar(450) NULL,
    [Score] real NOT NULL,
    [UpdatedOn] datetime2 NOT NULL,
    CONSTRAINT [PK_ReviewFeatures] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_ReviewFeatures_ReviewFeatureTypes_ReviewFeatureTypeId] FOREIGN KEY ([ReviewFeatureTypeId]) REFERENCES [ReviewFeatureTypes] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_ReviewFeatures_Reviews_ReviewId] FOREIGN KEY ([ReviewId]) REFERENCES [Reviews] ([Id]) ON DELETE NO ACTION
);

GO


CREATE INDEX [IX_AspNetRoleClaims_RoleId] ON [AspNetRoleClaims] ([RoleId]);

GO

CREATE UNIQUE INDEX [RoleNameIndex] ON [AspNetRoles] ([NormalizedName]) WHERE [NormalizedName] IS NOT NULL;

GO

CREATE INDEX [IX_AspNetUserClaims_UserId] ON [AspNetUserClaims] ([UserId]);

GO

CREATE INDEX [IX_AspNetUserLogins_UserId] ON [AspNetUserLogins] ([UserId]);

GO

CREATE INDEX [IX_AspNetUserRoles_RoleId] ON [AspNetUserRoles] ([RoleId]);

GO

CREATE INDEX [EmailIndex] ON [AspNetUsers] ([NormalizedEmail]);

GO

CREATE UNIQUE INDEX [UserNameIndex] ON [AspNetUsers] ([NormalizedUserName]) WHERE [NormalizedUserName] IS NOT NULL;

GO

CREATE INDEX [IX_Booking_RoomId] ON [Booking] ([RoomId]);

GO

CREATE INDEX [IX_Booking_TransactionId] ON [Booking] ([TransactionId]);

GO

CREATE INDEX [IX_Calendar_HotelId] ON [Calendar] ([HotelId]);

GO

CREATE INDEX [IX_CalendarManual_HotelId] ON [CalendarManual] ([HotelId]);

GO

CREATE INDEX [IX_CalendarManual_RoomId] ON [CalendarManual] ([RoomId]);

GO

CREATE INDEX [IX_Countries_CurrencyId] ON [Countries] ([CurrencyId]);

GO

CREATE INDEX [IX_Facilities_FacilityCategoryId] ON [Facilities] ([FacilityCategoryId]);

GO

CREATE INDEX [IX_HotelCreditCards_CreditCardId] ON [HotelCreditCards] ([CreditCardId]);

GO

CREATE INDEX [IX_HotelFacilities_FacilityId] ON [HotelFacilities] ([FacilityId]);

GO

CREATE INDEX [IX_HotelLanguages_HotelId] ON [HotelLanguages] ([HotelId]);

GO

CREATE INDEX [IX_HotelPhotos_HotelId] ON [HotelPhotos] ([HotelId]);

GO

CREATE INDEX [IX_HotelPromotionRooms_RoomId] ON [HotelPromotionRooms] ([RoomId]);

GO

CREATE INDEX [IX_HotelRoomAmenities_RoomAmenityId] ON [HotelRoomAmenities] ([RoomAmenityId]);

GO

CREATE INDEX [IX_Hotels_BreakFastOptionId] ON [Hotels] ([BreakFastOptionId]);

GO

CREATE INDEX [IX_Hotels_CancellationFeeId] ON [Hotels] ([CancellationFeeId]);

GO

CREATE INDEX [IX_Hotels_CancellationPolicyId] ON [Hotels] ([CancellationPolicyId]);

GO

CREATE INDEX [IX_Hotels_InternetOptionId] ON [Hotels] ([InternetOptionId]);

GO

CREATE INDEX [IX_Hotels_ParkingOptionId] ON [Hotels] ([ParkingOptionId]);

GO

CREATE INDEX [IX_Hotels_ParkingReservationId] ON [Hotels] ([ParkingReservationId]);

GO

CREATE INDEX [IX_Hotels_ParkingSiteId] ON [Hotels] ([ParkingSiteId]);

GO

CREATE INDEX [IX_Hotels_ParkingTypeId] ON [Hotels] ([ParkingTypeId]);

GO

CREATE INDEX [IX_Hotels_PropertyTypeId] ON [Hotels] ([PropertyTypeId]);

GO

CREATE INDEX [IX_Hotels_SmokingOptionId] ON [Hotels] ([SmokingOptionId]);

GO

CREATE INDEX [IX_Invoices_ServiceId] ON [Invoices] ([ServiceId]);

GO

CREATE INDEX [IX_InvoiceStatusLog_InvoiceId] ON [InvoiceStatusLog] ([InvoiceId]);

GO

CREATE INDEX [IX_Payments_InvoiceId] ON [Payments] ([InvoiceId]);

GO

CREATE UNIQUE INDEX [IX_ProfilePhotos_UserId] ON [ProfilePhotos] ([UserId]);

GO

CREATE INDEX [IX_ReviewFeatures_ReviewFeatureTypeId] ON [ReviewFeatures] ([ReviewFeatureTypeId]);

GO

CREATE INDEX [IX_ReviewFeatures_ReviewId] ON [ReviewFeatures] ([ReviewId]);

GO

CREATE INDEX [IX_Reviews_ClientId1] ON [Reviews] ([ClientId1]);

GO

CREATE INDEX [IX_Reviews_ServiceId] ON [Reviews] ([ServiceId]);

GO

CREATE INDEX [IX_Reviews_TransactionId] ON [Reviews] ([TransactionId]);

GO

CREATE INDEX [IX_RoomAmenities_RoomAmenityCategoryId] ON [RoomAmenities] ([RoomAmenityCategoryId]);

GO

CREATE INDEX [IX_Rooms_HotelId] ON [Rooms] ([HotelId]);

GO

CREATE INDEX [IX_Rooms_RoomTypeNameId] ON [Rooms] ([RoomTypeNameId]);

GO

CREATE INDEX [IX_RoomTypeNames_RoomTypeId] ON [RoomTypeNames] ([RoomTypeId]);

GO

CREATE INDEX [IX_ServiceNearbies_ServiceId] ON [ServiceNearbies] ([ServiceId]);

GO

CREATE INDEX [IX_ServiceNearbies_ServiceNearbyCategoryId] ON [ServiceNearbies] ([ServiceNearbyCategoryId]);

GO

CREATE INDEX [IX_ServiceNearbyCategories_ServiceNearbyTypeId] ON [ServiceNearbyCategories] ([ServiceNearbyTypeId]);

GO

CREATE INDEX [IX_ServiceProfiles_ServiceId] ON [ServiceProfiles] ([ServiceId]);

GO

CREATE INDEX [IX_ServiceReviewFeaturesScores_ServiceId] ON [ServiceReviewFeaturesScores] ([ServiceId]);

GO

CREATE INDEX [IX_Services_CityId] ON [Services] ([CityId]);

GO

CREATE INDEX [IX_Services_ProviderId] ON [Services] ([ProviderId]);

GO

CREATE INDEX [IX_ServiceStatusLog_ServiceId] ON [ServiceStatusLog] ([ServiceId]);

GO

CREATE INDEX [IX_Transactions_ClientId] ON [Transactions] ([ClientId]);

GO

CREATE INDEX [IX_Transactions_InvoiceId] ON [Transactions] ([InvoiceId]);

GO

CREATE INDEX [IX_Transactions_ServiceId] ON [Transactions] ([ServiceId]);

GO

CREATE INDEX [IX_TransactionStatusLog_TransactionId] ON [TransactionStatusLog] ([TransactionId]);

GO

CREATE INDEX [IX_UsersPermissions_PermissionId] ON [UsersPermissions] ([PermissionId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20180512120100_init', N'2.0.1-rtm-125');

GO

ALTER TABLE [Reviews] DROP CONSTRAINT [FK_Reviews_AspNetUsers_ClientId1];

GO

DROP INDEX [IX_Reviews_ClientId1] ON [Reviews];

GO

DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'Reviews') AND [c].[name] = N'ClientId1');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Reviews] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Reviews] DROP COLUMN [ClientId1];

GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'Languages') AND [c].[name] = N'Description');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [Languages] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [Languages] DROP COLUMN [Description];

GO

DECLARE @var2 sysname;
SELECT @var2 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'Reviews') AND [c].[name] = N'ClientId');
IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [Reviews] DROP CONSTRAINT [' + @var2 + '];');
ALTER TABLE [Reviews] ALTER COLUMN [ClientId] bigint NOT NULL;

GO

CREATE INDEX [IX_Reviews_ClientId] ON [Reviews] ([ClientId]);

GO

ALTER TABLE [Reviews] ADD CONSTRAINT [FK_Reviews_AspNetUsers_ClientId] FOREIGN KEY ([ClientId]) REFERENCES [AspNetUsers] ([Id]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20180513103721_languages', N'2.0.1-rtm-125');

GO

ALTER TABLE [HotelPhotos] DROP CONSTRAINT [PK_HotelPhotos];

GO

DECLARE @var3 sysname;
SELECT @var3 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'HotelPhotos') AND [c].[name] = N'Id');
IF @var3 IS NOT NULL EXEC(N'ALTER TABLE [HotelPhotos] DROP CONSTRAINT [' + @var3 + '];');
ALTER TABLE [HotelPhotos] DROP COLUMN [Id];

GO

DECLARE @var4 sysname;
SELECT @var4 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'HotelPhotos') AND [c].[name] = N'CreatedDate');
IF @var4 IS NOT NULL EXEC(N'ALTER TABLE [HotelPhotos] DROP CONSTRAINT [' + @var4 + '];');
ALTER TABLE [HotelPhotos] DROP COLUMN [CreatedDate];

GO

DECLARE @var5 sysname;
SELECT @var5 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'HotelPhotos') AND [c].[name] = N'DeletedOn');
IF @var5 IS NOT NULL EXEC(N'ALTER TABLE [HotelPhotos] DROP CONSTRAINT [' + @var5 + '];');
ALTER TABLE [HotelPhotos] DROP COLUMN [DeletedOn];

GO

DECLARE @var6 sysname;
SELECT @var6 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'HotelPhotos') AND [c].[name] = N'IsDeleted');
IF @var6 IS NOT NULL EXEC(N'ALTER TABLE [HotelPhotos] DROP CONSTRAINT [' + @var6 + '];');
ALTER TABLE [HotelPhotos] DROP COLUMN [IsDeleted];

GO

DECLARE @var7 sysname;
SELECT @var7 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'HotelPhotos') AND [c].[name] = N'UpdatedOn');
IF @var7 IS NOT NULL EXEC(N'ALTER TABLE [HotelPhotos] DROP CONSTRAINT [' + @var7 + '];');
ALTER TABLE [HotelPhotos] DROP COLUMN [UpdatedOn];

GO

DECLARE @var8 sysname;
SELECT @var8 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'HotelPhotos') AND [c].[name] = N'FileName');
IF @var8 IS NOT NULL EXEC(N'ALTER TABLE [HotelPhotos] DROP CONSTRAINT [' + @var8 + '];');
ALTER TABLE [HotelPhotos] ALTER COLUMN [FileName] nvarchar(450) NOT NULL;

GO

ALTER TABLE [HotelPhotos] ADD CONSTRAINT [PK_HotelPhotos] PRIMARY KEY ([FileName]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20180514011902_removeHotelPhotosId', N'2.0.1-rtm-125');

GO

EXEC sp_rename N'HotelPhotos.FileName', N'Id', N'COLUMN';

GO

ALTER TABLE [HotelPhotos] ADD [CreatedDate] datetime2 NOT NULL DEFAULT '0001-01-01T00:00:00.000';

GO

ALTER TABLE [HotelPhotos] ADD [DeletedOn] datetime2 NOT NULL DEFAULT '0001-01-01T00:00:00.000';

GO

ALTER TABLE [HotelPhotos] ADD [IsDeleted] bit NOT NULL DEFAULT 0;

GO

ALTER TABLE [HotelPhotos] ADD [UpdatedOn] datetime2 NOT NULL DEFAULT '0001-01-01T00:00:00.000';

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20180514014413_updateHotelPhotos', N'2.0.1-rtm-125');

GO

EXEC sp_rename N'Localization.LocKey', N'Id', N'COLUMN';

GO

EXEC sp_rename N'Localization.CultureId', N'LanguageId', N'COLUMN';

GO

ALTER TABLE [Localization] ADD [CreatedDate] datetime2 NOT NULL DEFAULT '0001-01-01T00:00:00.000';

GO

ALTER TABLE [Localization] ADD [DeletedOn] datetime2 NOT NULL DEFAULT '0001-01-01T00:00:00.000';

GO

ALTER TABLE [Localization] ADD [IsDeleted] bit NOT NULL DEFAULT 0;

GO

ALTER TABLE [Localization] ADD [UpdatedOn] datetime2 NOT NULL DEFAULT '0001-01-01T00:00:00.000';

GO

ALTER TABLE [Localization] ADD CONSTRAINT [FK_Localization_Languages_LanguageId] FOREIGN KEY ([LanguageId]) REFERENCES [Languages] ([Id]) ON DELETE CASCADE;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20180515011403_localization', N'2.0.1-rtm-125');

GO

