﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Zowarah.Data.Mappings;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Invoices;
using Zowarah.Domain.Entities.Reviews;
using Zowarah.Domain.Entities.Transactions;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.Data.Contexts
{
    public class ApplicationDbContext : IdentityDbContext<AppUser, AppRole, long>
    {
        public static readonly LoggerFactory loggerFactory = new LoggerFactory(new[] { new ConsoleLoggerProvider((_, __) => true, true) });
        //private readonly string _connectionString = "Server=ANSARI-PC; Database =zowarahDB; user id = sa; password = Noori@Sys";       
        // private readonly string _connectionString = "Server= 37.48.109.145\\MSSQLSERVER2014; Database = ZowarahDB; user id = apizowarah; password = Hov5l!73";
       // private readonly string _connectionString = "Server=dell-PC\\SQLEXPRESS; Database =zowarahDB; Integrated Security=True";
        private readonly string _connectionString = "Server= 88.99.227.71; Database = admin_zowar2020; user id = superadmin; password = 4G59ma*b";
        

        public ApplicationDbContext()
        {
        }

        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            //todo: connection string should be fetched from appsettings
            //  base.OnConfiguring(optionsBuilder);
            optionsBuilder.EnableSensitiveDataLogging();
            optionsBuilder
            //.UseLoggerFactory(loggerFactory)
            .UseSqlServer(_connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new HotelFacilityEntityMapping());
            modelBuilder.ApplyConfiguration(new HotelCreditCardEntityMapping());
            modelBuilder.ApplyConfiguration(new HotelRoomAmenityEntityMapping());
            modelBuilder.ApplyConfiguration(new LocalizationEntityMapping());
            modelBuilder.ApplyConfiguration(new AppPermissionEntityMapping());
            modelBuilder.ApplyConfiguration(new UserPermissionEntityMapping());
            modelBuilder.ApplyConfiguration(new HotelLanguageEntityMapping());
            modelBuilder.ApplyConfiguration(new HotelReviewFeatureRateEntityMapping());
            modelBuilder.ApplyConfiguration(new HotelPromotionRoomEntityMapping());
            modelBuilder.ApplyConfiguration(new HotelMealsEntityMapping());
            modelBuilder.ApplyConfiguration(new HotelPhotoEntityMapping());
        }

        public DbSet<Hotel> Hotels { get; set; }
        public DbSet<BreakFastType> BreakfastTypes { get; set; }
        public DbSet<HotelFacility> HotelFacilities { get; set; }
        public DbSet<InternetOption> InternetOptions { get; set; }
        public DbSet<ParkingOption> ParkingOptions { get; set; }
        public DbSet<ParkingReservation> ParkingReservations { get; set; }
        public DbSet<RoomType> RoomTypes { get; set; }
        public DbSet<RoomTypeName> RoomTypeNames { get; set; }
        public DbSet<SmokingOption> SmokingOptions { get; set; }
        public DbSet<ParkingType> ParkingTypes { get; set; }
        public DbSet<BreakFastOption> BreakfastOptions { get; set; }
        public DbSet<PropertyType> PropertyTypes { get; set; }
        public DbSet<Facility> Facilities { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<HotelPhoto> HotelPhotos { get; set; }
        public DbSet<HotelCreditCard> HotelCreditCards { get; set; }
        public DbSet<CancellationPolicy> CancellationPolicies { get; set; }
        public DbSet<CancellationFee> CancellationFees { get; set; }
        public DbSet<RoomAmenityCategory> RoomAmenityCategories { get; set; }
        public DbSet<RoomAmenity> RoomAmenities { get; set; }
        public DbSet<HotelRoomAmenity> HotelRoomAmenities { get; set; }

     //   public DbSet<CalendarRateManual> CalendarRateManual { get; set; } //shoaib  9 March Merge

        public DbSet<CalendarManual> CalendarManual { get; set; }
        public DbSet<Calendar> Calendar { get; set; }
        public DbSet<Booking> Booking { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<CreditCard> CreditCards { get; set; }
        public DbSet<ProfilePhoto> ProfilePhotos { get; set; }
        public DbSet<ServiceStatus> ServiceStatuses { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<ServiceProfile> ServiceProfiles { get; set; }
        public DbSet<ServiceType> ServiceTypes { get; set; }
        public DbSet<Localization> Localization { get; set; }
        public DbSet<AppPermission> Permissions { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceCheck> InvoiceChecks { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<HotelLanguage> HotelLanguages { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<FacilityCategory> FacilityCategories { get; set; }
        public DbSet<HotelMeals> HotelMeals { get; set; }
        public DbSet<HotelFeaturesDescription> HotelFeaturesDescriptions { get; set; }
        public DbSet<ServiceNearby> ServiceNearbies { get; set; }
        public DbSet<ServiceNearbyCategory> ServiceNearbyCategories { get; set; }
        public DbSet<ServiceNearbyType> ServiceNearbyTypes { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<ReviewFeature> ReviewFeatures { get; set; }
        public DbSet<ReviewFeatureType> ReviewFeatureTypes { get; set; }
        public DbSet<ServiceReviewFeaturesScore> ServiceReviewFeaturesScores { get; set; }
        public DbSet<LookUpModel> LookUpModels { get; set; }
        public DbSet<LookUpTable> LookUpTables { get; set; }
        public DbSet<HotelPromotion> HotelPromotions { get; set; }
        public DbSet<HotelPromotionRoom> HotelPromotionRooms { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<DimCalendar> DimCalendars { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<MessageTrail> MessageTrails { get; set; }
        public DbSet<MessageCategory> MessageCategories { get; set; }
        public DbSet<CreditCardInfo> CreditCardInfos { get; set; }
        public DbSet<Rtcpay> Rtcpay { get; set; }
    }
}