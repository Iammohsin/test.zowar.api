﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Reviews;

namespace Zowarah.Data.Contexts {

    public class DbInitializer {
        private ApplicationDbContext _context;

        public DbInitializer(ApplicationDbContext context) {
            _context = context;
        }

        public async Task Seed() {
            try {
                if (!_context.SmokingOptions.Any()) {
                    _context.AddRange(_SmokingOption);
                    await _context.SaveChangesAsync();
                }

                if (!_context.ParkingTypes.Any()) {
                    _context.AddRange(_ParkingType);
                    await _context.SaveChangesAsync();
                }

                if (!_context.RoomTypes.Any()) {
                    _context.AddRange(_RoomType);
                    await _context.SaveChangesAsync();

                }
                if (!_context.RoomTypeNames.Any()) {
                    _context.AddRange(_RoomTypeName);
                    await _context.SaveChangesAsync();

                }

                if (!_context.ParkingOptions.Any()) {
                    _context.AddRange(_ParkingOption);
                    await _context.SaveChangesAsync();

                }

                if (!_context.InternetOptions.Any()) {
                    _context.AddRange(_InternetOption);
                    await _context.SaveChangesAsync();

                }

                if (!_context.HotelFacilities.Any()) {
                    _context.AddRange(_Facilities);
                    await _context.SaveChangesAsync();

                }

                if (!_context.BreakfastOptions.Any()) {
                    _context.AddRange(_BreakFastOption);
                    await _context.SaveChangesAsync();

                }


                if (!_context.PropertyTypes.Any()) {
                    _context.AddRange(_PropertyType);
                    await _context.SaveChangesAsync();

                }
                if (!_context.RoomAmenityCategories.Any()) {
                    _context.AddRange(_roomAmenityCategories);
                    await _context.SaveChangesAsync();

                }
                if (!_context.RoomAmenities.Any()) {
                    _context.AddRange(_roomAmenities);
                    await _context.SaveChangesAsync();
                }

                if (!_context.CancellationPolicies.Any()) {
                    _context.AddRange(_cancellationPolicies);
                    await _context.SaveChangesAsync();
                }

                if (!_context.CancellationFees.Any()) {
                    _context.AddRange(_cancellationFees);
                    await _context.SaveChangesAsync();
                }


                if (!_context.CreditCards.Any()) {
                    _context.AddRange(_creditCards);
                    await _context.SaveChangesAsync();
                }

                if (!_context.ServiceTypes.Any()) {
                    _context.AddRange(_serviceTypes);
                    await _context.SaveChangesAsync();
                }
                if (!_context.HotelFeaturesDescriptions.Any()) {
                    _context.AddRange(_hotelFeaturesDescriptions);
                    await _context.SaveChangesAsync();
                }
                if (!_context.ServiceNearbyTypes.Any()) {
                    _context.AddRange(_ServiceNearbyTypes);
                    await _context.SaveChangesAsync();
                }
                if (!_context.ReviewFeatureTypes.Any()) {
                    _context.AddRange(_ReviewFeatureTypes);
                    await _context.SaveChangesAsync();
                }
                if (!_context.Languages.Any()) {
                    _context.AddRange(_languages);
                    await _context.SaveChangesAsync();
                }
                if (!_context.LookUpModels.Any()) {
                    _context.AddRange(_LookUpModels);
                    await _context.SaveChangesAsync();
                }
                if (!_context.MessageCategories.Any()) {
                    _context.AddRange(_messageCategories);
                }
                if (!_context.DimCalendars.Any()) {
                    _context.Database.SetCommandTimeout(new TimeSpan(1, 0, 0));
                    var date = new DateTime(2018, 1, 1);
                    var end = new DateTime(2038, 12, 31);
                    do {
                        _context.DimCalendars.Add(new DimCalendar { Date = date.Date });
                        date = date.AddDays(1);
                    } while (date <= end);
                    await _context.SaveChangesAsync();
                }
               

            } catch (Exception ex) {

            }

        }



        List<InternetOption> _InternetOption = new List<InternetOption>
        {
            new InternetOption()
            {
                Description = "No"
            },
              new InternetOption()
            {
                Description = "Yes, Free"
            },
             new InternetOption()
            {
                Description = "Yes, Paid"
            }
        };

        List<ParkingOption> _ParkingOption = new List<ParkingOption>
        {
            new ParkingOption()
            {
                Description = "No"
            },
              new ParkingOption()
            {
                Description = "Yes, Free"
            },
             new ParkingOption()
            {
                Description = "Yes, Paid"
            }
        };

        List<SmokingOption> _SmokingOption = new List<SmokingOption>
        {
            new SmokingOption()
            {
                Description = "No"
            },
              new SmokingOption()
            {
                Description = "Yes, Free"
            },
             new SmokingOption()
            {
                Description = "Yes, Paid"
            }
        };

        List<ParkingType> _ParkingType = new List<ParkingType>
        {
            new ParkingType()
            {
                Description = "Private"
            },
              new ParkingType()
            {
                Description = "Public"
            }
        };

        List<ParkingSite> _ParkingSite = new List<ParkingSite>
        {
            new ParkingSite()
            {
                Description = "On Site"
            },
              new ParkingSite()
            {
                Description = "Off Site"
            }
        };


        List<ParkingReservation> _ParkingReservation = new List<ParkingReservation>
        {
            new ParkingReservation()
            {
                Description = "Needed"
            },
              new ParkingReservation()
            {
                Description = "Not Needed"
            }
        };

        List<Facility> _Facilities = new List<Facility>
        {
            new Facility()
            {
                Description = "Air Conditioning"
            },
              new Facility()
            {
                Description = "Non-Smoking Rooms"
            },
             new Facility()
            {
                Description = "Playground"
            }
             ,
             new Facility()
            {
                Description = "BBQ Facility"
            }
             ,
             new Facility()
            {
                Description = "Sauna"
            }
             ,
             new Facility()
            {
                Description = "Family Rooms" }
                 ,
             new Facility()
            {
                Description = "All Spaces Non-Smoking (public and private)" }
                 ,
             new Facility()
            {
                Description = "Airport Shuttle (additional charge)"
            }
             ,
             new Facility()
            {
                Description = "Elevator"
            }
             ,
             new Facility()
            {
                Description = "Baggage Storage"
            }
             ,
             new Facility()
            {
                Description = "Airport Shuttle"
            }
             ,
             new Facility()
            {
                Description = "Restaurant"
            }
             ,
             new Facility()
            {
                Description = "Fitness Center"
            }
             ,
             new Facility()
            {
                Description = "Outdoor Pool"
            }
             ,
             new Facility()
            {
                Description = "Grounds"
            }
             ,
             new Facility()
            {
                Description = "Daily Housekeeping"
            }
        };

        List<RoomType> _RoomType = new List<RoomType>
        {
            new RoomType()
            {
                Description = "Single Room"
            },
              new RoomType()
            {
                Description = "Double Room"
            },
             new RoomType()
            {
                Description = "Twin"
            }
        };

        List<BreakFastOption> _BreakFastOption = new List<BreakFastOption>
        {
            new BreakFastOption()
            {
                Description = "No"
            },
              new BreakFastOption()
            {
                Description = "Yes, It's included in the price"
            },
             new BreakFastOption()
            {
                Description = "Yes, It's Optional"
            }
        };


        List<PropertyType> _PropertyType = new List<PropertyType>
        {
            new PropertyType()
            {
                Description = "Hotel"
            },
              new PropertyType()
            {
                Description = "Motel"
            },
             new PropertyType()
            {
                Description = "Apartment"
            },
            new PropertyType()
            {
                Description = "Resort"
            }
        };

        List<RoomTypeName> _RoomTypeName = new List<RoomTypeName>
        {
            new RoomTypeName()
            {
                Description = "Budget Single Room",
                RoomTypeId=1
            },
              new RoomTypeName()
            {
                  Description = "Delux Single Room",
                  RoomTypeId=2
            },
             new RoomTypeName()
            {
                Description = "Economy Single Room",
                RoomTypeId=3
            },
            new RoomTypeName()
            {
                Description = "Budget Double Room",
                RoomTypeId=1
            },
              new RoomTypeName()
            {
                  Description = "Delux Double Room",
                  RoomTypeId=2
            },
             new RoomTypeName()
            {
                Description = "Economy Double Room",
                RoomTypeId=3
            },
             new RoomTypeName()
            {
                Description = "Budget Twin Room",
                RoomTypeId=1

            },
              new RoomTypeName()
            {
                Description = "Delux Twin Room",
                RoomTypeId=2
            },
             new RoomTypeName()
            {
                Description = "Economy Twin Room",
                RoomTypeId=3
            }

        };

        List<RoomAmenityCategory> _roomAmenityCategories = new List<RoomAmenityCategory>
        {
            new RoomAmenityCategory
            {
                    Description = "Room amenities"
            },
            new RoomAmenityCategory
            {
                Description ="Bathroom"
            },
            new RoomAmenityCategory
            {
                Description = "Media & Technology"
            },
            new RoomAmenityCategory
            {
                Description ="Food and Drink"
            },
            new RoomAmenityCategory
            {
                Description ="Services & Extras"
            },
            new RoomAmenityCategory
            {
                Description = "Outdoor & View"
            },
            new RoomAmenityCategory
            {
                Description = "Accessibility"
            },
            new RoomAmenityCategory
            {
                Description = "Entertainment & Family Services"
            }
        };

        List<RoomAmenity> _roomAmenities = new List<RoomAmenity>
        {
            new RoomAmenity
            {
                Description = "Clothes rack",
                RoomAmenityCategoryId=1
            },
            new RoomAmenity
            {
                Description = "Clothes rack",
                RoomAmenityCategoryId=1
            },
            new RoomAmenity
            {
                Description = "Clothes rack",
                RoomAmenityCategoryId=2
            },
            new RoomAmenity
            {
                Description = "Clothes rack",
                RoomAmenityCategoryId=2
            },
            new RoomAmenity
            {
                Description = "Clothes rack",
                RoomAmenityCategoryId=3
            },
            new RoomAmenity
            {
                Description = "Clothes rack",
                RoomAmenityCategoryId=3
            },
            new RoomAmenity
            {
                Description = "Clothes rack",
                RoomAmenityCategoryId=4
            },
            new RoomAmenity
            {
                Description = "Clothes rack",
                RoomAmenityCategoryId=4
            },
            new RoomAmenity
            {
                Description = "Clothes rack",
                RoomAmenityCategoryId=5
            },
            new RoomAmenity
            {
                Description = "Clothes rack",
                RoomAmenityCategoryId=5
            },
            new RoomAmenity
            {
                Description = "Clothes rack",
                RoomAmenityCategoryId=6
            },
             new RoomAmenity
            {
                Description = "Clothes rack",
                RoomAmenityCategoryId=6
            }

        };

        List<CancellationPolicy> _cancellationPolicies = new List<CancellationPolicy>
        {
            new CancellationPolicy
            {
                Description = "Day Of Arrival (6 pm)",
                NumberOfDays=0,
                NumberOfHours=18
            },
            new CancellationPolicy
            {
                Description = "1 day",
                 NumberOfDays=1,
                NumberOfHours=0
            }
        };

        List<CancellationFee> _cancellationFees = new List<CancellationFee>
        {
            new CancellationFee
            {
                Description = "1 Full Day "
            },
            new CancellationFee
            {
                Description = "Option 2"
            }
        };


        List<CreditCard> _creditCards = new List<CreditCard>
        {
            new CreditCard
            {
                Description = "Master Card"
            },
            new CreditCard
            {
                Description = "Visa Card"
            },
            new CreditCard
            {
                Description = "Union Card"
            }
        };

        List<ServiceType> _serviceTypes = new List<ServiceType>
        {
            new ServiceType
            {
                Description = "Hotels"
            },
            new ServiceType
            {
                Description = "Resturants"
            },
            new ServiceType
            {
                Description = "Travel"
            }
        };

        List<HotelFeaturesDescription> _hotelFeaturesDescriptions = new List<HotelFeaturesDescription>()
        {
            new HotelFeaturesDescription{
                Feature="freeWifi",
                Order=10,
                Description="Featuring free WiFi throughout the property"
            },
             new HotelFeaturesDescription{
                Feature="accommodationsOffer",
                Order=11,
                Description="{hotelName} Hotel offers accommodations in {cityName}"
            },
             new HotelFeaturesDescription{
                Feature="sauna",
                Order=121,
                Description="The hotel has a sauna"
            }              ,
             new HotelFeaturesDescription{
                Feature="BBQ",
                Order=121,
                Description="The hotel has a BBQ"
            }  ,
             new HotelFeaturesDescription{
                Feature="restaurant",
                Order=13,
                Description="guests can enjoy the on-site restaurant"
            } ,
             new HotelFeaturesDescription{
                Feature="freeParking",
                Order=20,
                Description="Free private parking is available on site"
            } ,
             new HotelFeaturesDescription{
                Feature="paidParking",
                Order=21,
                Description="private parking is available on site"
            },
              new HotelFeaturesDescription{
                Feature="room",
                Order=30,
                Description="<room>The rooms are equipped with a </room>"
            },
              new HotelFeaturesDescription{
                Feature="frontdeskandBaggageStorage",
                Order=40,
                Description="You will find a 24-hour front desk and Baggage Storage at the property"
            },
               new HotelFeaturesDescription{
                Feature="frontdesk",
                Order=41,
                Description="You will find a 24-hour front desk at the property"
            },
              new HotelFeaturesDescription{
                Feature="greatFacilities",
                Order=50,
                Description="{hotelName} Hotel is a great choice for travelers interested in business, luxury brand shopping and clothes shopping"
            },
               new HotelFeaturesDescription{
                Feature="favoriteInCity",
                Order=60,
                Description="This is our guest's favorite part of {cityName}, according to independent reviews"
            },
             new HotelFeaturesDescription{
                Feature="topRatedLocation",
                Order=70,
                Description="{hotelName} property also has one of the top-rated locations in {cityName}! Guests are happier about it compared to other properties in the area"
            } ,
              new HotelFeaturesDescription{
                Feature="location",
                Order=71,
                Description="Guests in particular like the location."
            } ,
             new HotelFeaturesDescription{
                Feature="promotion",
                Order=80,
                Description="This property is also rated for the best value in {cityName}. Guests are getting more for their money when compared to other properties in this city"
            } ,
             new HotelFeaturesDescription{
                Feature="language",
                Order=90,
                Description="We speak your language"
            }

        };


        List<ServiceNearbyType> _ServiceNearbyTypes = new List<ServiceNearbyType> {
            new ServiceNearbyType{
                Description="Nearest essentials",
                 ServiceNearbyCategories=new List<ServiceNearbyCategory>{
                    new ServiceNearbyCategory{
                        Description="Airports"
                    },
                     new ServiceNearbyCategory{
                        Description="Public transportation"
                    },
                      new ServiceNearbyCategory{
                        Description="Hospital or clinic"
                    },
                       new ServiceNearbyCategory{
                        Description="Pharmacy"
                     },
                       new ServiceNearbyCategory{
                        Description="Cash withdrawal"
                       }
                }

            },
            new ServiceNearbyType{
                Description="Shopping and Dining",
                ServiceNearbyCategories=new List<ServiceNearbyCategory>{
                    new ServiceNearbyCategory{
                        Description="Restaurant"
                    },
                     new ServiceNearbyCategory{
                        Description="Cafe/bar"
                    },
                      new ServiceNearbyCategory{
                        Description="Grocery store/supermarket"
                    },
                       new ServiceNearbyCategory{
                        Description="Market"
                    }
                }
            },
             new ServiceNearbyType{
                Description="Places of Interest",
                ServiceNearbyCategories=new List<ServiceNearbyCategory>{
                     new ServiceNearbyCategory{
                           Description="popular landmarks"
                       },
                    new ServiceNearbyCategory{
                        Description="Mountain"
                    },
                     new ServiceNearbyCategory{
                        Description="Lake"
                    },
                      new ServiceNearbyCategory{
                        Description="River"
                    },
                       new ServiceNearbyCategory{
                        Description="Sea/ocean"
                    },
                       new ServiceNearbyCategory{
                           Description="Beach"
                       },
                        new ServiceNearbyCategory{
                           Description="Ski lift"
                       }

                }
            }
        };


        List<ReviewFeatureType> _ReviewFeatureTypes = new List<ReviewFeatureType>() {
            new ReviewFeatureType{
                Description="Cleanliness"
            },
             new ReviewFeatureType{
                Description="Room comfort and quality"
            },
              new ReviewFeatureType{
                Description="Facilities"
            },
               new ReviewFeatureType{
                Description="Service"
            },
                new ReviewFeatureType{
                Description="Value for money"
            },
             new ReviewFeatureType{
                Description="Location"
            }
        };

        List<Language> _languages = new List<Language>()
        {
            new Language  { Id = "ab",Name = "Abkhaz", NativeName= "аҧсуа"},
            new Language {Id="aa",Name="Afar",NativeName="Afaraf"},
            new Language {Id="af",Name="Afrikaans",NativeName="Afrikaans"},
            new Language {Id="ak",Name="Akan",NativeName="Akan"},
            new Language {Id="sq",Name="Albanian",NativeName="Shqip"},
            new Language {Id="am",Name="Amharic",NativeName="አማርኛ"},
            new Language {Id="ar",Name="Arabic",NativeName="العربية"},
            new Language {Id="an",Name="Aragonese",NativeName="Aragonés"},
            new Language {Id="hy",Name="Armenian",NativeName="Հայերեն"},
            new Language {Id="as",Name="Assamese",NativeName="অসমীয়া"},
            new Language {Id="av",Name="Avaric",NativeName="авар мацӀ, магӀарул мацӀ"},
            new Language {Id="ae",Name="Avestan",NativeName="avesta"},
            new Language {Id="ay",Name="Aymara",NativeName="aymar aru"},
            new Language {Id="az",Name="Azerbaijani",NativeName="azərbaycan dili"},
            new Language {Id="bm",Name="Bambara",NativeName="bamanankan"},
            new Language {Id="ba",Name="Bashkir",NativeName="башҡорт теле"},
            new Language {Id="eu",Name="Basque",NativeName="euskara, euskera"},
            new Language {Id="be",Name="Belarusian",NativeName="Беларуская"},
            new Language {Id="bn",Name="Bengali",NativeName="বাংলা"},
            new Language {Id="bh",Name="Bihari",NativeName="भोजपुरी"},
            new Language {Id="bi",Name="Bislama",NativeName="Bislama"},
            new Language {Id="bs",Name="Bosnian",NativeName="bosanski jezik"},
            new Language {Id="br",Name="Breton",NativeName="brezhoneg"},
            new Language {Id="bg",Name="Bulgarian",NativeName="български език"},
            new Language {Id="my",Name="Burmese",NativeName="ဗမာစာ"},
            new Language {Id="ca",Name="Catalan; Valencian",NativeName="Català"},
            new Language {Id="ch",Name="Chamorro",NativeName="Chamoru"},
            new Language {Id="ce",Name="Chechen",NativeName="нохчийн мотт"},
            new Language {Id="ny",Name="Chichewa; Chewa; Nyanja",NativeName="chiCheŵa, chinyanja"},
            new Language {Id="zh",Name="Chinese",NativeName="中文 (Zhōngwén), 汉语, 漢語"},
            new Language {Id="cv",Name="Chuvash",NativeName="чӑваш чӗлхи"},
            new Language {Id="kw",Name="Cornish",NativeName="Kernewek"},
            new Language {Id="co",Name="Corsican",NativeName="corsu, lingua corsa"},
            new Language {Id="cr",Name="Cree",NativeName="ᓀᐦᐃᔭᐍᐏᐣ"},
            new Language {Id="hr",Name="Croatian",NativeName="hrvatski"},
            new Language {Id="cs",Name="Czech",NativeName="česky, čeština"},
            new Language {Id="da",Name="Danish",NativeName="dansk"},
            new Language {Id="dv",Name="Divehi; Dhivehi; Maldivian;",NativeName="ދިވެހި"},
            new Language {Id="nl",Name="Dutch",NativeName="Nederlands, Vlaams"},
            new Language {Id="en",Name="English",NativeName="English"},
            new Language {Id="eo",Name="Esperanto",NativeName="Esperanto"},
            new Language {Id="et",Name="Estonian",NativeName="eesti, eesti keel"},
            new Language {Id="ee",Name="Ewe",NativeName="Eʋegbe"},
            new Language {Id="fo",Name="Faroese",NativeName="føroyskt"},
            new Language {Id="fj",Name="Fijian",NativeName="vosa Vakaviti"},
            new Language {Id="fi",Name="Finnish",NativeName="suomi, suomen kieli"},
            new Language {Id="fr",Name="French",NativeName="français, langue française"},
            new Language {Id="ff",Name="Fula; Fulah; Pulaar; Pular",NativeName="Fulfulde, Pulaar, Pular"},
            new Language {Id="gl",Name="Galician",NativeName="Galego"},
            new Language {Id="ka",Name="Georgian",NativeName="ქართული"},
            new Language {Id="de",Name="German",NativeName="Deutsch"},
            new Language {Id="el",Name="Greek, Modern",NativeName="Ελληνικά"},
            new Language {Id="gn",Name="Guaraní",NativeName="Avañeẽ"},
            new Language {Id="gu",Name="Gujarati",NativeName="ગુજરાતી"},
            new Language {Id="ht",Name="Haitian; Haitian Creole",NativeName="Kreyòl ayisyen"},
            new Language {Id="ha",Name="Hausa",NativeName="Hausa, هَوُسَ"},
            new Language {Id="he",Name="Hebrew (modern)",NativeName="עברית"},
            new Language {Id="hz",Name="Herero",NativeName="Otjiherero"},
            new Language {Id="hi",Name="Hindi",NativeName="हिन्दी, हिंदी"},
            new Language {Id="ho",Name="Hiri Motu",NativeName="Hiri Motu"},
            new Language {Id="hu",Name="Hungarian",NativeName="Magyar"},
            new Language {Id="ia",Name="Interlingua",NativeName="Interlingua"},
            new Language {Id="id",Name="Indonesian",NativeName="Bahasa Indonesia"},
            new Language {Id="ie",Name="Interlingue",NativeName="Originally called Occidental; then Interlingue after WWII"},
            new Language {Id="ga",Name="Irish",NativeName="Gaeilge"},
            new Language {Id="ig",Name="Igbo",NativeName="Asụsụ Igbo"},
            new Language {Id="ik",Name="Inupiaq",NativeName="Iñupiaq, Iñupiatun"},
            new Language {Id="io",Name="Ido",NativeName="Ido"},
            new Language {Id="is",Name="Icelandic",NativeName="Íslenska"},
            new Language {Id="it",Name="Italian",NativeName="Italiano"},
            new Language {Id="iu",Name="Inuktitut",NativeName="ᐃᓄᒃᑎᑐᑦ"},
            new Language {Id="ja",Name="Japanese",NativeName="日本語 (にほんご／にっぽんご)"},
            new Language {Id="jv",Name="Javanese",NativeName="basa Jawa"},
            new Language {Id="kl",Name="Kalaallisut, Greenlandic",NativeName="kalaallisut, kalaallit oqaasii"},
            new Language {Id="kn",Name="Kannada",NativeName="ಕನ್ನಡ"},
            new Language {Id="kr",Name="Kanuri",NativeName="Kanuri"},
            new Language {Id="ks",Name="Kashmiri",NativeName="कश्मीरी, كشميري‎"},
            new Language {Id="kk",Name="Kazakh",NativeName="Қазақ тілі"},
            new Language {Id="km",Name="Khmer",NativeName="ភាសាខ្មែរ"},
            new Language {Id="ki",Name="Kikuyu, Gikuyu",NativeName="Gĩkũyũ"},
            new Language {Id="rw",Name="Kinyarwanda",NativeName="Ikinyarwanda"},
            new Language {Id="ky",Name="Kirghiz, Kyrgyz",NativeName="кыргыз тили"},
            new Language {Id="kv",Name="Komi",NativeName="коми кыв"},
            new Language {Id="kg",Name="Kongo",NativeName="KiKongo"},
            new Language {Id="ko",Name="Korean",NativeName="한국어 (韓國語), 조선말 (朝鮮語)"},
            new Language {Id="ku",Name="Kurdish",NativeName="Kurdî, كوردی‎"},
            new Language {Id="kj",Name="Kwanyama, Kuanyama",NativeName="Kuanyama"},
            new Language {Id="la",Name="Latin",NativeName="latine, lingua latina"},
            new Language {Id="lb",Name="Luxembourgish, Letzeburgesch",NativeName="Lëtzebuergesch"},
            new Language {Id="lg",Name="Luganda",NativeName="Luganda"},
            new Language {Id="li",Name="Limburgish, Limburgan, Limburger",NativeName="Limburgs"},
            new Language {Id="ln",Name="Lingala",NativeName="Lingála"},
            new Language {Id="lo",Name="Lao",NativeName="ພາສາລາວ"},
            new Language {Id="lt",Name="Lithuanian",NativeName="lietuvių kalba"},
            new Language {Id="lu",Name="Luba-Katanga",NativeName=""},
            new Language {Id="lv",Name="Latvian",NativeName="latviešu valoda"},
            new Language {Id="gv",Name="Manx",NativeName="Gaelg, Gailck"},
            new Language {Id="mk",Name="Macedonian",NativeName="македонски јазик"},
            new Language {Id="mg",Name="Malagasy",NativeName="Malagasy fiteny"},
            new Language {Id="ms",Name="Malay",NativeName="bahasa Melayu, بهاس ملايو‎"},
            new Language {Id="ml",Name="Malayalam",NativeName="മലയാളം"},
            new Language {Id="mt",Name="Maltese",NativeName="Malti"},
            new Language {Id="mi",Name="Māori",NativeName="te reo Māori"},
            new Language {Id="mr",Name="Marathi (Marāṭhī)",NativeName="मराठी"},
            new Language {Id="mh",Name="Marshallese",NativeName="Kajin M̧ajeļ"},
            new Language {Id="mn",Name="Mongolian",NativeName="монгол"},
            new Language {Id="na",Name="Nauru",NativeName="Ekakairũ Naoero"},
            new Language {Id="nv",Name="Navajo, Navaho",NativeName="Diné bizaad, Dinékʼehǰí"},
            new Language {Id="nb",Name="Norwegian Bokmål",NativeName="Norsk bokmål"},
            new Language {Id="nd",Name="North Ndebele",NativeName="isiNdebele"},
            new Language {Id="ne",Name="Nepali",NativeName="नेपाली"},
            new Language {Id="ng",Name="Ndonga",NativeName="Owambo"},
            new Language {Id="nn",Name="Norwegian Nynorsk",NativeName="Norsk nynorsk"},
            new Language {Id="no",Name="Norwegian",NativeName="Norsk"},
            new Language {Id="ii",Name="Nuosu",NativeName="ꆈꌠ꒿ Nuosuhxop"},
            new Language {Id="nr",Name="South Ndebele",NativeName="isiNdebele"},
            new Language {Id="oc",Name="Occitan",NativeName="Occitan"},
            new Language {Id="oj",Name="Ojibwe, Ojibwa",NativeName="ᐊᓂᔑᓈᐯᒧᐎᓐ"},
            new Language {Id="cu",Name="Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic",NativeName="ѩзыкъ словѣньскъ"},
            new Language {Id="om",Name="Oromo",NativeName="Afaan Oromoo"},
            new Language {Id="or",Name="Oriya",NativeName="ଓଡ଼ିଆ"},
            new Language {Id="os",Name="Ossetian, Ossetic",NativeName="ирон æвзаг"},
            new Language {Id="pa",Name="Panjabi, Punjabi",NativeName="ਪੰਜਾਬੀ, پنجابی‎"},
            new Language {Id="pi",Name="Pāli",NativeName="पाऴि"},
            new Language {Id="fa",Name="Persian",NativeName="فارسی"},
            new Language {Id="pl",Name="Polish",NativeName="polski"},
            new Language {Id="ps",Name="Pashto, Pushto",NativeName="پښتو"},
            new Language {Id="pt",Name="Portuguese",NativeName="Português"},
            new Language {Id="qu",Name="Quechua",NativeName="Runa Simi, Kichwa"},
            new Language {Id="rm",Name="Romansh",NativeName="rumantsch grischun"},
            new Language {Id="rn",Name="Kirundi",NativeName="kiRundi"},
            new Language {Id="ro",Name="Romanian, Moldavian, Moldovan",NativeName="română"},
            new Language {Id="ru",Name="Russian",NativeName="русский язык"},
            new Language {Id="sa",Name="Sanskrit (Saṁskṛta)",NativeName="संस्कृतम्"},
            new Language {Id="sc",Name="Sardinian",NativeName="sardu"},
            new Language {Id="sd",Name="Sindhi",NativeName="सिन्धी, سنڌي، سندھی‎"},
            new Language {Id="se",Name="Northern Sami",NativeName="Davvisámegiella"},
            new Language {Id="sm",Name="Samoan",NativeName="gagana faa Samoa"},
            new Language {Id="sg",Name="Sango",NativeName="yângâ tî sängö"},
            new Language {Id="sr",Name="Serbian",NativeName="српски језик"},
            new Language {Id="gd",Name="Scottish Gaelic; Gaelic",NativeName="Gàidhlig"},
            new Language {Id="sn",Name="Shona",NativeName="chiShona"},
            new Language {Id="si",Name="Sinhala, Sinhalese",NativeName="සිංහල"},
            new Language {Id="sk",Name="Slovak",NativeName="slovenčina"},
            new Language {Id="sl",Name="Slovene",NativeName="slovenščina"},
            new Language {Id="so",Name="Somali",NativeName="Soomaaliga, af Soomaali"},
            new Language {Id="st",Name="Southern Sotho",NativeName="Sesotho"},
            new Language {Id="es",Name="Spanish; Castilian",NativeName="español, castellano"},
            new Language {Id="su",Name="Sundanese",NativeName="Basa Sunda"},
            new Language {Id="sw",Name="Swahili",NativeName="Kiswahili"},
            new Language {Id="ss",Name="Swati",NativeName="SiSwati"},
            new Language {Id="sv",Name="Swedish",NativeName="svenska"},
            new Language {Id="ta",Name="Tamil",NativeName="தமிழ்"},
            new Language {Id="te",Name="Telugu",NativeName="తెలుగు"},
            new Language {Id="tg",Name="Tajik",NativeName="тоҷикӣ, toğikī, تاجیکی‎"},
            new Language {Id="th",Name="Thai",NativeName="ไทย"},
            new Language {Id="ti",Name="Tigrinya",NativeName="ትግርኛ"},
            new Language {Id="bo",Name="Tibetan Standard, Tibetan, Central",NativeName="བོད་ཡིག"},
            new Language {Id="tk",Name="Turkmen",NativeName="Türkmen, Түркмен"},
            new Language {Id="tl",Name="Tagalog",NativeName="Wikang Tagalog, ᜏᜒᜃᜅ᜔ ᜆᜄᜎᜓᜄ᜔"},
            new Language {Id="tn",Name="Tswana",NativeName="Setswana"},
            new Language {Id="to",Name="Tonga (Tonga Islands)",NativeName="faka Tonga"},
            new Language {Id="tr",Name="Turkish",NativeName="Türkçe"},
            new Language {Id="ts",Name="Tsonga",NativeName="Xitsonga"},
            new Language {Id="tt",Name="Tatar",NativeName="татарча, tatarça, تاتارچا‎"},
            new Language {Id="tw",Name="Twi",NativeName="Twi"},
            new Language {Id="ty",Name="Tahitian",NativeName="Reo Tahiti"},
            new Language {Id="ug",Name="Uighur, Uyghur",NativeName="Uyƣurqə, ئۇيغۇرچە‎"},
            new Language {Id="uk",Name="Ukrainian",NativeName="українська"},
            new Language {Id="ur",Name="Urdu",NativeName="اردو"},
            new Language {Id="uz",Name="Uzbek",NativeName="zbek, Ўзбек, أۇزبېك‎"},
            new Language {Id="ve",Name="Venda",NativeName="Tshivenḓa"},
            new Language {Id="vi",Name="Vietnamese",NativeName="Tiếng Việt"},
            new Language {Id="vo",Name="Volapük",NativeName="Volapük"},
            new Language {Id="wa",Name="Walloon",NativeName="Walon"},
            new Language {Id="cy",Name="Welsh",NativeName="Cymraeg"},
            new Language {Id="wo",Name="Wolof",NativeName="Wollof"},
            new Language {Id="fy",Name="Western Frisian",NativeName="Frysk"},
            new Language {Id="xh",Name="Xhosa",NativeName="isiXhosa"},
            new Language {Id="yi",Name="Yiddish",NativeName="ייִדיש"},
            new Language {Id="yo",Name="Yoruba",NativeName="Yorùbá"},
            new Language {Id="za",Name="Zhuang, Chuang",NativeName="Saɯ cueŋƅ, Saw cuengh"}
              };

        List<LookUpModel> _LookUpModels = new List<LookUpModel> {
     new LookUpModel {
     Description = "Common",
      LookUpTables = new List <LookUpTable> {
       new LookUpTable {
        Id = nameof(Currency),
         Description = nameof(Currency)
       },
       new LookUpTable {
        Id = nameof(City),
         Description = nameof(City)
       },
       new LookUpTable {
        Id = nameof(Country),
         Description = nameof(Country)
       },
       new LookUpTable {
        Id = nameof(CreditCard),
         Description = "Credit Card"
       },
       new LookUpTable{
           Id=nameof(MessageCategory),
           Description="Message Categories"
       },
       new LookUpTable {
        Id = nameof(ServiceNearbyType),
         Description = "Service Nearby Types"
       },
       new LookUpTable {
        Id = nameof(ServiceNearbyCategory),
         Description = "Service Nearby Categories"
       },
       new LookUpTable {
        Id = nameof(ReviewFeatureType),
         Description = "Review Feature Types"
       }
      }
    },
    new LookUpModel {
     Description = "Hotel",
      LookUpTables = new List<LookUpTable> {
       new LookUpTable {
        Id = nameof(BreakFastOption),
         Description = "BreakFast Options"
       },
       new LookUpTable {
        Id = nameof(BreakFastType),
         Description = "BreakFast Types"
       },
       new LookUpTable {
        Id = nameof(CancellationFee),
         Description = "Cancellation Fees"
       },
       new LookUpTable {
        Id = nameof(CancellationPolicy),
         Description = "Cancellation Policy"
       },
       new LookUpTable {
        Id = nameof(InternetOption),
         Description = "Internet Options"
       },
       new LookUpTable {
        Id = nameof(SmokingOption),
         Description = "Smoking Options"
       },
       new LookUpTable {
        Id = nameof(Facility),
         Description = "Facility"
       },
       new LookUpTable {
        Id = nameof(FacilityCategory),
         Description = "Facility Categories"
       },
       new LookUpTable {
        Id = nameof(ParkingOption),
         Description = "Parking Options"
       },
       new LookUpTable {
        Id = nameof(ParkingReservation),
         Description = "Parking Reservation"
       },
       new LookUpTable {
        Id = nameof(ParkingSite),
         Description = "Parking Site"
       },
       new LookUpTable {
        Id = nameof(ParkingType),
         Description = "Parking Type"
       },
       new LookUpTable {
        Id = nameof(PropertyType),
         Description = "Property Type"
       },
       new LookUpTable {
        Id = nameof(RoomAmenityCategory),
         Description = "Room Amenity Category"
       },
       new LookUpTable {
        Id = nameof(RoomAmenity),
         Description = "Room Amenity"
       },
       new LookUpTable {
        Id = nameof(RoomType),
         Description = "Room Type"
       },
       new LookUpTable {
        Id = nameof(RoomTypeName),
         Description = "Room Type Name"
       }
      }
    }, new LookUpModel {
     Description = "Global",
      LookUpTables = new List<LookUpTable> {
       new LookUpTable {
        Id = "Setting",
         Description = "Settings"
       }
      }
    }
   };
        List<MessageCategory> _messageCategories = new List<MessageCategory> {
            new MessageCategory{
                Description="Complaints"
            },
            new MessageCategory{
                Description="Cancellation Issue"
            }
        };
    }
}
