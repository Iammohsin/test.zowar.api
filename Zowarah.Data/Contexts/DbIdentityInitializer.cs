﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.Data.Contexts
{
    public class DbIdentityInitializer
    {
        // this seed for testing 
        public static void Seed(ApplicationDbContext db, UserManager<AppUser> userManager, RoleManager<AppRole> roleManager) {
            var adminRole = new AppRole { Name = "Administrator" };
            roleManager.CreateAsync(adminRole).GetAwaiter();
            var providerRole = new AppRole { Name = "Provider" };
            roleManager.CreateAsync(providerRole).GetAwaiter();
            var clientRole = new AppRole { Name = "Client" };
            roleManager.CreateAsync(clientRole).GetAwaiter();

            // administrator
            var Admin = getUser("hatem.alhassani@mediu.my", "Admin");
            userManager.CreateAsync(Admin, "Hatem@123").GetAwaiter();
            userManager.AddToRoleAsync(Admin, adminRole.Name).GetAwaiter();

            // provider
            var Provider = getUser("hatemalhasani@hotmail.com", "provider");
            userManager.CreateAsync(Provider, "Hatem@123").GetAwaiter();
            userManager.AddToRoleAsync(Provider, providerRole.Name).GetAwaiter();

            // client
            var client = getUser("hatemalhassani@outlook.com", "client");
            userManager.CreateAsync(client, "Hatem@123").GetAwaiter();
            userManager.AddToRoleAsync(client, providerRole.Name).GetAwaiter();
        }
        private static AppUser getUser(string Email, string Name) {
            return new AppUser { CountryId = 231, CurrencyId = "RSD", IsActive = true, JoinDate = DateTime.Now, Email = Email, UserName = Email, FirstName = Name, LastName = Name };
        }
    }
}
