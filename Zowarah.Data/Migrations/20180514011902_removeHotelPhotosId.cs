﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Zowarah.Data.Migrations
{
    public partial class removeHotelPhotosId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_HotelPhotos",
                table: "HotelPhotos");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "HotelPhotos");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "HotelPhotos");

            migrationBuilder.DropColumn(
                name: "DeletedOn",
                table: "HotelPhotos");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "HotelPhotos");

            migrationBuilder.DropColumn(
                name: "UpdatedOn",
                table: "HotelPhotos");

            migrationBuilder.AlterColumn<string>(
                name: "FileName",
                table: "HotelPhotos",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_HotelPhotos",
                table: "HotelPhotos",
                column: "FileName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_HotelPhotos",
                table: "HotelPhotos");

            migrationBuilder.AlterColumn<string>(
                name: "FileName",
                table: "HotelPhotos",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<long>(
                name: "Id",
                table: "HotelPhotos",
                nullable: false,
                defaultValue: 0L)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "HotelPhotos",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedOn",
                table: "HotelPhotos",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "HotelPhotos",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedOn",
                table: "HotelPhotos",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddPrimaryKey(
                name: "PK_HotelPhotos",
                table: "HotelPhotos",
                column: "Id");
        }
    }
}
