﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Zowarah.Data.Migrations
{
    public partial class addCreditCardType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CreditCardInfos_CreditCards_CreditCardId",
                table: "CreditCardInfos");

            migrationBuilder.DropIndex(
                name: "IX_CreditCardInfos_CreditCardId",
                table: "CreditCardInfos");

            migrationBuilder.DropColumn(
                name: "CreditCardId",
                table: "CreditCardInfos");

            migrationBuilder.AddColumn<string>(
                name: "CardType",
                table: "CreditCardInfos",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CardType",
                table: "CreditCardInfos");

            migrationBuilder.AddColumn<int>(
                name: "CreditCardId",
                table: "CreditCardInfos",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_CreditCardInfos_CreditCardId",
                table: "CreditCardInfos",
                column: "CreditCardId");

            migrationBuilder.AddForeignKey(
                name: "FK_CreditCardInfos_CreditCards_CreditCardId",
                table: "CreditCardInfos",
                column: "CreditCardId",
                principalTable: "CreditCards",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
