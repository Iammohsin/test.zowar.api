﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zowarah.Data.Migrations
{
    public partial class update_hotelPromotions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BookingDaysCondition",
                table: "HotelPromotions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "FreeNights",
                table: "HotelPromotions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PromotionType",
                table: "HotelPromotions",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BookingDaysCondition",
                table: "HotelPromotions");

            migrationBuilder.DropColumn(
                name: "FreeNights",
                table: "HotelPromotions");

            migrationBuilder.DropColumn(
                name: "PromotionType",
                table: "HotelPromotions");
        }
    }
}
