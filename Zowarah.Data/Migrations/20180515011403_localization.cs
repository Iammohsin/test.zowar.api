﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Zowarah.Data.Migrations
{
    public partial class localization : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "LocKey",
                table: "Localization",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "CultureId",
                table: "Localization",
                newName: "LanguageId");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "Localization",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedOn",
                table: "Localization",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Localization",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedOn",
                table: "Localization",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddForeignKey(
                name: "FK_Localization_Languages_LanguageId",
                table: "Localization",
                column: "LanguageId",
                principalTable: "Languages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Localization_Languages_LanguageId",
                table: "Localization");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "Localization");

            migrationBuilder.DropColumn(
                name: "DeletedOn",
                table: "Localization");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Localization");

            migrationBuilder.DropColumn(
                name: "UpdatedOn",
                table: "Localization");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Localization",
                newName: "LocKey");

            migrationBuilder.RenameColumn(
                name: "LanguageId",
                table: "Localization",
                newName: "CultureId");
        }
    }
}
