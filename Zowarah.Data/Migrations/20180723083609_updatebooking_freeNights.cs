﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zowarah.Data.Migrations
{
    public partial class updatebooking_freeNights : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FreeNights",
                table: "Booking",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FreeNights",
                table: "Booking");
        }
    }
}
