﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zowarah.Data.Migrations
{
    public partial class alter_message : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsRecieverDeleted",
                table: "Messages",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSenderDeleted",
                table: "Messages",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsRecieverDeleted",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "IsSenderDeleted",
                table: "Messages");
        }
    }
}
