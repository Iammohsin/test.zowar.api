﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Zowarah.Data.Migrations
{
    public partial class languages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reviews_AspNetUsers_ClientId1",
                table: "Reviews");

            migrationBuilder.DropIndex(
                name: "IX_Reviews_ClientId1",
                table: "Reviews");

            migrationBuilder.DropColumn(
                name: "ClientId1",
                table: "Reviews");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Languages");

            migrationBuilder.AlterColumn<long>(
                name: "ClientId",
                table: "Reviews",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Reviews_ClientId",
                table: "Reviews",
                column: "ClientId");

            migrationBuilder.AddForeignKey(
                name: "FK_Reviews_AspNetUsers_ClientId",
                table: "Reviews",
                column: "ClientId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reviews_AspNetUsers_ClientId",
                table: "Reviews");

            migrationBuilder.DropIndex(
                name: "IX_Reviews_ClientId",
                table: "Reviews");

            migrationBuilder.AlterColumn<string>(
                name: "ClientId",
                table: "Reviews",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<long>(
                name: "ClientId1",
                table: "Reviews",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Languages",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Reviews_ClientId1",
                table: "Reviews",
                column: "ClientId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Reviews_AspNetUsers_ClientId1",
                table: "Reviews",
                column: "ClientId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
