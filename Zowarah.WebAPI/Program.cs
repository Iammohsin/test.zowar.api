﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml;
using log4net;
using log4net.Config;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.EventLog;

namespace Zowarah.WebAPI {
    public class Program {
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(Program));
        public static void Main(string[] args) {
            /*  XmlDocument log4netConfig = new XmlDocument();
              log4netConfig.Load(File.OpenRead("log4net.config"));
              var repo = log4net.LogManager.CreateRepository(Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));
              log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);
              */

            /*
            XmlDocument log4netConfig = new XmlDocument();
            log4netConfig.Load(File.OpenRead("log4net.config"));

            var repo = log4net.LogManager.CreateRepository(
                Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));

            log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);

            log.Info("Application - Main is invoked");
            */

            /*
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());

            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));

            var logger = LogManager.GetLogger(typeof(Program));
            */
            //logger.Error("Hello World!");

            //log4net.LogManager.CreateDomain()
            // BuildWebHost(args).Run();

            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                   .ConfigureAppConfiguration((builderContext, config) => {
                        IHostingEnvironment env = builderContext.HostingEnvironment;
                        config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                       .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: false, reloadOnChange: true);
                   })
                  // .UseUrls("http://*:5030")
                   .UseStartup<Startup>();
    }
    //    public static IWebHost BuildWebHost(string[] args) =>

    //        WebHost.CreateDefaultBuilder(args)
    //            .ConfigureAppConfiguration((builderContext, config) =>
    //            {
    //                IHostingEnvironment env = builderContext.HostingEnvironment;
    //                     config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
    //                    .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: false, reloadOnChange: true);
    //            })
    //            .UseStartup<Startup>()
    //            .UseUrls("http://*:5030")
    //            .Build();

    //}
}
