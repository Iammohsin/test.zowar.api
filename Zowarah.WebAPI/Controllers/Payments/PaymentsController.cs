﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Zowarah.WebAPI.Handlers.Payments;

namespace Zowarah.WebAPI.Controllers.Payments {
    [Route("api/payments/[controller]/[action]")]
    [Authorize(Roles = "Administrator")]
    public class PaymentsController : Controller
    {
        private readonly IMediator mediator;

        public PaymentsController(IMediator _mediator)
        {
            mediator = _mediator;
        }
        [HttpPost]
        public IActionResult ConfirmInvoicePayment([FromBody] ConfirmInvoicePaymentRequest payment)
        {
            var confirm = mediator.Send(payment);

            return (confirm.Result != null) ? Ok(confirm.Result) : (IActionResult)NotFound();
        }
    }
}