﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Microsoft.AspNetCore.Identity;
using System.Net.Mail;
using System.Net;
using Zowarah.Domain.Entities.Users;
using Zowarah.WebAPI.Handlers.Users;
using Zowarah.WebAPI.Handlers.Common.NotificationsHandlers;
using System.Text;
using Microsoft.AspNetCore.WebUtilities;
using Zowarah.WebAPI.Config.AppSettings;
using Zowarah.WebAPI.Config.Notification;
using Microsoft.AspNetCore.Authorization;

namespace Zowarah.WebAPI.Controllers.Users {
    [Route("api/users/[controller]/[action]")]
    [AllowAnonymous]
    public class SignupController : Controller {
        private readonly IMediator _mediator;
        public UserManager<AppUser> _userManager;
        //private readonly IEmailSender _emailSender;
        //,IEmailSender emailSender
        public SignupController(IMediator mediator, UserManager<AppUser> userManager) {
            _mediator = mediator;
            _userManager = userManager;
            //_emailSender = emailSender;
        }

        [HttpPost]
        public async Task<IActionResult> Signup([FromBody] SignupUserRequest request) {
            try {
                if (!ModelState.IsValid)
                    throw new Exception("Incomplete Information ..");

                var result = await _mediator.Send(request);

                if (result != null)
                    SendConfirmationEmail(result.Id);

                return result != null ? Ok(result) : (IActionResult)NotFound();

                //  AppUser user = await _userManager.FindByEmailAsync("mazen506@hotmail.com");
                // var result = SendConfirmationEmail(user);
                //return Ok(result.Status);
                //    if (result == null)
                //    return BadRequest();
            } catch (Exception ex) {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost]
        public async Task<IActionResult> ReSendConfirmEmail(long userId) {
            await SendConfirmationEmail(userId);
            return Ok("");
        }

        public async Task SendConfirmationEmail(long Id) {
            var user = await _userManager.FindByIdAsync(Id.ToString());
            if (user == null)
                return;

            string confirmationToken = _userManager.
                   GenerateEmailConfirmationTokenAsync(user).Result;
            byte[] tokenGeneratedBytes = Encoding.UTF8.GetBytes(confirmationToken);
            var codeEncoded = Base64UrlTextEncoder.Encode(tokenGeneratedBytes);
            string confirmationLink = $"{ConfirmationEmail.ConfirmDomain}/{user.Id}/{ codeEncoded}";

            var template = await this._mediator.Send(new GetMailTemplateRequest() { Id = ConfirmationEmail.TemplateId });
            var htmlContent = template.Template.Replace("$$DisplayName$$", user.FirstName).Replace("$$confirmHref$$", confirmationLink);

            await _mediator.Send(new SendMailNotificationRequest() {
                To = user.Email,
                DisplayName = user.FirstName,
                Subject = ConfirmationEmail.Subject,
                Content = htmlContent
            });

        }

        [HttpPost]
        public async Task<IActionResult> ConfirmEmail(string userid, string token) {
            byte[] tokenBytes = Base64UrlTextEncoder.Decode(token);
            var tokenEncoded = Encoding.UTF8.GetString(tokenBytes);
            AppUser user = await _userManager.FindByIdAsync(userid);
            var roles = await _userManager.GetRolesAsync(user);

            IdentityResult result = await _userManager.
                                     ConfirmEmailAsync(user, tokenEncoded);
            //return Ok(Json(roles[0]));
            if (result.Succeeded) {
                return Ok(Json(roles[0]));
            } else {
                return BadRequest(Json("Failed !!"));
            }
        }
        
    }
}
