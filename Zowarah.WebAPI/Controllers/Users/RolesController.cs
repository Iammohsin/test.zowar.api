﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Users;
using Microsoft.AspNetCore.Authorization;

namespace Zowarah.WebAPI.Controllers.Users
{
    [Route("api/users/[controller]")]
    public class RolesController : Controller
    {  
        private readonly ApplicationDbContext _context;
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly RoleManager<AppRole> _roleManager;

        public RolesController(ApplicationDbContext context,
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signinManager,
            RoleManager<AppRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signinManager;
            _roleManager = roleManager;
        }
      
        [HttpPost("{roleName}")]
        public async Task<IActionResult> Post(string roleName)
        {
            if (!await _roleManager.RoleExistsAsync(roleName))
            {
                var role = new AppRole();
                role.Name = roleName;
                var result = await _roleManager.CreateAsync(role);
                if (result.Succeeded)
                    return Ok();
                else
                    return BadRequest();
            }
            else
                return BadRequest();
        }       
    }
}
