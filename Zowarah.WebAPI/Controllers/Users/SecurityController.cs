﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Zowarah.Domain.Entities.Users;
using Zowarah.WebAPI.Handlers.Users;
using Zowarah.WebAPI.Handlers.Users.Security;

namespace Zowarah.WebAPI.Controllers.Users
{
    [Route("api/users/[controller]/[action]")]
    [AllowAnonymous]
    public class SecurityController : BaseController
    {        
        public UserManager<AppUser> _userManager;

        public SecurityController(IMediator mediator,IHttpContextAccessor accessor, UserManager<AppUser> userManager):base(mediator, accessor)
        {            
            _userManager = userManager;
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword([FromBody]ChangePasswordRequest request)
        {
            try
            {
                request.UserId = UserId+"";
                var result = await _mediator.Send(request);
                return result != null ? Ok(result) : (IActionResult)NotFound();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> ForgotPassword([FromBody]ForgotPasswordRequest request)
        {
            try
            {
                var result = await _mediator.Send(request);
                return result != null ? Ok(result) : (IActionResult)NotFound();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> ResetPassword([FromBody]ResetPasswordRequest request)
        {
            try
            {
                var result = await _mediator.Send(request);
                return result != null ? Ok(result) : (IActionResult)NotFound();
            }
            catch (Exception ex)
            {
                return NotFound(Json(ex.Message));
            }
        }
    }
}