﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zowarah.WebAPI.Handlers;
using Zowarah.WebAPI.Handlers.Common;
using Zowarah.WebAPI.Handlers.Users;
using static Zowarah.Identity.Api.Handlers.UserHandlers.GetProfile;

namespace Zowarah.WebAPI.Controllers.Users {
    [Route("api/users/[controller]/[action]")]
    [AllowAnonymous]
    public class ProfilesController : BaseController
    {
        public ProfilesController(IMediator mediator, IHttpContextAccessor contextAccessor) : base(mediator, contextAccessor) { }

        [HttpGet("{userId}")]
        public async Task<IActionResult> GetCurrency(string userId) {
            var result = await _mediator.Send(new GetUserCurrencyRequest { Id = userId });
            return result != null ? Ok(result.CurrencyId) : (IActionResult)NotFound();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id) {
            if (!UserHasRoleAdmin())
                if (id == 0)
                {
                    UserId = 10123;
                }
                id = UserId;
            var result = await _mediator.Send(new GetProfileRequest { Id = id });
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetPreferences(string id) {
            try {
                var result = await _mediator.Send(new GetPreferencesRequest { UserId = id });
                return result != null ? Ok(result) : (IActionResult)NotFound();
            } catch (Exception ex) {
                return NotFound(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> UpdateProfile([FromBody]UpdateProfileRequest request) {
            try {
                request.Id = UserId + "";
                var result = await _mediator.Send(request);
                return result.Message == "Ok" ? Ok() : StatusCode(500);
            } catch (Exception ex) {
                return NotFound(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> UpdatePreferences([FromBody]UpdatePreferencesRequest request) {
            try {
                request.UserId = UserId + "";
                var result = await _mediator.Send(request);
                return result != null ? Ok(result) : (IActionResult)NotFound();
            } catch (Exception ex) {
                return NotFound(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> SetCulture([FromBody] SetCultureRequest request) {
            try {
                request.UserId = UserId;
                var result = await _mediator.Send(request);
                return result != null ? Ok(result) : (IActionResult)NotFound();
            } catch (Exception ex) {
                return NotFound(ex.Message);
            }
        }
    }
}