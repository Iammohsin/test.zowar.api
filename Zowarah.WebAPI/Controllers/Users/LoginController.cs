﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Zowarah.WebAPI.Handlers.Users;
using Zowarah.WebAPI.Handlers.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

namespace Zowarah.WebAPI.Controllers.Users
{
    [AllowAnonymous]
    [Route("api/users/[controller]/[action]")]
    public class LoginController : Controller
    {
        private readonly IMediator _mediator;
        private readonly IConfiguration _configuration;
        public LoginController(
            IMediator mediator,
            IConfiguration configuration
            )
        {
            _mediator = mediator;
            _configuration = configuration;
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginUserRequest request)
        {
            try
            {
                HttpContext.Session.SetString("lang", "en");

                if (!ModelState.IsValid)
                    return BadRequest("Invalid Credentials !!");

                var result = await _mediator.Send(request);

                if (result == null)
                    return Unauthorized();

                var claims = new[]
                        {
                new Claim("id", result.user.Id.ToString()),
                new Claim("email", result.user.Email),
                new Claim("role", request.Role.ToString()),
                new Claim("currency", result.user.CurrencyId !=null  ? result.user.CurrencyId : "OMR" ),
                //new Claim("currency", "INR"),
                new Claim("name", result.user.FirstName + ' ' + result.user.LastName),
                new Claim("culture", result.user.CultureId!=null ? result.user.CultureId : "en" ),
                new Claim("photo", result.user.ProfilePhoto!=null ? result.user.ProfilePhoto.FileName : "user-placeholder.jpg" ),
                new Claim("privs", "")
            };

                var token = new JwtSecurityToken
                        (
                            issuer: _configuration["Token:Issuer"],
                            audience: _configuration["Token:Audience"],
                            claims: claims,
                            expires: DateTime.UtcNow.AddDays(60),
                            notBefore: DateTime.UtcNow,
                            signingCredentials: new SigningCredentials(new SymmetricSecurityKey
                                        (Encoding.UTF8.GetBytes(_configuration["Token:Key"])),
                                            SecurityAlgorithms.HmacSha256)
                        );
                return Ok(new { token = new JwtSecurityTokenHandler().WriteToken(token) });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
