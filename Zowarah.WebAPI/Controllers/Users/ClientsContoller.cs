﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Zowarah.Domain.Enums;
using Zowarah.WebAPI.Handlers.Common;
using Zowarah.WebAPI.Handlers.Users.Clients;

namespace Zowarah.WebAPI.Controllers.Users
{
    [Route("api/users/[controller]/[action]")]
    public class ClientsController : Controller
    {
        private readonly IMediator _mediator;

        public ClientsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetOverview([FromQuery] GetClientsOverviewRequest request)
        {
            var result = await _mediator.Send(request);
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }
    }
}