﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zowarah.WebAPI.Handlers;
using Zowarah.WebAPI.Handlers.Users;

namespace Zowarah.WebAPI.Controllers.Users {
    [Route("api/users/[controller]/[action]")]
    public class PhotosController : BaseController {

        public PhotosController(IMediator mediator, IHttpContextAccessor accessor) : base(mediator, accessor) {

        }
        // ToDo: remove userId parameter
        // GET: api/Hotels/1
        [HttpGet("{userId}")]
        public async Task<IActionResult> Get(long userId) {           
            var result = await _mediator.Send(new GetProfilePhotoRequest { UserId = userId });
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        // ToDo: remove userId parameter
        [HttpPost("{userId}")]
        public async Task<IActionResult> Add(long userId, IFormFile file) {
            var result = await _mediator.Send(new AddProfilePhotoRequest { UserId = UserId, File = file });
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        //[HttpDelete("{profilePhotoId}")]
        //public async Task<IActionResult> Delete([FromBody] DeleteProfilePhotoRequest request)
        //{
        //    var result = await _mediator.Send(new DeleteProfilePhotoRequest { });
        //    return result != null ? Ok(result) : (IActionResult)NotFound();
        //}
    }
}