﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zowarah.WebAPI.Handlers;
using Zowarah.WebAPI.Handlers.Common;
using Zowarah.WebAPI.Handlers.Users;
using Zowarah.WebAPI.Handlers.Users.CreditCard;

namespace Zowarah.WebAPI.Controllers.Users
{
    [Route("api/users/[controller]/[action]")]
    [AllowAnonymous]
    public class CreditCardController : BaseController
    {
        public CreditCardController(IMediator mediator, IHttpContextAccessor contextAccessor) : base(mediator, contextAccessor) { }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetByUserId(long Id)
        {
            var res = await _mediator.Send(new GetByUserIdRequest() { UserId = Id });
            return (res != null) ? Ok(res) : (IActionResult)NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> add([FromBody] AddNewCreditCardRequest request)
        {
            request.UserId = UserId;
            var result = await _mediator.Send(request);
            return result != null ? Ok(result.Message) : (IActionResult)NotFound();
        }
    }
}