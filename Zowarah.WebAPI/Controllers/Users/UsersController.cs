﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zowarah.Domain.Entities.Users;
using Zowarah.WebAPI.Handlers.Users.Users;

namespace Zowarah.WebAPI.Controllers.Users
{
    [Produces("application/json")]
    [Route("api/Users/[action]")]
    public class UsersController : Controller
    {
        private readonly IMediator _mediator;

        public UsersController(IMediator mediator)
        {
            _mediator = mediator;
        }

        // GET: api/Users
        [HttpGet]
        public async Task<IActionResult> Get()
        {   //** ToAdd: recieve the role type from the request **/
            var result = await _mediator.Send(new GetUsersRequest() { Role = RoleTypes.Administrator });
            return result != null ? Ok(result.Users) : (IActionResult)BadRequest();
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<GetUserByIdResponse> GetUserById(long id)
        {
            var result = await _mediator.Send(new GetUserByIdRequest { Id = id });
            return result;
        }

        [HttpGet]
        public async Task<IActionResult> IsInRole([FromQuery]IsUserInRoleRequest request)
        {
            var result = await _mediator.Send(request);
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        // POST: api/Users
        [HttpPost]
        public async Task<IActionResult> CreateUser([FromBody]CreateUserRequest request)
        {
            var result = await _mediator.Send(request);
            return result != null ? Ok(result) : (IActionResult)BadRequest();
        }

        [HttpPut]
        public async Task<IActionResult> UpdateUser([FromBody]UpdateUserRequest request)
        {
            var result = await _mediator.Send(request);
            return result != null ? Ok(result) : (IActionResult)BadRequest();
        }

        [HttpPost]
        public async Task<IActionResult> ChangeStatus([FromBody]ChangeStatusRequest request)
        {
            try
            {
                var result = await _mediator.Send(request);
                return result != null ? Ok(result) : (IActionResult)BadRequest();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }       

        [HttpGet]
        public async Task<IActionResult> GetAdminPermissions()
        {
            var result = await _mediator.Send(new GetAdminPermssionsRequest() { });
            return Ok(result.Permissions);
        }
    }
}