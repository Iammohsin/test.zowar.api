﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.WebAPI.Controllers {
    public abstract class BaseController : Controller {
        public IMediator _mediator { get; private set; }
        public IHttpContextAccessor _contextAccessor { get; private set; }
        public long UserId { get; set; }

        public BaseController(IMediator mediator, IHttpContextAccessor contextAccessor) {
            _mediator = mediator;
            _contextAccessor = contextAccessor;
            if (_contextAccessor.HttpContext.User.Identity.IsAuthenticated) {
                var _userid = _contextAccessor.HttpContext.User.FindFirstValue("id");
                if (!string.IsNullOrEmpty(_userid)) {
                    UserId = long.Parse(_userid);
                }
            }
        }

        public bool UserHasRoleAdmin() {
            return _contextAccessor.HttpContext.User.Identity.IsAuthenticated &&
                _contextAccessor.HttpContext.User.IsInRole(Enum.GetName(typeof(RoleTypes), RoleTypes.Administrator));
        }
        public bool UserInRoles(RoleTypes role) {
            return _contextAccessor.HttpContext.User.Identity.IsAuthenticated &&
                _contextAccessor.HttpContext.User.IsInRole(Enum.GetName(typeof(RoleTypes), role));
        }
    }
}