﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zowarah.Domain.Entities.Users;
using Zowarah.WebAPI.Handlers.Transactions;

namespace Zowarah.WebAPI.Controllers.Common
{
    [Route("api/transactions/[controller]/[action]")]
    [Authorize(Roles= "Administrator")]
    public class ServiceController : Controller
    {
        private readonly IMediator _mediator;

        public ServiceController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<GetPendingApproveResponse> GetPendingRequests()
        {
            var result = await _mediator.Send(new GetPendingApproveRequest());
            return result;
        }

        [HttpGet]
        public async Task<IActionResult> GetRequests([FromQuery] GetRequestsRequest getProviderRequestsRequest)
        {
            try
            {
                var result = await _mediator.Send(getProviderRequestsRequest);
                return result != null ? Ok(result) : (IActionResult)NotFound();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPost("{page}/{size}")]
        public async Task<IActionResult> GetServices([FromBody] GetServicesRequest request, int page, int size = 10)
        {
            request.PageNumber = page;
            request.PageSize = size;
            var result = await _mediator.Send(request);
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [HttpPut]
        public async Task<IActionResult> UpdateRequestStatus([FromBody] UpdateRequestStatusRequest request)
        {
            var approvedSuccessfully = await _mediator.Send(request);
            if (approvedSuccessfully)
                return NoContent();
            return BadRequest();
        }
    }
}