﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Transactions;
using Zowarah.Domain.Entities.Users;
using Zowarah.WebAPI.Handlers.Hotels;
using Zowarah.WebAPI.Handlers.Transactions.TransactionHandlers;

namespace Zowarah.WebAPI.Controllers.Transactions {
    [Route("api/transactions/[controller]/[action]")]
    public class TransactionController : BaseController {

        public TransactionController(IMediator mediator, IHttpContextAccessor accessor) : base(mediator, accessor) {

        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(string id) {
            try {
                var result = await _mediator.Send(new GetTransactionRequest() { Id = id });
                return result != null ? Ok(result) : (IActionResult)NotFound();
            } catch (Exception ex) {
                return NotFound(ex);
            }
        }

        [HttpGet("{ClientId}/{BookingFilterId}")]
        public async Task<IActionResult> GetClientTransactions(long ClientId,BookingFilterEnum BookingFilterId) {
            var res = await _mediator.Send(new ClientServiceTransactionListRequest() { ClientId = ClientId, BookingFilterId = BookingFilterId });
            return Ok(res.TransactionList);
        }

        [HttpGet("{ProviderId}")]
        public async Task<IActionResult> GetProviderTransactions(long ProviderId) {
            var res = await _mediator.Send(new ProviderServiceTransactionListRequest() { ProviderId = ProviderId });
            return Ok(res.TransactionList);
        }

        [HttpPost]
        public async Task<IActionResult> GetAllBookingTransactions([FromBody] GetAllBookingTransactionsRequest request) {
            var res = await _mediator.Send(request);
            return Ok(res.Transactions);
        }

        [HttpPost]
        public async Task<IActionResult> SearchClientTransactions([FromBody] SearchClientServicesRequest request) {
            var res = await _mediator.Send(request);
            return Ok(res.TransactionList);
        }

        [HttpPost]
        public async Task<IActionResult> SearchProviderTransactions([FromBody] SearchProviderServicesRequest request) {
            var res = await _mediator.Send(request);
            return Ok(res.TransactionList);
        }
        [HttpPost("{page}/{size}")]
        public async Task<IActionResult> GetAllTransactionsByFilter(int page, int size, [FromBody] GetTransactionsByFilterRequest request) {
            request.Page = page;
            request.PageSize = size;
            var res = await _mediator.Send(request);
            return Ok(res);
        }
        [HttpPost]
        public async Task<IActionResult> GetTranactionsReportByFilter([FromBody] GetTranactionsReportByFilterRequest request) {
            var res = await _mediator.Send(request);
            return Ok(res.Transactions);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateTranascationStatus([FromBody] UpdateTransactionSatusRequest request) {
            if (UserInRoles(RoleTypes.Provider)) {
                request.ProviderId = UserId;
            } else if (UserInRoles(RoleTypes.Provider)) {
                request.ClientId = UserId;
            }
            var res = await _mediator.Send(request);
            return (res != null) ? Ok(res) : (IActionResult)NotFound();
        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> CancelTransaction(string id) {
            var request = new UpdateTransactionSatusRequest();
            request.Id = id;
            request.Status = TransactionStatus.Cancelled;
            if (UserInRoles(RoleTypes.Provider)) {
                request.ProviderId = UserId;
            } else if (UserInRoles(RoleTypes.Provider)) {
                request.ClientId = UserId;
            }
            var res = await _mediator.Send(request);
            return (res != null) ? Ok(res) : (IActionResult)NotFound();
        }
    }
}