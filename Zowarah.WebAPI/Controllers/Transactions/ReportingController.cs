﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Zowarah.WebAPI.Handlers.Transactions;

namespace Zowarah.WebAPI.Controllers.Transactions
{
    [Route("api/transactions/[controller]/[action]")]
    public class ReportingController : Controller
    {
        private readonly IMediator _mediator;

        public ReportingController(IMediator mediator)
        {
            _mediator = mediator;
        }
        

       [HttpPost]
        public async Task<IActionResult> GetTransactions([FromBody] GetSalesReportRequest request)
        {           
            var result = await _mediator.Send(request);
            return result != null ? Json(result.Sales) : (IActionResult)NotFound();
        }

        [HttpPost("{date}")]
        public async Task<IActionResult> TransactionDetails(DateTime date,[FromBody] GetSalesReportDetailsRequest request)
        {
            request.Date = date;
            var result = await _mediator.Send(request);
            return result != null ? Json(result) : (IActionResult)NotFound();
        }
    }
}