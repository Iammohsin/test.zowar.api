﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Zowarah.WebAPI.Handlers.Common;

namespace Zowarah.WebAPI.Controllers.Common
{
    [Route("api/common/[controller]/[action]")]
    [AllowAnonymous]
    public class ProvidersController : Controller
    {
        private readonly IMediator _mediator;

        public ProvidersController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("{page}/{pageSize}/{from}/{to}")]
        public async Task<GetProvidersResponse> GetAll(int page,int pageSize, DateTime from,DateTime to)
        {
            return await _mediator.Send(new GetProvidersRequest { PageNumber=page,PageSize= pageSize , From=from,To=to});
        }

        [HttpGet("{from}/{to}")]
        public async Task<GetProvidersReportResponse> ProvidersReport(DateTime from, DateTime to)
        {
            return await _mediator.Send(new GetProvidersReportRequest { From = from, To = to });
        }
    }
}