﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Zowarah.WebAPI.Handlers.Common.Reporting;
using Microsoft.AspNetCore.Authorization;


namespace Zowarah.WebAPI.Controllers.Common
{
    [AllowAnonymous]
    [Route("api/common/[controller]/[action]")]
    public class ReportingController : Controller
    {
        private readonly IMediator _mediator;

        public ReportingController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [HttpGet]
        public async Task<IActionResult> GetAdminLandingPageDashboard()
        {
            var result = await _mediator.Send(new GetAdminDashboardRequest());
            return result != null ? Json(result) : (IActionResult)NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> GetServicesReport([FromBody] GetServicesReportRequest request)
        {
            var result = await _mediator.Send(request);
            return result != null ? Json(result.ServiceReports) : (IActionResult)NotFound();
        }
    }
}