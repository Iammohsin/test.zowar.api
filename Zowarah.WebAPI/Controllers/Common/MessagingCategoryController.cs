﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zowarah.WebAPI.Handlers;
using Zowarah.WebAPI.Handlers.Common;

namespace Zowarah.WebAPI.Controllers.Common
{
    [AllowAnonymous]
    [Route("api/common/[controller]/[action]")]
    public class MessagingCategoryController : Controller
    {
        private readonly IMediator _mediator;

        public MessagingCategoryController(IMediator mediator)
        {
            _mediator = mediator;
        }

        // GET: api/Hotels/1
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _mediator.Send(new GetMessagingCategoryListRequest() { });
            return result != null ? Ok(result.MessagesCategories) : (IActionResult)NotFound();
        }
    }
}