﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zowarah.WebAPI.Handlers;
using Zowarah.WebAPI.Handlers.Common;

namespace Zowarah.WebAPI.Controllers.Common {
    [Route("api/common/[controller]/[action]")]
    [AllowAnonymous]
    public class LocalizationController : Controller {
        private readonly IMediator _mediator;

        public LocalizationController(IMediator mediator) {
            _mediator = mediator;
        }

        [HttpGet("{languageId}")]
        public async Task<IActionResult> GetAll(string languageId) {
            var result = await _mediator.Send(new GetLocalizationListRequest { LanguageId = languageId });
            return result != null ? Ok(result.list) : (IActionResult)NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> Add([FromBody] AddLocalizationRequest request) {
            var result = await _mediator.Send(request);
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [HttpPut]
        public async Task<IActionResult> UpdateText([FromBody] UpdateLocalizationTextRequest request) {
            var result = await _mediator.Send(request);
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }     
        
    }
}