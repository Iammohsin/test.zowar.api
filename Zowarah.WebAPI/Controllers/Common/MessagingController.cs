﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zowarah.Domain.Entities.Users;
using Zowarah.WebAPI.Handlers;
using Zowarah.WebAPI.Handlers.Common;

namespace Zowarah.WebAPI.Controllers.Common {
    [AllowAnonymous]
    [Route("api/common/[controller]/[action]")]
    public class MessagingController : BaseController {
        public MessagingController(IMediator mediator, IHttpContextAccessor contextAccessor) : base(mediator, contextAccessor) { }

        [HttpGet("{UserId}/{MessageId}")]
        public async Task<IActionResult> Get(long UserId,string MessageId) {
            var result = await _mediator.Send(new GetMessageRequest() { UserId = UserId, MessageId = MessageId });
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Compose([FromBody] ComposeMessageRequest request) {
            if (UserInRoles(RoleTypes.Provider))
            {
                request.SendByProvider = true;
                request.UserId = UserId;
            }
            else
                request.SenderId = UserId;
                
            var result = await _mediator.Send(request);
            return result != null ? Ok(result.MessageId) : (IActionResult)NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Reply([FromBody]ReplyMessageRequest request) {
            var result = await _mediator.Send(request);
            return result != null ? Json(result.MessageTrailId) : (IActionResult)NotFound();
        }

        [HttpPost("{MessageId}")]
        public async Task<IActionResult> Close(string MessageId) {
            var result = await _mediator.Send(new CloseMessageRequest { MessageId = MessageId });
            return Ok(result);
        }

        // ToDo: remove userId from parameter
        [HttpDelete("{UserId}/{MessageId}")]
        public async Task<IActionResult> Delete(long userId, string MessageId) {
            var request = new DeleteMessageRequest();
            request.MessageId = MessageId;
            request.UserId = UserId;
            request.SenderId = userId;
            if (UserInRoles(RoleTypes.Provider))
                request.SendByProvider = true;
            else
                request.SendByProvider = false;
            var result = await _mediator.Send(request);
            return Ok(result);
        }

        // ToDo: remove userId from parameter
        [HttpGet("{UserId}/{RoleType}")]
        public async Task<IActionResult> GetNoPendingMessages(long userId, RoleTypes roleType) {
            var result = await _mediator.Send(new GetNoPendingMessagesRequest { UserId = UserId, RoleType = roleType });
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> AdminNewMessagesCount() {
            var result = await _mediator.Send(new AdminNewMessageCountRequest { });
            return Ok(result);
        }
        [HttpGet]
        public async Task<IActionResult> AdminActiveMessagesCount() {
            var result = await _mediator.Send(new AdminActiveMessageCountRequest { UserId = UserId });
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetAdminNewMessages() {
            var result = await _mediator.Send(new GetAdminNewMessagesRequest { });
            return Ok(result.Messages);
        }

        [HttpGet]
        public async Task<IActionResult> GetAdminActiveMessages() {
            var result = await _mediator.Send(new GetAdminActiveMessageRequest { UserId = UserId });
            return Ok(result.Messages);
        }

        // ToDo: remove userId from parameter
        [HttpPost]
        public async Task<IActionResult> StartProcessing(string MessageId, long userId) {
            var result = await _mediator.Send(new StartProcessingRequest { MessageId = MessageId, UserId = UserId });
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> MarkRead([FromBody] MarkReadRequest request) {
            var result = await _mediator.Send(request);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> GetMessages([FromBody]GetMessagesRequest request) {
            request.UserId = UserId;
            var result = await _mediator.Send(request);
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> IsValidTransaction([FromBody]IsValidTransactionRequest request) {
            var result = await _mediator.Send(request);
            return Json(result);
        }
    }
}