﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Reviews;
using Zowarah.Domain.Entities.Invoices;
using Zowarah.Domain.Entities.Transactions;
using Zowarah.Domain.Entities.Users;
using Zowarah.WebAPI.Handlers.Common;
using Microsoft.AspNetCore.Authorization;

namespace Zowarah.WebAPI.Controllers.Common {
    [Route("api/common/[controller]/[action]")]
    [AllowAnonymous]
    public class LookUpController : Controller {

        private readonly IMediator _mediator;

        public LookUpController(IMediator mediator) {
            _mediator = mediator;
        }
        [HttpGet("{Name}")]
        public async Task<IActionResult> Get(string Name) {
            var res = await _mediator.Send(new GetLookUpsDataRequest() { Name = Name });
            return Json(res.List);
        }



        [HttpPost("{Name}")]
        [Authorize(Roles = "Administrator")]
        public async Task<CreateOrUpdateLookUpResponse> update(string Name, [FromBody] JObject payload) {
            return await _mediator.Send(new CreateOrUpdateLookUpRequest() { Name = Name, JObject = payload });
        }



    }
}