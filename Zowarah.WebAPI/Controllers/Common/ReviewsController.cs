﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Zowarah.Application.Services.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.WebAPI.Handlers.Common;
using Zowarah.WebAPI.Handlers.Reviews;

namespace Zowarah.WebAPI.Controllers.Reviews
{
    [Route("api/common/[controller]/[action]")]
    public class ReviewsController : Controller
    {
        private readonly IMediator mediator;

        public ReviewsController(IMediator _mediator)
        {
            mediator = _mediator;
        }

        [AllowAnonymous]
        [HttpGet("{serviceType}")]
        public async Task<IActionResult> GetServiceFeatureTypes(ServiceTypeEnum serviceType) {
            var res = await mediator.Send(new GetServiceFeatureTypesRequest() { ServiceTypeId= serviceType });
            return Json(res.FeatureTypes);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetFeatureTypes()
        {
            var res = await mediator.Send(new GetFeatureTypesRequest());
            return Json(res.FeatureTypes);
        }

        [AllowAnonymous]
        [HttpGet("{ServiceId}/{Page}/{Count}/{OrderBy}")]
        public async Task<IActionResult> GetByServiceId(long ServiceId, int Page, int Count, ReviewOrderbyEnum OrderBy)
        {
            var res = await mediator.Send(new GetAllServiceReviewsRequest { ServiceId = ServiceId, Page = Page, Count = Count, OrderBy = OrderBy });
            return Json(res.Reviews);
        }

        [AllowAnonymous]
        [HttpGet("{ServiceId}")]
        public async Task<IActionResult> GetServiceReviewsScore(long ServiceId)
        {

            var res = await mediator.Send(new GetServiceReviewsScoreRequest { ServiceId = ServiceId });
            return Json(res);
        }

  

        [HttpPost]
        public async Task<IActionResult> ReplyReview([FromBody] CreateReplyReviewRequest request)
        {
            var review = await mediator.Send(request);
            if (review != null)
                return Json(review);
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> CreateReview([FromBody] CreateReviewRequest request)
        {
            var review = await mediator.Send(request);
            if (review != null)
                return Json(review.Id);
            return NotFound();
        }
    }
}