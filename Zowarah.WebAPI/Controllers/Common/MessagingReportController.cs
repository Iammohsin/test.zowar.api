﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zowarah.Domain.Entities.Users;
using Zowarah.WebAPI.Handlers.Common.MessagingReportHandlers;

namespace Zowarah.WebAPI.Controllers.Common
{
    [AllowAnonymous]
    [Route("api/common/[controller]/[action]")]
    public class MessagingReportController : ControllerBase
    {
        private readonly IMediator mediator;

        public MessagingReportController(IMediator _mediator)
        {
            mediator = _mediator;
        }

        [HttpGet("{role}")]
        public async Task<IActionResult> GetMessages(RoleTypes role)
        {
            var result = await mediator.Send(new GetMessagesReportsRequest { role = role });
            return result != null ? Ok(result.messages) : (IActionResult)NotFound();
        }

        [HttpGet]
        public async Task<IActionResult> GetAll(RoleTypes role)
        {
            var result = await mediator.Send(new GetAllRequest { });
            return result != null ? Ok(result.messages) : (IActionResult)NotFound();
        }
    }
}