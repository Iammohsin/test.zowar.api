﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zowarah.WebAPI.Handlers;
using Zowarah.WebAPI.Handlers.Common;

namespace Zowarah.WebAPI.Controllers.Common
{
    [AllowAnonymous]
    [Route("api/common/[controller]/[action]")]
    public class CityController : Controller
    {
        private readonly IMediator _mediator;

        public CityController(IMediator mediator)
        {
            _mediator = mediator;
        }               

        [HttpPost]
        public async Task<IActionResult> Search([FromBody] SearchCityRequest request )
        {
            var result = await _mediator.Send(request);
            return result != null ? Ok(result.Cities) : (IActionResult)NotFound();
        }

        // GET: api/Hotels/1
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _mediator.Send(new GetCityListRequest { });
            return result != null ? Ok(result.CityList) : (IActionResult)NotFound();
        }
    }
}