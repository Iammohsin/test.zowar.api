﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Controllers.Hotels {
    [Route("api/hotels/[controller]/[action]")]
    public class HotelReportingController : BaseController {


        public HotelReportingController(IMediator mediator, IHttpContextAccessor accessor) : base(mediator, accessor) {

        }
        [HttpGet("{hotelId}")]
        public async Task<IActionResult> LandingPage(long hotelId) {
            var result = await _mediator.Send(new GetHotelLandPageRequest { HotelId = hotelId, UserId = UserId });
            return result != null ? Json(result) : (IActionResult)NotFound();
        }

        [HttpPost("{HotelId}")]
        public async Task<IActionResult> HotelsReportsSales(long HotelId, [FromBody] GetHotelSaleReportingRequest request) {
            request.HodelId = HotelId;
            request.UserId = UserId;
            var result = await _mediator.Send(request);
            return result != null ? Json(result.Rooms) : (IActionResult)NotFound();
        }

        [HttpPost("{HotelId}")]
        public async Task<IActionResult> HotelsReportsSalesDetails(long HotelId, [FromBody] GetHotelRoomSaleDetailsRequest request) {
            request.HodelId = HotelId;
            request.UserId = UserId;
            var result = await _mediator.Send(request);
            return result != null ? Json(result) : (IActionResult)NotFound();
        }
    }
}