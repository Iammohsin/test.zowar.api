﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zowarah.WebAPI.Handlers;
using MediatR;
using Zowarah.WebAPI.Handlers.Hotels;
using Zowarah.WebAPI.Handlers.Hotels.Promotions;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace Zowarah.WebAPI.Controllers.Hotels {
    [Route("api/hotels/[controller]/[action]")]
    public class PromotionsController : BaseController {

        public PromotionsController(IMediator mediator, IHttpContextAccessor accessor) : base(mediator, accessor) {

        }


        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long Id) {
            var result = await _mediator.Send(new GetPromotionRequest { Id = Id });
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        // GET: api/Hotels/1
        [HttpGet("{ServiceId}")]
        public async Task<IActionResult> GetByServiceId(long ServiceId) {

            var result = await _mediator.Send(new GetPromotionsByServiceIdRequest { ServiceId = ServiceId });
            return result != null ? Ok(result.promotions) : (IActionResult)NotFound();
        }
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetRoomsPromotions([FromQuery]GetRoomsPromotionsRequest request) {
            var result = await _mediator.Send(request);
            return result != null ? Ok(result.Promotions) : (IActionResult)NotFound();
        }

        [HttpGet]
        public async Task<IActionResult> GetRoomPromotion([FromQuery]GetRoomPromotionRequest request) {
            var result = await _mediator.Send(request);
            return result != null ? Ok(result.Promotion) : (IActionResult)NotFound();
        }
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] AddPromotionRequest request) {
            request.UserId = UserId;
            var result = await _mediator.Send(request);
            return result != null ? Ok(result.Id) : (IActionResult)NotFound();
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] UpdatePromotionRequest request) {
            request.UserId = UserId;
            var result = await _mediator.Send(request);
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> Delete(long id) {
            var result = await _mediator.Send(new DeletePromotionRequest { Id = id, UserId = UserId });
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [HttpGet("{PromotionId}/{Status}")]
        public async Task<IActionResult> SetStatus(long promotionId, Boolean status) {
            var result = await _mediator.Send(new SetPromotionStatusRequest { Id = promotionId, Status = status, UserId = UserId });
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [AllowAnonymous]
        [HttpGet("{noOfPromotions}")]
        public async Task<IActionResult> GetTopPromotions(int noOfPromotions) {
            var result = await _mediator.Send(new GetTopPromotionsRequest { NoOfPromotions = noOfPromotions });
            return result != null ? Ok(result.Promotions) : (IActionResult)NotFound();
        }
    }
}
