﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zowarah.WebAPI.Handlers;
using MediatR;
using Zowarah.WebAPI.Handlers.Hotels;
using Zowarah.Domain.Entities.Common;
using Zowarah.WebAPI.Handlers.Hotels.HotelDescription;
using Zowarah.WebAPI.Handlers.Hotels.PropertyLanguagesHandlers;
using Microsoft.AspNetCore.Authorization;

namespace Zowarah.WebAPI.Controllers.Hotels {
    [Route("api/hotels/[controller]/[action]")]
    public class HotelsController : BaseController {

        public HotelsController(IMediator mediator, IHttpContextAccessor accessor) : base(mediator, accessor) {
        }

        // GET: api/Hotels/1
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id) {
            var result = await _mediator.Send(new GetHotelRequest { Id = id });
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> IsAvailable(long id, [FromBody] SearchHotelRequest searchOption) {
            var result = await _mediator.Send(new GetHotelRequest { Id = id });
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSpokenLanguages(long id) {
            var result = await _mediator.Send(new GetPropertyLanguagesRequest { Id = id });
            return result != null ? Json(result.Languages) : (IActionResult)NotFound();
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GenerateDesciption(long id) {
            var result = await _mediator.Send(new GenerateHotelDescptrionRequest { ServiceId = id });
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }
        [AllowAnonymous]
        [HttpGet("{ProviderId}")]
        public async Task<IActionResult> GetByProviderId(long providerId) {
            var result = await _mediator.Send(new GetHotelByProviderIdRequest { ProviderId = providerId });
            return result != null ? Ok(result.HotelsLst) : (IActionResult)NotFound();
        }
        [AllowAnonymous]
        [HttpGet("{ServiceId}")]
        public async Task<IActionResult> GetActiveBookings(long ServiceId) {
            var result = await _mediator.Send(new GetActiveBookingsRequest { ServiceId = ServiceId });
            return result != null ? Ok(result.BookingList) : (IActionResult)NotFound();
        }
        [AllowAnonymous]
        [HttpGet("{ServiceId}")]
        public async Task<IActionResult> GetTodayBookings(long ServiceId)
        {
            var result = await _mediator.Send(new GetActiveBookingsRequest { ServiceId = ServiceId });
                    return result != null ? Ok(result.BookingList) : (IActionResult)NotFound();
        }
        
        [HttpPost]
        public async Task<IActionResult> SearchBookingTransactions([FromBody]SearchBookingListRequest SearchBookingListRequest) {
            SearchBookingListRequest.UserId = UserId;
            var result = await _mediator.Send(SearchBookingListRequest);
            return result != null ? Ok(result.BookingList) : (IActionResult)NotFound();
        }

        // DELETE: api/Hotels/1
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id) {
            try {
                await _mediator.Send(new DeleteHotelRequest { Id = id ,UserId=UserId});
                return Ok();
            } catch (Exception ex) {
                return BadRequest(ex.Message);
            }
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRoomAmenities(long id) {
            var result = await _mediator.Send(new GetRoomAmenitiesRequest { HotelId = id });
            return result != null ? Ok(result.RoomAmenities) : (IActionResult)NotFound();
        }

        [HttpPut("{hotelId}")]
        public async Task<IActionResult> UpdateRoomAmenities(long hotelId, [FromBody]HotelRoomAmenityDetails[] amenities) {
            var result = await _mediator.Send(new UpdateRoomAmenitiesRequest { HotelId = hotelId, UserId = UserId, RoomAmenities = amenities });
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Search([FromBody] SearchHotelRequest request) {
            var result = await _mediator.Send(request);
            return result != null ? Json(result.lst) : (IActionResult)NotFound();
        }
        [AllowAnonymous] 
        [HttpGet("{RoomId}")]
        public async Task<IActionResult> GetHotelRoom(long roomId) {
            var result = await _mediator.Send(new GetHotelRoomRequest { RoomId = roomId });
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [HttpGet("{HotelId}")]
        public async Task<IActionResult> GetConfigFlags(long hotelId) {
            var result = await _mediator.Send(new GetConfigFlagsRequest { Id = hotelId });
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }
        [HttpGet("{hotelId}")]
        public async Task<IActionResult> GetHotelConfigurationState(long hotelId) {
            var result = await _mediator.Send(new GetHotelConfigurationStateRequest { HotelId = hotelId, UserId = UserId });
            return result != null ? Json(result) : (IActionResult)NotFound();
        }


        [HttpGet]
        public async Task<IActionResult> GetHotelSearchParameters([FromQuery]GetHotelSearchParametersRequest request) {
            var result = await _mediator.Send(request);
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [HttpGet]
        public async Task<IActionResult> GetRoomSearchParameters([FromQuery]GetRoomSearchParametersRequest request) {
            var result = await _mediator.Send(request);
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [HttpPost("{HotelId}")]
        public async Task<IActionResult> SubmitDetails(long hotelId) {
            try {
                var result = await _mediator.Send(new SubmitDetailsRequest { Id = hotelId, UserId = UserId });
                return result != null ? Ok(result) : (IActionResult)NotFound();
            } catch (Exception ex) {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet]
        public async Task<IActionResult> GetRtcpayDetails()
        {
            var result = await _mediator.Send(new GetRtcpayRequest { });           
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }
    }
}
