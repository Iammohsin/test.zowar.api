﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zowarah.WebAPI.Handlers;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Controllers.Hotels {
    [Route("api/hotels/[controller]/[action]")]
    public class PaymentPolicyController : BaseController {

        public PaymentPolicyController(IMediator mediator, IHttpContextAccessor accessor) : base(mediator, accessor) {

        }
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long Id) {
            var result = await _mediator.Send(new GetPaymentPolicyRequest { Id = Id });
            //if (result == null)
            //return BadRequest();
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        // POST api/PaymentPolicy
        // PUT api/PaymentPolicy/1
        [HttpPut("{hotelId}")]
        public async Task<IActionResult> Update(long hotelId, [FromBody] UpdatePaymentPolicyRequest request) {
            if (ModelState.IsValid) {
                request.UserId = UserId;
                request.Id = hotelId;
                await _mediator.Send(request);
                return Ok();
            } else
                return NotFound();
        }
    }
}
