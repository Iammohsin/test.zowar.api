﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zowarah.WebAPI.Handlers;
using MediatR;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Controllers.Hotels
{
    [Route("api/hotels/[controller]/[action]")]
    public class InternetOptionsController : Controller
    {
        private readonly IMediator _mediator;
        public InternetOptionsController(IMediator mediator)
        {
            _mediator = mediator;
        }
                
        // GET: api/Hotels/1
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _mediator.Send(new GetInternetOptionsListRequest {});
            return result != null ? Ok(result.InternetOptions) : (IActionResult)NotFound();
        }

        
    }
}
