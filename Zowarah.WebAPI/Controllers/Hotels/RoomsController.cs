﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Newtonsoft.Json;
using Zowarah.WebAPI.Handlers;
using Zowarah.WebAPI.Handlers.Hotels;
using Microsoft.AspNetCore.Authorization;

namespace Zowarah.WebAPI.Controllers.Hotels {
    [Route("api/hotels/[controller]/[action]")]
    public class RoomsController : BaseController {

        public RoomsController(IMediator mediator, IHttpContextAccessor accessor) : base(mediator, accessor) {
        }

        // GET: api/Rooms/1
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id) {
            try {
                var result = await _mediator.Send(new GetRoomsRequest { Id = id });
                return result != null ? Ok(result) : (IActionResult)NotFound();
            } catch (Exception ex) {
                return NotFound(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateRoomsRequest request) {
            try {
                request.UserId = UserId;
                var result = await _mediator.Send(request);
                if (result == null)
                    return BadRequest();
                return Json(result.Id);
            } catch (Exception ex) {
                return NotFound(ex.Message);
            }
        }


        // DELETE: api/Rooms/1
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id) {

            var result = await _mediator.Send(new DeleteRoomsRequest { Id = id,UserId=UserId });
            return Ok(result);
        }
        [AllowAnonymous]
        [HttpGet("{HotelId}")]
        public async Task<IActionResult> GetByHotelId(long hotelId) {
            try {
                var result = await _mediator.Send(new GetRoomsByHotelIdRequest { HotelId = hotelId });
                return result != null ? Ok(result.RoomsLst) : (IActionResult)NotFound();
            } catch (Exception ex) {
                return NotFound(ex.Message);
            }
        }


        // PUT api/PropertyBasicInfo/1
        [HttpPut]
        public async Task<IActionResult> Update([FromBody] UpdateRoomsRequest request) {
            try {
                request.UserId = UserId;
                var result = await _mediator.Send(request);
                return result != null ? Json(result.Id) : (IActionResult)NotFound();
            } catch (Exception ex) {
                return NotFound(ex.Message);
            }
        }
    }
}
