﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Zowarah.WebAPI.Handlers.Hotels.ServiceProfilesHandler;

namespace Zowarah.WebAPI.Controllers.Hotels {
    [Route("api/hotels/[controller]/[action]")]
    public class ServiceProfileController : BaseController {


        public ServiceProfileController(IMediator mediator, IHttpContextAccessor accessor) : base(mediator, accessor) {

        }
        [AllowAnonymous]
        [HttpGet("{Language}/{ServiceId}")]
        public async Task<IActionResult> GetPropertyProfile(string Language, long ServiceId) {
            var result = await _mediator.Send(new GetServiceProfileRequest() { Language = Language, ServiceId = ServiceId });
            return result != null ? Json(result) : (IActionResult)NotFound();
        }

        [HttpGet("{ServiceId}")]
        public async Task<IActionResult> GetAll(long ServiceId) {
            var result = await _mediator.Send(new GetAllServiceProfileRequest() { ServiceId = ServiceId });
            return result != null ? Json(result.Profiles) : (IActionResult)NotFound();
        }
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateServiceProfileRequest request) {
            request.UserId = UserId;
            var result = await _mediator.Send(request);
            if (result == null)
                return BadRequest();
            return Json(result.Id);
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] UpdateServiceProfileRequest request) {
            request.UserId = UserId;
            var result = await _mediator.Send(request);
            return result != null ? Json(result.Id) : (IActionResult)NotFound();
        }

        [HttpDelete("{id}")]
        public async Task<long> Delete(long id) {
            await _mediator.Send(new DeleteServiceProfileRequest { Id = id });
            return id;
        }
    }
}