﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zowarah.Application.Services.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.WebAPI.Handlers.Hotels.ServiceNearbyHandlers;

namespace Zowarah.WebAPI.Controllers.Hotels {
    [Route("api/hotels/[controller]/[action]")]
    public class ServiceNearbyController : BaseController {
        public ServiceNearbyController(IMediator mediator, IHttpContextAccessor accesor) : base(mediator, accesor) {
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetServiceNearByTypes() {
            var result = await _mediator.Send(new GetServiceNearbyTypesRequest());
            return result != null ? Json(result.types) : (IActionResult)NotFound();
        }
        [AllowAnonymous]
        [HttpGet("{ServiceId}")]
        public async Task<IActionResult> GetAllServiceNearby(long ServiceId) {
            var result = await _mediator.Send(new GetAllServiceNearbyRequest() { ServiceId = ServiceId });
            return result != null ? Json(result.Nearbies) : (IActionResult)NotFound();
        }

        [HttpPost("{ServiceId}")]
        public async Task<IActionResult> UpdateServiceNearby(long ServiceId, [FromBody] List<ServiceNearbyQuery> ServiceNearbies) {
            var result = await _mediator.Send(new UpdateServiceNearbyRequest { ServiceId = ServiceId, UserId = UserId, ServiceNearbies = ServiceNearbies });
            if (result != null)
                return Json(result.ServiceNearbies);
            return NotFound();
        }


        // DELETE: api/ServiceNearby/1
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var result = await _mediator.Send(new DeleteServiceNearbyRequest { Id = id});
            return Ok(result);
        }        
    }
}