﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Controllers.Hotels {
    [Route("api/hotels/[controller]/[action]")]
    public class BookingController : BaseController {

        public BookingController(IMediator mediator, IHttpContextAccessor accessor) : base(mediator, accessor) {
        }



        [HttpGet("{id}")]
        public async Task<IActionResult> GetPropertyBookingParams(long id) {
            // Current Parameters : LastReservedRooms
            var result = await _mediator.Send(new GetPropertyBookingParametersRequest { HotelId = id });
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetBookingDetails(string id) {
            var request = new GetBookingDetailsRequest { Id = id };
            if (UserInRoles(Domain.Entities.Users.RoleTypes.Client)) {
                request.UserId = UserId;
            }
            var result = await _mediator.Send(request);
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [HttpGet("{roomId}/{fromDate}/{toDate}")]
        public async Task<IActionResult> GetNetBooking(long roomId, DateTime fromDate, DateTime toDate) {
            var result = await _mediator.Send(new GetNetBookingRequest { RoomId = roomId, FromDate = fromDate, ToDate = toDate });
            return result != null ? Ok(result.lst) : (IActionResult)NotFound();
        }


        [AllowAnonymous]
        [HttpGet("{hotelId}/{fromDate}/{toDate}/{noOfRooms}/{persons}/{children}")]
        public async Task<IActionResult> AvailableRooms(long hotelId, DateTime fromDate, DateTime toDate, int noOfRooms, int persons, int children) {
            var result = await _mediator.Send(new AvailableRoomsRequest { HotelId = hotelId, noOfRooms = noOfRooms, Persons = persons, Children = children, FromDate = fromDate, ToDate = toDate });
            return Ok(result.Rooms);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> CreateBooking([FromBody] CreateBookingRequest request) {
            request.ClientId = UserId;
            var result = await _mediator.Send(request);
           return result != null ? Ok(result.Id) : (IActionResult)NotFound();
            //return NotFound();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> CreateGuestBooking([FromBody] CreateGuestBookingRequest request)
        {
            //request.ClientId = UserId;
            var result = await _mediator.Send(request);
            return result != null ? Ok(result.Id) : (IActionResult)NotFound();
            //return NotFound();
        }
        //public class MobilePaymentAuthorizeRequestMessage
        //{
        //     public string BranchID { get; set; }
        //     public string RefNum { get; set; }

        //     public string BillNumber { get; set; }
        //     public string MobileNo { get; set; }
        //     public decimal Amount { get; set; }

        //     public int ExpiryPeriodType { get; set; }
        //     public int ExpiryPeriod { get; set; }

        // }
        [AllowAnonymous]
        [HttpGet("{mobilenum}/{refnum}/{billnum}/{amount}")]
        public async Task<IActionResult> OnlinePayment(string mobilenum, string refnum, string billnum, decimal amount)
        {
            //MobilePaymentAuthorizeRequestMessage mobilepay = new MobilePaymentAuthorizeRequestMessage();

            using (var httpClient = new HttpClient())
            {
                //using (var request = new HttpRequestMessage(new HttpMethod("POST"), "http://payment.zowar.com.sa/index.php?RefNum=" + refnum + "&BillNumber=" + billnum + "&MobileNo=" + mobilenum + "&Amount=" + amount))
                //{
                //    //request.Headers.TryAddWithoutValidation("content-type", "application/json");
                //    //request.Headers.TryAddWithoutValidation("X-ClientCode", "61247531004");
                //    //request.Headers.TryAddWithoutValidation("X-UserName", "ZoTstWrUSR");
                //    //request.Headers.TryAddWithoutValidation("X-Password", "Zr@$y157$cc");                  
                //    //request.Content = new StringContent("{\"MobilePaymentAuthorizeRequestMessage\": {\"BranchID\": \"BR2\",\"RefNum\": \"" + refnum + "\",\"BillNumber\": \"" + billnum + "\",\"MobileNo\": \"" + mobilenum + "\",\"Amount\": " + amount + ",\"ExpiryPeriodType\": 1, \"ExpiryPeriod\": 10}}");
                //    //request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                //    var result = await httpClient.SendAsync(request);
                //    return result != null ? Ok(result) : (IActionResult)NotFound();
                //}
            }
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), "https://b2btest.stcpay.com.sa/B2B.MerchantWebApi/Merchant/v3/MobilePaymentAuthorize"))
                {
                    request.Headers.TryAddWithoutValidation("content-type", "application/json");
                    request.Headers.TryAddWithoutValidation("x-clientcode", "61247531004");
                    request.Headers.TryAddWithoutValidation("x-password", "Zr@$y157$cc");
                    request.Headers.TryAddWithoutValidation("x-username", "ZoTstWrUSR");
                    request.Content = new StringContent("{\"MobilePaymentAuthorizeRequestMessage\":{\"BranchID\": \"BR2\",\"TellerID\": \"Teller2\",\"DeviceID\": \"Device2\",\"RefNum\": \"MerRefNo267127\",\"BillNumber\": \"Bill8\",\"MobileNo\": \"966557146325\",\"Amount\": 10.23,\"ExpiryPeriodType\": 1,\"ExpiryPeriod\": 10}}");
                    request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                    var result = await httpClient.SendAsync(request);
                    return result != null ? Ok(result) : (IActionResult)NotFound();
                }
            }
        }
        //[HttpPost]
        //public async Task<IActionResult> CancelBooking([FromBody] CancelBookingRequest request)
        //{
        //    var result = await _mediator.Send(request);
        //    return result  != null ? Ok(result.status) : (IActionResult)NotFound();
        //    //return NotFound();
        //}
    }
}
