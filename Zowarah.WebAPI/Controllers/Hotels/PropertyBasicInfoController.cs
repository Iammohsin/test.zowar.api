﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zowarah.WebAPI.Handlers;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Controllers.Hotels
{
    [Route("api/hotels/[controller]/[action]")]    
    public class PropertyBasicInfoController : BaseController
    {
       
        public PropertyBasicInfoController(IMediator mediator,IHttpContextAccessor accassor):base(mediator,accassor)
        {
       
        }
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long Id)
        {
            var user = HttpContext.User;
            var result = await _mediator.Send(new GetPropertyBasicInfoRequest { Id = Id });
            //if (result == null)
            //return BadRequest();
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        // POST api/PropertyBasicInfo
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreatePropertyBasicInfoRequest request)
        {
            try {
                request.ProviderId = UserId+"";
            var result= await _mediator.Send(request);
            if (result == null)
                return BadRequest();
            return Ok(JsonConvert.SerializeObject(result.Id));

            } catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
            //return CreatedAtRoute("Post", new { result.Id }, request);
        }

        // PUT api/PropertyBasicInfo/1
        [HttpPut]
        public async Task<IActionResult> Update([FromBody] UpdatePropertyBasicInfoRequest request)
        {
            if (ModelState.IsValid)
            {
                request.UserId = UserId;
                await _mediator.Send(request);
                return Ok();
            }
            else
                return NotFound();
        }
    }
}
