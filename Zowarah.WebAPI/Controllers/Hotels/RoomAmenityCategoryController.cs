﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Zowarah.WebAPI.Handlers;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Controllers.Hotels {
    [Route("api/hotels/[controller]/[action]")]
    public class RoomAmenityCategoryController : Controller {
        private readonly IMediator _mediator;
        public RoomAmenityCategoryController(IMediator mediator) {
            _mediator = mediator;
        }


        // GET: api/Hotels/1
        [HttpGet]
        public async Task<IActionResult> GetAll() {
            var result = await _mediator.Send(new GetRoomAmenityCategoryListRequest { });
            return result != null ? Ok(result.RoomAmenityCategoryList) : (IActionResult)NotFound();
        }


    }
}
