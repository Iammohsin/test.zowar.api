﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Zowarah.WebAPI.Handlers;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Controllers.Hotels {
    [Route("api/hotels/[controller]/[action]")]
    public class CalendarController : BaseController {

        public CalendarController(IMediator mediator, IHttpContextAccessor accessor) : base(mediator, accessor) {

        }

        // GET: api/Calendar/1
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCalendar(long id) {
            var result = await _mediator.Send(new GetCalendarRequest { HotelId = id, UserId = UserId });
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetCalendarManual(long id) {
            var result = await _mediator.Send(new GetCalendarManualRequest { RoomId = id, UserId = UserId });
            return result != null ? Ok(result.CalenderManuals) : (IActionResult)NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> SetCalendar([FromBody] SetCalendarRequest request) {
            request.UserId = UserId;
            var result = await _mediator.Send(request);
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> CloseRooms([FromBody] CloseRoomsRequest request) {
            request.UserId = UserId;
            var result = await _mediator.Send(request);
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> SetCalendarStatus([FromBody] SetCalendarStatusRequest request) {
            request.UserId = UserId;
            var result = await _mediator.Send(request);
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> SetCalendarRooms([FromBody] SetCalendarRoomsRequest request) {
            request.UserId = UserId;
            var result = await _mediator.Send(request);
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        //   [HttpPost]
        /*  [HttpGet("{ProviderId}/{Status}")]
          public async Task<IActionResult> GetByProviderId(string providerId,int status)
          {
              var result = await _mediator.Send(new GetHotelByProviderIdRequest { ProviderId = providerId, Status = status });
              return result != null ? Ok(result.CalendarList) : (IActionResult)NotFound();
          }

          // DELETE: api/Calendar/1
          [HttpDelete("{id}")]
          public async Task<string> Delete(string id)
          {
              await _mediator.Send(new DeleteCalendarRequest { Id = id });
              return id;
          }*/
    }
}
