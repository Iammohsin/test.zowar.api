﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Zowarah.WebAPI.Handlers;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.WebAPI.Handlers.Hotels;
using Zowarah.WebAPI.Config.AppSettings;
using Zowarah.Infrastructure.Config;
using Microsoft.AspNetCore.Authorization;

namespace Zowarah.WebAPI.Controllers.Hotels
{

    [Route("api/hotels/[controller]/[action]")]
    public class PropertyTypesController : Controller
    {
        private readonly IMediator _mediator;
                public PropertyTypesController(IMediator mediator)
        {
            _mediator = mediator;
        }

       
        [AllowAnonymous]
        // GET: api/Hotels/1
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _mediator.Send(new GetPropertyTypeListRequest { });
            return result != null ? Ok(result.PropertyTypes) : (IActionResult)NotFound();
            /* try
             {
                 var result = await _mediator.Send(new GetPropertyTypeListRequest { });
                 //object result = null;
                 //throw new Exception("What is wrong Man!!"); 
                 throw
                 //                return result != null ? Ok(result.PropertyTypes) : (IActionResult)NotFound();
                 //return result != null ? Ok(result) : (IActionResult)NotFound();
             }
             catch (Exception ex)
             {
                 return BadRequest(ex.Message);
             }*/
            //throw new CustomException("this is a custom exception");
            //return BadRequest(new Error() { code ="1000", message="HII WORLD"});
        }
       
    }
}
