﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Zowarah.WebAPI.Handlers;
using Zowarah.WebAPI.Handlers.Hotels;
using Microsoft.AspNetCore.Authorization;

namespace Zowarah.WebAPI.Controllers.Hotels {
    [Route("api/hotels/[controller]/[action]")]
    public class PhotosController : BaseController {

        public PhotosController(IMediator mediator, IHttpContextAccessor accessor) : base(mediator, accessor) {

        }



        [AllowAnonymous]
        // GET: api/Hotels/1
        [HttpGet("{hotelId}")]
        public async Task<IActionResult> GetAll(long hotelId) {
            var result = await _mediator.Send(new GetPhotoListRequest { HotelId = hotelId });
            return result != null ? Ok(result.PhotoList) : (IActionResult)NotFound();
        }

        [HttpPost("{hotelId}")]
        public async Task<IActionResult> Add(long hotelId, IFormFile file) {
            var result = await _mediator.Send(new AddPhotoRequest { HotelId = hotelId, UserId = UserId, File = file });
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [HttpDelete("{photoId}")]
        public async Task<IActionResult> Delete(string photoId) {
            try {
                var result = await _mediator.Send(new DeletePhotoRequest { Id = photoId, UserId = UserId });
                return result != null ? Ok(result) : (IActionResult)NotFound();
            } catch (Exception ex) {
                return NotFound(ex.Message);
            }
        }


        [HttpPost("{photoId}")]
        public async Task<IActionResult> SetMain(string photoId) {
            try {
                var result = await _mediator.Send(new SetMainRequest { Id = photoId, UserId = UserId });
                return result != null ? Ok(result) : (IActionResult)NotFound();
            } catch (Exception ex) {
                return NotFound(ex.Message);
            }
        }
    }
}
