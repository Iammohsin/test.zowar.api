﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zowarah.WebAPI.Handlers;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Controllers.Hotels
{
    [Route("api/hotels/[controller]/[action]")]
    public class PropertyDetailsController : BaseController
    {
        
        public PropertyDetailsController(IMediator mediator,IHttpContextAccessor accessor):base(mediator,accessor)
        {
        
        }
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long Id)
        {
            var result = await _mediator.Send(new GetPropertyDetailsRequest { Id = Id });
            //if (result == null)
            //return BadRequest();
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        // POST api/PropertyDetails
        [HttpPut]
        public async Task<IActionResult> Update([FromBody] UpdatePropertyDetailsRequest request)
        {
            request.UserId = UserId;
            var result= await _mediator.Send(request);
            if (result == null)
                return BadRequest();
            return Json(result.Id);
        }

    }
}
