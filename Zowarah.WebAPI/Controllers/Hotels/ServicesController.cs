﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zowarah.WebAPI.Handlers;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Controllers.Hotels {
    [Route("api/hotels/[controller]/[action]")]
    public class ServicesController : BaseController {
        public ServicesController(IMediator mediator, IHttpContextAccessor accessor) : base(mediator, accessor) {

        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long Id) {
            try {
                var result = await _mediator.Send(new GetServicesRequest { Id = Id });
                return result != null ? Ok(result) : (IActionResult)NotFound();
            } catch (Exception ex) {
                return NotFound(ex);
            }
        }

        // POST api/Services
        [HttpPut]
        public async Task<IActionResult> Update([FromBody] UpdateServicesRequest request) {
            request.UserId = UserId;
            var result = await _mediator.Send(request);
            return result != null ? Json(result) : (IActionResult)NotFound();

        }
    }
}
