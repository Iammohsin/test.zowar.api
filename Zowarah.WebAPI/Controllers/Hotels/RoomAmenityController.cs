﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Zowarah.WebAPI.Handlers;
using Zowarah.WebAPI.Handlers.Hotels;
using Microsoft.AspNetCore.Authorization;

namespace Zowarah.WebAPI.Controllers.Hotels
{
    [Route("api/hotels/[controller]/[action]")]
    public class RoomAmenityController : Controller
    {
        private readonly IMediator _mediator;
        public RoomAmenityController(IMediator mediator)
        {
            _mediator = mediator;
        }
               
        [AllowAnonymous]
        // GET: api/Hotels/1
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _mediator.Send(new GetRoomAmenityListRequest {});
            return result != null ? Ok(result.RoomAmenityList) : (IActionResult)NotFound();
        }

        
    }
}
