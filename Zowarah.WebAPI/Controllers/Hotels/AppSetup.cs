﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zowarah.Data.Contexts;

namespace Zowarah.Hotels.API.Controllers
{
    [Produces("application/json")]
    [Route("api/AppSetup")]
    public class AppSetup : Controller
    {
        private DbInitializer _dbInitializer;

        public AppSetup(DbInitializer dbInitializer)
        {
            _dbInitializer = dbInitializer;
        }

        [HttpGet]
        public async Task<IActionResult> InitializeDb()
        {
            await _dbInitializer.Seed();
            return Ok("Initiliazed the database successfully");
        }
    }
}
