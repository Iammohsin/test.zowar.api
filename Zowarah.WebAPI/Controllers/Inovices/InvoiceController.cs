﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zowarah.WebAPI.Handlers.Invoices;

namespace Zowarah.WebAPI.Controllers.Inovices {
    [Route("api/invoices/[controller]/[action]")]
    public class InvoiceController : BaseController {

        public InvoiceController(IMediator mediator, IHttpContextAccessor accessor) : base(mediator, accessor) {

        }
        [HttpGet("{Id}")]
        public async Task<IActionResult> GetInvoice(string Id) {
            var res = await _mediator.Send(new GetInvoiceRequest() { InvoiceId = Id });
            return (res != null) ? Ok(res) : (IActionResult)NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> GetAllInvoices([FromBody] GetAllInvoicesByFilterRequest filter) {
            if (!UserHasRoleAdmin())
                filter.ProviderId = UserId;
            var res = await _mediator.Send(filter);
            return Ok(res.List);
        }

        [HttpPost]
        public async Task<IActionResult> GetInvoicesReport([FromBody] GetInvoicesReportsRequest filter) {
            if (!UserHasRoleAdmin())
                filter.ProviderId = UserId;
            var res = await _mediator.Send(filter);
            return Ok(res.List);
        }
        [HttpPost("{date}")]
        public async Task<IActionResult> GetInvoicesReportDetails(DateTime date, [FromBody] GetInvoicesReportDetailsRequest filter) {
            if (!UserHasRoleAdmin())
                filter.ProviderId = UserId;
            filter.Date = date;
            return Ok(await _mediator.Send(filter));
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> GetAllInvoicesTransactions(string Id) {
            var res = await _mediator.Send(new GetAllInvoiceTtransactionsRequest() { InvoiceId = Id });
            return Ok(res.list);
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> GetTransactionInvoiceDetails(string Id) {
            var res = await _mediator.Send(new GetTransactionInvoiceDetailsRequest() { TransactionId = Id });
            return (res != null) ? Ok(res) : (IActionResult)NotFound();
        }
    }
}