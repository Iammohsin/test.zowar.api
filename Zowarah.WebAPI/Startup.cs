﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Zowarah.Data.Contexts;
using AutoMapper;
using Zowarah.Infrastructure.IoC;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Zowarah.Domain.Entities.Users;
using Microsoft.AspNetCore.Identity;
using Zowarah.WebAPI.Config.AppSettings;
using Hangfire;
using Zowarah.Application.Interfaces.Invoices;
using System;
using Zowarah.WebAPI.Config;
using Microsoft.Extensions.Logging;
using Zowarah.WebAPI.Config.AutoMapping;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;

namespace Zowarah.WebAPI
{

    public class Startup
    {
        private string[] allowedOrginins { get; set; } = new string[] { "http://localhost:5030", "http://alzowar.com", "http://alzowar.com:5050", "http://localhost:4200", "http://52.230.1.71:5050", "http://zowar.com.sa", "http://37.48.109.145", "http://37.48.109.145:8880","https://zowar.com.sa","http://37.48.109.145:443", "https://api.zowar.com.sa", "http://88.99.227.71", "http://localhost:5000" };

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // register appSettings
            services.Configure<AppSettings>(Configuration.GetSection("apikeys"));

            services.AddHangfire(config =>
            // config.UseSqlServerStorage("Server= 37.48.109.145\\MSSQLSERVER2014; Database = ZowarahDB; user id = apizowarah; password = Hov5l!73")       
            //config.UseSqlServerStorage("Server=ANSARI-PC; Database =zowarahDB; user id = sa; password = Noori@Sys")
            // config.UseSqlServerStorage("Server=dell-PC\\SQLEXPRESS; Database =zowarahDB; Integrated Security=True")
            config.UseSqlServerStorage("Server= 88.99.227.71; Database = admin_zowar2020; user id = superadmin; password = 4G59ma*b")


                );

            services.AddMediatR();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll", p =>
                {
                    p.AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials()
                    .Build();
                });
                //same origin
                options.AddPolicy("defaultCors", p =>
                {
                    p.WithOrigins(allowedOrginins)
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials()
                    .Build();
                });
            });

            var sqlConnection = Configuration.GetSection("DefaultConnection").Value;

            services.AddScoped<ApplicationDbContext>();
            RegisterAutoMapper.Register(services);

            // services.AddDbContext<ApplicationDbContext>(options =>
            // options.UseSqlServer(sqlConnection), ServiceLifetime.Transient);

            // =================== Add Identity ===================

            services.AddIdentity<AppUser, AppRole>(config =>
            {
                config.SignIn.RequireConfirmedEmail = true;
            }
                )
            .AddEntityFrameworkStores<ApplicationDbContext>()
            .AddDefaultTokenProviders();

            // =================== Add Jwt Authentication  ===================
            //  JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // => remove default claims
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }
                )
         .AddJwtBearer(jwtBearerOptions =>
         {
             jwtBearerOptions.RequireHttpsMetadata = false;
             jwtBearerOptions.SaveToken = true;
             jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters
             {
                 ValidateIssuer = true,
                 ValidateActor = false,
                 ValidateAudience = true,
                 ValidateLifetime = false,
                 ValidateIssuerSigningKey = true,
                 ValidIssuer = Configuration["Token:Issuer"],
                 ValidAudience = Configuration["Token:Audience"],
                 IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes
                                                    (Configuration["Token:Key"])),
                 ClockSkew = TimeSpan.Zero
             };
         });

            services.AddSession();
            IoCRegistration.RegisterServices(services);
            services.AddMvc(
            config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                .RequireAuthenticatedUser()
                .Build();
                config.Filters.Add(typeof(ExceptionHandlerFilter));
                config.Filters.Add(new AuthorizeFilter(policy));
            }
            );
            //services.AddAutoMapper();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IMediator mediator, IInvoiceService invoiceService, ILoggerFactory loggerFactory)
        {
            var allowedOrginins = new string[] { "http://localhost:5030", "http://alzowar.com", "http://alzowar.com:5050", "http://localhost:4200", "http://52.230.1.71:5050", "http://zowar.com.sa", "http://37.48.109.145", "http://37.48.109.145:8880", "https://zowar.com.sa", "https://api.zowar.com.sa", "http://37.48.109.145:443","http://88.99.227.71","http://localhost:5000" };
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                allowedOrginins = new string[] { "*" };
            }
            else if (env.IsEnvironment("Testing"))
            {
                IServiceScopeFactory scopeFactory = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>();
                using (IServiceScope scope = scopeFactory.CreateScope())
                {
                    var userManager = scope.ServiceProvider.GetRequiredService<UserManager<AppUser>>();
                    var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<AppRole>>();
                    var _context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
                    // Seed database code goes here
                    DbIdentityInitializer.Seed(_context, userManager, roleManager);
                }
            }

            loggerFactory.AddLog4Net();

            //loggerFactory.AddDebug();
            app.UseCors(builder =>
            {
                builder.WithOrigins(allowedOrginins)
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials();
            });
            app.UseHangfireServer();
            app.UseHangfireDashboard();

            // Call Background Process
            /*
            RecurringJob.AddOrUpdate(
            () =>
                invoiceService.GenerateInvoices()
                ,
            Cron.Monthly);*/

            // Exception Handling
            /*        app.UseExceptionHandler(
             options => {
                 options.Run(
             async context =>
             {
                 context.Response.StatusCode = 400;
                 context.Response.ContentType = "application/json";
                 var ex = context.Features.Get<IExceptionHandlerFeature>();

                 var code = "1000"; // System Error
                 if (ex.Error.GetType() == typeof(CustomException)) // Custom
                     code = "1001"; // Custom Error

                 if (ex != null)
                 {
                     var err = JsonConvert.SerializeObject(new Error{ code=code, message=ex.Error.Message });
                     //await context = JsonConvert.SerializeObject(err));
                   //  var err1 = (JsonConvert.SerializeObject(new JsonResult(err))).ToString();
                     await context.Response.WriteAsync("hii world");
                 }
             });
             }
            );
            */
            app.UseSession();
            app.Use(async (context, next) =>
            {
                context.Response.Headers.Add(
                    "Content-Security-Policy",
                    "script-src 'self'; " +
                    "style-src 'self'; " +
                    "img-src 'self'");
                context.Response.Headers.Add("X-FRAME-OPTIONS", "DENY");
                context.Response.Headers.Add("X-XSS-Protection", "1; mode=block");
                context.Response.Headers.Add("X-Content-Type-Options", "nosniff");
                context.Response.Headers.Add("Referrer-Policy", "strict-origin-when-cross-origin");
				//context.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                await next();
            });

            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}