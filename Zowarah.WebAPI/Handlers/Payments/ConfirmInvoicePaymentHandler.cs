﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Invoices;
using Zowarah.Application.Interfaces.Payments;
using Zowarah.Application.Services.Invoices;
using Zowarah.Domain.Entities.Invoices;
using Zowarah.Domain.Interfaces;
using Zowarah.WebAPI.Config.Notification;
using Zowarah.WebAPI.Extensions;
using Zowarah.WebAPI.Handlers.Common.NotificationsHandlers;
using Zowarah.WebAPI.Handlers.Invoices;
using static Zowarah.Identity.Api.Handlers.UserHandlers.GetProfile;

namespace Zowarah.WebAPI.Handlers.Payments
{
    public class ConfirmInvoicePaymentRequest : IRequest<ConfirmInvoicePaymentResponse>
    {
        public string InvoiceId { get; set; }
        public string Reference { get; set; }
        public string Bank { get; set; }
        public DateTime Date { get; set; }
        public string AccountNo { get; set; }
    }

    public class ConfirmInvoicePaymentResponse
    {
        public string InvoiceId { get; set; }
    }
    public class ConfirmInvoicePaymentHandler : IRequestHandler<ConfirmInvoicePaymentRequest, ConfirmInvoicePaymentResponse>
    {
        private readonly IPaymentService paymentService;
        private readonly IUnitOfWork uow;
        private readonly IInvoiceService invoiceService;
        private readonly IMediator _mediator;

        public ConfirmInvoicePaymentHandler(IPaymentService _paymentService, IUnitOfWork _uow, IInvoiceService _invoiceService, IMediator mediator)
        {
            paymentService = _paymentService;
            uow = _uow;
            invoiceService = _invoiceService;
            _mediator = mediator;
        }
        public async Task<ConfirmInvoicePaymentResponse> Handle(ConfirmInvoicePaymentRequest request, CancellationToken cancellationToken)
        {
            var payment = await paymentService.GetByInvoiceId(request.InvoiceId);
            var invoice = await invoiceService.GetInvoiceDetails(request.InvoiceId);
            if (payment == null && invoice != null)
            {
                var confirm = new Payment();
                confirm = confirm.FromRequest(request);
                paymentService.Create(confirm);
                await uow.SaveChangesAsync();
                var updateInvoice = await _mediator.Send(new UpdateInvoiceStatusRequest() { Id = confirm.InvoiceId, Status = InvoiceStatus.Collected });
                if (updateInvoice != null)
                {
                    var provider = await _mediator.Send(new GetProfileRequest() { Id = invoice.ProviderId });
                    if (provider != null)
                        SendMailNotification(invoice, confirm, provider.Email);
                    return new ConfirmInvoicePaymentResponse() { InvoiceId = confirm.InvoiceId };
                }
            }
            return null;
        }
        async Task SendMailNotification(InvoiceQuery invoice, Payment payment, string Email)
        {
            try
            {
                var template = await _mediator.Send(new GetMailTemplateRequest() { Id = InovicePaymentConfirmation.TemplateId });

                var htmlContent = template.Template
                    .Replace("$$ProviderName$$", invoice.ProviderName)
                    .Replace("$$ServiceName$$", invoice.ServiceName)
                    .Replace("$$Bank$$", payment.Bank)
                    .Replace("$$Reference$$", payment.Reference)
                    .Replace("$$Amount$$", invoice.Amount + "")
                    .Replace("$$Date$$", payment.Date.ToString("ddd - MMM dd yyyy"))
                    .Replace("$$AccountNo$$", payment.AccountNo);


                await _mediator.Send(new SendMailNotificationRequest()
                {
                    To = Email,
                    DisplayName = invoice.ProviderName,
                    Subject = $"Confirm Payment For {invoice.ServiceName}",
                    Content = htmlContent
                });
            }
            catch { }
        }
    }
}
