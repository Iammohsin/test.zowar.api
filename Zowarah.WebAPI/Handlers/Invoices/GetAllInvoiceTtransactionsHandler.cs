﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Invoices;
using Zowarah.Application.Services.Invoices;

namespace Zowarah.WebAPI.Handlers.Invoices
{
    public class GetAllInvoiceTtransactionsRequest : IRequest<GetAllInvoiceTtransactionsResponse>
    {
        public string InvoiceId { get; set; }
    }

    public class GetAllInvoiceTtransactionsResponse
    {
        public GetAllInvoiceTtransactionsResponse()
        {
            list = new List<InvoiceTransaction>();
        }
        public ICollection<InvoiceTransaction> list { get; set; }
    }


    public class GetAllInvoiceTtransactionsRequestHandler : IRequestHandler<GetAllInvoiceTtransactionsRequest, GetAllInvoiceTtransactionsResponse>
    {
        private readonly IInvoiceService invoiceService;
        public GetAllInvoiceTtransactionsRequestHandler(IInvoiceService _invoiceService)
        {
            invoiceService = _invoiceService;
        }
        public async Task<GetAllInvoiceTtransactionsResponse> Handle(GetAllInvoiceTtransactionsRequest request, CancellationToken cancellationToken)
        {
            var result = await invoiceService.GetAllInvoiceTransactions(request.InvoiceId);
            var res = new GetAllInvoiceTtransactionsResponse();
            res.list = result;
            return res;
        }
    }
}
