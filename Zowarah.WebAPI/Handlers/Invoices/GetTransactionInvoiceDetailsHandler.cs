﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Invoices;
using Zowarah.Application.Services.Invoices;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Invoices
{

    public class GetTransactionInvoiceDetailsRequest : IRequest<GetTransactionInvoiceDetailsResponse>
    {
        public string TransactionId { get; internal set; }
    }

    public class GetTransactionInvoiceDetailsResponse: InvoiceTransactionDetails
    {

    }
    public class GetTransactionInvoiceDetailsHandler : IRequestHandler<GetTransactionInvoiceDetailsRequest, GetTransactionInvoiceDetailsResponse>
    {
        private readonly IInvoiceService _InvoiceService;

        public GetTransactionInvoiceDetailsHandler(IInvoiceService invoiceService) {
            _InvoiceService = invoiceService;
        }
        public async Task<GetTransactionInvoiceDetailsResponse> Handle(GetTransactionInvoiceDetailsRequest request, CancellationToken cancellationToken)
        {
            var result = await _InvoiceService.GetTransactionInvoiceDetails(request.TransactionId);
            if(result!=null)
            {
            var res = new GetTransactionInvoiceDetailsResponse();
                result.ToResponse(res);
                return res;
            }
            return null;
        }
    }
}
