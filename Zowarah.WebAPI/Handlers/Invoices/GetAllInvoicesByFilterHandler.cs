﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Invoices;
using Zowarah.Application.Services.Invoices;
using System.Collections.Generic;
using Zowarah.Domain.Entities.Invoices;
using Zowarah.Domain.Entities.Common;
using Zowarah.Application.Enums.Hotels;

namespace Zowarah.WebAPI.Handlers.Invoices {

    public class GetAllInvoicesByFilterRequest : IRequest<GetAllInvoicesByFilterResponse> {
        public string InvoiceId { get; set; }
        public long ProviderId { get; set; }
        public long ServiceId { get; set; }
        public long ClientId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public ServiceTypeEnum ServiceType { get; set; }
        public InvoiceStatus Status { get; set; }
        public int City { get; set; }
    }

    public class GetAllInvoicesByFilterResponse {
        public ICollection<InvoiceQuery> List { get; set; } = new List<InvoiceQuery>();
    }
    public class GetAllInvoicesByFilterHandler : IRequestHandler<GetAllInvoicesByFilterRequest, GetAllInvoicesByFilterResponse> {
        private readonly IInvoiceService invoiceService;

        public GetAllInvoicesByFilterHandler(IInvoiceService _invoiceService) {
            invoiceService = _invoiceService;
        }
        public async Task<GetAllInvoicesByFilterResponse> Handle(GetAllInvoicesByFilterRequest request, CancellationToken cancellationToken) {
            return new GetAllInvoicesByFilterResponse {
                List = await invoiceService.GetAllByFilter(request.InvoiceId, request.ProviderId, request.ServiceId,
                request.From, request.To, request.City, request.ServiceType, request.Status)
            };
        }
    }
}
