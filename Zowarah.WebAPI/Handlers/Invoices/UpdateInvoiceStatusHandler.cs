﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Invoices;
using Zowarah.Domain.Entities.Invoices;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Invoices
{
   
    public class UpdateInvoiceStatusRequest : IRequest<UpdateInvoiceStatusResponse>
    {
        public string Id { get; set; }
        public InvoiceStatus Status { get; set; }
    }

    public class UpdateInvoiceStatusResponse
    {

    }
    public class UpdateInvoiceStatusHandler : IRequestHandler<UpdateInvoiceStatusRequest, UpdateInvoiceStatusResponse>
    {
        private readonly IInvoiceService repo;
        private readonly IUnitOfWork uow; 

        public UpdateInvoiceStatusHandler(IInvoiceService _repo, IUnitOfWork _uow)
        {
            repo = _repo;
            uow = _uow;
        }
        public async Task<UpdateInvoiceStatusResponse> Handle(UpdateInvoiceStatusRequest request, CancellationToken cancellationToken)
        {
            var invoice = await repo.GetById(request.Id);
            if (invoice != null)
            {
                invoice.Status = request.Status;
                await uow.SaveChangesAsync();
                return new UpdateInvoiceStatusResponse();
            }
            return null;
        }
    }
}
