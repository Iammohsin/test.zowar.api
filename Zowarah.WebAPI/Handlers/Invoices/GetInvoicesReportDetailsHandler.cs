﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Enums.Hotels;
using Zowarah.Application.Interfaces.Invoices;
using Zowarah.Application.Services.Invoices;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Invoices;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Invoices {

    public class GetInvoicesReportDetailsRequest : IRequest<GetInvoicesReportDetailsResponse> {
        public long ProviderId { get; set; }
        public long ServiceId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public ServiceTypeEnum ServiceType { get; set; }
        public InvoiceStatus Status { get; set; }
        public int? City { get; set; }
        public ReportView View { get; set; }
        public DateTime Date { get; set; }

    }

    public class GetInvoicesReportDetailsResponse: InvoiceReport {
       
    }
    public class GetInvoicesReportDetailsHandler : IRequestHandler<GetInvoicesReportDetailsRequest, GetInvoicesReportDetailsResponse> {
        private readonly IInvoiceService invoiceService;

        public GetInvoicesReportDetailsHandler(IInvoiceService _invoiceService) {
            invoiceService = _invoiceService;
        }
        public async Task<GetInvoicesReportDetailsResponse> Handle(GetInvoicesReportDetailsRequest request, CancellationToken cancellationToken) {
            var result = await invoiceService.GetInvoicesReportDetails(request.ProviderId, request.ServiceId,request.Date,
                request.From, request.To, request.ServiceType, request.City, request.Status, request.View);
            var res = new GetInvoicesReportDetailsResponse();
            result.ToResponse(res);
            return res;
        }
    }
}
