﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Enums.Hotels;
using Zowarah.Application.Interfaces.Invoices;
using Zowarah.Application.Services.Invoices;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Invoices;

namespace Zowarah.WebAPI.Handlers.Invoices {

    public class GetInvoicesReportsRequest : IRequest<GetInvoicesReportsResponse> {
        public long ProviderId { get; set; }
        public long ServiceId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public ServiceTypeEnum ServiceType { get; set; }
        public InvoiceStatus Status { get; set; }
        public int? City { get; set; }
        public ReportView View { get; set; }

    }

    public class GetInvoicesReportsResponse {
        public ICollection<InvoiceReport> List { get; set; } = new List<InvoiceReport>();
    }
    public class GetInvoicesReportsHandler : IRequestHandler<GetInvoicesReportsRequest, GetInvoicesReportsResponse> {
        private readonly IInvoiceService invoiceService;

        public GetInvoicesReportsHandler(IInvoiceService _invoiceService) {
            invoiceService = _invoiceService;
        }
        public async Task<GetInvoicesReportsResponse> Handle(GetInvoicesReportsRequest request, CancellationToken cancellationToken) {

            return new GetInvoicesReportsResponse {
                List = await invoiceService.GetInvoicesReport(request.ProviderId, request.ServiceId,
                request.From, request.To, request.ServiceType, request.City, request.Status, request.View)
            };
        }
    }
}
