﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Invoices;
using Zowarah.Application.Services.Invoices;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Invoices
{
    public class GetInvoiceRequest : IRequest<GetInvoiceResponse>
    {
        public string InvoiceId { get; set; }
    }

    public class GetInvoiceResponse : InvoiceQuery
    {

    }
    public class GetInvoiceHandler : IRequestHandler<GetInvoiceRequest, GetInvoiceResponse>
    {
        private readonly IInvoiceService invoiceService;

        public GetInvoiceHandler(IInvoiceService _invoiceService)
        {
            invoiceService = _invoiceService;
        }
        public async Task<GetInvoiceResponse> Handle(GetInvoiceRequest request, CancellationToken cancellationToken)
        {
            var result = await invoiceService.GetInvoiceDetails(request.InvoiceId);
            if (result != null)
            {
                var res = new GetInvoiceResponse();
                result.ToResponse(res);
                return res;
            }
            return null;
        }
    }
}
