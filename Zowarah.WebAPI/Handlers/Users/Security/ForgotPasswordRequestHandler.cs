﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Zowarah.Application.Services.Common;
using Zowarah.Application.Services.Security;
using Zowarah.Domain.Entities.Users;
using Zowarah.WebAPI.Config.Notification;
using Zowarah.WebAPI.Handlers.Common.NotificationsHandlers;
using Zowarah.WebAPI.Config.AppSettings;
namespace Zowarah.WebAPI.Handlers.Users.Security
{
    public class ForgotPasswordRequest : IRequest<ForgotPasswordResponse>
    {
        public string Email { get; set; }
    }

    public class ForgotPasswordResponse
    {
        public string message { get; set; }
        public string stringToken { get; set; }
    }

    public class ForgotPasswordRequestHandler : IRequestHandler<ForgotPasswordRequest, ForgotPasswordResponse>
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly IMediator _mediator;
        private readonly Zowarah.WebAPI.Config.AppSettings.AppSettings _AppSettings;

        public ForgotPasswordRequestHandler(UserManager<AppUser> userManager, IMediator mediator, IOptions<Zowarah.WebAPI.Config.AppSettings.AppSettings> settings)
        {
            _userManager = userManager;
            _mediator = mediator;
            _AppSettings = settings.Value;
        }

        public async Task<ForgotPasswordResponse> Handle(ForgotPasswordRequest request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);
            // var url = _AppSettings.GeneralSettings.Url;
            var url = "localhost:4200";
            if (user == null || !await _userManager.IsEmailConfirmedAsync(user))
            {
                throw new Exception("Invalid User Or Email..");
            }

            var code = await _userManager.GenerateUserTokenAsync(user, "Default", "ResetPassword");
            byte[] tokenGeneratedBytes = Encoding.UTF8.GetBytes(code);
            var codeEncoded = Base64UrlTextEncoder.Encode(tokenGeneratedBytes);
            
            string confirmationLink = $"http://{url}{ForgotPassword.ConfirmDomain}/{user.Id}/{ codeEncoded}";
            var template = await _mediator.Send(new GetMailTemplateRequest() { Id = ForgotPassword.TemplateId });
            var htmlContent = template.Template.Replace("$$DisplayName$$", user.FirstName).Replace("$$confirmHref$$", confirmationLink);

            await _mediator.Send(new SendMailNotificationRequest()
            {
                To = user.Email,
                DisplayName = user.FirstName,
                Subject = ForgotPassword.Subject,
                Content = htmlContent
            });

            return new ForgotPasswordResponse() { message = "Link has been sent to : " + request.Email + " successfully", stringToken = codeEncoded };
            throw new NotImplementedException("Something went wrong.We cannot sent the link, please contact Administrator..");
        }
    }
}