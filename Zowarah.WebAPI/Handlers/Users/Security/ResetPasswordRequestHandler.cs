﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Zowarah.Application.Services.Security;
using Zowarah.Domain.Entities.Users;
using Zowarah.WebAPI.Config.Notification;
using Zowarah.WebAPI.Handlers.Common.NotificationsHandlers;

namespace Zowarah.WebAPI.Handlers.Users.Security
{
    public class ResetPasswordRequest : IRequest<ResetPasswordResponse>
    {
        public string UserId { get; set; }
        public string UserToken { get; set; }
        public string NewPassword { get; set; }
    }

    public class ResetPasswordResponse
    {
    }

    public class ResetPasswordRequestHandler : IRequestHandler<ResetPasswordRequest, ResetPasswordResponse>
    {
        private readonly UserManager<AppUser> userManager;
        private readonly IMediator mediator;

        public ResetPasswordRequestHandler(UserManager<AppUser> _userManager, IMediator _mediator)
        {
            userManager = _userManager;
            mediator = _mediator;
        }

        public async Task<ResetPasswordResponse> Handle(ResetPasswordRequest request, CancellationToken cancellationToken)
        {
            byte[] tokenBytes = Base64UrlTextEncoder.Decode(request.UserToken);
            var tokenEncoded = Encoding.UTF8.GetString(tokenBytes);

            var user = await userManager.FindByIdAsync(request.UserId);
            var isValid = await userManager.VerifyUserTokenAsync(user, "Default", "ResetPassword", tokenEncoded);

            if (user != null && isValid)
            {
                // update password here
                await userManager.RemovePasswordAsync(user); // remove old password instead of update it
                var pResult = await userManager.AddPasswordAsync(user, request.NewPassword); // add new password once old password removed.

                //if (pResult.Succeeded)
                //    return new ResetPasswordResponse { };
                //else
                //{
                //    throw new Exception("Token Invalid");
                //}
                return new ResetPasswordResponse { };
            }
            else
            {
                throw new Exception("Token Or UserId Is Invalid..");
            }
        }
    }
}