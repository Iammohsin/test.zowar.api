﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.WebAPI.Handlers.Users
{
    public class ChangePasswordRequest:IRequest<ChangePasswordResponse>
    {
        public string UserId { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
    }

    public class ChangePasswordResponse
    {
    }

    public class ChangePasswordRequestHandler : IRequestHandler<ChangePasswordRequest, ChangePasswordResponse>
    {
        private readonly UserManager<AppUser> _userManager;

        public ChangePasswordRequestHandler(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<ChangePasswordResponse> Handle(ChangePasswordRequest request, CancellationToken cancellationToken)
        {
            var user = _userManager.FindByIdAsync(request.UserId).Result;
            if (user == null)
                throw new Exception("Invalid User !!");
             var result = await _userManager.ChangePasswordAsync(user, request.CurrentPassword, request.NewPassword);
            if (result.Succeeded)
                return new ChangePasswordResponse { };
            else
                throw new Exception(result.Errors.ToList()[0].Description);
        }
    }
}
