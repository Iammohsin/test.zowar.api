﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Security;

namespace Zowarah.WebAPI.Handlers.Users.Permissions
{
    public class AttachUserPermissionRequest : IRequest<AttachUserPermissionResponse>
    {
        public long UserId { get; set; }
        public List<long> Permissions { get; set; }
    }

    public class AttachUserPermissionResponse
    {
        public bool Status { get; set; }
    }
    public class AttachUserPermissionRequestHandler : IRequestHandler<AttachUserPermissionRequest, AttachUserPermissionResponse>
    {
        private readonly IUserSecurityService _userSecurityService;
        public AttachUserPermissionRequestHandler(IUserSecurityService userSecurityService)
        {
            _userSecurityService = userSecurityService;
        }
        public async Task<AttachUserPermissionResponse> Handle(AttachUserPermissionRequest request, CancellationToken cancellationToken)
        {
            var result= await _userSecurityService.AttachUserPermission(request.UserId, request.Permissions);
            return new AttachUserPermissionResponse { Status = result };
        }
    }
}
