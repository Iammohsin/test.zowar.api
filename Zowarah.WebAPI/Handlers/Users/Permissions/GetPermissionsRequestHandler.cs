﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Security;

namespace Zowarah.WebAPI.Handlers.Users.Permissions
{
    public class GetPermissionsRequest : IRequest<GetPermissionsResponse>
    {
    }
    public class GetPermissionsResponse
    {
        public IReadOnlyCollection<long> Permissions { get; set; }
    }


    public class GetPermissionsRequestHandler : IRequestHandler<GetPermissionsRequest, GetPermissionsResponse>
    {
        private readonly IUserSecurityService _userSecurityService;
        public GetPermissionsRequestHandler(IUserSecurityService userSecurityService)
        {
            _userSecurityService = userSecurityService;
        }
        public async Task<GetPermissionsResponse> Handle(GetPermissionsRequest request, CancellationToken cancellationToken)
        {
            var result = await _userSecurityService.GetPermissions();
            return new GetPermissionsResponse { Permissions= await _userSecurityService.GetPermissions() };
        }
    }
}
