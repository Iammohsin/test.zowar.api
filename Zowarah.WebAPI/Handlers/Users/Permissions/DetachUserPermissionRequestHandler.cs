﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Security;

namespace Zowarah.WebAPI.Handlers.Users.Permissions
{
    public class DetachUserPermissionRequest : IRequest<DetachUserPermissionResponse>
    {
        public long UserId { get; set; }
        public List<long> Permissions { get; set; }
    }

    public class DetachUserPermissionResponse
    {
        public bool Status { get; set; }
    }
    public class DetachUserPermissionRequestHandler : IRequestHandler<DetachUserPermissionRequest, DetachUserPermissionResponse>
    {
        private readonly IUserSecurityService _userSecurityService;
        public DetachUserPermissionRequestHandler(IUserSecurityService userSecurityService)
        {
            _userSecurityService = userSecurityService;
        }
        public async Task<DetachUserPermissionResponse> Handle(DetachUserPermissionRequest request, CancellationToken cancellationToken)
        {
            var result = await _userSecurityService.DetachUserPermission(request.UserId, request.Permissions);
            return new DetachUserPermissionResponse { Status = result };
        }
    }
}
