﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.WebAPI.Handlers.Users
{
    public class SignupUserRequestHandler : IRequestHandler<SignupUserRequest, SignupUserRequestResponse>
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly RoleManager<AppRole> _roleManager;
        public SignupUserRequestHandler(
            ApplicationDbContext context,
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signinManager,
            RoleManager<AppRole> roleManager
            )
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signinManager;
            _roleManager = roleManager;
        }

        public async Task<SignupUserRequestResponse> Handle(SignupUserRequest request, CancellationToken cancellationToken)
        {
            var user = new AppUser {IsActive=true, JoinDate = DateTime.Now, Email = request.Email, UserName = request.Email, FirstName = request.FirstName, LastName = request.LastName };

            // Set Default Country & Currency 
            var country = _context.Countries.Where(x => x.IsoCode == request.CountryCode).FirstOrDefault();
            if (country != null)
            {
                user.CountryId = country.Id;
                user.CurrencyId = country.CurrencyId;
            }

                using (IDbContextTransaction tran = _context.Database.BeginTransaction())
                {
                      var userResult = await _userManager.CreateAsync(user, request.Password);
                      var roleResult = await _userManager.AddToRoleAsync(user, request.Role.ToString());

                if (!userResult.Succeeded)
                {
                    tran.Rollback();
                    throw new Exception(userResult.Errors.ToList()[0].Description);
                }

                if (!roleResult.Succeeded)
                {
                    tran.Rollback();
                    throw new Exception(userResult.Errors.ToList()[0].Description);
                }
                          await _context.SaveChangesAsync();
                          tran.Commit();
                          return new SignupUserRequestResponse() { Id = user.Id };
             }
        }
    }
}