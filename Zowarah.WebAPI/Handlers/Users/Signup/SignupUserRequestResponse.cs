﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zowarah.WebAPI.Handlers.Users
{
    public class SignupUserRequestResponse
    {
        public long Id { get; set; }
    }
}
