﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Interfaces.Users;
using Zowarah.Application.Services.Common;
using Zowarah.Domain.Enums;
using Zowarah.Domain.Queries;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Users.Clients
{

    public class GetClientsOverviewRequest : IRequest<GetClientsOverviewResponse>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int Country { get; set; }
        public PeriodEnum period { get; set; }
    }

    public class GetClientsOverviewResponse 
    {
        public int TotalCount { get; set; }
        public ICollection<ClientsOverview> records { get; set; }
    }
    public class GetClientsOverviewHandler : IRequestHandler<GetClientsOverviewRequest, GetClientsOverviewResponse>
    {
        public IClientService repo;
        public GetClientsOverviewHandler(IClientService _repo)
        {
            repo = _repo;
        }
        public async Task<GetClientsOverviewResponse> Handle(GetClientsOverviewRequest request, CancellationToken cancellationToken)
        {
            var result = await repo.GetOverview(request.period, request.FromDate, request.ToDate, request.Country);
            
            var res = new GetClientsOverviewResponse();
            res.TotalCount = result.Count();
            res.records =  result.Skip((request.PageNumber - 1) * request.PageSize)
                                .Take(request.PageSize).ToList();
            return res;
        }
    }
}
