﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Users;
using Zowarah.Data.Contexts;

namespace Zowarah.WebAPI.Handlers.Users.CreditCard
{
    public class UpdateCreditCardRequest : IRequest<UpdateCreditCardResponse>
    {
        public int Id { get; set; }
        public int CreditCardId { get; set; }
        public string NameOnCard { get; set; }
        public string CardNumber { get; set; }
        public string SecurityCode { get; set; }
        public int ExpiryMonth { get; set; }
        public int ExpiryYear { get; set; }
    }

    public class UpdateCreditCardResponse
    {
    }

    public class UpdateCreditCardRequestHandler : IRequestHandler<UpdateCreditCardRequest, UpdateCreditCardResponse>
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ICreditCardService _creditCardService;

        public UpdateCreditCardRequestHandler(ApplicationDbContext dbContext, ICreditCardService creditCardService)
        {
            _dbContext = dbContext;
            _creditCardService = creditCardService;
        }

        public async Task<UpdateCreditCardResponse> Handle(UpdateCreditCardRequest request, CancellationToken cancellationToken)
        {
            var card = _dbContext.CreditCardInfos.SingleOrDefault(s => s.Id.Equals(request.Id));

            if (card != null)
            {
                card.Id = request.Id;
                card.CreditCardId = request.CreditCardId;
                card.NameOnCard = request.NameOnCard.ToUpper();
                card.CardNumber = request.CardNumber.ToUpper();
                card.SecurityCode = request.SecurityCode.ToUpper();
                card.ExpiryMonth = Convert.ToInt32(request.ExpiryMonth);
                card.ExpiryYear = Convert.ToInt32(request.ExpiryYear);
                card.UpdatedOn = DateTime.Now;

                await _dbContext.SaveChangesAsync();

                return new UpdateCreditCardResponse() { };
            }
            else
            {
                throw new Exception($"Something went wrong..Update Failed..");
            }
        }
    }
}