﻿using System;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Users;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Users;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.WebAPI.Extensions;
using Zowarah.WebAPI.Handlers.Common.NotificationsHandlers;
using Zowarah.WebAPI.Config.Notification;
using Zowarah.WebAPI.Helpers.Users;
using Zowarah.WebAPI.Handlers.Transactions.TransactionHandlers;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Transactions;
using Zowarah.Application.Services.Hotels;

namespace Zowarah.WebAPI.Handlers.Users.CreditCard
{
    public class AddNewCreditCardRequest : IRequest<AddNewCreditCardResponse>
    {
        public long UserId { get; set; }
        public int CreditCardId { get; set; }
        public string CardNumber { get; set; }
        public string NameOnCard { get; set; }
        public string SecurityCode { get; set; }
        public int ExpiryMonth { get; set; }
        public int ExpiryYear { get; set; }
    }

    public class AddNewCreditCardResponse
    {
        public string Message { get; set; }
    }

    public class AddNewCreditCardRequestHandler : IRequestHandler<AddNewCreditCardRequest, AddNewCreditCardResponse>
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ICreditCardService _creditCardService;

        public AddNewCreditCardRequestHandler(ApplicationDbContext dbContext, ICreditCardService creditCardService)
        {
            _dbContext = dbContext;
            _creditCardService = creditCardService;
        }

        public async Task<AddNewCreditCardResponse> Handle(AddNewCreditCardRequest request, CancellationToken cancellationToken)
        {
            var ifExist = _dbContext.CreditCardInfos.SingleOrDefault(s => s.CardNumber.Equals(request.CardNumber));
            if (ifExist == null)
            {
                var card = new CreditCardInfo()
                {
                    UserId = request.UserId,
                    CreditCardId = request.CreditCardId,
                    CardNumber = request.CardNumber.ToUpper(),
                    NameOnCard = request.NameOnCard.ToUpper(),
                    SecurityCode = request.SecurityCode.ToUpper(),
                    ExpiryMonth = Convert.ToInt32(request.ExpiryMonth),
                    ExpiryYear = Convert.ToInt32(request.ExpiryYear),
                    CreatedDate = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    IsDeleted = false
                };
                await _creditCardService.Add(card);
                await _dbContext.SaveChangesAsync();

                AddNewCreditCardResponse response = new AddNewCreditCardResponse();
                response.Message = request.CardNumber;
                return await Task.FromResult(response);
            }
            else
            {
                throw new Exception($"Oppss.. Registration Found For : {request.CardNumber.ToUpper()}");
            }
        }
    }
}