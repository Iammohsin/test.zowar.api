﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Users;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.WebAPI.Handlers.Users.CreditCard
{
    public class GetByUserIdRequest : IRequest<GetByUserIdResponse>
    {
        public long UserId { get; set; }
    }

    public class GetByUserIdResponse
    {
        public ICollection<CreditCardInfo> CreditCardInfos { get; set; }
    }

    public class GetByUserIdRequestHandler : IRequestHandler<GetByUserIdRequest, GetByUserIdResponse>
    {
        private readonly ApplicationDbContext _dbContext;
        public readonly ICreditCardService _creditCardService;

        public GetByUserIdRequestHandler(ApplicationDbContext dbContext, ICreditCardService creditCardService)
        {
            _dbContext = dbContext;
            _creditCardService = creditCardService;
        }

        public async Task<GetByUserIdResponse> Handle(GetByUserIdRequest request, CancellationToken cancellationToken)
        {
            var user = _dbContext.CreditCardInfos.Where(w => w.UserId.Equals(request.UserId)).FirstOrDefault();
            if (user != null)
            {
                var result = _creditCardService.GetByUserId(request.UserId);
                var response = new GetByUserIdResponse();
                response.CreditCardInfos = result;
                return await Task.FromResult(response);
            }
            throw new Exception("No Records Found");
        }
    }
}