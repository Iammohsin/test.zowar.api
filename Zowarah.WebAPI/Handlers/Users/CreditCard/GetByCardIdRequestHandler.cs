﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Users;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.WebAPI.Handlers.Users.CreditCard
{
    public class GetByCardIdRequest : IRequest<GetByCardIdResponse>
    {
        public int cardId { get; set; }
    }

    public class GetByCardIdResponse
    {
        public CreditCardInfo CreditCardInfo { get; set; }
    }

    public class GetByCardIdRequestHandler : IRequestHandler<GetByCardIdRequest, GetByCardIdResponse>
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ICreditCardService _creditCardService;

        public GetByCardIdRequestHandler(ApplicationDbContext dbContext, ICreditCardService creditCardService)
        {
            _dbContext = dbContext;
            _creditCardService = creditCardService;
        }

        public async Task<GetByCardIdResponse> Handle(GetByCardIdRequest request, CancellationToken cancellationToken)
        {
            var CardId = _dbContext.CreditCardInfos.SingleOrDefault(s => s.Id.Equals(request.cardId));
            if (CardId != null)
            {
                var result = _creditCardService.GetByCardId(request.cardId);
                var response = new GetByCardIdResponse();
                response.CreditCardInfo = result;
                return await Task.FromResult(response);
            }
            throw new Exception("No Record Found");
        }
    }
}