﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Users;
using Zowarah.Data.Contexts;

namespace Zowarah.WebAPI.Handlers.Users.CreditCard
{
    public class DeleteCreditCardRequest : IRequest<DeleteCreditCardResponse>
    {
        public int Id { get; set; }
    }

    public class DeleteCreditCardResponse
    {
    }

    public class DeleteCreditCardRequestHandler : IRequestHandler<DeleteCreditCardRequest, DeleteCreditCardResponse>
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ICreditCardService _creditCardService;

        public DeleteCreditCardRequestHandler(ApplicationDbContext dbContext, ICreditCardService creditCardService)
        {
            _dbContext = dbContext;
            _creditCardService = creditCardService;
        }

        public async Task<DeleteCreditCardResponse> Handle(DeleteCreditCardRequest request, CancellationToken cancellationToken)
        {
            var card = _dbContext.CreditCardInfos.SingleOrDefault(s => s.Id.Equals(request.Id));

            if (card != null)
            {
                card.Id = request.Id;
                card.DeletedOn = DateTime.Now;
                card.IsDeleted = true;

                await _dbContext.SaveChangesAsync();

                return new DeleteCreditCardResponse() { };
            }
            else
            {
                throw new Exception($"Something went wrong..Delete Failed..");
            }
        }
    }
}