﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Users;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Users;


namespace Zowarah.WebAPI.Handlers.Common
{
    public class LoginUserRequest : IRequest<LoginUserRequestResponse>
    {
        public String Email { get; set; }
        public String Password { get; set; }
        public RoleTypes Role { get; set; }
    }
    public class LoginUserRequestResponse
    {
        public AppUser user { get; set; }
    }
    public class LoginUserRequestHandler : IRequestHandler<LoginUserRequest, LoginUserRequestResponse>
    {
        private readonly ApplicationDbContext _context;
        private readonly IPhotoService photoRepository;
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly RoleManager<AppRole> _roleManager;
        private readonly IHttpContextAccessor httpContextAccessor;
        public LoginUserRequestHandler(
            ApplicationDbContext context,
            IPhotoService _photoRepository,
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signinManager,
            RoleManager<AppRole> roleManager,
            IHttpContextAccessor _httpContextAccessor
            )
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signinManager;
            _roleManager = roleManager;
            photoRepository = _photoRepository;
            httpContextAccessor = _httpContextAccessor;

        }
        public async Task<LoginUserRequestResponse> Handle(LoginUserRequest request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);
            var result = await _userManager.CheckPasswordAsync(user, request.Password);
            
            
            if (result && user.IsActive)
            {
                // Check Privilage
                var roleResult = true;
                if (request.Role != RoleTypes.Client)
                    roleResult = await _userManager.IsInRoleAsync(user, request.Role.ToString());

                if (roleResult)
                {   // Get Photo 
                    var profilePhoto = await photoRepository.GetByUserId(user.Id);
                    if (profilePhoto != null)
                        user.ProfilePhoto = profilePhoto;
                    var culture = user.CultureId != null ? user.CultureId : "en";
                    httpContextAccessor.HttpContext.Session.SetString("culture",culture);
                    return new LoginUserRequestResponse() { user = user };
                }
                else
                    throw new Exception("Invalid Role");
            }
            else
                throw new Exception("Invalid User Id or Password !!");
        }
    }
}
