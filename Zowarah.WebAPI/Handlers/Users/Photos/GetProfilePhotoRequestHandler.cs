﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Users;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Users
{
    public class GetProfilePhotoRequest : IRequest<GetProfilePhotoResponse>
    {
        public long UserId { get; set; }
    }

    public class GetProfilePhotoResponse
    {
        public long Id { get; set; }
        public string Filename { get; set; }
    }

    public class GetProfilePhotoRequestHandler : IRequestHandler<GetProfilePhotoRequest, GetProfilePhotoResponse>
    {
        private readonly IPhotoService _photoRepository;
        private readonly IUnitOfWork _uow;
        public GetProfilePhotoRequestHandler(IPhotoService photoRepository, IUnitOfWork uow)
        {
            _photoRepository = photoRepository;
            _uow = uow;
        }
        public async Task<GetProfilePhotoResponse> Handle(GetProfilePhotoRequest request, CancellationToken cancellationToken)
        {
            var profilePhoto = await _photoRepository.GetByUserId(request.UserId);
            var response = new GetProfilePhotoResponse { Id = profilePhoto.Id, Filename = profilePhoto.FileName };
            return response;
        }
    }
}
