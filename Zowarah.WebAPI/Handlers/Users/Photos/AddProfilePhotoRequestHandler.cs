﻿using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Users;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Users
{
    public class AddProfilePhotoRequest : IRequest<AddProfilePhotoResponse>
    {
        public long UserId { get; set; }
        public IFormFile File { get; set; }
    }

    public class AddProfilePhotoResponse
    {
        public string Id { get; set; }
    }

    public class AddProfilePhotoRequestHandler : IRequestHandler<AddProfilePhotoRequest, AddProfilePhotoResponse>
    {
        private readonly IPhotoService _photoRepository;
        private readonly IHostingEnvironment _host;
        private readonly IUnitOfWork _uow;
        public AddProfilePhotoRequestHandler(IPhotoService photoRepository, IHostingEnvironment host, IUnitOfWork uow)
        {
            _photoRepository = photoRepository;
            _host = host;
            _uow = uow;
        }

        public async Task<AddProfilePhotoResponse> Handle(AddProfilePhotoRequest request, CancellationToken cancellationToken)
        {
            string path = Path.Combine(_host.WebRootPath, "uploads\\users\\photos");
            try
            {
                var response = new AddProfilePhotoResponse();
                var existing = await _photoRepository.GetByUserId(request.UserId);
                if (existing == null)
                {
                    var filename = await UploadFile(request, path);
                    var profilePhoto = new ProfilePhoto { UserId = request.UserId, FileName = filename, CreatedDate = DateTime.Now };
                    _photoRepository.Create(profilePhoto);
                    await _uow.SaveChangesAsync();
                    response.Id = profilePhoto.FileName;
                }
                else
                {
                    File.Delete(Path.Combine(path, existing.FileName));
                    var filename = await UploadFile(request, path);
                    existing.FileName = filename;
                    existing.UpdatedOn = DateTime.Now;
                    _photoRepository.Update(existing);
                    await _uow.SaveChangesAsync();
                    response.Id = existing.FileName;
                }
                return await Task.FromResult(response);
            }          
            catch(Exception err)
            {
                throw err;
            }            
        }

        private async Task<string> UploadFile(AddProfilePhotoRequest request, string path)
        {
            var fileName = Guid.NewGuid().ToString() + Path.GetExtension(request.File.FileName);
            var filePath = Path.Combine(path, fileName);
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await request.File.CopyToAsync(stream);
            }
            return fileName;
        }
    }
}
