﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.WebAPI.Handlers.Users.Users
{
    public class ChangeStatusRequest : IRequest<ChangeStatusResponse>
    {
        public string Id { get; set; }
        public Boolean Status { get; set; }
    }

    public class ChangeStatusResponse
    {
         }
    public class ChangeStatusRequestHandler : IRequestHandler<ChangeStatusRequest, ChangeStatusResponse>
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<AppUser> _userManager;
        public ChangeStatusRequestHandler(
            ApplicationDbContext context,
            UserManager<AppUser> userManager
            )
        {
            _context = context;
            _userManager = userManager;
        }
        public async Task<ChangeStatusResponse> Handle(ChangeStatusRequest request, CancellationToken cancellationToken)
        {

            var user = await _userManager.FindByIdAsync(request.Id);
            if (user != null)
                user.IsActive = request.Status;

            _context.Users.Update(user);
            await _context.SaveChangesAsync();
            return new ChangeStatusResponse(){ };
        }
    }
}
