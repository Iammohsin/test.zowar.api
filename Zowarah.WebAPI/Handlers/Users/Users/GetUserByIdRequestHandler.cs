﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.WebAPI.Handlers.Users.Users
{
    public class GetUserByIdRequest : IRequest<GetUserByIdResponse>
    {
        public long Id { get; set; }
    }

    public class GetUserByIdResponse
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public RoleTypes Role { get; set; }
        public List<long> Permissions { get; set; }
    }
    public class GetUserByIdRequestHandler : IRequestHandler<GetUserByIdRequest, GetUserByIdResponse>
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<AppUser> _userManager;

        public GetUserByIdRequestHandler(
            ApplicationDbContext context,
            UserManager<AppUser> userManager
            )
        {
            _context = context;
            _userManager = userManager;
        }
        public async Task<GetUserByIdResponse> Handle(GetUserByIdRequest request, CancellationToken cancellationToken)
        {
            var user = _context.Users.Include(x=>x.UserPermissions).FirstOrDefault(x => x.Id == request.Id);
            var role =  _userManager.GetRolesAsync(user).Result.ElementAt(0);
            var getUserByIdResponse = new GetUserByIdResponse
            {
                Email=user.Email,
                FirstName=user.FirstName,
                LastName=user.LastName,
                Role= (RoleTypes)Enum.Parse(typeof(RoleTypes), role),
                Permissions =user.UserPermissions.Select(x=>x.PermissionId).ToList()
            };

            return getUserByIdResponse;
        }
    }
}

