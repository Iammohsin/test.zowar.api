﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.WebAPI.Handlers.Users.Users
{
    public class GetUsersRequest : IRequest<GetUsersResponse>
    {
        public RoleTypes Role { get; set; }
    }

    public class GetUsersResponse
    {
        public GetUsersResponse()
        {
            Users = new List<GetUsersResponseItem>();
        }
        public List<GetUsersResponseItem> Users { get; set; }
    }

    public class GetUsersResponseItem
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Role { get; set; }
        public Boolean Status { get; set; }
        public List<long> Permissions { get; set; }
    }
    public class GetUsersRequestHandler : IRequestHandler<GetUsersRequest, GetUsersResponse>
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<AppRole> _roleManager;


        public GetUsersRequestHandler(
            ApplicationDbContext context,
            UserManager<AppUser> userManager,
            RoleManager<AppRole> roleManager
            )
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }
        public async Task<GetUsersResponse> Handle(GetUsersRequest request, CancellationToken cancellationToken)
        {
            var getUsersResponse = new GetUsersResponse();
            var roleUsers = await _userManager.GetUsersInRoleAsync(request.Role.ToString());
            var users =  (from u in _context.Users.Include(x => x.UserPermissions)
                               join d in roleUsers
                                on u.Id equals d.Id
                               select u).ToList();

            //var users = await _roleManager.FindByNameAsync(request.Role.ToString());
            //_context.Users.Where(x=> x.userin).Include(x => x.UserPermissions).ToList();
            foreach (var user in users)
            {
                    var getUsersResponseItem = new GetUsersResponseItem
                    {
                        Id=user.Id,
                        Email = user.Email,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        UserName=user.UserName,
                        Status = user.IsActive,
                        Permissions = user.UserPermissions.Select(x => x.PermissionId).ToList()
                    };
                    getUsersResponse.Users.Add(getUsersResponseItem);
            }         

            return getUsersResponse;
        }
    }
}
