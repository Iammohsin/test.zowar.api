﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.WebAPI.Handlers.Users.Users
{
    public class IsUserInRoleRequest : IRequest<Boolean>
    {
        public string Id { get; set; }
        public RoleTypes Role { get; set; }
    }

 
    public class IsUserInRoleRequestHandler : IRequestHandler<IsUserInRoleRequest, Boolean>
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<AppUser> _userManager;

        public IsUserInRoleRequestHandler(
            ApplicationDbContext context,
            UserManager<AppUser> userManager
            )
        {
            _context = context;
            _userManager = userManager;
        }
        public async Task<Boolean> Handle(IsUserInRoleRequest request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(request.Id);
            if (user == null)
                return false;
            return await _userManager.IsInRoleAsync(user, request.Role.ToString());
        }
    }
}

