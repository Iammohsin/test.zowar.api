﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Interfaces;
using static Zowarah.WebAPI.Handlers.Users.Users.GetAdminPermssionsResponse;

namespace Zowarah.WebAPI.Handlers.Users.Users
{
    public class GetAdminPermssionsRequest : IRequest<GetAdminPermssionsResponse>
    {
        public long Id { get; set; }
    }

    public class GetAdminPermssionsResponse
    {
        public ICollection<AdminPermissionQuery> Permissions { get; set; }
        public class AdminPermissionQuery
        {
            public long Id { get; set; }
            public string Description { get; set; }
        }
    }
    public class GetAdminPermssionsRequestHandler : IRequestHandler<GetAdminPermssionsRequest, GetAdminPermssionsResponse>
    {
        private readonly ApplicationDbContext context;

        public GetAdminPermssionsRequestHandler(
            ApplicationDbContext _context
            )
        {
            context = _context;
            
        }
        public async Task<GetAdminPermssionsResponse> Handle(GetAdminPermssionsRequest request, CancellationToken cancellationToken)
        {
            var permisions = await context.Permissions.Select(x => new AdminPermissionQuery {Id =  x.Id, Description =x.Description }).ToListAsync();
            return new GetAdminPermssionsResponse { Permissions = permisions};
        }
    }
}

