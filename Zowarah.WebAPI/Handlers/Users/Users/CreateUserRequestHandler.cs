﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Zowarah.Application.Services.Common;
using Zowarah.Application.Services.Security;
using Zowarah.Domain.Entities.Users;
using Zowarah.WebAPI.Config.Notification;
using Zowarah.WebAPI.Handlers.Common.NotificationsHandlers;
using Zowarah.WebAPI.Config.AppSettings;
using Zowarah.Data.Contexts;
using Microsoft.EntityFrameworkCore.Storage;

namespace Zowarah.WebAPI.Handlers.Users.Users
{
    public class CreateUserRequest : IRequest<CreateUserResponse>
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password => "Abc@123"; // Default Password - To be discussed with Mazen
        public RoleTypes Role { get; set; }
        public List<long> Permissions { get; set; }
    }

    public class CreateUserResponse
    {
        public long UId { get; set; }
    }
    public class CreateUserRequestHandler : IRequestHandler<CreateUserRequest, CreateUserResponse>
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly RoleManager<AppRole> _roleManager;
        private readonly IMediator _mediator;

        public CreateUserRequestHandler(
            ApplicationDbContext context,
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signinManager,
            RoleManager<AppRole> roleManager,
            IMediator mediator
            )
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signinManager;
            _roleManager = roleManager;
            _mediator = mediator;
        }
        public async Task<CreateUserResponse> Handle(CreateUserRequest request, CancellationToken cancellationToken)
        {
            var user = new AppUser
            {
                IsActive = true,
                FirstName = request.FirstName,
                LastName = request.LastName,
                UserName=request.Email,
                Email = request.Email,
                UserPermissions = request.Permissions.Select(permission=> new UserPermission {PermissionId= permission }).ToList()
            };           

            using (IDbContextTransaction tran = _context.Database.BeginTransaction())
            {

                var userResult = await _userManager.CreateAsync(user, request.Password);
                var roleResult = await _userManager.AddToRoleAsync(user, request.Role.ToString());

                if (!userResult.Succeeded)
                {
                    tran.Rollback();
                    throw new Exception(userResult.Errors.ToList()[0].Description);
                }

                if (!roleResult.Succeeded)
                {
                    tran.Rollback();
                    throw new Exception(userResult.Errors.ToList()[0].Description);
                }
                await _context.SaveChangesAsync();
                tran.Commit();
                var template = await _mediator.Send(new GetMailTemplateRequest() { Id = Registration.TemplateId });

                var htmlContent = template.Template.Replace("$$FirstName$$", user.FirstName).Replace("$$Email$$", user.Email);
                await _mediator.Send(new SendMailNotificationRequest()
                {
                    To = user.Email,
                    DisplayName = user.FirstName,
                    Subject = Registration.Subject,
                    Content = htmlContent
                });
                return new CreateUserResponse() { UId = user.Id };
            }
        }
      
    }
}
