﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.WebAPI.Handlers.Users.Users
{
    public class UpdateUserRequest : IRequest<UpdateUserResponse>
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public RoleTypes OldRole { get; set; }
        public RoleTypes NewRole { get; set; }
        public List<long> Permissions { get; set; }
    }

    public class UpdateUserResponse
    {
        public long Id { get; set; }
    }
    public class UpdateUserRequestHandler : IRequestHandler<UpdateUserRequest, UpdateUserResponse>
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly RoleManager<AppRole> _roleManager;
        public UpdateUserRequestHandler(
            ApplicationDbContext context,
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signinManager,
            RoleManager<AppRole> roleManager
            )
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signinManager;
            _roleManager = roleManager;
        }
        public async Task<UpdateUserResponse> Handle(UpdateUserRequest request, CancellationToken cancellationToken)
        {
            
            var user = _context.Users.Include(x => x.UserPermissions).FirstOrDefault(x => x.Id == request.Id);
            var newPermissions = request.Permissions.Select(permission => new UserPermission { PermissionId = permission}).ToList();
            user.FirstName = request.FirstName;
            user.LastName = request.LastName;
            user.Email = request.Email;
      
            using (IDbContextTransaction tran = _context.Database.BeginTransaction())
            {
                user.UserPermissions.Clear();

                var removePermissionsResult = await _context.SaveChangesAsync();

                foreach (var newPermission in newPermissions)
                {
                    user.UserPermissions.Add(newPermission);
                }
                
                _context.Users.Update(user);

                var userResult = await _context.SaveChangesAsync();
                if (request.OldRole!= request.NewRole)
                {
                    var oldRoleResult = await _userManager.RemoveFromRoleAsync(user, request.OldRole.ToString());

                    if (!oldRoleResult.Succeeded)
                    {
                        tran.Rollback();
                        throw new Exception(oldRoleResult.Errors.ToList()[0].Description);
                    }

                    var newRoleResult = await _userManager.AddToRoleAsync(user, request.NewRole.ToString());

                    if (!newRoleResult.Succeeded)
                    {
                        tran.Rollback();
                        throw new Exception(newRoleResult.Errors.ToList()[0].Description);
                    }
                }
                

                if (userResult<1)
                {
                    tran.Rollback();
                    throw new Exception("Failed to update the user");
                }

                
                await _context.SaveChangesAsync();
                tran.Commit();
                return new UpdateUserResponse() { Id = user.Id };
            }
        }
    }
}
