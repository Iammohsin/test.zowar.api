﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.WebAPI.Handlers.Users
{
    public class UpdatePreferencesRequest:IRequest<UpdatePreferencesResponse>
    {
        public string UserId { get; set; }
        public string Currency { get; set; }
        public int SmokingOption { get; set; }
        public int Rating { get; set; }
        
    }

    public class UpdatePreferencesResponse
    {
    }

    public class UpdatePreferencesRequestHandler : IRequestHandler<UpdatePreferencesRequest, UpdatePreferencesResponse>
    {
        private readonly UserManager<AppUser> _userManager;

        public UpdatePreferencesRequestHandler(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<UpdatePreferencesResponse> Handle(UpdatePreferencesRequest request, CancellationToken cancellationToken)
        {
                var user = await _userManager.FindByIdAsync(request.UserId);
                user.CurrencyId = request.Currency;
                user.SmokingOption = request.SmokingOption;
                user.Rating = request.Rating;
                await _userManager.UpdateAsync(user);
                return new UpdatePreferencesResponse { };
              }
    }
}
