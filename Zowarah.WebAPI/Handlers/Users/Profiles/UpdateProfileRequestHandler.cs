﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.WebAPI.Handlers.Users
{
    public class UpdateProfileRequest:IRequest<ResponseMessage<string>>
    {
        public string Id { get; set; }
        public string Address { get; set; }
        public int? CityId { get; set; }
        public string PostalCode { get; set; }
        public int? CountryId { get; set; }
        public string PassportNo { get; set; }
        public string MobilePhoneNo { get; set; }
        public int? CitizenshipCountryId { get; set; }
        public string PassportExpiryDate { get; set; }
        public int? Gender { get; set; }
        public int? Title { get; set; }
        public string Birthday { get; set; }
        public int? DisplayCountryId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
    }

    public class ResponseMessage<T>
    {
        public ResponseMessage()
        {

        }

        public string Message { get; set; }

        public T ReturnResult { get; set; }
    }

    public class UpdateProfileRequestHandler : IRequestHandler<UpdateProfileRequest, ResponseMessage<string>>
    {
        private readonly UserManager<AppUser> _userManager;

        public UpdateProfileRequestHandler(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<ResponseMessage<string>> Handle(UpdateProfileRequest request, CancellationToken cancellationToken)
        {
                var user = await _userManager.FindByIdAsync(request.Id);
                user.Address = request.Address;
                user.MobilePhoneNo = request.MobilePhoneNo;
                user.CityId = request.CityId;
                user.CountryId = request.CountryId;
                user.PassportNo = request.PassportNo;
                user.CitizenshipCountryId = request.CitizenshipCountryId;
                user.PostalCode = request.PostalCode;
                user.Birthday = DateTime.Parse(request.Birthday);
                user.DisplayCountryId = request.DisplayCountryId;
                user.FirstName = request.FirstName;
                user.LastName = request.LastName;
               // user.PassportExpiryDate = DateTime.Parse(request.PassportExpiryDate);
                user.Title = request.Title;
                user.Gender = request.Gender;
                user.UserName = request.Email;
                user.DisplayName = request.DisplayName;

                // Set Default currency if not
                if (user.CurrencyId == null)
                 {
                       // Laster when Monolothic is ready  var currency = _dbconte
                 }
        
                await _userManager.UpdateAsync(user);
                return new ResponseMessage<string> { Message = "Ok" };

              }
    }
}
