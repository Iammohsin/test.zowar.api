﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.WebAPI.Handlers.Users
{
    public class SetCultureRequest:IRequest<SetCultureResponse>
    {
        public long UserId { get; set; }
        public string CultureId { get; set; }
    }

    public class SetCultureResponse
    {
    }

    public class SetCultureRequestHandler : IRequestHandler<SetCultureRequest, SetCultureResponse>
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly IHttpContextAccessor httpContextAccessor;
        public SetCultureRequestHandler(UserManager<AppUser> userManager,
                                        IHttpContextAccessor _httpContextAccessor
                                        )
        {
            //HttpContext context
            _userManager = userManager;
            httpContextAccessor = _httpContextAccessor;
        }

        public async Task<SetCultureResponse> Handle(SetCultureRequest request, CancellationToken cancellationToken)
        {
                var user = await _userManager.FindByIdAsync(request.UserId+"");
                httpContextAccessor.HttpContext.Session.SetString("culture", request.CultureId);
                user.CultureId = request.CultureId;
                await _userManager.UpdateAsync(user);
                return new SetCultureResponse { };
        }
    }
}
