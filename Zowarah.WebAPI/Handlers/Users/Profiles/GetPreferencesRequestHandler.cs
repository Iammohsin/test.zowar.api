﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.WebAPI.Handlers.Users
{
    public class GetPreferencesRequest:IRequest<GetPreferencesResponse>
    {
        public string UserId { get; set; }
    }

    public class GetPreferencesResponse
    {
        public string Currency { get; set; }
        public int SmokingOption { get; set; }
        public int Rating { get; set; }
    }

    public class GetPreferencesRequestHandler : IRequestHandler<GetPreferencesRequest, GetPreferencesResponse>
    {
        private readonly UserManager<AppUser> _userManager;

        public GetPreferencesRequestHandler(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }
        public async Task<GetPreferencesResponse> Handle(GetPreferencesRequest request, CancellationToken cancellationToken)
        {
                var user = await _userManager.FindByIdAsync(request.UserId);
                await _userManager.UpdateAsync(user);
                return new GetPreferencesResponse { Currency = user.CurrencyId, SmokingOption = user.SmokingOption, Rating= user.Rating };
              }
    }
}
