﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Interfaces;

namespace Zowarah.Identity.Api.Handlers.UserHandlers
{
    public class GetProfile
    {

        public class GetProfileRequest : IRequest<GetProfileResponse>
        {
            public long Id { get; set; }
        }

        public class GetProfileResponse
        {
            public string Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Address { get; set; }
            public int? CityId { get; set; }
            public string PostalCode { get; set; }
            public int? CountryId { get; set; }
            public string CurrencyId { get; set; }
            public string PassportNo { get; set; }
            public string PassportExpiryDate { get; set; }
            public string MobilePhoneNo { get; set; }
            public int? CitizenshipCountryId { get; set; }
            public int? Gender { get; set; }
            public int? Title { get; set; }
            public string Birthday { get; set; }
            public int? DisplayCountryId { get; set; }
            public string DisplayName { get; set; }
            public string Email { get; set; }
        }
        public class GetProfileRequestHandler : IRequestHandler<GetProfileRequest, GetProfileResponse>
        {
            private readonly UserManager<AppUser> _UserManager;
            private readonly IUnitOfWork _uow;

            public GetProfileRequestHandler(UserManager<AppUser> UserManage, IUnitOfWork uow)
            {
                _UserManager = UserManage;
                _uow = uow;
            }

            public async Task<GetProfileResponse> Handle(GetProfileRequest request, CancellationToken cancellationToken)
            {
                var profile = await _UserManager.FindByIdAsync(request.Id.ToString());

                if (profile == null)
                    return null;

                GetProfileResponse response = new GetProfileResponse();
                Mapper.Map(profile, response);
                if(profile.Birthday != null)
                {
                    response.Birthday = profile.Birthday.Value.ToString("dd/MM/yyyy");
                }
                if (profile.PassportExpiryDate != null)
                {
                    response.PassportExpiryDate = profile.PassportExpiryDate.Value.ToString("dd/MM/yyyy");
                }
                response.Email = profile.UserName;
                return await Task.FromResult(response);
            }
        }
    }
}
