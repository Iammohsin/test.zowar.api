﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.WebAPI.Handlers.Users
{

    public class GetUserCurrencyRequest : IRequest<GetUserCurrencyResponse>
    {
        public string Id { get; set; }
    }

    public class GetUserCurrencyResponse
    {
        public string CurrencyId { get; set; }
    }

    public class GetUserCurrencyRequestHandler : IRequestHandler<GetUserCurrencyRequest, GetUserCurrencyResponse>
    {
        private readonly UserManager<AppUser> _userManager;

        public GetUserCurrencyRequestHandler(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<GetUserCurrencyResponse> Handle(GetUserCurrencyRequest request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(request.Id);

            if (user == null)
                return null;

            GetUserCurrencyResponse response = new GetUserCurrencyResponse();
            // ToDo: to set the default currecy at time of registration
            if (user.CurrencyId == null)
                user.CurrencyId = "USD";

            response.CurrencyId = user.CurrencyId;
            return await Task.FromResult(response);
        }
    }
}