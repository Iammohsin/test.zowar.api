﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Services.Common;

namespace Zowarah.WebAPI.Handlers.Hotels.ServiceNearbyHandlers {

    public class GetServiceNearbyTypesRequest : IRequest<GetServiceNearbyTypesResponse> {

    }

    public class GetServiceNearbyTypesResponse {
        public ICollection<ServiceNearbyCategoryQuery> types { get; set; } = new List<ServiceNearbyCategoryQuery>();
    }
    public class GetServiceNearbyTypesHandler : IRequestHandler<GetServiceNearbyTypesRequest, GetServiceNearbyTypesResponse> {
        private readonly IServiceNearbyService service;

        public GetServiceNearbyTypesHandler(IServiceNearbyService _service) {
            service = _service;
        }
        public async Task<GetServiceNearbyTypesResponse> Handle(GetServiceNearbyTypesRequest request, CancellationToken cancellationToken) {
            return new GetServiceNearbyTypesResponse {
                types = await service.GetServiceNearbyTypes()
            };
        }


    }
}
