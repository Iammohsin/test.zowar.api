﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Application.Services.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Hotels.ServiceNearbyHandlers {
    public class UpdateServiceNearbyRequest : IRequest<UpdateServiceNearbyResponse> {
        public long ServiceId { get; set; }
        public ICollection<ServiceNearbyQuery> ServiceNearbies { get; set; }
        public long UserId { get; set; }
    }

    public class UpdateServiceNearbyResponse {
        public ICollection<ServiceNearbyQuery> ServiceNearbies { get; set; } = new List<ServiceNearbyQuery>();
    }
    public class UpdateServiceNearbyHandler : IRequestHandler<UpdateServiceNearbyRequest, UpdateServiceNearbyResponse> {
        private readonly IHotelService repoHotel;
        private readonly IServiceNearbyService rep;
        private readonly IUnitOfWork uow;
        public UpdateServiceNearbyHandler(IServiceNearbyService _service, IUnitOfWork _uow, IHotelService _repoHotel) {
            repoHotel = _repoHotel;
            rep = _service;
            uow = _uow;
        }
        public async Task<UpdateServiceNearbyResponse> Handle(UpdateServiceNearbyRequest request, CancellationToken cancellationToken) {
            var hotel = await repoHotel.GetById(request.ServiceId);
            if (hotel != null && hotel.ProviderId == request.UserId) {
                var existsNearbies = await rep.GetAllByServiceId(request.ServiceId);

                //delete nearby
                var updatedIds = request.ServiceNearbies.Select(s => s.Id);
                var deleted = existsNearbies.Where(d => !updatedIds.Contains(d.Id));
                foreach (var del in deleted) {
                    var nrby = await rep.GetById(del.Id);
                    rep.Delete(nrby);
                }
                foreach (var nearby in request.ServiceNearbies) {
                    nearby.ServiceId = request.ServiceId;
                    if (nearby.Id.Equals(0)) {
                        var nrby = new ServiceNearby();
                        nrby = nrby.FromRequest(nearby);
                        rep.Create(nrby);
                    } else {
                        var nrby = await rep.GetById(nearby.Id);
                        if (nrby == null)
                            return null;
                        // nrby = nrby.FromRequest(nearby);
                        Mapper.Map(nearby, nrby);
                        rep.Update(nrby);
                    }
                }
                if (request.ServiceNearbies.Count > 0)
                    hotel.IsNearbyConfigured = true;
                else
                    hotel.IsNearbyConfigured = false;
                repoHotel.Update(hotel);
                await uow.SaveChangesAsync();
                return new UpdateServiceNearbyResponse { ServiceNearbies = request.ServiceNearbies };
            }
            return null;
        }
    }
}
