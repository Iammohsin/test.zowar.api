﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Services.Common;
using Zowarah.Domain.Entities.Common;

namespace Zowarah.WebAPI.Handlers.Hotels.ServiceNearbyHandlers
{

    public class GetAllServiceNearbyRequest : IRequest<GetAllServiceNearbyResponse>
    {
        public long ServiceId { get; set; }
    }

    public class GetAllServiceNearbyResponse
    {
        public ICollection<ServiceNearbyQuery> Nearbies { get; set; } = new List<ServiceNearbyQuery>();
    }
    public class GetAllServiceNearbyHandler : IRequestHandler<GetAllServiceNearbyRequest, GetAllServiceNearbyResponse>
    {
        private readonly IServiceNearbyService service;
        public GetAllServiceNearbyHandler(IServiceNearbyService _service)
        {
            service = _service;
        }
        public async Task<GetAllServiceNearbyResponse> Handle(GetAllServiceNearbyRequest request, CancellationToken cancellationToken)
        {            
            return new GetAllServiceNearbyResponse() {Nearbies = await service.GetAllByServiceId(request.ServiceId) };
        }
    }
}
