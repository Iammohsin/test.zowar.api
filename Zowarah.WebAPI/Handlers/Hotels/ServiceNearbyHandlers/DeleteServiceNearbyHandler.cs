﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Zowarah.Domain.Interfaces;
using Zowarah.Application.Interfaces.Common;

namespace Zowarah.WebAPI.Handlers.Hotels.ServiceNearbyHandlers
{
    public class DeleteServiceNearbyRequest : IRequest<Boolean>
    {
        public long Id { get; set; }
        public long UserId { get; set; }
    }
    public class DeleteServiceNearbyRequestHandler : IRequestHandler<DeleteServiceNearbyRequest, Boolean>
    {
        private readonly IServiceNearbyService _Repo;
        private readonly IUnitOfWork _uow;

        public DeleteServiceNearbyRequestHandler(IServiceNearbyService Repo, IUnitOfWork uow)
        {
            _Repo = Repo;
            _uow = uow;
        }
        public async Task<Boolean> Handle(DeleteServiceNearbyRequest request, CancellationToken cancellationToken)
        {
            var service = await _Repo.GetById(request.Id);
            if (service != null && service.Id == request.Id)
            {
                _Repo.RealDelete(service);
                await _uow.SaveChangesAsync();
                return true;
            }
            return false;
        }
    }
}
