﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using System.ComponentModel.DataAnnotations;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Common;
using Microsoft.EntityFrameworkCore;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    public class GetServicesRequest : IRequest<GetServicesResponse>
    {
        public long Id { get; set; }
    }

    public class GetServicesResponse
    {
        public Boolean IsFoodServed { get; set; }
        public ICollection<string> Languages { get; set; }
        public MealsInfo Meals { get; set; }
        public ICollection<int> Facilities { get; set; }
        public class MealsInfo
        {
            public Boolean BreakFast { get; set; }
            public Boolean Dinner { get; set; }
            public Boolean Lunch { get; set; }
            public decimal BreakFastRate { get; set; }
            public decimal LunchRate { get; set; }
            public decimal DinnerRate { get; set; }
        }
    }

    public class GetServicesRequestHandler : IRequestHandler<GetServicesRequest,GetServicesResponse>
    {
        private readonly IRepository<Hotel> repo;
        private readonly IHotelService _HotelRepo;
        private readonly IUnitOfWork _uow;
      
        public GetServicesRequestHandler(
            IRepository<Hotel> _repo,
            IHotelService HotelRepo, 
            IUnitOfWork uow)
        {
            repo = _repo;
            _HotelRepo = HotelRepo;
            _uow = uow;
        }

        public async Task<GetServicesResponse> Handle(GetServicesRequest request, CancellationToken cancellationToken)
        {
            //"Languages,Facilities,HotelMeals"
            var hotel = await _HotelRepo.GetById(request.Id, x => x.Languages,x=> x.Facilities, x=>x.HotelMeals );
            if (hotel == null)
                return null;

             GetServicesResponse response = new GetServicesResponse();
             hotel.ToResponse(response);
             return await Task.FromResult(response);
        }
    }
}
