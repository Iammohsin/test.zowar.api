﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using System.ComponentModel.DataAnnotations;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Common;
using Microsoft.EntityFrameworkCore;

namespace Zowarah.WebAPI.Handlers.Hotels {
    /*public class UpdateServicesRequestValidator : AbstractValidator<UpdateServicesRequest>
    {
        public UpdateServicesRequestValidator()
        {
           // RuleFor(PropertyDetails => PropertyDetails.Id).NotEmpty();
        }
    }*/
    public class UpdateServicesRequest : IRequest<UpdateServicesResponse> {
        public long Id { get; set; }
        public Boolean IsFoodServed { get; set; }
        public ICollection<string> Languages { get; set; }
        public MealsInfo Meals { get; set; }
        public ICollection<int> Facilities { get; set; }
        public class MealsInfo {
            public Boolean BreakFast { get; set; }
            public Boolean Dinner { get; set; }
            public Boolean Lunch { get; set; }
            public decimal BreakFastRate { get; set; }
            public decimal LunchRate { get; set; }
            public decimal DinnerRate { get; set; }
        }
        public long UserId { get;  set; }        
       
    }

    public class UpdateServicesResponse {

    }

    public class UpdateServicesRequestHandler : IRequestHandler<UpdateServicesRequest, UpdateServicesResponse> {
        private readonly IHotelService _Repo;
        private readonly IUnitOfWork _uow;

        public UpdateServicesRequestHandler(IHotelService Repo, IUnitOfWork uow) {
            _Repo = Repo;
            _uow = uow;
        }
        public async Task<UpdateServicesResponse> Handle(UpdateServicesRequest request, CancellationToken cancellationToken) {
            var hotel = await _Repo.GetById(request.Id, x => x.Languages, x => x.Facilities, x => x.HotelMeals);
            if (hotel != null && hotel.ProviderId == request.UserId) {
                hotel.IsFacilitiesConfigured = true;
                Mapper.Map(request, hotel);
                _Repo.Update(hotel);
                var result = await _uow.SaveChangesAsync();
                return new UpdateServicesResponse { };
            }
            return null;
        }
    }
}
