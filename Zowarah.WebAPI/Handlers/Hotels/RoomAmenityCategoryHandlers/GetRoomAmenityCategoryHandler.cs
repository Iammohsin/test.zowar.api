﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{

    public class GetRoomAmenityCategoryRequest : IRequest<GetRoomAmenityCategoryResponse>
    {
        public int Id { get; set; }
    }

    public class GetRoomAmenityCategoryResponse
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetRoomAmenityCategoryRequestHandler : IRequestHandler<GetRoomAmenityCategoryRequest, GetRoomAmenityCategoryResponse>
    {
        private readonly ILookupRepository<RoomAmenityCategory> _Repo;
        private readonly IUnitOfWork _uow;

        public GetRoomAmenityCategoryRequestHandler(ILookupRepository<RoomAmenityCategory> Repo, IUnitOfWork uow)
        {
            _Repo = Repo;
            _uow = uow;
        }

        public async Task<GetRoomAmenityCategoryResponse> Handle(GetRoomAmenityCategoryRequest request, CancellationToken cancellationToken)
        {
            var Hotel = await _Repo.GetById(request.Id);

            if (Hotel == null)
                return null;

            GetRoomAmenityCategoryResponse response = new GetRoomAmenityCategoryResponse();
            Hotel.ToResponse(response);

            return await Task.FromResult(response);
        }
    }
}
