﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class GetRoomAmenityCategoryListRequest : IRequest<GetRoomAmenityCategoryListResponse> {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetRoomAmenityCategoryListResponse {
        public GetRoomAmenityCategoryListResponse() {
            RoomAmenityCategoryList = new List<GetRoomAmenityCategoryResponse>();
        }

        public List<GetRoomAmenityCategoryResponse> RoomAmenityCategoryList;
    }
    public class GetRoomAmenityCategoryListRequestHandler : IRequestHandler<GetRoomAmenityCategoryListRequest, GetRoomAmenityCategoryListResponse> {
        private readonly ILookupRepository<RoomAmenityCategory> _RoomAmenityCategoryRepo;
        private readonly IUnitOfWork _uow;
        public GetRoomAmenityCategoryListRequestHandler(ILookupRepository<RoomAmenityCategory> RoomAmenityCategoryRepo, IUnitOfWork uow) {
            _RoomAmenityCategoryRepo = RoomAmenityCategoryRepo;
            _uow = uow;
        }

        public async Task<GetRoomAmenityCategoryListResponse> Handle(GetRoomAmenityCategoryListRequest request, CancellationToken cancellationToken) {
            var lst = await _RoomAmenityCategoryRepo.GetAll();
            if (lst == null)
                return null;
            return new GetRoomAmenityCategoryListResponse {
                RoomAmenityCategoryList = Mapper.Map<List<GetRoomAmenityCategoryResponse>>(lst)
            };
        }
    }

}
