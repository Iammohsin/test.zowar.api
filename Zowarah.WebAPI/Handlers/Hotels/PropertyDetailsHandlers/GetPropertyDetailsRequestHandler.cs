﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using System.ComponentModel.DataAnnotations;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    public class GetPropertyDetailsRequest : IRequest<GetPropertyDetailsResponse>
    {
        public long Id { get; set; }
    }

    public class GetPropertyDetailsResponse
    {
        public GetPropertyDetailsResponse()
        {
            Facilities = new List<int>();
        }
        [Required]
        public string Id { get; set; }

        [Required]
        public int checkInFrom { get; set; }
        [Required]
        public int checkInTo { get; set; }
        [Required]
        public int checkOutFrom { get; set; }
        [Required]
        public int checkOutTo { get; set; }
        [Required]
        public Boolean isPetsAllowed { get; set; }
        [Required]
        public Boolean isChildrenAllowed { get; set; }
        public int internetOptionId { get; set; }
        public int parkingOptionId { get; set; }
        public ICollection<int> Facilities { get; set; }
    }

    public class GetPropertyDetailsRequestHandler : IRequestHandler<GetPropertyDetailsRequest,GetPropertyDetailsResponse>
    {
        private readonly IHotelService _Repo;
        private readonly IUnitOfWork _uow;
      
        public GetPropertyDetailsRequestHandler(IHotelService Repo, IUnitOfWork uow)
        {
            _Repo = Repo;
            _uow = uow;
        }
        public async Task<GetPropertyDetailsResponse> Handle(GetPropertyDetailsRequest request, CancellationToken cancellationToken)
        {
            var hotel = await _Repo.GetById(request.Id, x => x.Facilities);
            if (hotel == null)
                return null;

             GetPropertyDetailsResponse response = new GetPropertyDetailsResponse();
             hotel.ToResponse(response);
             return await Task.FromResult(response);
        }
    }

}
