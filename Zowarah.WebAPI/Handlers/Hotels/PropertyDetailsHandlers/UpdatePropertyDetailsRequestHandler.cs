﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using System.ComponentModel.DataAnnotations;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Microsoft.EntityFrameworkCore;

namespace Zowarah.WebAPI.Handlers.Hotels {
    /*public class UpdatePropertyDetailsRequestValidator : AbstractValidator<UpdatePropertyDetailsRequest>
    {
        public UpdatePropertyDetailsRequestValidator()
        {
           // RuleFor(PropertyDetails => PropertyDetails.Id).NotEmpty();
        }
    }*/
    public class UpdatePropertyDetailsRequest : IRequest<UpdatePropertyDetailsResponse> {

        public UpdatePropertyDetailsRequest() {
            Facilities = new List<int>();
        }

        [Required]
        public long Id { get; set; }

        [Required]
        public int checkInFrom { get; set; }
        [Required]
        public int checkInTo { get; set; }
        [Required]
        public int checkOutFrom { get; set; }
        [Required]
        public int checkOutTo { get; set; }
        [Required]
        public Boolean isPetsAllowed { get; set; }
        [Required]
        public Boolean isChildrenAllowed { get; set; }
        public int internetOptionId { get; set; }
        public int parkingOptionId { get; set; }
        public ICollection<int> Facilities { get; set; }
        public long UserId { get; set; }

    }
    public class UpdatePropertyDetailsResponse {
        public long Id { get; set; }
    }
    public class UpdatePropertyDetailsRequestHandler : IRequestHandler<UpdatePropertyDetailsRequest, UpdatePropertyDetailsResponse> {
        private readonly IHotelService _Repo;
        private readonly IUnitOfWork _uow;

        public UpdatePropertyDetailsRequestHandler(IHotelService Repo, IUnitOfWork uow) {
            _Repo = Repo;
            _uow = uow;
        }
        public async Task<UpdatePropertyDetailsResponse> Handle(UpdatePropertyDetailsRequest request, CancellationToken cancellationToken) {
            var hotel = await _Repo.GetById(request.Id, x => x.Facilities);
            if (hotel != null && hotel.ProviderId == request.UserId) {
                Mapper.Map(request, hotel);
                hotel.IsPropertyDetailsConfigured = true;
                _Repo.Update(hotel);
                var result = await _uow.SaveChangesAsync();
                return new UpdatePropertyDetailsResponse { Id = hotel.Id };
            }
            return null;
        }
    }
}
