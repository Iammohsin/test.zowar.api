﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    public class GetSmokingOptionsListRequest : IRequest<GetSmokingOptionsListResponse>
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetSmokingOptionsListResponse
    {
        public GetSmokingOptionsListResponse()
        {
            SmokingOptionsList = new List<GetSmokingOptionResponse>();
        }

        public List<GetSmokingOptionResponse> SmokingOptionsList;
    }
    public class GetSmokingOptionsListRequestHandler : IRequestHandler<GetSmokingOptionsListRequest, GetSmokingOptionsListResponse>
    {
        private readonly ILookupRepository<SmokingOption> _SmokingOptionRepo;
        private readonly IUnitOfWork _uow;

        public GetSmokingOptionsListRequestHandler(ILookupRepository<SmokingOption> SmokingOptionRepo, IUnitOfWork uow)
        {
            _SmokingOptionRepo = SmokingOptionRepo;
            _uow = uow;
        }

        public async Task<GetSmokingOptionsListResponse> Handle(GetSmokingOptionsListRequest request, CancellationToken cancellationToken)
        {
            var lst = await _SmokingOptionRepo.GetAll();
            if (lst == null)
                return null;

            return new GetSmokingOptionsListResponse {
                SmokingOptionsList=Mapper.Map<List<GetSmokingOptionResponse>>(lst)
            };
        }
    }

}
