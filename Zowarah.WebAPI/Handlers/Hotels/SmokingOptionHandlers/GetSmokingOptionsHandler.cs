﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{

    public class GetSmokingOptionRequest : IRequest<GetSmokingOptionResponse>
    {
        public int Id { get; set; }
    }

    public class GetSmokingOptionResponse
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetSmokingOptionRequestHandler : IRequestHandler<GetSmokingOptionRequest, GetSmokingOptionResponse>
    {
        private readonly ILookupRepository<SmokingOption> _Repo;
        private readonly IUnitOfWork _uow;

        public GetSmokingOptionRequestHandler(ILookupRepository<SmokingOption> Repo, IUnitOfWork uow)
        {
            _Repo = Repo;
            _uow = uow;
        }

        public async Task<GetSmokingOptionResponse> Handle(GetSmokingOptionRequest request, CancellationToken cancellationToken)
        {
            var Hotel = await _Repo.GetById(request.Id);

            if (Hotel == null)
                return null;

            GetSmokingOptionResponse response = new GetSmokingOptionResponse();
            Hotel.ToResponse(response);

            return await Task.FromResult(response);
        }
    }
}
