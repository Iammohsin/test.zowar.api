﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{

    public class GetPropertyTypeRequest : IRequest<GetPropertyTypeResponse>
    {
        public int Id { get; set; }
    }

    public class GetPropertyTypeResponse
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }

    public class GetPropertyTypeRequestHandler : IRequestHandler<GetPropertyTypeRequest, GetPropertyTypeResponse>
    {
        private readonly ILookupRepository<PropertyType> _repo;
        private readonly IUnitOfWork _uow;

        public GetPropertyTypeRequestHandler(ILookupRepository<PropertyType> repo, IUnitOfWork uow)
        {
            _repo = repo;
            _uow = uow;
        }

        public async Task<GetPropertyTypeResponse> Handle(GetPropertyTypeRequest request, CancellationToken cancellationToken)
        {
            var Hotel = await _repo.GetById(request.Id);

            if (Hotel == null)
                return null;

            GetPropertyTypeResponse response = new GetPropertyTypeResponse();
            Hotel.ToResponse(response);

            return await Task.FromResult(response);
        }
    }
}
