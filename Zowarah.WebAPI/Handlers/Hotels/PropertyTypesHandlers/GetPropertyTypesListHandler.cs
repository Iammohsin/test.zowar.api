﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class GetPropertyTypeListRequest : IRequest<GetPropertyTypeListResponse> {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetPropertyTypeListResponse {
        public GetPropertyTypeListResponse() {
            PropertyTypes = new List<GetPropertyTypeResponse>();
        }

        public List<GetPropertyTypeResponse> PropertyTypes;
    }
    public class GetPropertyTypesRequestHandler : IRequestHandler<GetPropertyTypeListRequest, GetPropertyTypeListResponse> {
        private readonly ILookupRepository<PropertyType> _PropertyTypeRepo;
        private readonly IUnitOfWork _uow;

        public GetPropertyTypesRequestHandler(ILookupRepository<PropertyType> PropertyTypeRepo, IUnitOfWork uow) {
            _PropertyTypeRepo = PropertyTypeRepo;
            _uow = uow;
        }

        public async Task<GetPropertyTypeListResponse> Handle(GetPropertyTypeListRequest request, CancellationToken cancellationToken) {
            var lst = await _PropertyTypeRepo.GetAll();
            if (lst == null)
                return null;

            return new GetPropertyTypeListResponse {
                PropertyTypes = Mapper.Map<List<GetPropertyTypeResponse>>(lst)
            };
        }
    }

}
