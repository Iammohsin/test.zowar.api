﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class GetParkingOptionsListRequest : IRequest<GetParkingOptionsListResponse> {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetParkingOptionsResponse {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetParkingOptionsListResponse {
        public GetParkingOptionsListResponse() {
            ParkingOptions = new List<GetParkingOptionsResponse>();
        }

        public List<GetParkingOptionsResponse> ParkingOptions;
    }
    public class GetParkingOptionsListRequestHandler : IRequestHandler<GetParkingOptionsListRequest, GetParkingOptionsListResponse> {
        private readonly ILookupRepository<ParkingOption> _ParkingOptionRepo;
        private readonly IUnitOfWork _uow;

        public GetParkingOptionsListRequestHandler(ILookupRepository<ParkingOption> ParkingOptionRepo, IUnitOfWork uow) {
            _ParkingOptionRepo = ParkingOptionRepo;
            _uow = uow;
        }

        public async Task<GetParkingOptionsListResponse> Handle(GetParkingOptionsListRequest request, CancellationToken cancellationToken) {
            var List = await _ParkingOptionRepo.GetAll();
            if (List == null)
                return null;

            return new GetParkingOptionsListResponse {
                ParkingOptions = Mapper.Map<List<GetParkingOptionsResponse>>(List)
            };
        }
    }

}
