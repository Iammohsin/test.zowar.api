﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Application.Interfaces.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels {

    public class DeletePhotoRequest : IRequest<Boolean> {
        public String Id { get; set; }
        public long UserId { get; set; }

    }


    public class DeletePhotoRequestHandler : IRequestHandler<DeletePhotoRequest, Boolean> {
        private readonly IRepository<HotelPhoto> _Repo;
        private readonly IHotelService _hotelSer;
        private readonly IUnitOfWork _uow;
        private readonly IHostingEnvironment _host;

        public DeletePhotoRequestHandler(IRepository<HotelPhoto> Repo, IHotelService hotelSer, IUnitOfWork uow, IHostingEnvironment host) {
            _Repo = Repo;
            _hotelSer = hotelSer;
            _uow = uow;
            _host = host;
        }

        public async Task<Boolean> Handle(DeletePhotoRequest request, CancellationToken cancellationToken) {
            var photo = await _Repo.GetById(request.Id);
            if (photo != null) {
                var hotel = await _hotelSer.GetById(photo.HotelId);
                if (hotel != null && hotel.ProviderId == request.UserId) {
                    _Repo.RealDelete(photo);                    
                    await _uow.SaveChangesAsync();
                    await DeleteFile(photo.FileName);
                    return true;
                }
            }
            return false;
        }

        private async Task DeleteFile(string fileName) {
            var uploadFolderPath = Path.Combine(_host.WebRootPath, "uploads\\hotels\\photos");
            var filePath = Path.Combine(uploadFolderPath, fileName);

            await Task.Run(() => {
                if (File.Exists(filePath)) {
                    File.Delete(filePath);
                }
            });
        }
    }
}
