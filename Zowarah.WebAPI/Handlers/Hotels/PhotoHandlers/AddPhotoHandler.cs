﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels {

    public class AddPhotoRequest : IRequest<AddPhotoResponse> {
        public long HotelId { get; set; }
        public IFormFile File { get; set; }
        public long UserId { get; set; }

    }

    public class AddPhotoResponse {
        public String Id { get; set; }
        public String Path { get; set; }
    }
    public class AddPhotoRequestHandler : IRequestHandler<AddPhotoRequest, AddPhotoResponse> {
        private readonly IHotelService _HotelRepo;
        private readonly IRepository<HotelPhoto> _Repo;
        private readonly IUnitOfWork _uow;
        private readonly IHostingEnvironment _host;

        public AddPhotoRequestHandler(IHotelService HotelRepo, IRepository<HotelPhoto> Repo, IUnitOfWork uow, IHostingEnvironment host) {
            _HotelRepo = HotelRepo;
            _Repo = Repo;
            _uow = uow;
            _host = host;
        }

        public async Task<AddPhotoResponse> Handle(AddPhotoRequest request, CancellationToken cancellationToken) {
            var hotel = _HotelRepo.GetById(request.HotelId).Result;
            if (hotel != null && hotel.ProviderId == request.UserId) {
                var uploadFolderPath = Path.Combine(_host.WebRootPath, "uploads\\hotels\\photos");
                var fileName = Guid.NewGuid().ToString() + Path.GetExtension(request.File.FileName);
                var filePath = Path.Combine(uploadFolderPath, fileName);
                using (var stream = new FileStream(filePath, FileMode.Create)) {
                    await request.File.CopyToAsync(stream);
                }

                var photo = new HotelPhoto() { HotelId = request.HotelId, FileName = fileName, IsMain = true };
                _Repo.Create(photo);
                hotel.IsPhotosConfigured = true;
                await _uow.SaveChangesAsync();                
                return new AddPhotoResponse {
                    Id = photo.Id,
                    Path = _host.WebRootPath
                };
            }
            return null;
        }
    }
}
