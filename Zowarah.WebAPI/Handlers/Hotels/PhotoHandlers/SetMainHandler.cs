﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Application.Interfaces.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels {

    public class SetMainRequest : IRequest<Boolean> {
        public String Id { get; set; }
        public long UserId { get; set; }

    }


    public class SetMainRequestHandler : IRequestHandler<SetMainRequest, Boolean> {
        private readonly IRepository<HotelPhoto> repo;
        private readonly IHotelService _hotelSer;
        private readonly IUnitOfWork uow;

        public SetMainRequestHandler(IRepository<HotelPhoto> _repo, IHotelService hotelSer, IUnitOfWork _uow) {
            repo = _repo;
            _hotelSer = hotelSer;
            uow = _uow;
        }

        public async Task<Boolean> Handle(SetMainRequest request, CancellationToken cancellationToken) {
            var mainPhoto = await repo.GetById(request.Id);
            if (mainPhoto != null) {
                if (mainPhoto.IsMain)
                    return true;
                var hotel = await _hotelSer.GetHotelWithPhotos(mainPhoto.HotelId);
                if (hotel != null && hotel.ProviderId == request.UserId) {
                    foreach (var photo in hotel.Photos) {
                        if (mainPhoto.Id == photo.Id)
                            photo.IsMain = true;
                        else
                            photo.IsMain = false;
                        repo.Update(photo);
                    }                   
                    await uow.SaveChangesAsync();
                    return true;
                }
            }
            return false;
        }
       
    }
}
