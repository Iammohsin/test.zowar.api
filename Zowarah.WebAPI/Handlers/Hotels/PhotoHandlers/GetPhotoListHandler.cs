﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    public class GetPhotoListRequest : IRequest<GetPhotoListResponse>
    {
        public long HotelId { get; set; }
    }
    public class GetPhotoResponse {
        public string Id { get; set; }
        public string FileName { get; set; }
    }
    public class GetPhotoListResponse
    {
        public GetPhotoListResponse()
        {
            PhotoList = new List<GetPhotoResponse>();
        }
        public List<GetPhotoResponse> PhotoList;
    }
    public class GetPhotoListRequestHandler : IRequestHandler<GetPhotoListRequest, GetPhotoListResponse>
    {
        private readonly IRepository<HotelPhoto> _repo;
        private readonly IUnitOfWork _uow;

        public GetPhotoListRequestHandler(IRepository<HotelPhoto> PhotoRepo, IUnitOfWork uow)
        {
            _repo = PhotoRepo;
            _uow = uow;
        }

        public async Task<GetPhotoListResponse> Handle(GetPhotoListRequest request, CancellationToken cancellationToken)
        {
            var lst = await _repo.GetAll();
            lst = lst.Where(x => x.HotelId == request.HotelId).ToList();

            if (lst == null)
                return null;

            return new GetPhotoListResponse {
                PhotoList=Mapper.Map<List<GetPhotoResponse>>(lst)
            };
        }
    }
}
