﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class GetInternetOptionsListRequest : IRequest<GetInternetOptionsListResponse> {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetInternetOptionsResponse {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetInternetOptionsListResponse {
        public GetInternetOptionsListResponse() {
            InternetOptions = new List<GetInternetOptionsResponse>();
        }

        public List<GetInternetOptionsResponse> InternetOptions;
    }
    public class GetInternetOptionsListRequestHandler : IRequestHandler<GetInternetOptionsListRequest, GetInternetOptionsListResponse> {
        private readonly ILookupRepository<InternetOption> _InternetOptionRepo;
        private readonly IUnitOfWork _uow;

        public GetInternetOptionsListRequestHandler(ILookupRepository<InternetOption> InternetOptionRepo, IUnitOfWork uow) {
            _InternetOptionRepo = InternetOptionRepo;
            _uow = uow;
        }

        public async Task<GetInternetOptionsListResponse> Handle(GetInternetOptionsListRequest request, CancellationToken cancellationToken) {
            var List = await _InternetOptionRepo.GetAll();
            if (List == null)
                return null;

            return new GetInternetOptionsListResponse {
                InternetOptions = Mapper.Map<List<GetInternetOptionsResponse>>(List)
            };
        }
    }

}
