﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using Zowarah.Domain.Interfaces;
using Zowarah.Application.Interfaces.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class SetCalendarRoomsRequest : IRequest<SetCalendarRoomsResponse> {
        public long HotelId { get; set; }
        public long RoomId { get; set; }
        public DateTime Date { get; set; }
        public int Rooms { get; set; }
        public decimal RatePerNight { get; set; } //mohsin

        public long UserId { get; set; }
    }

    public class SetCalendarRoomsResponse {
    }

    public class SetCalendarRoomsRequestHandler : IRequestHandler<SetCalendarRoomsRequest, SetCalendarRoomsResponse> {
        private readonly ICalendarService _Repo;
        private readonly IUnitOfWork _uow;

        public SetCalendarRoomsRequestHandler(ICalendarService Repo, IUnitOfWork uow) {
            _Repo = Repo;
            _uow = uow;
        }
        public async Task<SetCalendarRoomsResponse> Handle(SetCalendarRoomsRequest request, CancellationToken cancellationToken) {
            await _Repo.SetCalendarRooms(request.HotelId,
                                    request.UserId,
                                    request.RoomId,
                                    request.Date,
                                    request.Rooms,
                                    request.RatePerNight //Mohsin
                                    );
            return new SetCalendarRoomsResponse { };
        }
    }
}
