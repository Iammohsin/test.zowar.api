﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class SetCalendarStatusRequest : IRequest<SetCalendarStatusResponse> {
        public long HotelId { get; set; }
        public long RoomId { get; set; }
        public long Rooms { get; set; }
        public decimal RatePerNight { get; set; }
        public DateTime Date { get; set; }
        public Boolean Status { get; set; }
        public long UserId { get; set; }
       

    }

    public class SetCalendarStatusResponse {
    }

    public class SetCalendarStatusRequestHandler : IRequestHandler<SetCalendarStatusRequest, SetCalendarStatusResponse> {
        private readonly ICalendarService _Repo;
        private readonly IUnitOfWork _uow;

        public SetCalendarStatusRequestHandler(ICalendarService Repo, IUnitOfWork uow) {
            _Repo = Repo;
            _uow = uow;
        }
        public async Task<SetCalendarStatusResponse> Handle(SetCalendarStatusRequest request, CancellationToken cancellationToken) {
            await _Repo.SetCalendarStatus(request.HotelId, request.UserId, request.RoomId, request.Rooms, request.RatePerNight, request.Date, request.Status);
            return new SetCalendarStatusResponse();
        }
    }
}
