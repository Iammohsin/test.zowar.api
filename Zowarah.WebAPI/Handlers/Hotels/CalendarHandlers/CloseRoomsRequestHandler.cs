﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using System.ComponentModel.DataAnnotations;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Hotels {
   
    public class CloseRoomsRequest : IRequest<CloseRoomsResponse> {
        public long Id { get; set; }
        public DateTime CloseFrom { get; set; }
        public DateTime CloseTo { get; set; }
        public Boolean Fri { get; set; }
        public Boolean Sat { get; set; }
        public Boolean Sun { get; set; }
        public Boolean Mon { get; set; }
        public Boolean Tue { get; set; }
        public Boolean Wed { get; set; }
        public Boolean Thr { get; set; }
        public long UserId { get; set; }
    }
    public class CloseRoomsResponse {
    }
    public class CloseRoomsRequestHandler : IRequestHandler<CloseRoomsRequest, CloseRoomsResponse> {
        private readonly ICalendarService _Repo;
        private readonly IUnitOfWork _uow;

        public CloseRoomsRequestHandler(ICalendarService Repo, IUnitOfWork uow) {
            _Repo = Repo;
            _uow = uow;
        }
        public async Task<CloseRoomsResponse> Handle(CloseRoomsRequest request, CancellationToken cancellationToken) {
            await _Repo.CloseRooms(request.Id,
                                    request.UserId,
                                    request.CloseFrom,
                                    request.CloseTo,
                                    request.Fri,
                                    request.Sat,
                                    request.Sun,
                                    request.Mon,
                                    request.Tue,
                                    request.Wed,
                                    request.Thr
                                );

            return new CloseRoomsResponse { };
        }
    }
}
