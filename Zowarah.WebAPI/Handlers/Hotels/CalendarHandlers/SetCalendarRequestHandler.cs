﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class SetCalendarRequest : IRequest<SetCalendarResponse> {
        public long Id { get; set; }
        public DateTime OpenFrom { get; set; }
        public DateTime OpenTo { get; set; }
        public DateTime CloseFrom { get; set; }
        public DateTime CloseTo { get; set; }
        public Boolean Fri { get; set; }
        public Boolean Sat { get; set; }
        public Boolean Sun { get; set; }
        public Boolean Mon { get; set; }
        public Boolean Tue { get; set; }
        public Boolean Wed { get; set; }
        public Boolean Thr { get; set; }
        public long UserId { get; set; }
    }
    public class SetCalendarResponse {
    }
    public class SetCalendarRequestHandler : IRequestHandler<SetCalendarRequest, SetCalendarResponse> {
        private readonly ICalendarService _Repo;
        private readonly IUnitOfWork _uow;


        public SetCalendarRequestHandler(ICalendarService Repo, IUnitOfWork uow) {
            _Repo = Repo;
            _uow = uow;
        }
        public async Task<SetCalendarResponse> Handle(SetCalendarRequest request, CancellationToken cancellationToken) {
            await _Repo.SetCalendar(request.Id, request.UserId, request.OpenFrom, request.OpenTo);
            return new SetCalendarResponse();
        }
    }
}
