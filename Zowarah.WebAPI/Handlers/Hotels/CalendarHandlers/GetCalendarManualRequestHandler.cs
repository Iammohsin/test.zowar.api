﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels {
    
    public class GetCalendarManualRequest : IRequest<GetCalendarManualResponse> {
        public long RoomId { get; set; }
        public long UserId { get; set; }
    }

    public class CalendarManualDetails {
        public DateTime Date { get; set; }
        public int? Rooms { get; set; }

        public decimal RatePerNight { get; set; }
        public Boolean? Status { get; set; }
    }

    public class GetCalendarManualResponse {
        public ICollection<CalendarManualDetails> CalenderManuals { get; set; } = new List<CalendarManualDetails>();
    }
    public class GetCalendarManualRequestHandler : IRequestHandler<GetCalendarManualRequest, GetCalendarManualResponse> {
        private readonly ICalendarService _repo;
        private readonly IUnitOfWork _uow;

        public GetCalendarManualRequestHandler(ICalendarService repo, IUnitOfWork uow) {
            _repo = repo;
            _uow = uow;
        }
        public async Task<GetCalendarManualResponse> Handle(GetCalendarManualRequest request, CancellationToken cancellationToken) {
            var calendar = await _repo.GetCalendarManual(request.RoomId, request.UserId);
            var res = new GetCalendarManualResponse();
            if (calendar != null)
                res.CalenderManuals = Mapper.Map<List<CalendarManualDetails>>(calendar);
            return res;
        }
    }
}
