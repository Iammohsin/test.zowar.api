﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Hotels {
      public class GetCalendarRequest : IRequest<GetCalendarResponse> {
        public long HotelId { get; set; }
        public long UserId { get; set; }
    }

    public class GetCalendarResponse {
        public string Id { get; set; }
        public DateTime OpenFrom { get; set; }
        public DateTime OpenTo { get; set; }
        public DateTime CloseFrom { get; set; }
        public DateTime CloseTo { get; set; }
        public Boolean Fri { get; set; }
        public Boolean Sat { get; set; }
        public Boolean Sun { get; set; }
        public Boolean Mon { get; set; }
        public Boolean Tue { get; set; }
        public Boolean Wed { get; set; }
        public Boolean Thr { get; set; }

    }
    public class GetCalendarRequestHandler : IRequestHandler<GetCalendarRequest, GetCalendarResponse> {
        private readonly ICalendarService _repo;
        private readonly IUnitOfWork _uow;


        public GetCalendarRequestHandler(ICalendarService repo, IUnitOfWork uow) {
            _repo = repo;
            _uow = uow;
        }
        public async Task<GetCalendarResponse> Handle(GetCalendarRequest request, CancellationToken cancellationToken) {
            var calendar = await _repo.GetCalendar(request.HotelId,request.UserId);
            if (calendar != null) {
                var response = new GetCalendarResponse();
                calendar.ToResponse(response);
                return response;
            }
            return null;
        }
    }
}
