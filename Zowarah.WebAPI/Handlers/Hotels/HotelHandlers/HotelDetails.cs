﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Text;
using Newtonsoft.Json;


namespace Zowarah.WebAPI.Handlers.Hotels
{
    public class HotelDetails
    {
        public string Id { get; set; }
        public string ProviderId { get; set; }
        public string Name { get; set; }
        public int PropertyTypeId { get; set; }
        public int NoOfRooms { get; set; }
        public string ContactName { get; set; }
        public string Address { get; set; }
        public int CityId { get; set; }
        public int Status { get; set; }
        public int CheckInFrom { get; set; }
        public int CheckInTo { get; set; }
        public int CheckOutFrom { get; set; }
        public int CheckOutTo { get; set; }
        public int InternetOptionId { get; set; }
        public int ParkingOptionId { get; set; }
        public int ParkingTypeId { get; set; }
        public int ParkingSiteId { get; set; }
        public int ParkingReservationId { get; set; }
        public int BreakfastOptionId { get; set; }
        public Boolean IsPetsAllowed { get; set; }
        public Boolean IsChildrenAllowed { get; set; }
        public Boolean IsCreditCardAccepted { get; set; }
        public int CancellationPolicyId { get; set; }
        public int CancellationFeeId { get; set; }
        public String InvoicingRecipientName { get; set; }
        public int InvoicingCountryId { get; set; }
        public int InvoicingCityId { get; set; }
        public string InvoicingAddress { get; set; }
        public string InvoicingPostalAddress { get; set; }
        public string IsInvoicingPropertyAddress { get; set; }
        public String CityName { get; set; }
        public String StatusName { get; set; }
        public ICollection<RoomDetails> Rooms { get; set; }
        public ICollection<int> Facilities { get; set; }
        public ICollection<int> CreditCards { get; set; }
        public int LocLng { get; set; }
        public int LocLat { get; set; }
       // public decimal TaxP { get; set; }

        // Indicators
        public Boolean IsBookable { get; set; }
        public int Arrivals { get; set; }
        public int Departures { get; set; }

    }
}