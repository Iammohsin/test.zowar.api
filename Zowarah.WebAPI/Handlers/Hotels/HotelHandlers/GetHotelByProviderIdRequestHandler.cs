﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Microsoft.EntityFrameworkCore;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using Zowarah.Application.Services.Hotels;
using Zowarah.Domain.Entities.Common;

namespace Zowarah.WebAPI.Handlers.Hotels
{

    public class GetHotelByProviderIdRequest : IRequest<GetHotelByProviderIdResponse>
    {
        public long ProviderId { get; set; }
    }

    public class GetHotelByProviderIdResponse
    {
        public GetHotelByProviderIdResponse()
        {
        }
        public IList<HotelDetails> HotelsLst;
    }

    public class GetHotelByProviderIdRequestHandler : IRequestHandler<GetHotelByProviderIdRequest, GetHotelByProviderIdResponse>
    {
        private readonly IHotelService _repo;
        private readonly IUnitOfWork _uow;

        public GetHotelByProviderIdRequestHandler(IHotelService repo, IUnitOfWork uow)
        {
            _repo = repo;
            _uow = uow;
        }

        public async Task<GetHotelByProviderIdResponse> Handle(GetHotelByProviderIdRequest request, CancellationToken cancellationToken)
        {
            try
            {
                var lst = await _repo.GetByProviderId(request.ProviderId).Result.ToListAsync();
                //Hotels.Result.AsParallel()

                if (lst == null)
                    return null;

                //Hotels = (IQueryable<HotelDesc>)Hotels;

                GetHotelByProviderIdResponse response = new GetHotelByProviderIdResponse();
                var lstResponse = Mapper.Map<IList<HotelQuery>, IList<HotelDetails>>(lst);
                response.HotelsLst = lstResponse;

                return await Task.FromResult(response);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
