﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Hotels
{

    public class GetHotelRequest : IRequest<GetHotelResponse>
    {
        public long Id { get; set; }
    }

    public class GetHotelResponse :HotelDetails
    {
          
    }

    public class GetHotelRequestHandler : IRequestHandler<GetHotelRequest,GetHotelResponse>
    {
        private readonly IHotelService _repo;
        private readonly IUnitOfWork _uow;

        public GetHotelRequestHandler(IHotelService repo, IUnitOfWork uow)
        {
            _repo = repo;
            _uow = uow;
        }

        public async Task<GetHotelResponse> Handle(GetHotelRequest request, CancellationToken cancellationToken)
        {
            try {
            var Hotel = await _repo.GetHotelById(request.Id);

            if (Hotel == null)
                return null;

            GetHotelResponse response = new GetHotelResponse();
            Hotel.ToResponse(response);

            return await Task.FromResult(response);
            } catch(Exception ex) { System.Diagnostics.Debug.WriteLine(ex.InnerException); }
            return null;
        }
    }

}
