﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Hotels
{

    public class GetConfigFlagsRequest : IRequest<GetConfigFlagsResponse>
    {
        public long Id { get; set; }
    }

    public class GetConfigFlagsResponse 
    {
        public Boolean IsBasicInfoConfigured { get; set; }
        public Boolean IsPaymentPolicyConfigured { get; set; }
        public Boolean IsRoomTypesConfigured { get; set; }
        public Boolean IsRoomAmenitiesConfigured { get; set; }
        public Boolean IsFacilitiesConfigured { get; set; }
        public Boolean IsSubmitted { get; set; }
        public Boolean IsPropertyDetailsConfigured { get; set; }
        public Boolean IsPhotosConfigured { get; set; }
    }

    public class GetConfigFlagsRequestHandler : IRequestHandler<GetConfigFlagsRequest,GetConfigFlagsResponse>
    {
        private readonly IHotelService _repo;
        private readonly IUnitOfWork _uow;

        public GetConfigFlagsRequestHandler(IHotelService repo, IUnitOfWork uow)
        {
            _repo = repo;
            _uow = uow;
        }

        public async Task<GetConfigFlagsResponse> Handle(GetConfigFlagsRequest request, CancellationToken cancellationToken)
        {
            try
            {
                var Hotel = await _repo.GetById(request.Id);

                if (Hotel == null)
                    return null;

                GetConfigFlagsResponse response = new GetConfigFlagsResponse();
                Hotel.ToResponse(response);

                return await Task.FromResult(response);
            } catch(Exception ex)
            {
                throw;
            }
        }
    }

}
