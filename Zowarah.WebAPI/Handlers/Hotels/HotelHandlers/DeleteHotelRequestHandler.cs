﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Application.Config;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class DeleteHotelRequest : IRequest<long> {
        public long Id { get; set; }
        public long UserId { get; internal set; }
    }
    public class DeleteHotelRequestHandler : IRequestHandler<DeleteHotelRequest, long> {
        private readonly IHotelService _Repo;
        private readonly IUnitOfWork _uow;

        public DeleteHotelRequestHandler(IHotelService Repo, IUnitOfWork uow) {
            _Repo = Repo;
            _uow = uow;
        }
        public async Task<long> Handle(DeleteHotelRequest request, CancellationToken cancellationToken) {
            var hotel = await _Repo.GetById(request.Id);
            if (hotel != null && hotel.ProviderId == request.UserId) {
                _Repo.Delete(hotel);
                await _uow.SaveChangesAsync();
                return request.Id;
            }
            throw new CustomException("Invalid Hotel!");
        }
    }
}
