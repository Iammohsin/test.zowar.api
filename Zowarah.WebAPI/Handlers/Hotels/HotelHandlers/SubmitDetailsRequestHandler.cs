﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using static Zowarah.Identity.Api.Handlers.UserHandlers.GetProfile;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.WebAPI.Handlers.Common.NotificationsHandlers;
using Zowarah.WebAPI.Config.Notification;
using Zowarah.Domain.Entities.Common;

namespace Zowarah.WebAPI.Handlers.Hotels {
    /* public class SubmitDetailsRequestValidator : AbstractValidator<SubmitDetailsRequest>
     {
         public SubmitDetailsRequestValidator()
         {
            // RuleFor(PropertyDetails => PropertyDetails.Id).NotEmpty();
         }
     }*/

    public class SubmitDetailsRequest : IRequest<SubmitDetailsResponse> {

        public SubmitDetailsRequest() {
            Facilities = new List<int>();
        }

        [Required]
        public long Id { get; set; }

        [Required]
        public int checkInFrom { get; set; }
        [Required]
        public int checkInTo { get; set; }
        [Required]
        public int checkOutFrom { get; set; }
        [Required]
        public int checkOutTo { get; set; }
        [Required]
        public Boolean isPetsAllowed { get; set; }
        [Required]
        public Boolean isChildrenAllowed { get; set; }
        public int internetOptionId { get; set; }
        public int parkingOptionId { get; set; }
        public ICollection<int> Facilities { get; set; }
        public long UserId { get; set; }


    }
    public class SubmitDetailsResponse {

    }
    public class SubmitDetailsRequestHandler : IRequestHandler<SubmitDetailsRequest, SubmitDetailsResponse> {
        private readonly IHotelService app;
        private readonly IRepository<Service> serviceRepo;
        private readonly IMediator _mediator;

        public SubmitDetailsRequestHandler(IMediator mediator, IHotelService _app, IRepository<Service> _serviceRepo) {
            app = _app;
            serviceRepo = _serviceRepo;
            _mediator = mediator;

        }
        public async Task<SubmitDetailsResponse> Handle(SubmitDetailsRequest request, CancellationToken cancellationToken) {

            var result = await app.SubmitHotel(request.Id, request.UserId);
            if (result != null) {
                await sendMailNotification(result);
            }
            return new SubmitDetailsResponse() { };
        }

        async Task sendMailNotification(Service service) {

            await _mediator.Send(new SendServiceStatusNotificationRequest() {
                Name = service.Name,
                ProviderId = service.ProviderId,
                Status = service.Status
            });
        }
    }
}
