﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Text;
using Newtonsoft.Json;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    public class RtcpayDetails
    {
        public string Id { get; set; }
        public string BookingId { get; set; }
        public DateTime TransactionDate { get; set; }
        public string BranchID { get; set; }
        public string RefNum { get; set; }
        public string BillNumber { get; set; }
        public decimal Amount { get; set; }
    }
}