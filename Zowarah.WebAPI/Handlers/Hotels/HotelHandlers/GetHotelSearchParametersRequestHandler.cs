﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Microsoft.EntityFrameworkCore;
using Zowarah.Application.Services.Hotels;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Queries.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{

    public class GetHotelSearchParametersRequest : IRequest<GetHotelSearchParametersResponse>
    {
        public long HotelId { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
    }

    public class GetHotelSearchParametersResponse:HotelSearchParameters
    {
    }

    public class GetHotelSearchParametersRequestHandler : IRequestHandler<GetHotelSearchParametersRequest,GetHotelSearchParametersResponse>
    {
        private readonly IHotelService _repo;
        private readonly IUnitOfWork _uow;

        public GetHotelSearchParametersRequestHandler(IHotelService repo, IUnitOfWork uow)
        {
            _repo = repo;
            _uow = uow;
        }

        public async Task<GetHotelSearchParametersResponse> Handle(GetHotelSearchParametersRequest request, CancellationToken cancellationToken)
        {
            var result = await _repo.GetHotelSearchParameters(request.HotelId,request.CheckInDate,request.CheckOutDate);
            if (result == null)
                return null;


            GetHotelSearchParametersResponse response = new GetHotelSearchParametersResponse();
            result.ToResponse(response);
            return await Task.FromResult(response);
            
        }
    }
}