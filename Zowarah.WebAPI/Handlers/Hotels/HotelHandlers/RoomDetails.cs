﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Text;
using Newtonsoft.Json;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    public class RoomDetails
    {
        public int SmokingOptionId { get; set; }
        public int NoOfRooms { get; set; }
        public bool IsLivingRoom { get; set; }
        public bool IsKitchen { get; set; }
        public int NoOfBathrooms { get; set; }
        public int NoOfPersons { get; set; }
        public decimal RatePerNight { get; set; }
        public int RoomTypeNameId { get; set; }
        public ICollection<int> RoomAmenities { get; set; }
    }
}
