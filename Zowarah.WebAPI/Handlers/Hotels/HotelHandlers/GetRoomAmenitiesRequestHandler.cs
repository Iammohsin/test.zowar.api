﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Microsoft.EntityFrameworkCore;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using Zowarah.Application.Services.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{

    public class GetRoomAmenitiesRequest : IRequest<GetRoomAmenitiesResponse>
    {
        public long HotelId { get; set; }
    }


    public class GetRoomAmenitiesResponse
    {
        public GetRoomAmenitiesResponse()
        {
            RoomAmenities = new List<HotelRoomAmenityDetails>();
        }
        public IList<HotelRoomAmenityDetails> RoomAmenities;
    }

    public class GetRoomAmenitiesRequestHandler : IRequestHandler<GetRoomAmenitiesRequest, GetRoomAmenitiesResponse>
    {
        private readonly IHotelService _repo;
        private readonly IUnitOfWork _uow;

        public GetRoomAmenitiesRequestHandler(IHotelService repo, IUnitOfWork uow)
        {
            _repo = repo;
            _uow = uow;
        }

        public async Task<GetRoomAmenitiesResponse> Handle(GetRoomAmenitiesRequest request, CancellationToken cancellationToken)
        {
            try
            {
                var lst = await _repo.GetRoomAmenities(request.HotelId).Result.ToListAsync();

                if (lst == null)
                    return null;
                GetRoomAmenitiesResponse response = new GetRoomAmenitiesResponse();
                var lstResponse = Mapper.Map<IList<HotelRoomAmenityQuery>, IList<HotelRoomAmenityDetails>>(lst);
                response.RoomAmenities = lstResponse;
                return await Task.FromResult(response);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
