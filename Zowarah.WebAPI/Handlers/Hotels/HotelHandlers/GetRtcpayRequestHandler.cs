﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Hotels
{    

    public class GetRtcpayRequest : IRequest<GetRtcpayResponse>
    {
       
    }

    public class GetRtcpayResponse : RtcpayDetails
    {

    }

    public class GetRtcpayRequestHandler : IRequestHandler<GetRtcpayRequest, GetRtcpayResponse>
    {
        private readonly IHotelService _repo;
        private readonly IUnitOfWork _uow;

        public GetRtcpayRequestHandler(IHotelService repo, IUnitOfWork uow)
        {
            _repo = repo;
            _uow = uow;
                }

        public async Task<GetRtcpayResponse> Handle(GetRtcpayRequest request, CancellationToken cancellationToken)
        {
            try
            {
                var Rtcpay = await _repo.GetRtcpayById();

                if (Rtcpay == null)
                    return null;

                GetRtcpayResponse response = new GetRtcpayResponse();
                Rtcpay.ToResponse(response);

                return await Task.FromResult(response);
            }
            catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.InnerException); }
            return null;
        }
    }
}
