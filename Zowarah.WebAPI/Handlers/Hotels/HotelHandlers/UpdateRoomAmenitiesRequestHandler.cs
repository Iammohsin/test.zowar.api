﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Microsoft.EntityFrameworkCore;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Hotels {

    public class UpdateRoomAmenitiesRequest : IRequest<UpdateRoomAmenitiesResponse> {
        public long HotelId { get; set; }
        public IList<HotelRoomAmenityDetails> RoomAmenities { get; set; }
        public long UserId { get; set; }
    }


    public class UpdateRoomAmenitiesResponse {

    }

    public class UpdateRoomAmenitiesRequestHandler : IRequestHandler<UpdateRoomAmenitiesRequest, UpdateRoomAmenitiesResponse> {
        private readonly IHotelService _repo;
        private readonly IUnitOfWork _uow;

        public UpdateRoomAmenitiesRequestHandler(IHotelService repo, IUnitOfWork uow) {
            _repo = repo;
            _uow = uow;
        }

        public async Task<UpdateRoomAmenitiesResponse> Handle(UpdateRoomAmenitiesRequest request, CancellationToken cancellationToken) {
            var hotel = await _repo.FindBy(x => x.Id == request.HotelId, x => x.RoomAmenities).FirstOrDefaultAsync();
            if (hotel != null && hotel.ProviderId == request.UserId) {
                Mapper.Map(request, hotel);
                hotel.IsRoomAmenitiesConfigured = true;
                _repo.Update(hotel);
                await _uow.SaveChangesAsync();
                return new UpdateRoomAmenitiesResponse();
            }
            return null;
        }
    }
}
