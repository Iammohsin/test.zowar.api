﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Microsoft.EntityFrameworkCore;
using Zowarah.Application.Services.Hotels;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Queries.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels {

    public class SearchHotelRequest : IRequest<SearchHotelResponse> {
        public Boolean IsMember { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public int NoOfPersons { get; set; }
        public int NoOfChildren { get; set; }
        public int NoOfRooms { get; set; }
        public String Location { get; set; }
        public int OrderBy { get; set; }
        public int[][] Filters { get; set; }
        //  public decimal[][] Filter { get; set; }
      //  public decimal TaxP { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }

    public class SearchHotelResponse {
        public ICollection<HotelSearchResult> lst = new List<HotelSearchResult>();
    }

    public class SearchHotelRequestHandler : IRequestHandler<SearchHotelRequest, SearchHotelResponse> {
        private readonly IHotelService _repo;
        private readonly IUnitOfWork _uow;

        public SearchHotelRequestHandler(IHotelService repo, IUnitOfWork uow) {
            _repo = repo;
            _uow = uow;
        }

        public async Task<SearchHotelResponse> Handle(SearchHotelRequest request, CancellationToken cancellationToken) {
            SearchHotelResponse response = new SearchHotelResponse();
            var result = await _repo.Search(
                request.Page,
                request.PageSize,
                request.IsMember,
                request.CheckInDate,
                request.CheckOutDate,
                request.NoOfPersons,
                request.NoOfChildren,
                request.NoOfRooms,
                request.Location,
                request.OrderBy,
                request.Filters[0],
              //  request.TaxP,
            //    request.Filter[0],
                request.Filters[1],
                request.Filters[2],
                request.Filters[3]
                

                                            );
            if (result != null)
                response.lst = result;
            return response;




        }
    }
}