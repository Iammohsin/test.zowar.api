﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Config;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Application.Services.Hotels;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class GetHotelConfigurationStateRequest : IRequest<GetHotelConfigurationStateResponse> {
        public long HotelId { get; set; }
        public long UserId { get; set; }
    }

    public class GetHotelConfigurationStateResponse {
        public long Id { get; set; }
        public bool IsBasicInfoConfigured { get; set; }
        public bool IsPropertyDetailsConfigured { get; set; }
        public bool IsRoomTypesConfigured { get; set; }
        public bool IsRoomAmenitiesConfigured { get; set; }
        public bool IsPaymentPolicyConfigured { get; set; }
        public bool IsSubmitted { get; set; }
        public bool IsPhotosConfigured { get; set; }
        public bool IsFacilitiesConfigured { get; set; }
        public bool IsProfileConfigured { get; set; }
        public bool IsNearbyConfigured { get; set; }
        public bool IsCalendarConfigured { get; set; }
    }
    public class GetHotelConfigurationStateHandler : IRequestHandler<GetHotelConfigurationStateRequest, GetHotelConfigurationStateResponse> {
        private readonly IHotelService repoHotel;
        private readonly ICalendarService repoCalendar;
        public GetHotelConfigurationStateHandler(IHotelService _repoHotel, ICalendarService _repoCalendar) {
            repoCalendar = _repoCalendar;
            repoHotel = _repoHotel;
        }
        public async Task<GetHotelConfigurationStateResponse> Handle(GetHotelConfigurationStateRequest request, CancellationToken cancellationToken) {
            var hotel = await repoHotel.GetById(request.HotelId);
            if (hotel != null && hotel.ProviderId == request.UserId) {
                var res = new GetHotelConfigurationStateResponse();
                hotel.ToResponse(res);
                res.IsCalendarConfigured = await repoCalendar.IsHotelCalenderConfigured(request.HotelId, request.UserId);
                return res;
            }
            throw new CustomException("Invalid Hotel!");
        }
    }


}
