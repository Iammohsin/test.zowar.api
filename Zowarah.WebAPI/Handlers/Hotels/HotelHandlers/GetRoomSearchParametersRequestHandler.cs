﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Microsoft.EntityFrameworkCore;
using Zowarah.Application.Services.Hotels;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Queries.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{

    public class GetRoomSearchParametersRequest : IRequest<GetRoomSearchParametersResponse>
    {
        public long RoomId { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
    }

    public class GetRoomSearchParametersResponse: RoomSearchParameters
    {
    }

    public class GetRoomSearchParametersRequestHandler : IRequestHandler<GetRoomSearchParametersRequest,GetRoomSearchParametersResponse>
    {
        private readonly IHotelService _repo;
        private readonly IUnitOfWork _uow;

        public GetRoomSearchParametersRequestHandler(IHotelService repo, IUnitOfWork uow)
        {
            _repo = repo;
            _uow = uow;
        }

        public async Task<GetRoomSearchParametersResponse> Handle(GetRoomSearchParametersRequest request, CancellationToken cancellationToken)
        {
            var result = await _repo.GetRoomSearchParameters(request.RoomId,request.CheckInDate,request.CheckOutDate);
            if (result == null)
                return null;


            GetRoomSearchParametersResponse response = new GetRoomSearchParametersResponse();
            result.ToResponse(response);
            return await Task.FromResult(response);
            
        }
    }
}