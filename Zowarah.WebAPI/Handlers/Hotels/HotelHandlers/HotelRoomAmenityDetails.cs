﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Text;
using Newtonsoft.Json;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    public class HotelRoomAmenityDetails
    {
        public int RoomAmenityId;
        public long RoomId;
    }
}
