﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Microsoft.EntityFrameworkCore;
using Zowarah.Application.Services.Hotels;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Hotels
{

    public class GetHotelRoomRequest : IRequest<GetHotelRoomResponse>
    {
        public long RoomId { get; set; }
    }

    public class GetHotelRoomResponse:HotelRoomQuery
    {
    }

    public class GetHotelRoomRequestHandler : IRequestHandler<GetHotelRoomRequest,GetHotelRoomResponse>
    {
        private readonly IHotelService _repo;
        private readonly IUnitOfWork _uow;

        public GetHotelRoomRequestHandler(IHotelService repo, IUnitOfWork uow)
        {
            _repo = repo;
            _uow = uow;
        }

        public async Task<GetHotelRoomResponse> Handle(GetHotelRoomRequest request, CancellationToken cancellationToken)
        {
            var result = await _repo.GetHotelRoom(request.RoomId);
            if (result == null)
                return null;


            GetHotelRoomResponse response = new GetHotelRoomResponse();
            result.ToResponse(response);
            return await Task.FromResult(response);
            
        }
    }
}