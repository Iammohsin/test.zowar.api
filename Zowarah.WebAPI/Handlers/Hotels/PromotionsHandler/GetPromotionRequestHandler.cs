﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Application.Interfaces.Hotels;
using Microsoft.EntityFrameworkCore;
using Zowarah.Infrastructure.Config;

namespace Zowarah.WebAPI.Handlers.Hotels
{

    public class GetPromotionRequest : IRequest<GetPromotionResponse>
    {
        public long Id { get; set; }
    }

    public class GetPromotionResponse
    {
        public long Id { get; set; }
        public long ServiceId { get; set; }
        public PromotionType PromotionType { get; set; }
        public int BookingDaysCondition { get; set; }
        public int FreeNights { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public Boolean IsActive { get; set; }
        public AudienceEnum AudienceId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int MinimumStay { get; set; }
        public float Discount { get; set; }
        public ICollection<Room> Rooms { get; set; }
    }
    public class GetPromotionRequestHandler : IRequestHandler<GetPromotionRequest, GetPromotionResponse>
    {
        private readonly IPromotionService repo;
        private readonly IUnitOfWork _uow;

        public GetPromotionRequestHandler(IPromotionService  _repo, IUnitOfWork uow)
        {
            repo = _repo;
            _uow = uow;
        }

        public async Task<GetPromotionResponse> Handle(GetPromotionRequest request, CancellationToken cancellationToken)
        {
            var promotion = await repo.GetById(request.Id, x => x.HotelPromotionRooms);
            if (promotion == null)
                throw new CustomException("Invalid Promotion !!");

            var response = new GetPromotionResponse();
            Mapper.Map(promotion, response);
            return await Task.FromResult(response);
        }
    }
}
