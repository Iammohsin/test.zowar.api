﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Application.Interfaces.Hotels;
using Microsoft.EntityFrameworkCore;
using Zowarah.Infrastructure.Config;

namespace Zowarah.WebAPI.Handlers.Hotels {

    public class DeletePromotionRequest : IRequest<DeletePromotionResponse> {
        public long Id { get; set; }
        public long UserId { get; set; }
    }

    public class DeletePromotionResponse {

    }
    public class DeletePromotionRequestHandler : IRequestHandler<DeletePromotionRequest, DeletePromotionResponse> {
        private readonly IPromotionService repo;
        private readonly IHotelService _repoHotel;
        private readonly IUnitOfWork _uow;

        public DeletePromotionRequestHandler(IPromotionService _repo, IHotelService repoHotel, IUnitOfWork uow) {
            repo = _repo;
            _repoHotel = repoHotel;
            _uow = uow;
        }

        public async Task<DeletePromotionResponse> Handle(DeletePromotionRequest request, CancellationToken cancellationToken) {
            var promotion = await repo.GetById(request.Id, x => x.HotelPromotionRooms);
            if (promotion != null) {
                var hotel = await _repoHotel.GetById(promotion.ServiceId);
                if (hotel != null && hotel.ProviderId == request.UserId) {
                    repo.Delete(promotion);
                    await _uow.SaveChangesAsync();
                    return new DeletePromotionResponse() { };
                }
            }
            throw new CustomException("Invalid Promotion !!");
        }
    }
}
