﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Application.Interfaces.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels.Promotions
{

    public class SetPromotionStatusRequest : IRequest<Boolean>
    {
        public long Id { get; set; }
        public Boolean Status { get; set; }
        public long UserId { get; set; }
    }

 
    public class SetPromotionStatusRequestHandler : IRequestHandler<SetPromotionStatusRequest, Boolean>
    {
        private readonly IPromotionService repo;
        private readonly IUnitOfWork uow;

        public SetPromotionStatusRequestHandler(IPromotionService _repo, IUnitOfWork _uow)
        {
            repo = _repo;
            uow = _uow;
        }

        public async Task<Boolean> Handle(SetPromotionStatusRequest request, CancellationToken cancellationToken)
        {
            var promotion = await repo.GetById(request.Id);
            if (promotion == null)
                return false;
            promotion.IsActive = request.Status;
            repo.Update(promotion);
            await uow.SaveChangesAsync();
            return await Task.FromResult(true);
       }
    }
}
