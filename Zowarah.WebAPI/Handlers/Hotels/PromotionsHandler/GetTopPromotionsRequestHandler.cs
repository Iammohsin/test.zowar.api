﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Application.Interfaces.Hotels;
using Microsoft.EntityFrameworkCore;
using Zowarah.Domain.Queries.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{

    public class GetTopPromotionsRequest : IRequest<GetTopPromotionsResponse>
    {
        public int NoOfPromotions { get; set; }
        
        
    }
    
    public class GetTopPromotionsResponse
    {
        public ICollection<TopPromotion> Promotions { get; set; }
    }
    public class GetTopPromotionsRequestHandler : IRequestHandler<GetTopPromotionsRequest, GetTopPromotionsResponse>
    {
        private readonly IPromotionService repo;
        private readonly IUnitOfWork _uow;

        public GetTopPromotionsRequestHandler(IPromotionService  _repo, IUnitOfWork uow)
        {
            repo = _repo;
            _uow = uow;
        }

        public async Task<GetTopPromotionsResponse> Handle(GetTopPromotionsRequest request, CancellationToken cancellationToken)
        {
            var promotions = await repo.GetTopPromotions(request.NoOfPromotions);

            if (promotions == null)
                return null;

            GetTopPromotionsResponse response = new GetTopPromotionsResponse();
            response.Promotions = promotions;
            return await Task.FromResult(response);

        }
    }
}
