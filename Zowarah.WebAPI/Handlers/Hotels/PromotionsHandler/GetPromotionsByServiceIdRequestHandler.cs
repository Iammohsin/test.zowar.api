﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Application.Interfaces.Hotels;
using Microsoft.EntityFrameworkCore;
using static Zowarah.WebAPI.Handlers.Hotels.GetPromotionsByServiceIdResponse;

namespace Zowarah.WebAPI.Handlers.Hotels
{

    public class GetPromotionsByServiceIdRequest : IRequest<GetPromotionsByServiceIdResponse>
    {
        public long ServiceId { get; set; }
    }

    public class GetPromotionsByServiceIdResponse
    {
        public ICollection<GetPromotionResponse> promotions { get; set; }
    
    }
    public class GetPromotionsByServiceIdRequestHandler : IRequestHandler<GetPromotionsByServiceIdRequest, GetPromotionsByServiceIdResponse>
    {
        private readonly IPromotionService repo;
        private readonly IUnitOfWork _uow;

        public GetPromotionsByServiceIdRequestHandler(IPromotionService  _repo, IUnitOfWork uow)
        {
            repo = _repo;
            _uow = uow;
        }

        public async Task<GetPromotionsByServiceIdResponse> Handle(GetPromotionsByServiceIdRequest request, CancellationToken cancellationToken)
        {
            var promotions = await repo.FindBy(x => x.ServiceId == request.ServiceId,x => x.HotelPromotionRooms).ToListAsync();
            GetPromotionsByServiceIdResponse response = new GetPromotionsByServiceIdResponse();
            response.promotions = Mapper.Map < IList <HotelPromotion>, IList<GetPromotionResponse>>(promotions);

            if (promotions == null)
                return null;

            //GetPromotionsByServiceIdResponse response = new GetPromotionsByServiceIdResponse();
            return await Task.FromResult(response);

        }
    }
}
