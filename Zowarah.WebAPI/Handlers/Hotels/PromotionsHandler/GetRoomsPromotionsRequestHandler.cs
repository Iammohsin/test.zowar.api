﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Application.Interfaces.Hotels;
using Microsoft.EntityFrameworkCore;
using Zowarah.Domain.Queries.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{

    public class GetRoomsPromotionsRequest : IRequest<GetRoomsPromotionsResponse>
    {
        public long ServiceId { get; set; }
        public Boolean IsMember { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        
    }
    
    public class GetRoomsPromotionsResponse
    {
        public ICollection<Promotion> Promotions { get; set; }
    }
    public class GetRoomPromoByServiceIdRequestHandler : IRequestHandler<GetRoomsPromotionsRequest, GetRoomsPromotionsResponse>
    {
        private readonly IPromotionService repo;
        private readonly IUnitOfWork _uow;

        public GetRoomPromoByServiceIdRequestHandler(IPromotionService  _repo, IUnitOfWork uow)
        {
            repo = _repo;
            _uow = uow;
        }

        public async Task<GetRoomsPromotionsResponse> Handle(GetRoomsPromotionsRequest request, CancellationToken cancellationToken)
        {
            var promotions = await repo.GetRoomsPromotions(request.ServiceId, request.IsMember, request.CheckInDate, request.CheckOutDate);

            if (promotions == null)
                return null;

            GetRoomsPromotionsResponse response = new GetRoomsPromotionsResponse() { Promotions = promotions };
            return await Task.FromResult(response);

        }
    }
}
