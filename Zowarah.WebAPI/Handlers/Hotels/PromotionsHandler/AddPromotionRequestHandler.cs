﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Application.Interfaces.Hotels;
using Microsoft.EntityFrameworkCore;
using Zowarah.Application.Config;

namespace Zowarah.WebAPI.Handlers.Hotels {
    /// <summary>
    /// BookingDaysCondition  used for two cases
    ///  1. last minute promotion: can guest booked before (n) days from  check-in date
    ///  2. early booked promotion:  guest can booked before (n) days from check-in date
    /// </summary>
    public class AddPromotionRequest : IRequest<AddPromotionResponse> {
        public long ServiceId { get; set; }
        public string Name { get; set; }
        public PromotionType PromotionType { get; set; }
        public DateTime Date { get; set; }
        public AudienceEnum AudienceId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public float Discount { get; set; }
        public int BookingDaysCondition { get; set; }
        public int FreeNights { get; set; }
        public int MinimumStay { get; set; }
        public ICollection<Room> Rooms { get; set; }
        public long UserId { get; set; }
    }

    public class AddPromotionResponse {
        public long Id { get; set; }
    }
    public class AddPromotionRequestHandler : IRequestHandler<AddPromotionRequest, AddPromotionResponse> {
        private readonly IPromotionService repo;
        private readonly IHotelService _repoHotel;
        private readonly IUnitOfWork _uow;

        public AddPromotionRequestHandler(IPromotionService _repo, IHotelService repoHotel, IUnitOfWork uow) {
            repo = _repo;
            _repoHotel = repoHotel;
            _uow = uow;
        }

        public async Task<AddPromotionResponse> Handle(AddPromotionRequest request, CancellationToken cancellationToken) {
            var hotel = await _repoHotel.GetById(request.ServiceId);
            if (hotel != null && hotel.ProviderId == request.UserId 
                && repo.IsValidPromotionTypePraramer(request.PromotionType, request.BookingDaysCondition, request.FreeNights, request.MinimumStay)) {
                var promotion = new HotelPromotion();
                promotion = promotion.FromRequest(request);
                promotion.IsActive = true;
                repo.Create(promotion);
                await _uow.SaveChangesAsync();
                return new AddPromotionResponse() { Id = promotion.Id };
            }
            throw new CustomException("Invalid Promotion !!");
        }
    }

}
