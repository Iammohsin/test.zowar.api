﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Application.Interfaces.Hotels;
using Microsoft.EntityFrameworkCore;
using Zowarah.Domain.Queries.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{

    public class GetRoomPromotionRequest : IRequest<GetRoomPromotionResponse>
    {
        public long RoomId { get; set; }
        public Boolean IsMember { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
    }

    public class GetRoomPromotionResponse 
    {
        public Promotion Promotion { get; set; }
    }
    public class GetRoomPromotionRequestHandler : IRequestHandler<GetRoomPromotionRequest, GetRoomPromotionResponse>
    {
        private readonly IPromotionService repo;
        private readonly IUnitOfWork _uow;

        public GetRoomPromotionRequestHandler(IPromotionService  _repo, IUnitOfWork uow)
        {
            repo = _repo;
            _uow = uow;
        }

        public async Task<GetRoomPromotionResponse> Handle(GetRoomPromotionRequest request, CancellationToken cancellationToken)
        {
            var promotion = await repo.GetRoomPromotion(request.RoomId,request.IsMember,request.CheckInDate,request.CheckOutDate);

            if (promotion == null)
                return null;

            GetRoomPromotionResponse response = new GetRoomPromotionResponse();
            response.Promotion = promotion;
            return await Task.FromResult(response);

        }
    }
}
