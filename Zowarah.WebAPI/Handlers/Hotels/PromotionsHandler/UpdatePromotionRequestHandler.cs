﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Application.Interfaces.Hotels;
using Microsoft.EntityFrameworkCore;
using Zowarah.Infrastructure.Config;

namespace Zowarah.WebAPI.Handlers.Hotels {

    public class UpdatePromotionRequest : IRequest<UpdatePromotionResponse> {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public AudienceEnum AudienceId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public PromotionType PromotionType { get; set; }
        public int BookingDaysCondition { get; set; }
        public int FreeNights { get; set; }
        public int MinimumStay { get; set; }
        public float Discount { get; set; }
        public ICollection<Room> Rooms { get; set; }
        public long UserId { get; set; }
    }

    public class UpdatePromotionResponse {
        public long Id { get; set; }
    }
    public class UpdatePromotionRequestHandler : IRequestHandler<UpdatePromotionRequest, UpdatePromotionResponse> {
        private readonly IPromotionService repo;
        private readonly IHotelService _repoHotel;
        private readonly IUnitOfWork _uow;

        public UpdatePromotionRequestHandler(IPromotionService _repo, IHotelService repoHotel, IUnitOfWork uow) {
            repo = _repo;
            _repoHotel = repoHotel;
            _uow = uow;
        }

        public async Task<UpdatePromotionResponse> Handle(UpdatePromotionRequest request, CancellationToken cancellationToken) {
            if (!repo.IsValidPromotionTypePraramer(request.PromotionType, request.BookingDaysCondition, request.FreeNights, request.MinimumStay)) {
                throw new CustomException("Invalid Promotion !!");
            }
            var promotion = await repo.GetById(request.Id, x => x.HotelPromotionRooms);
            if (promotion != null) {
                var hotel = await _repoHotel.GetById(promotion.ServiceId);
                if (hotel != null && hotel.ProviderId == request.UserId) {
                    Mapper.Map(request, promotion);
                    repo.Update(promotion);
                    await _uow.SaveChangesAsync();
                    return new UpdatePromotionResponse() { Id = promotion.Id };
                }
            }
            throw new CustomException("Invalid Promotion !!");
        }
    }

}
