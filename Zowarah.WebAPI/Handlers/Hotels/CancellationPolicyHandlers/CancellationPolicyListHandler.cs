﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.WebAPI.Handlers.Hotels;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    public class GetCancellationPolicyListRequest : IRequest<GetCancellationPolicyListResponse>
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetCancellationPolicyResponse {
        public int Id { get; set; }
        public string Description { get; set; }
        public int NumberOfDays { get; set; }
        public int NumberOfHours { get; set; }
    }
    public class GetCancellationPolicyListResponse
    {
        public GetCancellationPolicyListResponse()
        {
            CancellationPolicyList = new List<GetCancellationPolicyResponse>();
        }

        public IList<GetCancellationPolicyResponse> CancellationPolicyList;
    }
    public class GetCancellationPolicyListRequestHandler : IRequestHandler<GetCancellationPolicyListRequest, GetCancellationPolicyListResponse>
    {
        private readonly IRepository<CancellationPolicy> _CancellationPolicyRepo;
        private readonly IUnitOfWork _uow;

        public GetCancellationPolicyListRequestHandler(IRepository<CancellationPolicy> CancellationPolicyRepo, IUnitOfWork uow)
        {
            _CancellationPolicyRepo = CancellationPolicyRepo;
            _uow = uow;
        }

        public async Task<GetCancellationPolicyListResponse> Handle(GetCancellationPolicyListRequest request, CancellationToken cancellationToken)
        {
            var lst = await _CancellationPolicyRepo.GetAll();
            if (lst == null)
                return null;

            return new GetCancellationPolicyListResponse {
                CancellationPolicyList = Mapper.Map<List<GetCancellationPolicyResponse>>(lst)
            };
        }
    }

}
