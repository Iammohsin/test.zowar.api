﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using System.ComponentModel.DataAnnotations;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    public class GetPropertySearchPromotionRequest : IRequest<GetPropertySearchPromotionResponse>
    {
        public long Id { get; set; }
    }

    public class GetPropertySearchPromotionResponse
    {
        public string PromotionId { get; set; }
        public decimal Percentage { get; set; }
    }

    public class GetPropertySearchPromotionRequestHandler : IRequestHandler<GetPropertySearchPromotionRequest,GetPropertySearchPromotionResponse>
    {
        private readonly IHotelService _Repo;
        private readonly IUnitOfWork _uow;
      
        public GetPropertySearchPromotionRequestHandler(IHotelService Repo, IUnitOfWork uow)
        {
            _Repo = Repo;
            _uow = uow;
        }
        public async Task<GetPropertySearchPromotionResponse> Handle(GetPropertySearchPromotionRequest request, CancellationToken cancellationToken)
        {
            var hotel = await _Repo.FindBy(x=> x.Id == request.Id, x=> x.Facilities ).FirstOrDefaultAsync();
            if (hotel == null)
                return null;

             GetPropertySearchPromotionResponse response = new GetPropertySearchPromotionResponse();
             hotel.ToResponse(response);
             return await Task.FromResult(response);
        }
    }

}
