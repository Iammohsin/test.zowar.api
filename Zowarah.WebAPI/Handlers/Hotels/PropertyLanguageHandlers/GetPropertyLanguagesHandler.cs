﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels.PropertyLanguagesHandlers
{
  
    public class GetPropertyLanguagesRequest : IRequest<GetPropertyLanguagesResponse>
    {
        public long Id { get; set; }
    }

    public class GetPropertyLanguagesResponse
    {
       public ICollection<string> Languages { get; set; } = new List<string>();
    }
    public class GetPropertyLanguagesHandler : IRequestHandler<GetPropertyLanguagesRequest, GetPropertyLanguagesResponse>
    {
        private readonly IHotelService repo;
        public GetPropertyLanguagesHandler(IHotelService _Repo)
        {
            repo = _Repo;
        }
        public async Task<GetPropertyLanguagesResponse> Handle(GetPropertyLanguagesRequest request, CancellationToken cancellationToken)
        {
            var languages = await repo.GetSpokenLanguages(request.Id);
            var res = new GetPropertyLanguagesResponse();
            res.Languages = languages.Select(s => s.LanguageId).ToList();
            return res;

        }
    }
}
