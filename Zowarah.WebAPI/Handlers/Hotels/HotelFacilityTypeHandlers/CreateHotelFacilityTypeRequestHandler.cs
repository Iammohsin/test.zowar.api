﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using MediatR;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Application.Interfaces.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    public class CreateHotelFacilityTypeRequest : IRequest<CreateHotelFacilityTypeResponse>
    {
        public string Description { get; set; }
    }
    public class CreateHotelFacilityTypeResponse
    {
            public int Id { get; set; }
    }
    public class CreateHotelFacilityTypeRequestHandler : IRequestHandler<CreateHotelFacilityTypeRequest, CreateHotelFacilityTypeResponse>
    {
        private readonly IHotelService _Repo;
        private readonly IUnitOfWork _uow;

        public CreateHotelFacilityTypeRequestHandler(IHotelService Repo, IUnitOfWork uow)
        {
            _Repo = Repo;
            _uow = uow;
        }
        public async Task<CreateHotelFacilityTypeResponse> Handle(CreateHotelFacilityTypeRequest request, CancellationToken cancellationToken)
        {
            var Hotel =   Mapper.Map<CreateHotelFacilityTypeRequest, Hotel>(request);

            _Repo.Create(Hotel);

            try
            {
                await _uow.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw;
            }

            return new CreateHotelFacilityTypeResponse { Id = 2 };
        }
    }
}
