﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    public class GetRoomTypeNameListRequest : IRequest<GetRoomTypeNameListResponse>
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetRoomTypeNameResponse {
        public int Id { get; set; }
        public int RoomTypeId { get; set; }
        public string Description { get; set; }
    }
    public class GetRoomTypeNameListResponse
    {
        public GetRoomTypeNameListResponse()
        {
            RoomTypeNameList = new List<GetRoomTypeNameResponse>();
        }

        public List<GetRoomTypeNameResponse> RoomTypeNameList;
    }
    public class GetRoomTypeNameListRequestHandler : IRequestHandler<GetRoomTypeNameListRequest, GetRoomTypeNameListResponse>
    {
        private readonly IRepository<RoomTypeName> _RoomTypeNameRepo;
        private readonly IUnitOfWork _uow;

        public GetRoomTypeNameListRequestHandler(IRepository<RoomTypeName> RoomTypeNameRepo, IUnitOfWork uow)
        {
            _RoomTypeNameRepo = RoomTypeNameRepo;
            _uow = uow;
        }

        public async Task<GetRoomTypeNameListResponse> Handle(GetRoomTypeNameListRequest request, CancellationToken cancellationToken)
        {
            var lst = await _RoomTypeNameRepo.GetAll();
            if (lst == null)
                return null;

            return new GetRoomTypeNameListResponse {
               RoomTypeNameList= Mapper.Map<List<GetRoomTypeNameResponse>>(lst)
            };
        }
    }

}
