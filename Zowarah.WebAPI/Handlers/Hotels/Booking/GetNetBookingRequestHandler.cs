﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;

using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Zowarah.Domain.Interfaces;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Application.Services.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    /*public class GetNetBookingRequestValidator : AbstractValidator<GetNetBookingRequest>
    {
        public GetNetBookingRequestValidator()
        {
            //RuleFor(PropertyBasicInfo => PropertyBasicInfo.Name).NotEmpty();
        }
    }*/

    public class GetNetBookingRequest : IRequest<GetNetBookingResponse>
    {
        public long RoomId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

    public class NetBookingDetails
        {
        public long RoomId { get; set; }
        public DateTime Date { get; set; }
        public int? Rooms { get; set; }

    }

    public class GetNetBookingResponse
    {

        public ICollection<NetBookingDetails> lst;
    }
    public class GetNetBookingRequestHandler : IRequestHandler<GetNetBookingRequest, GetNetBookingResponse>
    {
        private readonly IBookingService _repo;
        private readonly IUnitOfWork _uow;
        
        public GetNetBookingRequestHandler(IBookingService repo, IUnitOfWork uow)
        {
            _repo = repo;
            _uow = uow;
        }
        public async Task<GetNetBookingResponse> Handle(GetNetBookingRequest request, CancellationToken cancellationToken)
        {
           // _Repo.get
            //var validator = new GetNetBookingRequestValidator();
            //var validation= await validator.ValidateAsync(request);

            //if (!validation.IsValid)
              //  return null;
            try
            {
                var result =  await _repo.GetNetBooking(request.RoomId,request.FromDate,request.ToDate).Result.ToListAsync();
                if (result == null)
                    return null;

                var response = new GetNetBookingResponse() { };
                response.lst  = Mapper.Map<IList<NetBooking>, IList<NetBookingDetails>>(result);
                return response;

                //await _repo.GetCalendar(request.HotelId);s
                //var result= await _uow.SaveChangesAsync();
                // Publish to Transaction 
                /*
                if (result>=1)
                {
                    var creatdHotelEventMessage = new HotelCreatedEventMessage
                    {
                        Name = request.Name,
                        ProviderId = request.ProviderId
                    };
                   // _publisher.Publish(creatdHotelEventMessage);
                }
                */
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
