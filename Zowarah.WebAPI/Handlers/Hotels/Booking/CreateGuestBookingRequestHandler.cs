﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using System.Linq;
using System.Linq.Expressions;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.WebAPI.Extensions;
using Zowarah.WebAPI.Handlers.Common.NotificationsHandlers;
using Zowarah.WebAPI.Config.Notification;
using Zowarah.WebAPI.Helpers.Users;
using Zowarah.WebAPI.Handlers.Transactions.TransactionHandlers;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Transactions;
//using Microsoft.Reporting.WebForms;
using Zowarah.Data.Contexts;
using System.Collections;
using System.Data;
using System.Reflection;
using System.Text;
using SelectPdf;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    /*public class CreateGuestBookingRequestValidator : AbstractValidator<CreateGuestBookingRequest>
    {
        public CreateGuestBookingRequestValidator()
        {
            //RuleFor(PropertyBasicInfo => PropertyBasicInfo.Name).NotEmpty();
        }
    }*/

    public class CreateGuestBookingRequest : IRequest<CreateGuestBookingResponse>
    {
        public long HotelId { get; set; }
        public long ServiceId { get; set; }

        public long RoomId { get; set; }
        public int Rooms { get; set; }
        public DateTime Date { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public int Status { get; set; }
        public decimal ProviderAmount { get; set; }
        public decimal ClientAmount { get; set; }
        public decimal ExchangeRate { get; set; }
        public string ProviderCurrency { get; set; }
        public string ClientCurrency { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Boolean IsPersonal { get; set; }
        public string RefereeName { get; set; }
        public string RefereeEmail { get; set; }
        public string CountryId { get; set; }
        public string Mobile { get; set; }
        public string CreditCardId { get; set; }
        public string CardNo { get; set; }
        public byte CardExpiryMonth { get; set; }
        public int CardExpiryYear { get; set; }
        public string SpecialRequest { get; set; }
        public DateTime ArrivalDate { get; set; }
        public string TransactionId { get; set; }
        public string PromotionId { get; set; }
        public decimal BaseRate { get; set; }
        public decimal Discount { get; set; }
        public int FreeNights { get; set; }
        public decimal RatePerNight { get; set; }
        public Boolean isPaymentHotel { get; set; }
    }

    
    public class CreateGuestBookingResponse
    {
        public string Id { get; set; }
    }
    public class CreateGuestBookingRequestHandler : IRequestHandler<CreateGuestBookingRequest, CreateGuestBookingResponse>
    {
        private IRepository<Service> serviceRepo { get; set; }
        private readonly IBookingService _repo;
        private readonly IUnitOfWork _uow;
        private readonly IMediator _mediator;
        private readonly ApplicationDbContext _dbContext;
        private readonly IHostingEnvironment _hostingEnvironment;
        public CreateGuestBookingRequestHandler(IBookingService repo, IUnitOfWork uow, IRepository<Service> _serviceRepo, IMediator mediator, ApplicationDbContext dbContext, IHostingEnvironment hostingEnvironment)
        {
            _repo = repo;
            _uow = uow;
            _mediator = mediator;
            serviceRepo = _serviceRepo;
            _dbContext = dbContext;
            _hostingEnvironment = hostingEnvironment;
        }
      
        public async Task<CreateGuestBookingResponse> Handle(CreateGuestBookingRequest request, CancellationToken cancellationToken)
        {
            // _Repo.get
            //var validator = new CreateGuestBookingRequestValidator();
            //var validation= await validator.ValidateAsync(request);

            //if (!validation.IsValid)
            //  return null;
            try
            {
                var booking = new Booking();
                booking.Id = Guid.NewGuid().ToString();
                //booking = Mapper.Map(request, booking);
                request.Date = DateTime.Now.Date;
                booking = booking.FromRequest(request);
                var transRequest = new CreateTransactionRequest();
                transRequest = transRequest.FromRequest(request);
                transRequest.TransactionType = TransactionType.BookingHotel;
                transRequest.OwnerId = booking.Id;
                transRequest.Status = TransactionStatus.Confirmed;
                var trans = await _mediator.Send(transRequest);
                booking.Id = trans.Id;
                booking.TransactionId = trans.Id;
                _repo.Create(booking);
                await _uow.SaveChangesAsync();

                var list = (from b in _dbContext.Booking
                            join r in _dbContext.Rooms on b.RoomId equals r.Id
                            join rt in _dbContext.RoomTypeNames on r.RoomTypeNameId equals rt.Id
                            join h in _dbContext.Hotels on r.HotelId equals h.Id
                            join t in _dbContext.Transactions on b.Id equals t.Id
                            join u in _dbContext.Users on t.ClientId equals u.Id
                            join c in _dbContext.Countries on t.CountryId equals c.Id
                            join ct in _dbContext.Cities on h.CityId equals ct.Id
                            join can in _dbContext.CancellationPolicies on h.CancellationPolicyId equals can.Id
                            where b.Id == trans.Id
                            select new
                            {
                                b.Id,
                                t.FirstName,
                                t.LastName,
                                ResCountry = c.Description,
                                CheckInDate = b.CheckInDate.ToString("dd-MMM-yyyy"),
                                CheckOutDate = b.CheckOutDate.ToString("dd-MMM-yyyy"),
                                rt.Description,
                                r.NoOfPersons,
                                b.Rooms,
                                HotelCity = ct.Description,
                                HotelName = h.Name,
                                CanPolicy = can.Description,
                                startDate = b.CheckInDate,
                                EndDate = b.CheckOutDate,
                                t.ClientCurrency,
                                t.ClientAmount,
                                r.RatePerNight,
                            });
                IEnumerable _list = list.ToList();
                DataTable dt = DataTableFromIEnumerable(_list);

                var benifitList = (from b in _dbContext.Booking
                                   join r in _dbContext.Rooms on b.RoomId equals r.Id
                                   join h in _dbContext.Hotels on r.HotelId equals h.Id
                                   join hf in _dbContext.HotelFacilities on h.Id equals hf.HotelId
                                   join f in _dbContext.Facilities on hf.FacilityId equals f.Id
                                   //groupBy(h.Id)
                                   where b.Id == trans.Id
                                   select new
                                   {
                                       f.Description
                                   }).ToList();

                string benifits = "";
                foreach (var ban in benifitList)
                {
                    benifits += ban.Description + ",";
                }
                benifits = benifits.Substring(0, benifits.Length - 1);

                var PromotionList = (from t in _dbContext.Transactions
                                     join b in _dbContext.Booking on t.Id equals b.Id
                                     join p in _dbContext.HotelPromotions on b.PromotionId equals p.Id
                                     join hp in _dbContext.HotelPromotionRooms on p.Id equals hp.HotelPromotionId
                                     where b.Id == trans.Id
                                     select new
                                     {
                                         p.Name
                                     }).ToList();
                var PromotionName = "";
                foreach (var prom in PromotionList)
                {
                    PromotionName = prom.Name;
                }

                //IEnumerable _List1 = benifitList.ToList();
                //DataTable dt1 = DataTableFromIEnumerable(_List1);

                var attachTemplate = await _mediator.Send(new GetMailTemplateRequest() { Id = ConfirmBooking.AttachTemplateId });
                //var query = "select * from hotels" 
                StringBuilder BodyText = new StringBuilder(attachTemplate.Template.ToString());
                DataRow dr;
                dr = dt.Rows[0];
                string DateTr = "";
                for (DateTime date = Convert.ToDateTime(dr["startDate"]); date <= Convert.ToDateTime(dr["EndDate"]); date = date.AddDays(1))
                {
                    DateTr += "<tr><td><p style='font-size: 14px;margin:0;padding: 0;'>" + date.ToString("dd-MMM-yyyy") + "</p></td><td align = 'right'><p style = 'color:#8e6bad;font-size: 14px;margin:0;padding: 0;'>" + Convert.ToString(dr["ClientCurrency"]) + " " + Convert.ToString(dr["RatePerNight"]) + "</p></td></tr>";
                }
                BodyText = BodyText.Replace("$$TransactionId$$", Convert.ToString(dr["Id"]));
                BodyText = BodyText.Replace("$$HotelName$$", Convert.ToString(dr["HotelName"]));
                BodyText = BodyText.Replace("$$CityName$$", Convert.ToString(dr["HotelCity"]));
                BodyText = BodyText.Replace("$$FirstName$$", Convert.ToString(dr["FirstName"]));
                BodyText = BodyText.Replace("$$LastName$$", Convert.ToString(dr["LastName"]));
                BodyText = BodyText.Replace("$$ChekinDate$$", Convert.ToString(dr["CheckInDate"]));
                BodyText = BodyText.Replace("$$ChekoutDate$$", Convert.ToString(dr["CheckOutDate"]));
                BodyText = BodyText.Replace("$$Rooms$$", Convert.ToString(dr["Rooms"]));
                BodyText = BodyText.Replace("$$RoomType$$", Convert.ToString(dr["Description"]));
                BodyText = BodyText.Replace("$$RoomAdult$$", Convert.ToString(dr["NoOfPersons"]));
                BodyText = BodyText.Replace("$$CustResidence$$", Convert.ToString(dr["ResCountry"]));
                BodyText = BodyText.Replace("$$Benifits$$", benifits);
                BodyText = BodyText.Replace("$$CanPolicy$$", Convert.ToString(dr["CanPolicy"]));
                BodyText = BodyText.Replace("$$RoomDescription$$", DateTr);
                BodyText = BodyText.Replace("$$Promotion$$", (PromotionName == "" ? "-" : PromotionName));
                BodyText = BodyText.Replace("$$TotalAmount$$", Convert.ToString(dr["ClientCurrency"]) + " " + Convert.ToString(dr["ClientAmount"]));


                // instantiate the html to pdf converter
                HtmlToPdf converter = new HtmlToPdf();
                string htmlContent = BodyText.ToString();

                string pdf_page_size = "A4";
                PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
                    pdf_page_size, true);

                string pdf_orientation = "Portrait";
                PdfPageOrientation pdfOrientation =
                    (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                    pdf_orientation, true);

                converter.Options.PdfPageSize = pageSize;
                converter.Options.PdfPageOrientation = pdfOrientation;
                converter.Options.WebPageWidth = 1024;
                converter.Options.WebPageHeight = 0;

                // convert the url to pdf
                PdfDocument doc = converter.ConvertHtmlString(htmlContent);

                string fileName = Guid.NewGuid().ToString();

                // save pdf document
                doc.Save($"{this._hostingEnvironment.WebRootPath }/Reports/" + fileName + ".Pdf");
                // close pdf document
                doc.Close();


                await sendMailNotification(booking, $"{this._hostingEnvironment.WebRootPath }/Reports/" + fileName + ".Pdf");

                //SendSmsNotification(booking);
                var response = new CreateGuestBookingResponse() { Id = booking.Id };
                //var response = new CreateGuestBookingResponse() { Id = "1" };
                return response;

                // Publish To Transaction Service 
                // response.lst  = Mapper.Map<IList<CalendarManual>, IList<CalendarManualDetails>>(calendar);
                //await _repo.GetCalendar(request.HotelId);
                //var result= await _uow.SaveChangesAsync();
                // Publish to Transaction 
                /*
                if (result>=1)
                {
                    var creatdHotelEventMessage = new HotelCreatedEventMessage
                    {
                        Name = request.Name,
                        ProviderId = request.ProviderId
                    };
                   // _publisher.Publish(creatdHotelEventMessage);
                }
                */
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                return null;
                
            }
        }
        async Task sendMailNotification(Booking booking, string AttachmentPath)
        {
            try
            {
                var service = await serviceRepo.GetById(booking.Transaction.ServiceId);
                var template = await _mediator.Send(new GetMailTemplateRequest() { Id = ConfirmBooking.TemplateId });

                var title = !string.IsNullOrEmpty(booking.Transaction.Title) ? Enum.GetName(typeof(Titles), int.Parse(booking.Transaction.Title)) : "";
                var specialRequest = !string.IsNullOrEmpty(booking.SpecialRequest) ? booking.SpecialRequest : "";
                var htmlContent = template.Template
                    .Replace("$$Title$$", title)
                    .Replace("$$HotelName$$", service.Name)
                    .Replace("$$FirstName$$", booking.Transaction.FirstName)
                    .Replace("$$LastName$$", booking.Transaction.LastName)
                    .Replace("$$Email$$", booking.Transaction.Email)
                    .Replace("$$CheckinDate$$", booking.CheckInDate.ToString("ddd - MMM dd yyyy"))
                    .Replace("$$CheckoutDate$$", booking.CheckOutDate.ToString("ddd - MMM dd yyyy"))
                    .Replace("$$NoOfRooms$$", booking.Rooms.ToString())
                    .Replace("$$SpecialRequest$$", specialRequest)
                    .Replace("$$Arrival Time$$", booking.ArrivalDate.ToString("ddd - MMM dd yyyy hh:mm:ss"));


                _mediator.Send(new SendMailNotificationRequest()
                {
                    To = booking.Transaction.Email,
                    DisplayName = booking.Transaction.FirstName,
                    Subject = ConfirmBooking.Subject + service.Name,
                    Content = htmlContent,
                    AttachmentPath = AttachmentPath,
                });


            }
            catch { }
        }
        async Task SendSmsNotification(Booking booking)
        {
            await _mediator.Send(new SendSmsNotificationRequest()
            {
                Mobile = booking.Transaction.Mobile,
                Content = ConfirmBooking.SMSMessage
            });
        }
        public static DataTable DataTableFromIEnumerable(IEnumerable ien)
        {
            DataTable dt = new DataTable();

            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (PropertyInfo pi in pis)
                    {
                        Type pt = pi.PropertyType;
                        if (pt.IsGenericType && pt.GetGenericTypeDefinition() == typeof(Nullable<>))
                            pt = Nullable.GetUnderlyingType(pt);

                        dt.Columns.Add(pi.Name, pt);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (PropertyInfo pi in pis)
                {
                    object value = (pi.GetValue(obj, null) != null) ? pi.GetValue(obj, null) : DBNull.Value;
                    dr[pi.Name] = value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public bool ByteArrayToFile(string fileName, byte[] byteArray)
        {
            try
            {
                using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(byteArray, 0, byteArray.Length);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught in process: {0}", ex);
                return false;
            }
        }
    }
}
