﻿
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using Zowarah.Application.Services.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class AvailableRoomsRequest : IRequest<AvailableRoomsResponse> {
        public long HotelId { get; set; }
        public int Persons { get; set; }
        public int Children { get; set; }
        public int noOfRooms { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }



    public class AvailableRoomsResponse {
        public ICollection<AvailableRoom> Rooms { get; set; } = new List<AvailableRoom>();
    }
    public class AvailableRoomsRequestHandler : IRequestHandler<AvailableRoomsRequest, AvailableRoomsResponse> {
        private readonly IHotelService _hotelsRepo;

        public AvailableRoomsRequestHandler(IHotelService hotelService) {
            _hotelsRepo = hotelService;
        }
        public async Task<AvailableRoomsResponse> Handle(AvailableRoomsRequest request, CancellationToken cancellationToken) {
            return new AvailableRoomsResponse() {
                Rooms = await _hotelsRepo.AvailableRooms(request.HotelId, request.FromDate, request.ToDate, request.noOfRooms,request.Persons, request.Children)
            };
        }
    }
}
