﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Application.Services.Hotels;
using Zowarah.Domain.Entities.Transactions;

namespace Zowarah.WebAPI.Handlers.Hotels {

    public class SearchBookingListRequest : IRequest<SearchBookingListResponse> {
        public long ServiceId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public TransactionStatus Status { get; set; }
        public long UserId { get; set; }

    }

    public class SearchBookingListResponse {
        public SearchBookingListResponse() {
            BookingList = new List<HotelBooking>();
        }
        public ICollection<HotelBooking> BookingList { get; set; }
    }


    public class SearchBookingListRequestHandler : IRequestHandler<SearchBookingListRequest, SearchBookingListResponse> {
        public readonly IHotelService hotelService;
        public SearchBookingListRequestHandler(IHotelService _hotelService) {
            hotelService = _hotelService;
        }
        public async Task<SearchBookingListResponse> Handle(SearchBookingListRequest request, CancellationToken cancellationToken) {
            return new SearchBookingListResponse {
                BookingList = await hotelService.SearchBooking(request.ServiceId, request.UserId, request.FromDate, request.ToDate, request.Status)
            };
        }
    }
}
