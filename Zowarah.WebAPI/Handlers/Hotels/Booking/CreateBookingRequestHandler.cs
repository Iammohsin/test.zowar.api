﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using System.Linq;
using System.Linq.Expressions;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.WebAPI.Extensions;
using Zowarah.WebAPI.Handlers.Common.NotificationsHandlers;
using Zowarah.WebAPI.Config.Notification;
using Zowarah.WebAPI.Helpers.Users;
using Zowarah.WebAPI.Handlers.Transactions.TransactionHandlers;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Transactions;
//using Microsoft.Reporting.WebForms;
using Zowarah.Data.Contexts;
using System.Collections;
using System.Data;
using System.Reflection;
using System.Text;
using SelectPdf;
using System.IO;
using Microsoft.AspNetCore.Hosting;
// shoaib
using System.Net;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
// shoaib

namespace Zowarah.WebAPI.Handlers.Hotels
{
    /*public class CreateBookingRequestValidator : AbstractValidator<CreateBookingRequest>
    {
        public CreateBookingRequestValidator()
        {
            //RuleFor(PropertyBasicInfo => PropertyBasicInfo.Name).NotEmpty();
        }
    }*/

    public class CreateBookingRequest : IRequest<CreateBookingResponse>
    {
        public long HotelId { get; set; }
        public long ServiceId { get; set; }
        public long ClientId { get; set; }
        public long RoomId { get; set; }
        public int Rooms { get; set; }
        public DateTime Date { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public int Status { get; set; }
        public decimal ProviderAmount { get; set; }
        public decimal ClientAmount { get; set; }
        public decimal ExchangeRate { get; set; }
        public string ProviderCurrency { get; set; }
        public string ClientCurrency { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Boolean IsPersonal { get; set; }
        public string RefereeName { get; set; }
        public string RefereeEmail { get; set; }
        public string CountryId { get; set; }
        public string Mobile { get; set; }
        public string CreditCardId { get; set; }
        public string CardNo { get; set; }
        public byte CardExpiryMonth { get; set; }
        public int CardExpiryYear { get; set; }
        public int Cvv { get; set; }  //mohsin27-2-2020

        public string SpecialRequest { get; set; }
        public DateTime ArrivalDate { get; set; }
        public string TransactionId { get; set; }
        public string PromotionId { get; set; }
        public decimal BaseRate { get; set; }
        public decimal Discount { get; set; }
        public int FreeNights { get; set; }
        public decimal RatePerNight { get; set; }
        public Boolean isPaymentHotel { get; set; }
        //shoaib
        public string PaymentMobileNumber { get; set; }
        //shoaib
    }



    public class CreateBookingResponse
    {
        public string Id { get; set; }
    }
    public class CreateBookingRequestHandler : IRequestHandler<CreateBookingRequest, CreateBookingResponse>
    {
        private IRepository<Service> serviceRepo { get; set; }
        private readonly IBookingService _repo;
        private readonly IUnitOfWork _uow;
        private readonly IMediator _mediator;
        private readonly ApplicationDbContext _dbContext;
        private readonly IHostingEnvironment _hostingEnvironment;
        public CreateBookingRequestHandler(IBookingService repo, IUnitOfWork uow, IRepository<Service> _serviceRepo, IMediator mediator, ApplicationDbContext dbContext, IHostingEnvironment hostingEnvironment)
        {
            _repo = repo;
            _uow = uow;
            _mediator = mediator;
            serviceRepo = _serviceRepo;
            _dbContext = dbContext;
            _hostingEnvironment = hostingEnvironment;
        }
        //Old code Mohsin 21-2-2020
        //public async Task<CreateBookingResponse> Handle(CreateBookingRequest request, CancellationToken cancellationToken)
        //{
        //    // _Repo.get
        //    //var validator = new CreateBookingRequestValidator();
        //    //var validation= await validator.ValidateAsync(request);

        //    //if (!validation.IsValid)
        //    //  return null;
        //    try
        //    {
        //        var booking = new Booking();
        //        booking.Id = Guid.NewGuid().ToString();
        //        //booking = Mapper.Map(request, booking);
        //        request.Date = DateTime.Now.Date;
        //        booking = booking.FromRequest(request);
        //        var transRequest = new CreateTransactionRequest();
        //        transRequest = transRequest.FromRequest(request);
        //        transRequest.TransactionType = TransactionType.BookingHotel;
        //        transRequest.OwnerId = booking.Id;
        //        transRequest.Status = TransactionStatus.Confirmed;
        //        var trans = await _mediator.Send(transRequest);
        //        booking.Id = trans.Id;
        //        booking.TransactionId = trans.Id;
        //        _repo.Create(booking);
        //        await _uow.SaveChangesAsync();

        //        var list = (from b in _dbContext.Booking
        //                    join r in _dbContext.Rooms on b.RoomId equals r.Id
        //                    join rt in _dbContext.RoomTypeNames on r.RoomTypeNameId equals rt.Id
        //                    join h in _dbContext.Hotels on r.HotelId equals h.Id
        //                    join t in _dbContext.Transactions on b.Id equals t.Id
        //                    join u in _dbContext.Users on t.ClientId equals u.Id
        //                    join c in _dbContext.Countries on t.CountryId equals c.Id
        //                    join ct in _dbContext.Cities on h.CityId equals ct.Id
        //                    join can in _dbContext.CancellationPolicies on h.CancellationPolicyId equals can.Id
        //                    where b.Id == trans.Id
        //                    select new
        //                    {
        //                        b.Id,
        //                        t.FirstName,
        //                        t.LastName,
        //                        ResCountry = c.Description,
        //                        CheckInDate = b.CheckInDate.ToString("dd-MMM-yyyy"),
        //                        CheckOutDate = b.CheckOutDate.ToString("dd-MMM-yyyy"),
        //                        rt.Description,
        //                        r.NoOfPersons,
        //                        b.Rooms,
        //                        HotelCity = ct.Description,
        //                        HotelName = h.Name,
        //                        CanPolicy = can.Description,
        //                        startDate = b.CheckInDate,
        //                        EndDate = b.CheckOutDate,
        //                        t.ClientCurrency,
        //                        t.ClientAmount,
        //                        r.RatePerNight,
        //                    });
        //        IEnumerable _list = list.ToList();
        //        DataTable dt = DataTableFromIEnumerable(_list);

        //        var benifitList = (from b in _dbContext.Booking
        //                           join r in _dbContext.Rooms on b.RoomId equals r.Id
        //                           join h in _dbContext.Hotels on r.HotelId equals h.Id
        //                           join hf in _dbContext.HotelFacilities on h.Id equals hf.HotelId
        //                           join f in _dbContext.Facilities on hf.FacilityId equals f.Id
        //                           //groupBy(h.Id)
        //                           where b.Id == trans.Id
        //                           select new
        //                           {
        //                               f.Description
        //                           }).ToList();

        //        string benifits = "";
        //        foreach (var ban in benifitList)
        //        {
        //            benifits += ban.Description + ",";
        //        }
        //        benifits = benifits.Substring(0, benifits.Length - 1);

        //        var PromotionList = (from t in _dbContext.Transactions
        //                             join b in _dbContext.Booking on t.Id equals b.Id
        //                             join p in _dbContext.HotelPromotions on b.PromotionId equals p.Id
        //                             join hp in _dbContext.HotelPromotionRooms on p.Id equals hp.HotelPromotionId
        //                             where b.Id == trans.Id
        //                             select new
        //                             {
        //                                 p.Name
        //                             }).ToList();
        //        var PromotionName = "";
        //        foreach (var prom in PromotionList)
        //        {
        //            PromotionName = prom.Name;
        //        }

        //        //IEnumerable _List1 = benifitList.ToList();
        //        //DataTable dt1 = DataTableFromIEnumerable(_List1);

        //        var attachTemplate = await _mediator.Send(new GetMailTemplateRequest() { Id = ConfirmBooking.AttachTemplateId });
        //        //var query = "select * from hotels" 
        //        StringBuilder BodyText = new StringBuilder(attachTemplate.Template.ToString());
        //        DataRow dr;
        //        dr = dt.Rows[0];
        //        string DateTr = "";
        //        for (DateTime date = Convert.ToDateTime(dr["startDate"]); date <= Convert.ToDateTime(dr["EndDate"]); date = date.AddDays(1))
        //        {
        //            DateTr += "<tr><td><p style='font-size: 14px;margin:0;padding: 0;'>" + date.ToString("dd-MMM-yyyy") + "</p></td><td align = 'right'><p style = 'color:#8e6bad;font-size: 14px;margin:0;padding: 0;'>" + Convert.ToString(dr["ClientCurrency"]) + " " + Convert.ToString(dr["ClientAmount"]) + "</p></td></tr>";
        //        }
        //        BodyText = BodyText.Replace("$$TransactionId$$", Convert.ToString(dr["Id"]));
        //        BodyText = BodyText.Replace("$$HotelName$$", Convert.ToString(dr["HotelName"]));
        //        BodyText = BodyText.Replace("$$CityName$$", Convert.ToString(dr["HotelCity"]));
        //        BodyText = BodyText.Replace("$$FirstName$$", Convert.ToString(dr["FirstName"]));
        //        BodyText = BodyText.Replace("$$LastName$$", Convert.ToString(dr["LastName"]));
        //        BodyText = BodyText.Replace("$$ChekinDate$$", Convert.ToString(dr["CheckInDate"]));
        //        BodyText = BodyText.Replace("$$ChekoutDate$$", Convert.ToString(dr["CheckOutDate"]));
        //        BodyText = BodyText.Replace("$$Rooms$$", Convert.ToString(dr["Rooms"]));
        //        BodyText = BodyText.Replace("$$RoomType$$", Convert.ToString(dr["Description"]));
        //        BodyText = BodyText.Replace("$$RoomAdult$$", Convert.ToString(dr["NoOfPersons"]));
        //        BodyText = BodyText.Replace("$$CustResidence$$", Convert.ToString(dr["ResCountry"]));
        //        BodyText = BodyText.Replace("$$Benifits$$", benifits);
        //        BodyText = BodyText.Replace("$$CanPolicy$$", Convert.ToString(dr["CanPolicy"]));
        //        BodyText = BodyText.Replace("$$RoomDescription$$", DateTr);
        //        BodyText = BodyText.Replace("$$Promotion$$", (PromotionName == "" ? "-" : PromotionName));
        //        BodyText = BodyText.Replace("$$TotalAmount$$", Convert.ToString(dr["ClientCurrency"]) + " " + Convert.ToString(dr["ClientAmount"]));


        //        // instantiate the html to pdf converter
        //        HtmlToPdf converter = new HtmlToPdf();
        //        string htmlContent = BodyText.ToString();

        //        string pdf_page_size = "A4";
        //        PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
        //            pdf_page_size, true);

        //        string pdf_orientation = "Portrait";
        //        PdfPageOrientation pdfOrientation =
        //            (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
        //            pdf_orientation, true);

        //        converter.Options.PdfPageSize = pageSize;
        //        converter.Options.PdfPageOrientation = pdfOrientation;
        //        converter.Options.WebPageWidth = 1024;
        //        converter.Options.WebPageHeight = 0;

        //        // convert the url to pdf
        //        PdfDocument doc = converter.ConvertHtmlString(htmlContent);

        //        string fileName = Guid.NewGuid().ToString();

        //        // save pdf document
        //        doc.Save($"{this._hostingEnvironment.WebRootPath }/Reports/" + fileName + ".Pdf");
        //        // close pdf document
        //        doc.Close();


        //        await sendMailNotification(booking, $"{this._hostingEnvironment.WebRootPath }/Reports/" + fileName + ".Pdf");

        //        //SendSmsNotification(booking);
        //        var response = new CreateBookingResponse() { Id = booking.Id };
        //        //var response = new CreateBookingResponse() { Id = "1" };
        //        return response;

        //        // Publish To Transaction Service 
        //        // response.lst  = Mapper.Map<IList<CalendarManual>, IList<CalendarManualDetails>>(calendar);
        //        //await _repo.GetCalendar(request.HotelId);
        //        //var result= await _uow.SaveChangesAsync();
        //        // Publish to Transaction 
        //        /*
        //        if (result>=1)
        //        {
        //            var creatdHotelEventMessage = new HotelCreatedEventMessage
        //            {
        //                Name = request.Name,
        //                ProviderId = request.ProviderId
        //            };
        //           // _publisher.Publish(creatdHotelEventMessage);
        //        }
        //        */
        //    }
        //    catch (Exception ex)
        //    {

        //        return null;
        //    }
        //}


        public async Task<CreateBookingResponse> Handle(CreateBookingRequest request, CancellationToken cancellationToken)
        {
            // _Repo.get
            //var validator = new CreateBookingRequestValidator();
            //var validation= await validator.ValidateAsync(request);

            //if (!validation.IsValid)
            //  return null;
            try
            {
                var resultpay = "";
                var booking = new Booking();
                booking.Id = Guid.NewGuid().ToString();
                //booking = Mapper.Map(request, booking);
                request.Date = DateTime.Now.Date;
                booking = booking.FromRequest(request);
                var transRequest = new CreateTransactionRequest();
                transRequest = transRequest.FromRequest(request);
                if (request.isPaymentHotel == true)
                {
                    transRequest.TransactionType = TransactionType.BookingHotel;
                }
                else { transRequest.TransactionType = TransactionType.BookingOnline; }
                transRequest.OwnerId = booking.Id;
                transRequest.Status = TransactionStatus.Confirmed;
                var trans = await _mediator.Send(transRequest);
                booking.Id = trans.Id;
                booking.TransactionId = trans.Id;
                _repo.Create(booking);
                await _uow.SaveChangesAsync();

                var list = (from b in _dbContext.Booking
                            join r in _dbContext.Rooms on b.RoomId equals r.Id
                            join rt in _dbContext.RoomTypeNames on r.RoomTypeNameId equals rt.Id
                            join h in _dbContext.Hotels on r.HotelId equals h.Id
                            join t in _dbContext.Transactions on b.Id equals t.Id
                            join u in _dbContext.Users on t.ClientId equals u.Id
                            join c in _dbContext.Countries on t.CountryId equals c.Id
                            join ct in _dbContext.Cities on h.CityId equals ct.Id
                            join can in _dbContext.CancellationPolicies on h.CancellationPolicyId equals can.Id
                            where b.Id == trans.Id
                            select new
                            {
                                b.Id,
                                t.FirstName,
                                t.LastName,
                                ResCountry = c.Description,
                                CheckInDate = b.CheckInDate.ToString("dd-MMM-yyyy"),
                                CheckOutDate = b.CheckOutDate.ToString("dd-MMM-yyyy"),
                                rt.Description,
                                r.NoOfPersons,
                                b.Rooms,
                                HotelCity = ct.Description,
                                HotelName = h.Name,
                                CanPolicy = can.Description,
                                startDate = b.CheckInDate,
                                EndDate = b.CheckOutDate,
                                t.ClientCurrency,
                                t.ClientAmount,
                                r.RatePerNight,
                                transId = t.Id,
                                TPayStatus = t.Status,


                            });
                IEnumerable _list = list.ToList();
                DataTable dt = DataTableFromIEnumerable(_list);

                if (request.isPaymentHotel != true)
                {
                    foreach (var bi in list)
                    {
                        var biilNO = bi.Id;
                        var tranId = bi.transId;
                        string url = "https://b2btest.stcpay.com.sa/B2B.MerchantWebApi/Merchant/v3/MobilePaymentAuthorize";
                        WebRequest reqObj = WebRequest.Create(url);
                        reqObj.Method = "POST";
                        reqObj.ContentType = "application/json";
                        reqObj.Headers.Add("x-clientcode", "61247531004");
                        reqObj.Headers.Add("x-password", "Zr@$y157$cc");
                        reqObj.Headers.Add("x-username", "ZoTstWrUSR");
                        //shoaib for server
                        string postdata = "{\"MobilePaymentAuthorizeRequestMessage\":{\"BranchID\": \"BR2\",\"TellerID\": \"Teller2\",\"DeviceID\": \"Device2\",\"RefNum\": \""+ biilNO + "\",\"BillNumber\": \"" + biilNO + "\",\"MobileNo\": \"" + request.PaymentMobileNumber + "\",\"Amount\": " + request.ProviderAmount + ",\"ExpiryPeriodType\": 1,\"ExpiryPeriod\": 10}}";
                        //shoaib for server end
                        //shoaib for test
                       /// string postdata = "{\"MobilePaymentAuthorizeRequestMessage\":{\"BranchID\": \"BR2\",\"TellerID\": \"Teller2\",\"DeviceID\": \"Device2\",\"RefNum\": \"" + biilNO + "\",\"BillNumber\": \"" + biilNO + "\",\"MobileNo\": \"966557146325\",\"Amount\": " + request.ProviderAmount + ",\"ExpiryPeriodType\": 1,\"ExpiryPeriod\": 1}}";
                        //shoaib for test end
                        using (var stremwriter = new StreamWriter(reqObj.GetRequestStream()))
                        {
                            stremwriter.Write(postdata);
                            stremwriter.Flush();
                            stremwriter.Close();

                            var httpresponse = (HttpWebResponse)reqObj.GetResponse();
                            using (var stremreader = new StreamReader(httpresponse.GetResponseStream()))
                            {
                                var result2 = stremreader.ReadToEnd();
                                dynamic data = JObject.Parse(result2);
                                var AuthurNo = Convert.ToString(data["MobilePaymentAuthorizeResponseMessage"]["AuthorizationReference"]);
                                var ExpDate = Convert.ToString(data["MobilePaymentAuthorizeResponseMessage"]["ExpiryDate"]);

                                Stopwatch stopwatch = new Stopwatch();
                                stopwatch.Start();
                                Thread.Sleep(1000 * 60 * 2);        // 5 Minutes (1000 * 60 * 5)
                                for (int i = 0; i <= 5; i++)
                                {
                                    // To check Payment Status Paid or Not 

                                    string urlpay = "https://b2btest.stcpay.com.sa/B2B.MerchantWebApi/Merchant/v3/PaymentInquiry";
                                    WebRequest reqObjpay = WebRequest.Create(urlpay);
                                    reqObjpay.Method = "POST";
                                    reqObjpay.ContentType = "application/json";
                                    reqObjpay.Headers.Add("x-clientcode", "61247531004");
                                    reqObjpay.Headers.Add("x-password", "Zr@$y157$cc");
                                    reqObjpay.Headers.Add("x-username", "ZoTstWrUSR");

                                    string postdatapay = "{\"PaymentInquiryRequestMessage\":{\"RefNum\": \"" + biilNO + "\"}}";
                                    //string postdatapay = "{\"PaymentInquiryRequestMessage\":{\"RefNum\": \"MerRefNo267126\"}}";
                                    using (var stremwriterpay = new StreamWriter(reqObjpay.GetRequestStream()))
                                    {
                                        stremwriterpay.Write(postdatapay);
                                        stremwriterpay.Flush();
                                        stremwriterpay.Close();

                                        var httpresponsepay = (HttpWebResponse)reqObjpay.GetResponse();
                                        using (var stremreaderpay = new StreamReader(httpresponsepay.GetResponseStream()))
                                        {
                                            resultpay = stremreaderpay.ReadToEnd();

                                        }
                                    }


                                    dynamic datapay = JObject.Parse(resultpay);
                                    JArray items = (JArray)datapay["PaymentInquiryResponseMessage"]["TransactionList"];
                                    int le = items.Count;
                                    var PayStatus = datapay["PaymentInquiryResponseMessage"]["TransactionList"][le - 1]["PaymentStatus"];
                                    var PayDate = Convert.ToString(datapay["PaymentInquiryResponseMessage"]["TransactionList"][le - 1]["PaymentDate"]);
                                    var STCPayNum = Convert.ToString(datapay["PaymentInquiryResponseMessage"]["TransactionList"][le - 1]["STCPayRefNum"]);


                                    if (PayStatus == 4)
                                    {
                                        // payment Expire then cancel booking
                                        Transaction objDoc = new Transaction();
                                        objDoc = _dbContext.Transactions.Where(dc => dc.Id == tranId).FirstOrDefault();
                                        if (objDoc != null)
                                        {
                                            objDoc.Status = TransactionStatus.Cancelled;
                                            _dbContext.Update(objDoc);
                                            _dbContext.SaveChanges();
                                        }
                                    }
                                    if (PayStatus == 5)
                                    {
                                        // payment Expire then cancel booking
                                        Transaction objDoc = new Transaction();
                                        objDoc = _dbContext.Transactions.Where(dc => dc.Id == tranId).FirstOrDefault();
                                        if (objDoc != null)
                                        {
                                            objDoc.Status = TransactionStatus.Cancelled;
                                            _dbContext.Update(objDoc);
                                            _dbContext.SaveChanges();
                                        }
                                    }
                                    if (PayStatus == 1)
                                    {
                                        // payment Done then Pending booking
                                        Transaction objDoc = new Transaction();
                                        objDoc = _dbContext.Transactions.Where(dc => dc.Id == tranId).FirstOrDefault();
                                        if (objDoc != null)
                                        {
                                            objDoc.Status = TransactionStatus.Pending;
                                            _dbContext.Update(objDoc);
                                            _dbContext.SaveChanges();
                                        }
                                    }
                                    if (PayStatus == 2)
                                    {
                                        // payment Done then Paid booking
                                        Transaction objDoc = new Transaction();
                                        objDoc = _dbContext.Transactions.Where(dc => dc.Id == tranId).FirstOrDefault();
                                        if (objDoc != null)
                                        {
                                            objDoc.STCPayRefNum = STCPayNum;
                                            _dbContext.Update(objDoc);
                                            _dbContext.SaveChanges();
                                        }

                                    }

                                    // To check Payment Status Paid or Not 
                                    if (stopwatch.Elapsed > TimeSpan.FromSeconds(5))
                                        break;


                                }


                            }
                        }
                    }
                }

                var benifitList = (from b in _dbContext.Booking
                                   join r in _dbContext.Rooms on b.RoomId equals r.Id
                                   join h in _dbContext.Hotels on r.HotelId equals h.Id
                                   join hf in _dbContext.HotelFacilities on h.Id equals hf.HotelId
                                   join f in _dbContext.Facilities on hf.FacilityId equals f.Id
                                   //groupBy(h.Id)
                                   where b.Id == trans.Id
                                   select new
                                   {
                                       f.Description
                                   }).ToList();

                string benifits = "";
                foreach (var ban in benifitList)
                {
                    benifits += ban.Description + ",";
                }
                benifits = benifits.Substring(0, benifits.Length - 1);

                var PromotionList = (from t in _dbContext.Transactions
                                     join b in _dbContext.Booking on t.Id equals b.Id
                                     join p in _dbContext.HotelPromotions on b.PromotionId equals p.Id
                                     join hp in _dbContext.HotelPromotionRooms on p.Id equals hp.HotelPromotionId
                                     where b.Id == trans.Id
                                     select new
                                     {
                                         p.Name
                                     }).ToList();
                var PromotionName = "";
                foreach (var prom in PromotionList)
                {
                    PromotionName = prom.Name;
                }

                //IEnumerable _List1 = benifitList.ToList();
                //DataTable dt1 = DataTableFromIEnumerable(_List1);

                var attachTemplate = await _mediator.Send(new GetMailTemplateRequest() { Id = ConfirmBooking.AttachTemplateId });
                //var query = "select * from hotels" 
                StringBuilder BodyText = new StringBuilder(attachTemplate.Template.ToString());
                DataRow dr;
                dr = dt.Rows[0];
                string DateTr = "";
                for (DateTime date = Convert.ToDateTime(dr["startDate"]); date <= Convert.ToDateTime(dr["EndDate"]); date = date.AddDays(1))
                {
                    DateTr += "<tr><td><p style='font-size: 14px;margin:0;padding: 0;'>" + date.ToString("dd-MMM-yyyy") + "</p></td><td align = 'right'><p style = 'color:#8e6bad;font-size: 14px;margin:0;padding: 0;'>" + Convert.ToString(dr["ClientCurrency"]) + " " + Convert.ToString(dr["ClientAmount"]) + "</p></td></tr>";
                }
                BodyText = BodyText.Replace("$$TransactionId$$", Convert.ToString(dr["Id"]));
                BodyText = BodyText.Replace("$$HotelName$$", Convert.ToString(dr["HotelName"]));
                BodyText = BodyText.Replace("$$CityName$$", Convert.ToString(dr["HotelCity"]));
                BodyText = BodyText.Replace("$$FirstName$$", Convert.ToString(dr["FirstName"]));
                BodyText = BodyText.Replace("$$LastName$$", Convert.ToString(dr["LastName"]));
                BodyText = BodyText.Replace("$$ChekinDate$$", Convert.ToString(dr["CheckInDate"]));
                BodyText = BodyText.Replace("$$ChekoutDate$$", Convert.ToString(dr["CheckOutDate"]));
                BodyText = BodyText.Replace("$$Rooms$$", Convert.ToString(dr["Rooms"]));
                BodyText = BodyText.Replace("$$RoomType$$", Convert.ToString(dr["Description"]));
                BodyText = BodyText.Replace("$$RoomAdult$$", Convert.ToString(dr["NoOfPersons"]));
                BodyText = BodyText.Replace("$$CustResidence$$", Convert.ToString(dr["ResCountry"]));
                BodyText = BodyText.Replace("$$Benifits$$", benifits);
                BodyText = BodyText.Replace("$$CanPolicy$$", Convert.ToString(dr["CanPolicy"]));
                BodyText = BodyText.Replace("$$RoomDescription$$", DateTr);
                BodyText = BodyText.Replace("$$Promotion$$", (PromotionName == "" ? "-" : PromotionName));
                BodyText = BodyText.Replace("$$TotalAmount$$", Convert.ToString(dr["ClientCurrency"]) + " " + Convert.ToString(dr["ClientAmount"]));


                // instantiate the html to pdf converter
                HtmlToPdf converter = new HtmlToPdf();
                string htmlContent = BodyText.ToString();

                string pdf_page_size = "A4";
                PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
                    pdf_page_size, true);

                string pdf_orientation = "Portrait";
                PdfPageOrientation pdfOrientation =
                    (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                    pdf_orientation, true);

                converter.Options.PdfPageSize = pageSize;
                converter.Options.PdfPageOrientation = pdfOrientation;
                converter.Options.WebPageWidth = 1024;
                converter.Options.WebPageHeight = 0;

                // convert the url to pdf
                PdfDocument doc = converter.ConvertHtmlString(htmlContent);

                string fileName = Guid.NewGuid().ToString();

                // save pdf document
                doc.Save($"{this._hostingEnvironment.WebRootPath }/Reports/" + fileName + ".Pdf");
                // close pdf document
                doc.Close();


                await sendMailNotification(booking, $"{this._hostingEnvironment.WebRootPath }/Reports/" + fileName + ".Pdf");

                //SendSmsNotification(booking);
                var response = new CreateBookingResponse() { Id = booking.Id };
                //var response = new CreateBookingResponse() { Id = "1" };
                return response;

                // Publish To Transaction Service 
                // response.lst  = Mapper.Map<IList<CalendarManual>, IList<CalendarManualDetails>>(calendar);
                //await _repo.GetCalendar(request.HotelId);
                //var result= await _uow.SaveChangesAsync();
                // Publish to Transaction 
                /*
                if (result>=1)
                {
                    var creatdHotelEventMessage = new HotelCreatedEventMessage
                    {
                        Name = request.Name,
                        ProviderId = request.ProviderId
                    };
                   // _publisher.Publish(creatdHotelEventMessage);
                }
                */
            }
            //catch (WebException ex)
            //{
            //    var response = (HttpWebResponse)ex.Response;
            //    return null;
            //}
            catch (Exception ex)
            {
                return null;
            }
        }



        async Task sendMailNotification(Booking booking, string AttachmentPath)
        {
            //try
            //{
            //    var service = await serviceRepo.GetById(booking.Transaction.ServiceId);
            //    var template = await _mediator.Send(new GetMailTemplateRequest() { Id = ConfirmBooking.TemplateId });

            //    var title = !string.IsNullOrEmpty(booking.Transaction.Title) ? Enum.GetName(typeof(Titles), int.Parse(booking.Transaction.Title)) : "";
            //    var specialRequest = !string.IsNullOrEmpty(booking.SpecialRequest) ? booking.SpecialRequest : "";
            //    var htmlContent = template.Template
            //        .Replace("$$Title$$", title)
            //        .Replace("$$HotelName$$", service.Name)
            //        .Replace("$$FirstName$$", booking.Transaction.FirstName)
            //        .Replace("$$LastName$$", booking.Transaction.LastName)
            //        .Replace("$$Email$$", booking.Transaction.Email)
            //        .Replace("$$CheckinDate$$", booking.CheckInDate.ToString("ddd - MMM dd yyyy"))
            //        .Replace("$$CheckoutDate$$", booking.CheckOutDate.ToString("ddd - MMM dd yyyy"))
            //        .Replace("$$NoOfRooms$$", booking.Rooms.ToString())
            //        .Replace("$$SpecialRequest$$", specialRequest)
            //        .Replace("$$Arrival Time$$", booking.ArrivalDate.ToString("ddd - MMM dd yyyy hh:mm:ss"));


            //    _mediator.Send(new SendMailNotificationRequest()
            //    {
            //        To = booking.Transaction.Email,
            //        DisplayName = booking.Transaction.FirstName,
            //        Subject = ConfirmBooking.Subject + service.Name,
            //        Content = htmlContent,
            //        AttachmentPath = AttachmentPath,
            //    });


            //}
            //catch { }
        }
        async Task SendSmsNotification(Booking booking)
        {
            await _mediator.Send(new SendSmsNotificationRequest()
            {
                Mobile = booking.Transaction.Mobile,
                Content = ConfirmBooking.SMSMessage
            });
        }
        public static DataTable DataTableFromIEnumerable(IEnumerable ien)
        {
            DataTable dt = new DataTable();

            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (PropertyInfo pi in pis)
                    {
                        Type pt = pi.PropertyType;
                        if (pt.IsGenericType && pt.GetGenericTypeDefinition() == typeof(Nullable<>))
                            pt = Nullable.GetUnderlyingType(pt);

                        dt.Columns.Add(pi.Name, pt);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (PropertyInfo pi in pis)
                {
                    object value = (pi.GetValue(obj, null) != null) ? pi.GetValue(obj, null) : DBNull.Value;
                    dr[pi.Name] = value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public bool ByteArrayToFile(string fileName, byte[] byteArray)
        {
            try
            {
                using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(byteArray, 0, byteArray.Length);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught in process: {0}", ex);
                return false;
            }
        }
    }
}
