﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Application.Services.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    public class GetActiveBookingsRequest : IRequest<GetActiveBookingsResponse>
    {
        public long ServiceId { get; set; }
    }

    public class GetActiveBookingsResponse
    {
        public GetActiveBookingsResponse() {
            BookingList = new List<HotelBooking>();
        }
        public ICollection<HotelBooking> BookingList { get; set; }
        
    }


    public class GetActiveBookingsRequestHandler : IRequestHandler<GetActiveBookingsRequest, GetActiveBookingsResponse>
    {
        public readonly IHotelService hotelService;
        public GetActiveBookingsRequestHandler(IHotelService _hotelService) {
            hotelService = _hotelService;
        }

        public async Task<GetActiveBookingsResponse> Handle(GetActiveBookingsRequest request, CancellationToken cancellationToken)
        {
            var result = await hotelService.GetActiveBookings(request.ServiceId);

            var res = new GetActiveBookingsResponse();
            res.BookingList = result;
            return res;
        }
    }
}