﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class GetBookingDetailsRequest : IRequest<GetBookingDetailsResponse> {
        public string Id { get; set; }
        public long? UserId { get; set; }
    }

    public class GetBookingDetailsResponse {
        public string Id { get; set; }
        public long ServiceId { get; set; }
        public long ClientId { get; set; }
        public long RoomId { get; set; }
        public string RoomName { get; set; }
        public int Rooms { get; set; }
        public DateTime Date { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public int Status { get; set; }
        public decimal ProviderAmount { get; set; }
        public decimal ClientAmount { get; set; }
        public decimal ExchangeRate { get; set; }
        public string ProviderCurrency { get; set; }
        public string ClientCurrency { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Boolean IsPersonal { get; set; }
        public string RefereeName { get; set; }
        public string RefereeEmail { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string Mobile { get; set; }
        public int CreditCardId { get; set; }
        public string CardNo { get; set; }
        public byte CardExpiryMonth { get; set; }
        public int CardExpiryYear { get; set; }
        //public int Cvv { get; set; }  //mohsin27-2-2020

        public string SpecialRequest { get; set; }
        public DateTime ArrivalDate { get; set; }
        public string TransactionId { get; set; }
        public Boolean IsInCheckOutDate { get; set; }
        public Boolean IsInCheckInDate { get; set; }
        public bool HasReview { get; set; }
        public Boolean isPaymentHotel { get; set; }
    }


    public class GetBookingDetailsHandler : IRequestHandler<GetBookingDetailsRequest, GetBookingDetailsResponse> {
        public readonly IBookingService bookingService;
        public GetBookingDetailsHandler(IBookingService _bookingService) {
            bookingService = _bookingService;
        }
        public async Task<GetBookingDetailsResponse> Handle(GetBookingDetailsRequest request, CancellationToken cancellationToken) {
            var booking = await bookingService.GetBookingDetails(request.Id);
            if (booking != null && (!request.UserId.HasValue || (request.UserId.HasValue && booking.ClientId == request.UserId.Value))) {
                var res = new GetBookingDetailsResponse();
                booking.ToResponse(res);
                return res;
            }
            return null;
        }
    }
}
