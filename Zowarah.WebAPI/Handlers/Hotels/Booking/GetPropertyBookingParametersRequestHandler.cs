﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.WebAPI.Extensions;
using Zowarah.WebAPI.Handlers.Common.NotificationsHandlers;
using Zowarah.WebAPI.Config.Notification;
using Zowarah.WebAPI.Helpers.Users;
using Zowarah.WebAPI.Handlers.Transactions.TransactionHandlers;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Transactions;
using Zowarah.Application.Services.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    /*public class CreateBookingRequestValidator : AbstractValidator<CreateBookingRequest>
    {
        public CreateBookingRequestValidator()
        {
            //RuleFor(PropertyBasicInfo => PropertyBasicInfo.Name).NotEmpty();
        }
    }*/

    public class GetPropertyBookingParametersRequest : IRequest<GetPropertyBookingParametersResponse>
    {
        public long HotelId { get; set; }
    }

    public class GetPropertyBookingParametersResponse
    {
        public int LastReservedRooms { get; set; }
    }
    public class GetPropertyBookingParametersHandler : IRequestHandler<GetPropertyBookingParametersRequest, GetPropertyBookingParametersResponse>
    {
        private IBookingService service { get; set; }

        public GetPropertyBookingParametersHandler(IBookingService _service)
        {
            service = _service;
        }
        public async Task<GetPropertyBookingParametersResponse> Handle(GetPropertyBookingParametersRequest request, CancellationToken cancellationToken)
        {
            // _Repo.get
            //var validator = new CreateBookingRequestValidator();
            //var validation= await validator.ValidateAsync(request);

            //if (!validation.IsValid)
            //  return null;
            var response = new GetPropertyBookingParametersResponse();
            response.LastReservedRooms = await service.GetLastReservedRooms(request.HotelId, 24);
            return response;
        }
     
    }
}
