﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using System.ComponentModel.DataAnnotations;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    public class GetPaymentPolicyRequest : IRequest<GetPaymentPolicyResponse>
    {
        public long Id { get; set; }
    }

    public class GetPaymentPolicyResponse
    {
        public bool IsCreditCardAccepted { get; set; }
        public ICollection<int> CreditCards { get; set; }
        public bool IsInvoicingHotelAddress { get; set; }
        public string InvoicingRecipientName { get; set; }
        public int InvoicingCountryId { get; set; }
        public int InvoicingCityId { get; set; }
        public string InvoicingAddress { get; set; }
        public string InvoicingPostalAddress { get; set; }
        public int? CancellationPolicyId { get; set; }
        public int? CancellationFeeId { get; set; }

    }
    public class GetPaymentPolicyRequestHandler : IRequestHandler<GetPaymentPolicyRequest,GetPaymentPolicyResponse>
    {
        private readonly IHotelService _repo;
        private readonly IUnitOfWork _uow;
      
        public GetPaymentPolicyRequestHandler(IHotelService repo, IUnitOfWork uow)
        {
            _repo = repo;
            _uow = uow;
        }

        public async Task<GetPaymentPolicyResponse> Handle(GetPaymentPolicyRequest request, CancellationToken cancellationToken)
        {
             var Hotel = await _repo.FindBy(x=> x.Id == request.Id,x=> x.CreditCards).FirstOrDefaultAsync();
             if (Hotel == null)
                 return null;
             GetPaymentPolicyResponse response = new GetPaymentPolicyResponse();
             Hotel.ToResponse(response);
             return await Task.FromResult(response);
        }
    }

}
