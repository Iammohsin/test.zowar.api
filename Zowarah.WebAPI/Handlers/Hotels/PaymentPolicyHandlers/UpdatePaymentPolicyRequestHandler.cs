﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using AutoMapper;
using System;
using System.Collections.Generic;
using Zowarah.Domain.Interfaces;
using Zowarah.Application.Interfaces.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class UpdatePaymentPolicyResponse {
    }
    public class UpdatePaymentPolicyRequest : IRequest<UpdatePaymentPolicyResponse> {
        public long Id { get; set; }
        public bool IsCreditCardAccepted { get; set; }
        public ICollection<int> CreditCards { get; set; }
        public bool IsInvoicingHotelAddress { get; set; }
        public string InvoicingRecipientName { get; set; }
        public int InvoicingCountryId { get; set; }
        public int InvoicingCityId { get; set; }
        public string InvoicingAddress { get; set; }
        public string InvoicingPostalAddress { get; set; }
        public int CancellationPolicyId { get; set; }
        public int CancellationFeeId { get; set; }
        public long UserId { get; set; }
    }
    public class UpdatePaymentPolicyRequestHandler : IRequestHandler<UpdatePaymentPolicyRequest, UpdatePaymentPolicyResponse> {
        private readonly IHotelService _repo;
        private readonly IUnitOfWork _uow;

        public UpdatePaymentPolicyRequestHandler(IHotelService repo, IUnitOfWork uow) {
            _repo = repo;
            _uow = uow;
        }
        public async Task<UpdatePaymentPolicyResponse> Handle(UpdatePaymentPolicyRequest request, CancellationToken cancellationToken) {
            var hotel = await _repo.GetById(request.Id, x => x.CreditCards);
            if (hotel != null && hotel.ProviderId == request.UserId) {
                Mapper.Map(request, hotel);
                hotel.IsPaymentPolicyConfigured = true;
                _repo.Update(hotel);
                await _uow.SaveChangesAsync();
                return new UpdatePaymentPolicyResponse();
            }
            return null;
        }
    }
}