﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class GetFacilityCategoryListRequest : IRequest<GetFacilityCategoryListResponse> {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetFacilityCategoryResponse {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetFacilityCategoryListResponse {
        public GetFacilityCategoryListResponse() {
            FacilityCategoryList = new List<GetFacilityCategoryResponse>();
        }

        public List<GetFacilityCategoryResponse> FacilityCategoryList;
    }
    public class GetFacilityCategoryListRequestHandler : IRequestHandler<GetFacilityCategoryListRequest, GetFacilityCategoryListResponse> {
        private readonly IRepository<FacilityCategory> _FacilityCategoryRepo;
        private readonly IUnitOfWork _uow;

        public GetFacilityCategoryListRequestHandler(IRepository<FacilityCategory> FacilityCategoryRepo, IUnitOfWork uow) {
            _FacilityCategoryRepo = FacilityCategoryRepo;
            _uow = uow;
        }

        public async Task<GetFacilityCategoryListResponse> Handle(GetFacilityCategoryListRequest request, CancellationToken cancellationToken) {
            var lst = await _FacilityCategoryRepo.GetAll();
            if (lst == null)
                return null;

            return new GetFacilityCategoryListResponse {
                FacilityCategoryList = Mapper.Map<List<GetFacilityCategoryResponse>>(lst)
            };
        }
    }

}
