﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Enums.Hotels;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Hotels.HotelDescription
{

    public class GenerateHotelDescptrionRequest : IRequest<GenerateHotelDescptrionResponse>
    {
        public long ServiceId { get; set; }
    }

    public class GenerateHotelDescptrionResponse
    {
        public string Description { get; set; }

    }
    public class GenerateHotelDescptrionHandler : IRequestHandler<GenerateHotelDescptrionRequest, GenerateHotelDescptrionResponse>
    {
        private readonly IHotelService hotelService;
        private readonly ILookupRepository<InternetOption> _Repo;
        private readonly IMediator mediator;

        public GenerateHotelDescptrionHandler(IHotelService _hotelService, IMediator _mediator, ILookupRepository<InternetOption> Repo)
        {
            hotelService = _hotelService;
            mediator = _mediator;
            _Repo = Repo;
        }
        public async Task<GenerateHotelDescptrionResponse> Handle(GenerateHotelDescptrionRequest request, CancellationToken cancellationToken)
        {
            List<int> descIDs = new List<int>();
            var features = await hotelService.GetHotelFeaturesDescriptions();

            var hotel = await hotelService.GetHotelById(request.ServiceId);

            descIDs.Add((int)HotelFeaturesDescriptionsEnum.AccommodationsOffer);
            descIDs.Add((int)HotelFeaturesDescriptionsEnum.Promotion);
            descIDs.Add((int)HotelFeaturesDescriptionsEnum.TopRatedLocation);
            descIDs.Add((int)HotelFeaturesDescriptionsEnum.Language);
            if (hotel.InternetOptionId == (int)InternetOptions.Free)
            {
                descIDs.Add((int)HotelFeaturesDescriptionsEnum.FreeWifi);
            }
            if (hotel.Facilities.Contains((int)HotelFacilitesEnum.Sauna))
            {
                descIDs.Add((int)HotelFeaturesDescriptionsEnum.Sauna);
            }
            if (hotel.Facilities.Contains((int)HotelFacilitesEnum.BBQ))
            {
                descIDs.Add((int)HotelFeaturesDescriptionsEnum.BBQ);
            }
            if (hotel.Facilities.Contains((int)HotelFacilitesEnum.BaggageStorage))
            {
                descIDs.Add((int)HotelFeaturesDescriptionsEnum.FrontdeskandBaggageStorage);
            }
            else {
                descIDs.Add((int)HotelFeaturesDescriptionsEnum.Frontdesk);
            }

            //if (hotel.RoomAmenities.Count()>0)
            //{

            //}

            if (hotel.Facilities.Count() > 3)
            {
                descIDs.Add((int)HotelFeaturesDescriptionsEnum.GreatFacilities);
            }
            if (hotel.Rating > 4)
            {
                descIDs.Add((int)HotelFeaturesDescriptionsEnum.FavoriteInCity);
            }
            string desc = string.Join(", ", features.Where(s => descIDs.Contains(s.Id)).OrderBy(r => r.Order).Select(s => s.Description));
            desc = desc.Replace("{hotelName}", hotel.Name).Replace("{cityName}", (!string.IsNullOrEmpty(hotel.CityName)?hotel.CityName:"this location"));
            return new GenerateHotelDescptrionResponse() { Description = desc };
        }


    }
}
