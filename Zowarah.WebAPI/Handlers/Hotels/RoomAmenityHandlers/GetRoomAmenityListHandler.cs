﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class GetRoomAmenityListRequest : IRequest<GetRoomAmenityListResponse> {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetRoomAmenityResponse {
        public int Id { get; set; }
        public int RoomAmenityCategoryId { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
    }
    public class GetRoomAmenityListResponse {
        public GetRoomAmenityListResponse() {
            RoomAmenityList = new List<GetRoomAmenityResponse>();
        }

        public IList<GetRoomAmenityResponse> RoomAmenityList;
    }
    public class GetRoomAmenityListRequestHandler : IRequestHandler<GetRoomAmenityListRequest, GetRoomAmenityListResponse> {
        private readonly IRepository<RoomAmenity> _RoomAmenityRepo;
        private readonly IUnitOfWork _uow;
        public GetRoomAmenityListRequestHandler(IRepository<RoomAmenity> RoomAmenityRepo, IUnitOfWork uow) {
            _RoomAmenityRepo = RoomAmenityRepo;
            _uow = uow;
        }

        public async Task<GetRoomAmenityListResponse> Handle(GetRoomAmenityListRequest request, CancellationToken cancellationToken) {
            var lst = await _RoomAmenityRepo.GetAll();
            if (lst == null)
                return null;
            return new GetRoomAmenityListResponse {
                RoomAmenityList = Mapper.Map<List<GetRoomAmenityResponse>>(lst)
            };
        }
    }

}
