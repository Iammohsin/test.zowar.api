﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Hotels.ServiceProfilesHandler {
    public class CreateServiceProfileRequest : IRequest<CreateServiceProfileResponse> {
        public long Id { get; set; }
        public long ServiceId { get; set; }
        public string Language { get; set; }
        public string PropertyName { get; set; }
        public string AboutProperty { get; set; }
        public string AboutStaff { get; set; }
        public string AboutNeighborhood { get; set; }
        public long UserId { get; set; }
    }

    public class CreateServiceProfileResponse {
        public long Id { get; set; }
    }
    public class CreateServiceProfileHandler : IRequestHandler<CreateServiceProfileRequest, CreateServiceProfileResponse> {
        private readonly IHotelService repoHotel;
        private readonly IServiceProfileService repo;
        private readonly IUnitOfWork uow;

        public CreateServiceProfileHandler(IServiceProfileService _repo, IUnitOfWork _uow, IHotelService _repoHotel) {
            repoHotel = _repoHotel;
            repo = _repo;
            uow = _uow;
        }
        public async Task<CreateServiceProfileResponse> Handle(CreateServiceProfileRequest request, CancellationToken cancellationToken) {
            var hotel = await repoHotel.GetById(request.ServiceId);
            if (hotel != null && hotel.ProviderId == request.UserId) {
                var profile = new ServiceProfile();
                profile = profile.FromRequest(request);

                repo.Create(profile);
                hotel.IsProfileConfigured = true;
                repoHotel.Update(hotel);
                var result = await uow.SaveChangesAsync();
                return new CreateServiceProfileResponse() { Id = profile.Id };
            }
            return null;
        }
    }
}
