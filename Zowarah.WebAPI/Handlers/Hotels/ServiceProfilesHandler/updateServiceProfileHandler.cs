﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Hotels.ServiceProfilesHandler {
    public class UpdateServiceProfileRequest : IRequest<UpdateServiceProfileResponse> {
        public long Id { get; set; }
        public long ServiceId { get; set; }
        public string Language { get; set; }
        public string PropertyName { get; set; }
        public string AboutProperty { get; set; }
        public string AboutStaff { get; set; }
        public string AboutNeighborhood { get; set; }
        public long UserId { get; set; }
    }

    public class UpdateServiceProfileResponse {
        public long Id { get; set; }
    }
    public class UpdateServiceProfileHandler : IRequestHandler<UpdateServiceProfileRequest, UpdateServiceProfileResponse> {
        private readonly IServiceProfileService repo;
        private readonly IHotelService repoHotel;
        private readonly IUnitOfWork uow;

        public UpdateServiceProfileHandler(IServiceProfileService _repo, IHotelService _repoHotel, IUnitOfWork _uow) {
            repo = _repo;
            repoHotel = _repoHotel;
            uow = _uow;
        }
        public async Task<UpdateServiceProfileResponse> Handle(UpdateServiceProfileRequest request, CancellationToken cancellationToken) {
            var hotel = await repoHotel.GetById(request.ServiceId);
            if (hotel != null && hotel.ProviderId == request.UserId) {
                var profile = await repo.GetById(request.Id);
                if (profile == null)
                    return null;
                Mapper.Map(request, profile);
                // profile = profile.FromRequest(request);
                repo.Update(profile);
                await uow.SaveChangesAsync();
                return new UpdateServiceProfileResponse() { Id = profile.Id };
            }
            return null;
        }
    }
}
