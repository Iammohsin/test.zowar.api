﻿
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Application.Services.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Hotels.ServiceProfilesHandler
{
    public class GetAllServiceProfileRequest : IRequest<GetAllServiceProfileResponse>
    {
        public long ServiceId { get; set; }
    }
    

    public class GetAllServiceProfileResponse
    {
        public ICollection<GetServiceProfile> Profiles { get; set; } = new List<GetServiceProfile>();
    }
    public class GetAllServiceProfileHandler : IRequestHandler<GetAllServiceProfileRequest, GetAllServiceProfileResponse>
    {
        private readonly IServiceProfileService repo;
        private readonly IUnitOfWork uow;

        public GetAllServiceProfileHandler(IServiceProfileService _repo, IUnitOfWork _uow)
        {
            repo = _repo;
            uow = _uow;
        }
        public async Task<GetAllServiceProfileResponse> Handle(GetAllServiceProfileRequest request, CancellationToken cancellationToken)
        {
            var profiles = await repo.GetAllByServiceId(request.ServiceId);

            return new GetAllServiceProfileResponse() { Profiles = profiles };
        }
    }
}
