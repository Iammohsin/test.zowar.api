﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Hotels.ServiceProfilesHandler
{
    public class DeleteServiceProfileRequest : IRequest<DeleteServiceProfileResponse>
    {
        public long Id { get; set; }        
    }

    public class DeleteServiceProfileResponse
    {
        public long Id { get; set; }
    }
    public class DeleteServiceProfileHandler : IRequestHandler<DeleteServiceProfileRequest, DeleteServiceProfileResponse>
    {
        private readonly IServiceProfileService repo;
        private readonly IUnitOfWork uow;

        public DeleteServiceProfileHandler(IServiceProfileService _repo, IUnitOfWork _uow)
        {
            repo = _repo;
            uow = _uow;
        }
        public async Task<DeleteServiceProfileResponse> Handle(DeleteServiceProfileRequest request, CancellationToken cancellationToken)
        {
            var profile = await repo.GetById(request.Id);
            if (profile == null)
                return null;           
            repo.Delete(profile);
            var result = await uow.SaveChangesAsync();
            return new DeleteServiceProfileResponse() { Id = profile.Id };
        }
    }
}
