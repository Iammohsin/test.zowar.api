﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Hotels.ServiceProfilesHandler
{

    public class GetServiceProfileRequest : IRequest<GetServiceProfileResponse>
    {
        public string Language { get; set; }
        public long ServiceId { get; set; }
    }

    public class GetServiceProfileResponse
    {
        public string AboutProperty { get; set; }
        public string AboutStaff { get; set; }
        public string AboutNeighborhood { get; set; }
    }
    public class GetServiceProfileHandler : IRequestHandler<GetServiceProfileRequest, GetServiceProfileResponse>
    {
        private readonly IServiceProfileService repo;
        public GetServiceProfileHandler(IServiceProfileService _repo)
        {
            repo = _repo;
        }
        public async Task<GetServiceProfileResponse> Handle(GetServiceProfileRequest request, CancellationToken cancellationToken)
        {
            var profile = await repo.GetByServiceIdAndLanguage(request.ServiceId, request.Language);
            if (profile == null && request.Language != "en")
            {
                profile = await repo.GetByServiceIdAndLanguage(request.ServiceId, "en");
            }
            if (profile == null)
                return null;
            var res = new GetServiceProfileResponse();
            profile.ToResponse(res);
            return res;
        }
    }
}
