﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class GetCancellationFeeListRequest : IRequest<GetCancellationFeeListResponse> {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetCancellationFeeResponse {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetCancellationFeeListResponse {
        public GetCancellationFeeListResponse() {
            CancellationFeeList = new List<GetCancellationFeeResponse>();
        }

        public List<GetCancellationFeeResponse> CancellationFeeList;
    }
    public class GetCancellationFeeListRequestHandler : IRequestHandler<GetCancellationFeeListRequest, GetCancellationFeeListResponse> {
        private readonly IRepository<CancellationFee> _CancellationFeeRepo;
        private readonly IUnitOfWork _uow;

        public GetCancellationFeeListRequestHandler(IRepository<CancellationFee> CancellationFeeRepo, IUnitOfWork uow) {
            _CancellationFeeRepo = CancellationFeeRepo;
            _uow = uow;
        }

        public async Task<GetCancellationFeeListResponse> Handle(GetCancellationFeeListRequest request, CancellationToken cancellationToken) {
            var lst = await _CancellationFeeRepo.GetAll();
            if (lst == null)
                return null;
            return new GetCancellationFeeListResponse {
                CancellationFeeList = Mapper.Map<List<GetCancellationFeeResponse>>(lst)
            };
        }
    }

}
