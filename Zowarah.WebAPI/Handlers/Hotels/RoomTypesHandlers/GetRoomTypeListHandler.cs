﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    public class GetRoomTypeListRequest : IRequest<GetRoomTypeListResponse>
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetRoomTypeResponse {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetRoomTypeListResponse
    {
        public GetRoomTypeListResponse()
        {
            RoomTypeList = new List<GetRoomTypeResponse>();
        }

        public List<GetRoomTypeResponse> RoomTypeList;
    }
    public class GetRoomTypeListRequestHandler : IRequestHandler<GetRoomTypeListRequest, GetRoomTypeListResponse>
    {
        private readonly ILookupRepository<RoomType> _RoomTypeRepo;
        private readonly IUnitOfWork _uow;

        public GetRoomTypeListRequestHandler(ILookupRepository<RoomType> RoomTypeRepo, IUnitOfWork uow)
        {
            _RoomTypeRepo = RoomTypeRepo;
            _uow = uow;
        }

        public async Task<GetRoomTypeListResponse> Handle(GetRoomTypeListRequest request, CancellationToken cancellationToken)
        {
            var lst = await _RoomTypeRepo.GetAll();
            if (lst == null)
                return null;

            return new GetRoomTypeListResponse {
                RoomTypeList=Mapper.Map<List<GetRoomTypeResponse>>(lst)
            };
        }
    }

}
