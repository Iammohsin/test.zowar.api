﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using AutoMapper;
using System;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Application.Interfaces.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class UpdatePropertyBasicInfoResponse {
    }
    /// <summary>
    ///  we send UserId from Controller to check the provider own this property
    /// </summary>
    public class UpdatePropertyBasicInfoRequest : IRequest<UpdatePropertyBasicInfoResponse> {
        public long Id { get; set; }
        public long ServiceId { get; set; }
        public string Name { get; set; }
        public int PropertyTypeId { get; set; }
        public int SmokingOptionId { get; set; }
        public int Rating { get; set; }
        //mohsin
        public decimal TaxP { get; set; }
        public decimal Vat { get; set; }

        public decimal LocLat { get; set; }
        public decimal LocLng { get; set; }
        public int NoOfRooms { get; set; }
        public string ContactName { get; set; }
        public string Address { get; set; }
        public int CityId { get; set; }
        public string Contact1 { get; set; }
        public string Contact2 { get; set; }
        public bool IsBasicInfoConfigured { get; set; }
        public long UserId { get; set; }
    }
    public class UpdatePropertyBasicInfoRequestHandler : IRequestHandler<UpdatePropertyBasicInfoRequest, UpdatePropertyBasicInfoResponse> {
        private readonly IHotelService _repo;
        private readonly IUnitOfWork _uow;

        public UpdatePropertyBasicInfoRequestHandler(IHotelService repo, IUnitOfWork uow) {
            _repo = repo;
            _uow = uow;
        }
        public async Task<UpdatePropertyBasicInfoResponse> Handle(UpdatePropertyBasicInfoRequest request, CancellationToken cancellationToken) {
            var hotel = await _repo.GetById(request.Id);
            if (hotel != null && hotel.ProviderId == request.UserId) {
                Mapper.Map(request, hotel);
                _repo.Update(hotel);
                await _uow.SaveChangesAsync();
                return new UpdatePropertyBasicInfoResponse();
            }
            return null;
        }
    }
}