﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using System.ComponentModel.DataAnnotations;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Application.Interfaces.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    public class GetPropertyBasicInfoRequest : IRequest<GetPropertyBasicInfoResponse>
    {
        public long Id { get; set; }
    }

    public class GetPropertyBasicInfoResponse
    {
        public long Id { get; set; }
        public String Name { get; set; }
        public int PropertyTypeId { get; set; }
        public int SmokingOptionId { get; set; }
        public int Rating { get; set; }
       // mohsin 
       // 23forTaxSuccess
       public decimal TaxP { get; set; }
        public decimal Vat { get; set; }
        public String Location { get; set; }
        public int NoOfRooms { get; set; }
        public String ContactName { get; set; }
        public String Address { get; set; }
        public int CityId { get; set; }
        public String Contact1 { get; set; }
        public String Contact2 { get; set; }
        public Boolean IsBasicInfoConfigured { get; set; }
        public long ProviderId { get; set; }
        public decimal LocLng { get; set; }
        public decimal LocLat { get; set; }
        public string MainPhoto { get; set;  }

    }
    public class GetPropertyBasicInfoRequestHandler : IRequestHandler<GetPropertyBasicInfoRequest,GetPropertyBasicInfoResponse>
    {
        private readonly IHotelService hotelRepo;
        private readonly IRepository<HotelPhoto> hotelPhotoRepo;
      
        public GetPropertyBasicInfoRequestHandler(IHotelService _hotelRepo, IRepository<HotelPhoto> _hotelPhotoRepo)
        {
            hotelRepo = _hotelRepo;
            hotelPhotoRepo = _hotelPhotoRepo;
        }
        //mohsin test 
        public async Task<GetPropertyBasicInfoResponse> Handle(GetPropertyBasicInfoRequest request, CancellationToken cancellationToken)
        {
            var hotel = await hotelRepo.GetById(request.Id);
             if (hotel == null)
                 return null;

             GetPropertyBasicInfoResponse response = new GetPropertyBasicInfoResponse();
             hotel.ToResponse(response);

            // Get Main Photo 
            var photo = hotelPhotoRepo.FindBy(x => x.HotelId == hotel.Id && x.IsMain == true).FirstOrDefault();

            if(photo!=null)
                    response.MainPhoto = photo.FileName;

            return await Task.FromResult(response);

        }
    }
}
