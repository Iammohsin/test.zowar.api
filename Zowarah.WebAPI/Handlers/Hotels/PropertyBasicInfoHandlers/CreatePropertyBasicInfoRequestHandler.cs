﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using System.ComponentModel.DataAnnotations;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Common;
using Zowarah.Application.Interfaces.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    /*public class CreatePropertyBasicInfoRequestValidator : AbstractValidator<CreatePropertyBasicInfoRequest>
    {
        public CreatePropertyBasicInfoRequestValidator()
        {
            //RuleFor(PropertyBasicInfo => PropertyBasicInfo.Name).NotEmpty();
        }
    }*/
    public class CreatePropertyBasicInfoRequest : IRequest<CreatePropertyBasicInfoResponse>
    {
       

        public string Id { get; set; }
        [Required]
        public string ProviderId { get; set; }
        [Required]
        public string Name { get; set; }
        public string CurrencyId { get; set; }
        [Required]
        public int PropertyTypeId { get; set; }
        [Required]
        public int SmokingOptionId { get; set; }
        [Required]
        public int Rating { get; set; }
        //mohsin
        public decimal TaxP { get; set; }

        public decimal Vat { get; set; }


        [Required]
        public decimal LocLat { get; set; }
        [Required]
        public decimal LocLng { get; set; }
        [Required]
        public int NoOfRooms { get; set; }
        [Required]
        public string ContactName { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public int CityId { get; set; }
        [Required]
        public string Contact1 { get; set; }
        public string Contact2 { get; set; }
  
      
    }

    public class CreatePropertyBasicInfoResponse
    {
        public long Id { get; set; }
    }
    public class CreatePropertyBasicInfoRequestHandler : IRequestHandler<CreatePropertyBasicInfoRequest, CreatePropertyBasicInfoResponse>
    {
        private readonly IHotelService repoHotel;
        private readonly IRepository<Service> repoService;
        private readonly IUnitOfWork _uow;

        public CreatePropertyBasicInfoRequestHandler(
            IHotelService _repoHotel, 
            IRepository<Service> _repoService, 
            IUnitOfWork uow)
        {
            repoHotel = _repoHotel;
            repoService = _repoService;
            _uow = uow;
        }
        public async Task<CreatePropertyBasicInfoResponse> Handle(CreatePropertyBasicInfoRequest request, CancellationToken cancellationToken)
        {
            //request.Id = Guid.NewGuid().ToString();
            var service = new Service();
            var hotel = new Hotel();

            service = service.FromRequest(request);
            hotel = hotel.FromRequest(request);

            service.ServiceType = ServiceTypeEnum.Hotel;
            hotel.Id = service.Id;
            //hotel.ServiceId = service.Id;


            //var validator = new CreatePropertyBasicInfoRequestValidator();
            //var validation= await validator.ValidateAsync(request);
            //if (!validation.IsValid)
            //  return null;
            // Validating No. Of Rooms

            repoHotel.Create(hotel);
            repoService.Create(service);
            var result = await _uow.SaveChangesAsync();
            return new CreatePropertyBasicInfoResponse { Id=hotel.Id};
        }
    }

}
