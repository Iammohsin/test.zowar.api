﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Enums.Hotels;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Application.Services.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class GetHotelSaleReportingRequest : IRequest<GetHotelSaleReportingResponse> {
        public long HodelId { get; set; }
        public int Room { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public ReportView View { get; set; }
        public long UserId { get; set; }
    }

    public class GetHotelSaleReportingResponse {
        public ICollection<HotelRoomSaleReporting> Rooms { get; set; } = new HashSet<HotelRoomSaleReporting>();
    }
    public class GetHotelSaleReportingHandler : IRequestHandler<GetHotelSaleReportingRequest, GetHotelSaleReportingResponse> {
        private readonly IHotelService repo;

        public GetHotelSaleReportingHandler(IHotelService _repo) {
            repo = _repo;
        }
        public async Task<GetHotelSaleReportingResponse> Handle(GetHotelSaleReportingRequest request, CancellationToken cancellationToken) {
            var result = await repo.GetHotelRoomSaleReporting(request.HodelId, request.UserId, request.From, request.To, request.Room, request.View);
            return new GetHotelSaleReportingResponse() { Rooms = result };
        }
    }
}
