﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Application.Interfaces.Invoices;
using Zowarah.Application.Interfaces.Transactions;
using Zowarah.Infrastructure.Config;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class GetHotelLandPageRequest : IRequest<GetHotelLandPageResponse> {
        public long HotelId { get; set; }
        public long UserId { get; set; }
    }

    public class GetHotelLandPageResponse {
        public decimal TodayBooking { get; set; }
        public decimal TodayArrival { get; set; }
        public decimal TodayDeparture { get; set; }
        public decimal TodayReview { get; set; }
        public decimal Booking { get; set; }
        public decimal Confirmed { get; set; }
        public decimal Cancelled { get; set; }
        public decimal PendingInvoices { get; set; }

    }
    public class GetHotelLandPageHandler : IRequestHandler<GetHotelLandPageRequest, GetHotelLandPageResponse> {
        private readonly IServiceService _service;
        private readonly IBookingService repoBooking;
        private readonly ITransactionService repoTrans;
        private readonly IReviewService repoReview;
        private readonly IInvoiceService repoInvoices;

        public GetHotelLandPageHandler(IServiceService service, IBookingService _repoBooking, ITransactionService _repoTrans, IReviewService _repoReview, IInvoiceService _repoInvoices) {
            _service = service;
            repoBooking = _repoBooking;
            repoTrans = _repoTrans;
            repoReview = _repoReview;
            repoInvoices = _repoInvoices;
        }
        public async Task<GetHotelLandPageResponse> Handle(GetHotelLandPageRequest request, CancellationToken cancellationToken) {
            var hotel = await _service.GetById(request.HotelId);
            if (hotel != null && hotel.ProviderId == request.UserId) {
                var res = new GetHotelLandPageResponse();
                var tasks = new Task<decimal>[8];
                tasks[0] = repoBooking.GetTodayBooking(request.HotelId);
                tasks[1] = repoBooking.GetTodayArrival(request.HotelId);
                tasks[2] = repoBooking.GetTodayDeparture(request.HotelId);
                tasks[3] = repoReview.GetTodayReview(request.HotelId);
                tasks[4] = repoBooking.GetTotalBooking(request.HotelId);
                tasks[5] = repoTrans.GetTotalConfirmed(request.HotelId);
                tasks[6] = repoTrans.GetTotalCancelled(request.HotelId);
                tasks[7] = repoInvoices.GetPenddingInvoices(request.HotelId);

                var results = await Task.WhenAll(tasks);
                res.TodayBooking = results[0];
                res.TodayArrival = results[1];
                res.TodayDeparture = results[2];
                res.TodayReview = results[3];
                res.Booking = results[4];
                res.Confirmed = results[5];
                res.Cancelled = results[6];
                res.PendingInvoices = results[7];
                return res;
            }
            throw new CustomException("Invalid Hotel Id");
        }
    }
}
