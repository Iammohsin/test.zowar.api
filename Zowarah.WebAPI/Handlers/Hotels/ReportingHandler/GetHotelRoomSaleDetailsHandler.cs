﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Enums.Hotels;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Application.Services.Hotels;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    public class GetHotelRoomSaleDetailsRequest : IRequest<GetHotelRoomSaleDetailsResponse>
    {
        public long HodelId { get; set; }
        public int Room { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public ReportView View { get; set; }
        public long UserId { get; set; }
    }

    public class GetHotelRoomSaleDetailsResponse: HotelRoomSaleDetails
    {

    }
    public class GetHotelRoomSaleDetailsHandler : IRequestHandler<GetHotelRoomSaleDetailsRequest, GetHotelRoomSaleDetailsResponse>
    {
        private readonly IHotelService repo;

        public GetHotelRoomSaleDetailsHandler(IHotelService _repo)
        {
            repo = _repo;
        }
        public async Task<GetHotelRoomSaleDetailsResponse> Handle(GetHotelRoomSaleDetailsRequest request, CancellationToken cancellationToken)
        {
            var result =await repo.GetHotelRoomSaleDetails(request.HodelId,request.UserId, request.From, request.To, request.Room, request.View);
            var res = new GetHotelRoomSaleDetailsResponse();
            result.ToResponse(res);
            return res;
            
        }
    }
}
