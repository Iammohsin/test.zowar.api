﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class DeleteRoomsRequest : IRequest<Boolean> {
        public long Id { get; set; }
        public long UserId { get; set; }
    }
    public class DeleteRoomsRequestHandler : IRequestHandler<DeleteRoomsRequest, Boolean> {
        private readonly IRoomService _Repo;
        private readonly IUnitOfWork _uow;

        public DeleteRoomsRequestHandler(IRoomService Repo, IUnitOfWork uow) {
            _Repo = Repo;
            _uow = uow;
        }
        public async Task<Boolean> Handle(DeleteRoomsRequest request, CancellationToken cancellationToken) {
            var room = await _Repo.GetById(request.Id, true);
            if (room != null && room.Hotel.ProviderId == request.UserId) {
                _Repo.Delete(room);
                await _uow.SaveChangesAsync();
                return true;
            }
            return false;


        }
    }
}
