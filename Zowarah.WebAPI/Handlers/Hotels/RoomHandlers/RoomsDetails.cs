﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Text;
using Newtonsoft.Json;


namespace Zowarah.WebAPI.Handlers.Hotels
{
    public class RoomsDetails
    {
        public long Id { get; set; }
        public long HotelId { get; set; }
        public int RoomTypeId { get; set; }
        public int RoomTypeNameId { get; set; }
        public int SmokingOptionId { get; set; }
        public int NoOfRooms { get; set; }
        public Boolean IsLivingRoom { get; set; }
        public Boolean IsKitchen { get; set; }
        public int NoOfPathrooms { get; set; }
        public int NoOfPersons { get; set; }
        public Decimal RatePerNight { get; set; }    

        public Decimal TaxOnRate { get; set; }

        public Decimal VatOnRate { get; set; }
        public Decimal TotalRatePerNight { get; set; }

        public String RoomName { get; set; }

    }
}
