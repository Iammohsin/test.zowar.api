﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Text;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using Zowarah.WebAPI.Extensions;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using AutoMapper;
using Zowarah.Infrastructure.Config;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class UpdateRoomsRequest : RoomsDetails, IRequest<UpdateRoomsResponse> {
        public long UserId { get; set; }
    }
    public class UpdateRoomsResponse {
        public long Id { get; set; }
    }
    public class UpdateRoomsRequestHandler : IRequestHandler<UpdateRoomsRequest, UpdateRoomsResponse> {
        private readonly IRoomService _repo;
        private readonly IHotelService _hotelRepo;
        private readonly IUnitOfWork _uow;

        public UpdateRoomsRequestHandler(IRoomService repo, IHotelService hotelRepo, IUnitOfWork uow) {
            _hotelRepo = hotelRepo;
            _repo = repo;
            _uow = uow;
        }

        public async Task<UpdateRoomsResponse> Handle(UpdateRoomsRequest request, CancellationToken cancellationToken) {
            var room = await _repo.GetById(request.Id, true);
            if (room != null && room.Hotel.ProviderId == request.UserId) {
                var hotel = room.Hotel;                
                Mapper.Map(request, room);
                var hotelRooms = hotel.noOfRooms;
                var allocatedRooms = await _hotelRepo.GetAllocatedRooms(hotel.Id, room.NoOfRooms);
                if (allocatedRooms + request.NoOfRooms > hotel.noOfRooms)
                    throw new CustomException("Exceeding Hotel Rooms!!");
                room.TotalRatePerNight = room.RatePerNight + room.TaxOnRate;
                room.TaxOnRate = room.RatePerNight * hotel.TaxP / 100;
                room.TotalRatePerNight = room.RatePerNight + room.TaxOnRate;
                room.VatOnRate = room.TotalRatePerNight * hotel.Vat / 100;
                room.TotalRatePerNight = room.TotalRatePerNight + room.VatOnRate;
                _repo.Update(room);
                await _uow.SaveChangesAsync();
                return new UpdateRoomsResponse { Id = room.Id };
            }
            return null;
        }
    }
}
