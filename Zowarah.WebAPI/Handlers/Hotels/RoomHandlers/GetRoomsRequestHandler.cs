﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Hotels
{
    public class GetRoomsRequest : IRequest<GetRoomsResponse>
    {
        public long Id { get; set; }
    }
    public class GetRoomsResponse:RoomsDetails
    {
    }

    public class GetRoomsRequestHandler : IRequestHandler<GetRoomsRequest,GetRoomsResponse>
    {
        private readonly IRoomService _repo;
        private readonly IUnitOfWork _uow;

        public GetRoomsRequestHandler(IRoomService repo, IUnitOfWork uow)
        {
            _repo = repo;
            _uow = uow;
        }

        public async Task<GetRoomsResponse> Handle(GetRoomsRequest request, CancellationToken cancellationToken)
        {
            var result = await _repo.GetDetailsById(request.Id);

            if (result == null)
                return null;

            GetRoomsResponse response = new GetRoomsResponse();
            result.ToResponse(response);

            return await Task.FromResult(response);
        }
    }

}
