﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Text;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class CreateRoomsRequest : IRequest<CreateRoomsResponse> {
        public long Id { get; set; }
        public long HotelId { get; set; }
        public int RoomTypeId { get; set; }
        public int RoomTypeNameId { get; set; }
        public int SmokingOptionId { get; set; }
        public int NoOfRooms { get; set; }
        
        public Boolean IsLivingRoom { get; set; }
        public Boolean IsKitchen { get; set; }
        public int NoOfPathrooms { get; set; }
        public int NoOfPersons { get; set; }
        public Decimal RatePerNight { get; set; }
        public Decimal TotalRatePerNight { get; set; }
        public Decimal TaxOnRate { get; set; }
        public Decimal VatOnRate { get; set; }

        public String RoomTypeName { get; set; }
        public long UserId { get; set; }
    }

    public class CreateRoomsResponse {
        public long Id { get; set; }
    }
    public class CreateRoomsRequestHandler : IRequestHandler<CreateRoomsRequest, CreateRoomsResponse> {
        private readonly IHotelService _HotelRepo;
        private readonly IRoomService _RoomRepo;
        private readonly IUnitOfWork _uow;

        public CreateRoomsRequestHandler(
            IHotelService HotelRepo,
            IRoomService RoomRepo, IUnitOfWork uow) {
            _HotelRepo = HotelRepo;
            _RoomRepo = RoomRepo;
            _uow = uow;
        }
        //mohsin 24 jan
        public async Task<CreateRoomsResponse> Handle(CreateRoomsRequest request, CancellationToken cancellationToken) {
            var hotel = await _HotelRepo.GetById(request.HotelId);
            if (hotel != null && hotel.ProviderId == request.UserId) {
                var room = new Room();
                room = room.FromRequest(request);
                var hotelRooms = hotel.noOfRooms;
                var allocatedRooms = await _HotelRepo.GetAllocatedRooms(hotel.Id);
                if (allocatedRooms + request.NoOfRooms > hotel.noOfRooms)
                    throw new Exception("Exceeding Hotel Rooms!!");
                //mohsin tax calculate
                room.TotalRatePerNight = room.RatePerNight + room.TaxOnRate;               
                room.TaxOnRate = room.RatePerNight * hotel.TaxP / 100;                
                room.TotalRatePerNight = room.RatePerNight + room.TaxOnRate;
                room.VatOnRate = room.TotalRatePerNight * hotel.Vat / 100;
                room.TotalRatePerNight = room.TotalRatePerNight + room.VatOnRate;
                _RoomRepo.Create(room);
                hotel.IsRoomTypesConfigured = true;
                await _uow.SaveChangesAsync();
                return new CreateRoomsResponse { Id = room.Id };
            }
            return null;
        }
    }
}
