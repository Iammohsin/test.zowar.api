﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Microsoft.EntityFrameworkCore;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Interfaces;
using Zowarah.Application.Services.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels
{

    public class GetRoomsByHotelIdRequest : IRequest<GetRoomsByHotelIdResponse>
    {
        public long HotelId { get; set; }
    }

    public class GetRoomsByHotelIdResponse
    {
        public GetRoomsByHotelIdResponse()
        {
        }
        public IList<RoomsDetails> RoomsLst;
    }

    public class GetRoomsByHotelIdRequestHandler : IRequestHandler<GetRoomsByHotelIdRequest, GetRoomsByHotelIdResponse>
    {
        private readonly IRoomService _repo;
        private readonly IUnitOfWork _uow;

        public GetRoomsByHotelIdRequestHandler(IRoomService repo, IUnitOfWork uow)
        {
            _repo = repo;
            _uow = uow;
        }

        public async Task<GetRoomsByHotelIdResponse> Handle(GetRoomsByHotelIdRequest request, CancellationToken cancellationToken)
        {
                var lst = await _repo.GetByHotelId(request.HotelId).Result.ToListAsync();
                //Hotels.Result.AsParallel()

                if (lst == null)
                    return null;

                //Hotels = (IQueryable<HotelDesc>)Hotels;

                GetRoomsByHotelIdResponse response = new GetRoomsByHotelIdResponse();
                var lstResponse = Mapper.Map<IList<RoomQuery>, IList<RoomsDetails>>(lst);
                response.RoomsLst = lstResponse;
                
                return await Task.FromResult(response);
        }
    }
}
