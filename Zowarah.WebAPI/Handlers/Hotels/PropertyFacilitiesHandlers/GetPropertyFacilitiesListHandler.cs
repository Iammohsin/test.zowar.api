﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using AutoMapper;
using Zowarah.WebAPI.Extensions;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.WebAPI.Handlers.Hotels {
    public class GetFacilitiesListRequest : IRequest<GetFacilitiesListResponse> {
        public int Id { get; set; }
        public string Description { get; set; }
    }

    public class GetFacilitiesResponse {
        public int Id { get; set; }
        public int FacilityCategoryId { get; set; }
        public string Description { get; set; }

        public string Icon { get; set; }
    }

    public class GetFacilitiesListResponse {
        public GetFacilitiesListResponse() {
            Facilities = new List<GetFacilitiesResponse>();
        }

        public List<GetFacilitiesResponse> Facilities;
    }
    public class GetFacilitiesListRequestHandler : IRequestHandler<GetFacilitiesListRequest, GetFacilitiesListResponse> {
        private readonly IRepository<Facility> _Repo;
        private readonly IUnitOfWork _uow;

        public GetFacilitiesListRequestHandler(IRepository<Facility> Repo, IUnitOfWork uow) {
            _Repo = Repo;
            _uow = uow;
        }

        public async Task<GetFacilitiesListResponse> Handle(GetFacilitiesListRequest request, CancellationToken cancellationToken) {
            var lst = await _Repo.GetAll();
            if (lst == null)
                return null;

            return new GetFacilitiesListResponse() {
                Facilities = Mapper.Map<List<GetFacilitiesResponse>>(lst)
            };
        }
    }

}
