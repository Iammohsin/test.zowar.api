﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Invoices;
using Zowarah.Application.Interfaces.Transactions;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Interfaces;
using Zowarah.WebAPI.Config.Notification;
using Zowarah.WebAPI.Handlers.Common.NotificationsHandlers;
using Zowarah.WebAPI.Handlers.Hotels;
using static Zowarah.Identity.Api.Handlers.UserHandlers.GetProfile;

namespace Zowarah.WebAPI.Handlers.Transactions
{

    public class RemoveInvoicesRequest : IRequest<bool>
    {
    }

    public class RemoveInvoicesResponse
    {

    }

    public class RemoveInvoicesHandler : IRequestHandler<RemoveInvoicesRequest, bool>
    {
        private readonly IInvoiceService service;
        public RemoveInvoicesHandler(IInvoiceService _service)
        {
            service = _service;
        }

        public async Task<bool> Handle(RemoveInvoicesRequest request, CancellationToken cancellationToken)
        {
                await service.RemoveInvoices();
                return true;
        }
    }
}