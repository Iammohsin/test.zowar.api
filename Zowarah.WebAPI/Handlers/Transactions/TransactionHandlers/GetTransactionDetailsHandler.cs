﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Zowarah.WebAPI.Handlers.Transactions.TransactionHandlers
{
    public class GetTransactionDetailsRequest : IRequest<GetTransactionDetailsResponse>
    {
        public string Id { get; set; }
    }

    public class GetTransactionDetailsResponse
    {

    }


    public class GetTransactionDetailsHandler : IRequestHandler<GetTransactionDetailsRequest, GetTransactionDetailsResponse>
    {
        public async Task<GetTransactionDetailsResponse> Handle(GetTransactionDetailsRequest request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
