﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Transactions;
using Zowarah.Application.Services.Transactions;

namespace Zowarah.WebAPI.Handlers.Transactions.TransactionHandlers
{
    public class ProviderServiceTransactionListRequest : IRequest<ProviderServiceTransactionListResponse>
    {
        public long ProviderId { get; set; }

    }
    public class ProviderServiceTransactionListResponse
    {
        public ProviderServiceTransactionListResponse()
        {
            TransactionList = new List<GetServiceTransaction>();
        }
        public List<GetServiceTransaction> TransactionList { get; set; }
    }
    public class ProviderServiceTransactionListRequestHandler : IRequestHandler<ProviderServiceTransactionListRequest, ProviderServiceTransactionListResponse>
    {
        private readonly ITransactionService repo;
        public ProviderServiceTransactionListRequestHandler(ITransactionService _repo)
        {
            repo = _repo;
        }
        public async Task<ProviderServiceTransactionListResponse> Handle(ProviderServiceTransactionListRequest request, CancellationToken cancellationToken)
        {
            var result = await repo.GetAllProviderServiceTranactions(request.ProviderId);
            var res = new ProviderServiceTransactionListResponse();
            if (result != null)
            {
                res.TransactionList = result;
            }
            return res;
        }
    }
}
