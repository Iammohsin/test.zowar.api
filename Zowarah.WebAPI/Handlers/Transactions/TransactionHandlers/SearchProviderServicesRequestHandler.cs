﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Transactions;
using Zowarah.Application.Services.Transactions;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Transactions;

namespace Zowarah.WebAPI.Handlers.Transactions.TransactionHandlers
{
    public class SearchProviderServicesRequest : IRequest<SearchProviderServicesResponse>
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public long ProviderId { get; set; }
        public ServiceTypeEnum ServiceType { get; set; }
        public TransactionStatus Status { get; set; }


    }

    public class SearchProviderServicesResponse
    {
        public SearchProviderServicesResponse()
        {
            TransactionList = new List<GetServiceTransaction>();
        }
        public List<GetServiceTransaction> TransactionList { get; set; }
    }


    public class SearchProviderServicesRequestHandler : IRequestHandler<SearchProviderServicesRequest, SearchProviderServicesResponse>
    {
        private readonly ITransactionService repo;
        public SearchProviderServicesRequestHandler(ITransactionService _repo)
        {
            repo = _repo;
        }
        public async Task<SearchProviderServicesResponse> Handle(SearchProviderServicesRequest request, CancellationToken cancellationToken)
        {
            var result = await repo.SearchProviderServiceTranactions(
                request.ProviderId, request.ServiceType, request.Status, request.From, request.To);
            var res = new SearchProviderServicesResponse();
            if (result != null)
            {
                res.TransactionList = result;
            }
            return res;
        }
    }
}