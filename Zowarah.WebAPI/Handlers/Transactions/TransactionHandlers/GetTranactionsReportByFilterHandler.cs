﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Transactions;
using Zowarah.Application.Services.Transactions;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Transactions;

namespace Zowarah.WebAPI.Handlers.Transactions.TransactionHandlers {
    public class GetTranactionsReportByFilterRequest : IRequest<GetTranactionsReportByFilterResponse> {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        public string InvoiceId { get; set; }
        public long? ServiceId { get; set; }
        public long? ProviderId { get; set; }
        public long? ClientId { get; set; }
        public ServiceTypeEnum ServiceType { get; set; }
        public TransactionStatus Status { get; set; }
    }

    public class GetTranactionsReportByFilterResponse {
        public ICollection<TransactionReport> Transactions { get; set; } = new HashSet<TransactionReport>();
    }


    public class GetTranactionsReportByFilterHandler : IRequestHandler<GetTranactionsReportByFilterRequest, GetTranactionsReportByFilterResponse> {
        private readonly ITransactionService repo;
        public GetTranactionsReportByFilterHandler(ITransactionService _repo) {
            repo = _repo;
        }
        public async Task<GetTranactionsReportByFilterResponse> Handle(GetTranactionsReportByFilterRequest request, CancellationToken cancellationToken) {          
            return new GetTranactionsReportByFilterResponse {               
                Transactions = await repo.GetTranactionsReportByFilter(request.Page, request.PageSize, request.InvoiceId, request.ServiceId, request.ProviderId, request.ClientId, request.ServiceType, request.Status, request.From, request.To),
            };
        }
    }
}

