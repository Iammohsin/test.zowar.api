﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Transactions;
using Zowarah.Application.Services.Transactions;
using Zowarah.Domain.Entities.Transactions;

namespace Zowarah.WebAPI.Handlers.Transactions.TransactionHandlers
{
    public class GetAllBookingTransactionsRequest : IRequest<GetAllBookingTransactionsResponse>
    {
        public long ServiceId { get; set; }
        public long ClientId { get; set; }
        public String InvoiceId { get; set; }
        public int? ServiceCity { get; set; }
        public int? RoomTypeId { get; set; }
        public bool IsShowUp { get; set; }
        public TransactionStatus Status { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }

    public class GetAllBookingTransactionsResponse
    {
        public ICollection<BookingTransaction> Transactions { get; set; } = new List<BookingTransaction>();
    }
    public class GetAllBookingTransactionsHandler : IRequestHandler<GetAllBookingTransactionsRequest, GetAllBookingTransactionsResponse>
    {
        private readonly ITransactionService transactionService;

        public GetAllBookingTransactionsHandler(ITransactionService _transactionService)
        {
            this.transactionService = _transactionService;
        }
        public async Task<GetAllBookingTransactionsResponse> Handle(GetAllBookingTransactionsRequest request, CancellationToken cancellationToken)
        {
            var result = await transactionService.GetAllBooking(
                request.InvoiceId,
                request.ServiceId,
                request.ClientId,
                request.RoomTypeId,
                request.ServiceCity,
                request.Status,
                request.IsShowUp,
                request.From,
                request.To
                );

            var res = new GetAllBookingTransactionsResponse();
            res.Transactions = result;
            return res;

        }
    }
}
