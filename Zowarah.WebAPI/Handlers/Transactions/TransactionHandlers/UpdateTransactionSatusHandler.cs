﻿using Hangfire;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Application.Interfaces.Transactions;
using Zowarah.Application.Services.Hotels;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Transactions;
using Zowarah.Domain.Interfaces;
using Zowarah.Identity.Api.Handlers.UserHandlers;
using Zowarah.WebAPI.Config.Notification;
using Zowarah.WebAPI.Handlers.Common.NotificationsHandlers;

using Zowarah.WebAPI.Handlers.Hotels;
using Zowarah.WebAPI.Helpers.Users;
using static Zowarah.Identity.Api.Handlers.UserHandlers.GetProfile;

namespace Zowarah.WebAPI.Handlers.Transactions.TransactionHandlers {
    public class UpdateTransactionSatusRequest : IRequest<UpdateTransactionSatusResponse> {
        public string Id { get; set; }
        public string Comment { get; set; }
        public TransactionStatus Status { get; set; }
        public long? ProviderId { get; set; }
        public long? ClientId { get; set; }
    }

    public class UpdateTransactionSatusResponse {

    }


    public class UpdateTransactionSatusRequestHandler : IRequestHandler<UpdateTransactionSatusRequest, UpdateTransactionSatusResponse> {
        private readonly ITransactionService repo;
        private readonly IHotelService _repoHotel;
        private readonly IServiceService _repoService;
        private readonly IBookingService bookingService;
        private readonly IUnitOfWork uow;
        private readonly IMediator _mediator;
        public UpdateTransactionSatusRequestHandler(ITransactionService _repo, IHotelService repoHotel, IServiceService repoService, IBookingService _bookingService, IUnitOfWork _uow, IMediator mediator) {
            repo = _repo;
            _repoHotel = repoHotel;
            _repoService = repoService;
            bookingService = _bookingService;
            uow = _uow;
            _mediator = mediator;

        }
        public async Task<UpdateTransactionSatusResponse> Handle(UpdateTransactionSatusRequest request, CancellationToken cancellationToken) {
            var transaction = await repo.GetById(request.Id);
            if (transaction != null) {
                var service = await _repoService.GetById(transaction.ServiceId);
                // verify this request done by client or provider relevant to this transaction  
                if ((request.ClientId.HasValue && request.ClientId != transaction.ClientId)
                    || (request.ProviderId.HasValue && service.ProviderId != request.ProviderId))
                    throw new Exception("Invalid transaction!");
                else {
                    // new status should be higher than old status except cancel state
                    if (transaction.Status != TransactionStatus.Cancelled && transaction.Status < request.Status) {
                        switch (transaction.TransactionType) {
                            case TransactionType.BookingHotel:
                                await CahngeBookingTransaction(request, transaction);
                                break;
                        }
                    }
                }
            }
            return new UpdateTransactionSatusResponse { };
        }

        private async Task CahngeBookingTransaction(UpdateTransactionSatusRequest request, Transaction transaction) {
            var booking = await bookingService.GetBookingDetails(transaction.Id);
            if (booking != null) {
                switch (request.Status) {
                    case TransactionStatus.Cancelled:
                        await CancelBooking(transaction, booking);
                        break;
                    case TransactionStatus.NoShow:
                        await SetBookingNoShow(transaction, booking);
                        break;
                }
            } else
                throw new Exception("Invalid Booking!");
        }

        private async Task CancelBooking(Transaction transaction, BookingDetails booking) {
            if (transaction.Status > TransactionStatus.Confirmed)
                throw new Exception("Can't Cancel the current Booking!");
            var room = await _repoHotel.GetHotelRoom(booking.RoomId);
            // scenario 1
            if (!await CheckBookingRoomCancelPolicy(room, booking))
                throw new Exception("Can't Cancel Since the booking bypassed the Check-in Date !!");
            // scenario 2
            //if (!room.IsFreeCancellation)
            //{

            //    return new CancelBookingResponse() { status = false };
            //}
            // scenario 3 
            await UpdateStatus(transaction, TransactionStatus.Cancelled, "This Transaction has been Canceled");
            var service = await _repoService.GetById(booking.ServiceId);
            await SendMailNotification(CancelBookingTemplate.ClientTemplateId, service.Name, transaction.Email, transaction.FirstName, transaction, booking, room.IsFreeCancellation);
            var provider = await _mediator.Send(new GetProfileRequest() { Id = service.ProviderId });
            await SendMailNotification(CancelBookingTemplate.ProviderTemplateId, service.Name, provider.Email, provider.FirstName, transaction, booking, room.IsFreeCancellation);
            // return new CancelBookingResponse() { status = true };
        }

        private async Task<bool> CheckBookingRoomCancelPolicy(HotelRoomQuery room, BookingDetails booking) {
            if (room != null && room.CancellationPolicyId.HasValue) {
                var policies = await _mediator.Send(new GetCancellationPolicyListRequest());
                var policy = policies.CancellationPolicyList.Where(e => e.Id == room.CancellationPolicyId).FirstOrDefault();

                var dateofArrival = (booking.ArrivalDate == DateTime.MinValue) ? booking.CheckInDate :
                    booking.ArrivalDate;
                dateofArrival = dateofArrival.AddDays(policy.NumberOfDays);
                dateofArrival = dateofArrival.AddHours(policy.NumberOfHours);
                if ((dateofArrival > DateTime.Now))
                    return true;
            }
            return false;
        }

        private async Task SetBookingNoShow(Transaction transaction, BookingDetails booking) {
            // the booking in check-in time 
            var minToChcek = booking.CheckOutDate.Date.Subtract(booking.CheckInDate.Date).Days > 2 ? 2 : 0;
            if (DateTime.Now >= booking.CheckInDate && DateTime.Now <= booking.CheckOutDate.AddDays(minToChcek)) {
                await UpdateStatus(transaction, TransactionStatus.NoShow, "This Transaction has been NoShow");
                booking.Status = TransactionStatus.NoShow;
                await SendMailNotification(booking, "TransactionStatus");
                // return new UpdateTransactionSatusResponse();
            } else
                throw new Exception("Can't Use This Options, the date has bypassed the checkout date by 48 hours or the checkIn date has not reached !! ");
        }

        private async Task UpdateStatus(Transaction transaction, TransactionStatus status, string comment) {
            transaction.Status = status;
            transaction.Transactionlogs.Add(new TransactionStatusLog() {
                TransactionId = transaction.Id,
                Id = Guid.NewGuid().ToString(),
                Comment = comment,
                CreatedDate = DateTime.Now
            });
            repo.Update(transaction);
            await uow.SaveChangesAsync();
        }

        async Task SendMailNotification(BookingDetails booking, string templateId) {
            var template = await _mediator.Send(new GetMailTemplateRequest() { Id = templateId });
            var title = !string.IsNullOrEmpty(booking.Title) ? Enum.GetName(typeof(Titles), int.Parse(booking.Title)) : "";
            var status = booking.Status.ToString();
            var htmlContent = template.Template
                .Replace("$$title$$", title)
                .Replace("$$HotelName$$", booking.ServiceName)
                .Replace("$$FirstName$$", booking.FirstName)
                .Replace("$$Status$$", status);
            var mailRequest = new SendMailNotificationRequest() {
                To = booking.Email,
                DisplayName = booking.FirstName,
                Subject = booking.ServiceName,
                Content = htmlContent
            };
            BackgroundJob.Enqueue(() => SendMail(mailRequest));
        }
        private async Task SendMailNotification(string templateId, string hotel, string to, string displayName, Transaction transaction, BookingDetails booking, bool isFreeCancellation) {
            var template = await _mediator.Send(new GetMailTemplateRequest() { Id = templateId });
            var specialRequest = !string.IsNullOrEmpty(booking.SpecialRequest) ? booking.SpecialRequest : "";
            var title = !string.IsNullOrEmpty(transaction.Title) ? Enum.GetName(typeof(Titles), int.Parse(transaction.Title)) : "";
            var htmlContent = RenderHtmlContent(template.Template,
                title
                , booking.Id
                , transaction.Date
                , hotel
                , transaction.FirstName
                , transaction.LastName
                , transaction.Email
                , booking.CheckInDate
                , booking.CheckOutDate
                , booking.Rooms
                , specialRequest
                , booking.ArrivalDate)
                .Replace("$$free$$", isFreeCancellation ? CancelBookingTemplate.FreeMessage : "");

            var mailRequest = new SendMailNotificationRequest() {
                To = to,
                DisplayName = displayName,
                Subject = CancelBookingTemplate.Subject.Replace("$$HotelName$$", hotel),
                Content = htmlContent
            };
            BackgroundJob.Enqueue(() => SendMail(mailRequest));
        }



        private string RenderHtmlContent(string template, string title, string bookingId, DateTime date,
            string hotel, string firstName, string lastName, string email, DateTime checkInDate, DateTime checkOutDate,
            int rooms, string specialRequest, DateTime arrivalDate) {
            return template
                .Replace("$$Title$$", title)
                .Replace("$$BookingId$$", bookingId)
                .Replace("$$BookingDate$$", date.ToString("ddd - MMM dd yyyy"))
                .Replace("$$HotelName$$", hotel)
                .Replace("$$FirstName$$", firstName)
                .Replace("$$LastName$$", lastName)
                .Replace("$$Email$$", email)
                .Replace("$$CheckinDate$$", checkInDate.ToString("ddd - MMM dd yyyy"))
                .Replace("$$CheckoutDate$$", checkOutDate.ToString("ddd - MMM dd yyyy"))
                .Replace("$$NoOfRooms$$", rooms.ToString())
                .Replace("$$SpecialRequest$$", specialRequest)
                .Replace("$$Arrival Time$$", arrivalDate.ToString("hh:mm:ss"));
        }

        public async Task SendMail(SendMailNotificationRequest mailRequest) {
            await _mediator.Send(mailRequest);
        }
    }
}