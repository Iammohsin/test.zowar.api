﻿using MediatR;
using System;
using System.Threading.Tasks;
using System.Threading;
using Zowarah.Domain.Interfaces;
using Zowarah.WebAPI.Extensions;
using Zowarah.Application.Interfaces.Transactions;
using Zowarah.Domain.Entities.Transactions;
using Zowarah.Domain.Entities.Common;

namespace Zowarah.WebAPI.Handlers.Transactions.TransactionHandlers
{
    public class CreateTransactionRequest :IRequest<CreateTransactionResponse>
    {
        public long ServiceId { get; set; }
        public long ClientId { get; set; }
        public TransactionStatus Status { get; set; }
        public string ProviderCurrency { get; set; }
        public string ClientCurrency { get; set; }
        public decimal ProviderAmount { get; set; }
        public decimal ClientAmount { get; set; }
        public decimal ExchangeRate { get; set; }
        public int CountryId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Boolean IsPersonal { get; set; }
        public string RefereeName { get; set; }
        public string RefereeEmail { get; set; }
        public string Mobile { get; set; }
        public int CreditCardId { get; set; }
        public string CardNo { get; set; }
        public byte CardExpiryMonth { get; set; }
        public int Cvv { get; set; }  //mohsin27-2-2020

        public int CardExpiryYear { get; set; }
        public string SpecialRequest { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime Date { get; set; }
        public TransactionType TransactionType { get; set; }
        public string OwnerId { get; set; }
    }
    public class CreateTransactionResponse {
        public string Id { get; set; }
    }
    public class CreateTransactionRequestHandler : IRequestHandler<CreateTransactionRequest,CreateTransactionResponse>
    {
        private readonly IMediator mediator;
        private readonly ITransactionService repo;
        private readonly IRepository<Currency> curRepo;
        private readonly IUnitOfWork uow;
        public CreateTransactionRequestHandler(IMediator _mediator, IRepository<Currency> _curRepo, ITransactionService _transService,IUnitOfWork _uow ) {
            mediator = _mediator;
            repo = _transService;
            uow = _uow;
            curRepo = _curRepo;
        }
        public async Task<CreateTransactionResponse> Handle(CreateTransactionRequest request, CancellationToken cancellationToken) {

            var trans = new Transaction();
            trans.Id = Guid.NewGuid().ToString();
            request.Date = DateTime.Now.Date;
            trans = trans.FromRequest(request);
            trans.BaseAmount = trans.ProviderAmount * (await curRepo.GetById(trans.ProviderCurrency)).ExchangeRate;
            trans.Transactionlogs.Add(new TransactionStatusLog()
            {
                TransactionId= trans.Id,
                Id = Guid.NewGuid().ToString(),
                Comment = "Create new Transaction",
                CreatedDate = DateTime.Now
            });
            repo.Create(trans);
            await uow.SaveChangesAsync();
            return new CreateTransactionResponse() { Id = trans.Id };
        }
    }
}
