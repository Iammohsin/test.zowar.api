﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Transactions;
using Zowarah.Application.Services.Transactions;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Transactions;

namespace Zowarah.WebAPI.Handlers.Transactions.TransactionHandlers
{    
    public class SearchClientServicesRequest : IRequest<SearchClientServicesResponse>
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public long ClientId { get; set; }
        public ServiceTypeEnum ServiceType { get; set; }
        public TransactionStatus Status { get; set; }

        
    }
   
    public class SearchClientServicesResponse
    {
        public SearchClientServicesResponse()
        {
            TransactionList = new List<GetServiceTransaction>();
        }
        public List<GetServiceTransaction> TransactionList { get; set; }
    }

   
    public class SearchClientServicesRequestHandler : IRequestHandler<SearchClientServicesRequest, SearchClientServicesResponse>
    {
        private readonly ITransactionService repo;
        public SearchClientServicesRequestHandler(ITransactionService _repo) {
            repo = _repo;
        }
        public async Task<SearchClientServicesResponse> Handle(SearchClientServicesRequest request, CancellationToken cancellationToken)
        {
            var result = await repo.SearchClientServiceTranactions(
                request.ClientId,request.ServiceType, request.Status,request.From,request.To);
            var res = new SearchClientServicesResponse();
            if (result != null)
            {
                res.TransactionList = result;
            }
            return res;
        }
    }
}