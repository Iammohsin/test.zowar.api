﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Transactions;
using Zowarah.Application.Services.Transactions;
using Zowarah.Domain.Entities.Hotels;

namespace Zowarah.WebAPI.Handlers.Transactions.TransactionHandlers
{
    public class ClientServiceTransactionListRequest : IRequest<ClientServiceTransactionListResponse> {
        public long ClientId { get; set; }
        public BookingFilterEnum BookingFilterId { get; set; }
    }
    public class ClientServiceTransactionListResponse
    {
        public ClientServiceTransactionListResponse()
        {
            TransactionList = new List<GetServiceTransaction>();
        }
        public List<GetServiceTransaction> TransactionList { get; set; }
    }
    public class ClientServiceTransactionListRequestHandler : IRequestHandler<ClientServiceTransactionListRequest, ClientServiceTransactionListResponse>
    {
        private readonly ITransactionService repo;
        public ClientServiceTransactionListRequestHandler(ITransactionService _repo) {
            repo = _repo;
        }
        public async Task<ClientServiceTransactionListResponse> Handle(ClientServiceTransactionListRequest request, CancellationToken cancellationToken)
        {
            var result = await repo.GetAllClientServiceTranactions(request.ClientId,request.BookingFilterId);
            var res = new ClientServiceTransactionListResponse();
            if (result != null)
            {
                res.TransactionList = result;
            }
            return res;
        }
    }
}
