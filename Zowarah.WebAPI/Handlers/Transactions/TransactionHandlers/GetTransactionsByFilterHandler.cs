﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Transactions;
using Zowarah.Application.Services.Transactions;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Transactions;

namespace Zowarah.WebAPI.Handlers.Transactions.TransactionHandlers {
    public class GetTransactionsByFilterRequest : IRequest<GetTransactionsByFilterResponse> {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        public string InvoiceId { get; set; }
        public long? ServiceId { get; set; }
        public long? ProviderId { get; set; }
        public long? ClientId { get; set; }
        public ServiceTypeEnum ServiceType { get; set; }
        public TransactionStatus Status { get; set; }
    }

    public class GetTransactionsByFilterResponse {
        public GetTransactionsByFilterResponse() {
            Transactions = new List<BasicTransactionInfo>();
        }
        public int Total { get; set; }
        public ICollection<BasicTransactionInfo> Transactions { get; set; }
    }


    public class GetTransactionsByFilterHandler : IRequestHandler<GetTransactionsByFilterRequest, GetTransactionsByFilterResponse> {
        private readonly ITransactionService repo;
        public GetTransactionsByFilterHandler(ITransactionService _repo) {
            repo = _repo;
        }
        public async Task<GetTransactionsByFilterResponse> Handle(GetTransactionsByFilterRequest request, CancellationToken cancellationToken) {
            var (total, transactions) = await repo.GetAllTranactionsByFilter(request.Page, request.PageSize, request.InvoiceId, request.ServiceId, request.ProviderId, request.ClientId, request.ServiceType, request.Status, request.From, request.To);
            return new GetTransactionsByFilterResponse {
                Total =total,
                Transactions =transactions,
            };
        }
    }
}
