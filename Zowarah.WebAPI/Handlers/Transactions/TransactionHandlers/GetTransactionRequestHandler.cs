﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Transactions;
using Zowarah.Application.Services.Transactions;
using Zowarah.Domain.Entities.Transactions;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Transactions.TransactionHandlers
{
    public class GetTransactionRequest : IRequest<GetTransactionResponse>
    {

        public string Id { get; set; }
    }
    public class GetTransactionResponse : Transaction
    {
    }
    public class GetTransactionRequestHandler : IRequestHandler<GetTransactionRequest, GetTransactionResponse>
    {
        private readonly ITransactionService repo;
        public GetTransactionRequestHandler(ITransactionService _repo)
        {
            repo = _repo;
        }

        public async Task<GetTransactionResponse> Handle(GetTransactionRequest request, CancellationToken cancellationToken)
        {
            var result = await repo.GetById(request.Id);
            if (result != null)
            {
                var res = new GetTransactionResponse();
                result.ToResponse(res);
                return res;
            }
            return null;

        }
    }
}
