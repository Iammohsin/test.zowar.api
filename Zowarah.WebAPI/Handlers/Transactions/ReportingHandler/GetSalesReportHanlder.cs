﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Enums.Hotels;
using Zowarah.Application.Interfaces.Transactions;
using Zowarah.Application.Services.Transactions;
using Zowarah.Domain.Entities.Common;

namespace Zowarah.WebAPI.Handlers.Transactions
{

    public class GetSalesReportRequest : IRequest<GetSalesReportResponse>
    {
        public long ClientId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public ServiceTypeEnum ServiceType { get; set; }
        public int City { get; set; }
        public ReportView View { get; set; }
    }

    public class GetSalesReportResponse
    {
        public ICollection<SaleReporting> Sales { get; set; } = new HashSet<SaleReporting>();
    }
    public class GetSalesReportHandler : IRequestHandler<GetSalesReportRequest, GetSalesReportResponse>
    {
        private readonly ITransactionService repo;

        public GetSalesReportHandler(ITransactionService repo)
        {
            this.repo = repo;
        }
        public async Task<GetSalesReportResponse> Handle(GetSalesReportRequest request, CancellationToken cancellationToken)
        {
            return new GetSalesReportResponse
            {
                Sales = await repo.GetSaleReporting(request.ClientId,request.From, request.To,request.ServiceType, request.City, request.View)
            };
        }
    }
}
