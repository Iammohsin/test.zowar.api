﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Enums.Hotels;
using Zowarah.Application.Interfaces.Transactions;
using Zowarah.Application.Services.Transactions;
using Zowarah.Domain.Entities.Common;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Transactions
{
 
    public class GetSalesReportDetailsRequest : IRequest<GetSalesReportDetailsResponse>
    {
        public long ClientId { get; set; }
        public DateTime Date { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public ServiceTypeEnum ServiceType { get; set; }
        public int City { get; set; }
        public ReportView View { get; set; }
    }

    public class GetSalesReportDetailsResponse: SaleReporting
    {
      
    }
    public class GetSalesReportDetailsHandler : IRequestHandler<GetSalesReportDetailsRequest, GetSalesReportDetailsResponse>
    {
        private readonly ITransactionService repo;

        public GetSalesReportDetailsHandler(ITransactionService repo)
        {
            this.repo = repo;
        }
        public async Task<GetSalesReportDetailsResponse> Handle(GetSalesReportDetailsRequest request, CancellationToken cancellationToken)
        {
            var result = await repo.GetSaleReportingDetails(request.ClientId,request.Date,request.From, request.To, request.ServiceType, request.City, request.View);
            var res = new GetSalesReportDetailsResponse();
            result.ToResponse(res);
            return res;
        }
    }
}
