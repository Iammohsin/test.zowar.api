﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Entities.Common;

namespace Zowarah.WebAPI.Handlers.Transactions
{
    public class GetPendingApproveRequest : IRequest<GetPendingApproveResponse>
    {

    }

    public class GetPendingApproveResponse
    {
        public int NewRequestsCount { get; set; }
        public int InProgressRequestsCount { get; set; }
    }

    public class GetPendingApproveRequestsHandler : IRequestHandler<GetPendingApproveRequest, GetPendingApproveResponse>
    {
        public IRepository<Service> _serviceRepo { get; set; }
        public IUnitOfWork _uow { get; set; }

        public GetPendingApproveRequestsHandler(IRepository<Service> serviceRepo, IUnitOfWork uow)
        {
            _serviceRepo = serviceRepo;
            _uow = uow;
        }

        public async Task<GetPendingApproveResponse> Handle(GetPendingApproveRequest request, CancellationToken cancellationToken)
        {
            var services = await _serviceRepo.GetAll();
            var newRequestsCount = services.Where(x => x.Status == ServiceStatusEnum.Submitted).Count();
            var inProgressRequestsCount = services.Where(x => x.Status == ServiceStatusEnum.UnderReview).Count();
            var getPendingApproveResponse = new GetPendingApproveResponse
            {
                NewRequestsCount = newRequestsCount,
                InProgressRequestsCount = inProgressRequestsCount
            };
            return getPendingApproveResponse;
        }
    }
}
