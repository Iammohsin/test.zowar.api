﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Transactions
{
    public class GetRequestsRequest : IRequest<GetRequestsResponse>
    {
        public int PageNumber { get; set; }
        public int PageSize => 3;
        public int CityId { get; set; }
        public ServiceTypeEnum ServiceType { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        public int PastWeekNumber { get; set; }
        public bool IsCustomDate { get; set; }
    }

    public class GetRequestsResponseItem
    {
        public string Id { get; set; }
        public ServiceTypeEnum ServiceType { get; set; }
        public int CityId { get; set; }
        public string Name { get; set; }
        public ServiceStatusEnum Status { get; set; }
    }

    public class GetRequestsResponse
    {
        public List<GetRequestsResponseItem> Items { get; set; }
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }
    }

    public class GetRequestsHandler : IRequestHandler<GetRequestsRequest, GetRequestsResponse>
    {
        public IRepository<Service> _serviceRepo { get; set; }
        public IUnitOfWork _uow { get; set; }

        public GetRequestsHandler(IRepository<Service> serviceRepo, IUnitOfWork uow)
        {
            _serviceRepo = serviceRepo;
            _uow = uow;
        }
        public async Task<GetRequestsResponse> Handle(GetRequestsRequest request, CancellationToken cancellationToken)
        {
            var services = await _serviceRepo.GetAll();

            if (!request.IsCustomDate)
            {
                request.From = DateTime.Today.AddDays(-(request.PastWeekNumber * 7));
                request.To = DateTime.Today;
            }

            services = services.Where(x => (
            x.Status == ServiceStatusEnum.Submitted || 
            x.Status == ServiceStatusEnum.UnderReview 
            ) 
            && x.CreatedDate >= request.From 
            && x.CreatedDate <= request.To).ToList();

            if (request.CityId != 0)
            {
                services = services.Where(x => x.CityId == request.CityId).ToList();
            }

            if (request.ServiceType !=0)
            {
                services = services.Where(x => x.ServiceType == request.ServiceType).ToList();
            }

            int count = services.Count();
            int CurrentPage = request.PageNumber;
            int PageSize = request.PageSize;
            int TotalCount = count;

            // Calculating Totalpage by Dividing (No of Records / Pagesize)  
            int totalPages = (int)Math.Ceiling(count / (double)PageSize);

            // Returns List of Customer after applying Paging   
            var items = services.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToList();

            var pagedItems = new List<GetRequestsResponseItem>();
            items.ToResponse(pagedItems);

            // Object which we are going to send in header   
            var response = new GetRequestsResponse
            {
                Items = pagedItems,
                CurrentPage = CurrentPage,
                TotalPages = totalPages
            };
            return response;
        }
    }
}
