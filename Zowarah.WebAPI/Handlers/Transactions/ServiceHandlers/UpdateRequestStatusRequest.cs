﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Common;
using MediatR;
using Zowarah.Domain.Interfaces;
using System.Threading;
using Zowarah.WebAPI.Handlers.Common.NotificationsHandlers;

namespace Zowarah.WebAPI.Handlers.Transactions
{
    public class UpdateRequestStatusRequest : IRequest<bool>
    {
        public long ServiceId { get; set; }
        public ServiceStatusEnum ServiceStatus { get; set; }
        public string Comments { get; set; }
    }

    public class UpdateRequestStatusResponse
    {
    }

    public class UpdateRequestStatusRequestHandler : IRequestHandler<UpdateRequestStatusRequest, bool>
    {
        private IRepository<Service> _serviceRepo { get; set; }
        private readonly IUnitOfWork _uow;
        private readonly IMediator mediator;

        public UpdateRequestStatusRequestHandler(IMediator _mediator, IRepository<Service> serviceRepo, IUnitOfWork uow)
        {
            _serviceRepo = serviceRepo;
            _uow = uow;
            mediator = _mediator;
        }

        public async Task<bool> Handle(UpdateRequestStatusRequest request, CancellationToken cancellationToken)
        {
            var service = await _serviceRepo.GetById(request.ServiceId);
            service.Status = request.ServiceStatus;
            service.AddServiceStatusLog(new ServiceStatusLog { Status = service.Status });
            _serviceRepo.Update(service);
            var approvedSuccessfully = await _uow.SaveChangesAsync();
            if (approvedSuccessfully >= 1)
            {
                await sendMailNotification(service);
                return true;
            }
            return false;
        }
        async Task sendMailNotification(Service service)
        {
            await mediator.Send(new SendServiceStatusNotificationRequest()
            {
                Name = service.Name,
                ProviderId = service.ProviderId,
                Status = service.Status
            });
        }
    }
}
