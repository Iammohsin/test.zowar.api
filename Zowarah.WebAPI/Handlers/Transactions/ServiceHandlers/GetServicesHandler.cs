﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Services.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Transactions
{
    public class GetServicesRequest : IRequest<GetServicesResponse>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int? City { get; set; }
        public ServiceTypeEnum ServiceType { get; set; }
        public ServiceStatusEnum Status { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        public string InvoiceId { get; set; }
        public long ServiceId { get; set; }
        public long ProviderId { get; set; }
    }


    public class GetServicesResponse : ServicePage
    {

    }
    public class GetServicesHandler : IRequestHandler<GetServicesRequest, GetServicesResponse>
    {
        public IServiceService _serviceRepo { get; set; }
        public GetServicesHandler(IServiceService serviceRepo)
        {
            _serviceRepo = serviceRepo;
        }
        public async Task<GetServicesResponse> Handle(GetServicesRequest request, CancellationToken cancellationToken)
        {
            var result = await _serviceRepo.GetServices(request.PageNumber,
                request.PageSize,
                request.City,
                request.ServiceType,
                request.Status,
                request.From,
                request.To,
                request.ServiceId,
                request.ProviderId);
            var res = new GetServicesResponse();
            result.ToResponse(res);
            return res;
        }
    }
}
