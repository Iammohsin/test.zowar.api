﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Interfaces;
using Zowarah.WebAPI.Config.Notification;
using Zowarah.WebAPI.Handlers.Common.NotificationsHandlers;
using Zowarah.WebAPI.Handlers.Hotels;
using static Zowarah.Identity.Api.Handlers.UserHandlers.GetProfile;

namespace Zowarah.WebAPI.Handlers.Transactions
{

    public class ApproveRequestRequest : IRequest<bool>
    {
        public long ServiceId { get; set; }
        public string Comments { get; set; }
    }

    public class ApproveRequestResponse
    {

    }
    public class ApproveRequestHandler : IRequestHandler<ApproveRequestRequest, bool>
    {
        private IRepository<Service> _serviceRepo { get; set; }
        private readonly IUnitOfWork _uow;
        private readonly Mediator _mediator;

        public ApproveRequestHandler(Mediator mediator,IRepository<Service> serviceRepo, IUnitOfWork uow)
        {
            _serviceRepo = serviceRepo;
            _uow = uow;
            mediator = _mediator;
        }

        public async Task<bool> Handle(ApproveRequestRequest request, CancellationToken cancellationToken)
        {
            var service = await _serviceRepo.GetById(request.ServiceId);
            service.Status = ServiceStatusEnum.Approved;
            service.AddServiceStatusLog(new ServiceStatusLog { Status = service.Status });
            _serviceRepo.Update(service);
            var approvedSuccessfully = await _uow.SaveChangesAsync();
            if (approvedSuccessfully >= 1)
            {
              await  sendMailNotification(service);
                return true;
            }
            return false;
        }
        async Task sendMailNotification(Service service)
        {
            await _mediator.Send(new SendServiceStatusNotificationRequest()
            {               
                Name = service.Name,
                ProviderId = service.ProviderId,
                Status = service.Status
            });
        }
    }
}