﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Common
{
    public class SearchTestRequest : IRequest<SearchTestResponse>
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class SearchTestResponse
    {
       
    }
    public class SearchTestRequestHandler : IRequestHandler<SearchTestRequest, SearchTestResponse>
    {
        private readonly IHotelService repo;
        private readonly IUnitOfWork _uow;

        public SearchTestRequestHandler(IHotelService _repo, IUnitOfWork uow)
        {
            repo = _repo;
        }

        public async Task<SearchTestResponse> Handle(SearchTestRequest request, CancellationToken cancellationToken)
        {
            try
            {
                var lst = await repo.GetAll();
                if (lst == null)
                    return null;

                SearchTestResponse response = new SearchTestResponse();
             //   var responseLst = Mapper.Map<IList<City>, IList<GetCityResponse>>(lst);

                //lst.ToResponse(response);
                return await Task.FromResult(response);
            } catch (Exception ex)
            {
                return null;
            }
        }
    }
}