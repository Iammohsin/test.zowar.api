﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Queries.Common;

namespace Zowarah.WebAPI.Handlers.Common.MessagingReportHandlers
{
    public class GetAllRequest : IRequest<GetAllResponse>
    {
    }

    public class GetAllResponse
    {
        public IEnumerable<MessageQuery> messages { get; set; }
    }

    public class GetAllRequestHandler : IRequestHandler<GetAllRequest, GetAllResponse>
    {
        private readonly IMessagingReportService messagingReportService;

        public GetAllRequestHandler(IMessagingReportService _messagingReportService)
        {
            messagingReportService = _messagingReportService;
        }

        public async Task<GetAllResponse> Handle(GetAllRequest request, CancellationToken cancellationToken)
        {
            var result = messagingReportService.GetAll();
            var response = new GetAllResponse();
            response.messages = result;
            return await Task.FromResult(response);
        }
    }
}