﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Queries.Common;

namespace Zowarah.WebAPI.Handlers.Common.MessagingReportHandlers
{
    public class GetMessagesReportsRequest : IRequest<GetMessagesReportsResponse>
    {
        public RoleTypes role { get; set; }
    }

    public class GetMessagesReportsResponse
    {
        public IEnumerable<MessageQuery> messages { get; set; }
    }

    public class GetMessagesReportsRequestHandler : IRequestHandler<GetMessagesReportsRequest, GetMessagesReportsResponse>
    {
        private readonly IMessagingReportService messagingReportService;

        public GetMessagesReportsRequestHandler(IMessagingReportService _messagingReportService)
        {
            messagingReportService = _messagingReportService;
        }

        public async Task<GetMessagesReportsResponse> Handle(GetMessagesReportsRequest request, CancellationToken cancellationToken)
        {
            var result = messagingReportService.GetMessages(request.role);
            var response = new GetMessagesReportsResponse();
            response.messages = result;
            return await Task.FromResult(response);
        }
    }
}