﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Common {
    public class GetCreditCardListRequest : IRequest<GetCreditCardListResponse> {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetCreditCardResponse {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetCreditCardListResponse {
        public GetCreditCardListResponse() {
            CreditCardList = new List<GetCreditCardResponse>();
        }

        public List<GetCreditCardResponse> CreditCardList;
    }

    public class GetCreditCardListRequestHandler : IRequestHandler<GetCreditCardListRequest, GetCreditCardListResponse> {
        private readonly ILookupRepository<CreditCard> _CreditCardRepo;
        private readonly IUnitOfWork _uow;

        public GetCreditCardListRequestHandler(ILookupRepository<CreditCard> CreditCardRepo, IUnitOfWork uow) {
            _CreditCardRepo = CreditCardRepo;
            _uow = uow;
        }

        public async Task<GetCreditCardListResponse> Handle(GetCreditCardListRequest request, CancellationToken cancellationToken) {
            var lst = await _CreditCardRepo.GetAll();
            if (lst == null)
                return null;

            return new GetCreditCardListResponse {
                CreditCardList = Mapper.Map<List<GetCreditCardResponse>>(lst)
            };

        }
    }
}