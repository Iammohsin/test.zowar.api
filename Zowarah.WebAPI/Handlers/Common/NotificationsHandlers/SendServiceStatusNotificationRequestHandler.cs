﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Common;
using Zowarah.WebAPI.Config.Notification;
using static Zowarah.Identity.Api.Handlers.UserHandlers.GetProfile;

namespace Zowarah.WebAPI.Handlers.Common.NotificationsHandlers
{

    public class SendServiceStatusNotificationRequest : IRequest<SendServiceStatusNotificationResponse>
    {
       
        public ServiceStatusEnum Status { get; set; }
        public long ProviderId { get; set; }
        public string Name { get; set; }
    }
    public class SendServiceStatusNotificationResponse
    {

    }
    public class SendServiceStatusNotificationRequestHandler : IRequestHandler<SendServiceStatusNotificationRequest, SendServiceStatusNotificationResponse>
    {
        private readonly IMediator _mediator;

        public SendServiceStatusNotificationRequestHandler(IMediator mediator)
        {
            _mediator = mediator;
        }
        public async Task<SendServiceStatusNotificationResponse> Handle(SendServiceStatusNotificationRequest request, CancellationToken cancellationToken)
        {
            var status = Enum.GetName(typeof(ServiceStatusEnum), request.Status);
            var provider = await _mediator.Send(new GetProfileRequest { Id = request.ProviderId });
            var template = await _mediator.Send(new GetMailTemplateRequest() { Id = ServiceStatusNotification.TemplateId });
            var htmlContent = template.Template
                .Replace("$$PropertyName$$", request.Name)
                .Replace("$$ServiceStatus$$", status)
                .Replace("$$ProviderName$$", provider.FirstName);
            await _mediator.Send(new SendMailNotificationRequest()
            {
                To = provider.Email,
                DisplayName = provider.FirstName,
                Subject = request.Name,
                Content = htmlContent
            });
            return new SendServiceStatusNotificationResponse();
        }

    }
}
