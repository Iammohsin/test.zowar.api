﻿using MediatR;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.WebAPI.Config.AppSettings;
using Microsoft.Extensions.Options;
using System.Net.Mail;
using System.Net;

namespace Zowarah.WebAPI.Handlers.Common.NotificationsHandlers
{

    public class SendMailNotificationRequest : IRequest<SendMailNotificationResponse>
    {
        internal object attachment;

        public string To { get; set; }
        public string Subject { get; set; }
        public string DisplayName { get; set; }
        public string Content { get; set; }

        public string AttachmentPath { get; set; }
    }

    public class SendMailNotificationResponse
    {
        public Boolean Status { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class SendMailNotificationRequestHandler : IRequestHandler<SendMailNotificationRequest, SendMailNotificationResponse>
    {
        private readonly IMediator _mediator;
        private readonly AppSettings _AppSettings;

        public SendMailNotificationRequestHandler(IMediator mediator, IOptions<AppSettings> settings)
        {
            _mediator = mediator;
            _AppSettings = settings.Value;
        }

        public async Task<SendMailNotificationResponse> Handle(SendMailNotificationRequest request, CancellationToken cancellationToken)
        {
            SendBySmtp(request);
            var response = new SendMailNotificationResponse
            {
                Status = true,
                ErrorMessage = ""
            };
            return response;
        }

        private async Task<SendMailNotificationResponse> SendBySmtp(SendMailNotificationRequest request)
        {
            try
            {
                var url = _AppSettings.GeneralSettings.Url;
                var from = new MailAddress(_AppSettings.MailSettings.EmailAddress, _AppSettings.MailSettings.DisplayName);
                var subject = request.Subject;
                var to = new MailAddress(request.To, request.DisplayName);
                var plainTextContent = request.Content;
                var template = await this._mediator.Send(new GetMailTemplateRequest() { Id = _AppSettings.MailSettings.MainTemplateId });
                var htmlContent = template.Template
                                 .Replace("$$title$$", request.Subject)
                                 .Replace("$$url$$", url)
                                 .Replace("$$planText$$", request.Content)
                                 .Replace("$$mail$$", request.To);
                /// Godaddy smtp by office365
                /// we use email info@zowarah.com
                try
                {
                    MailMessage mail = new MailMessage();
                    mail.To.Add(to);
                    mail.From = from;
                    //mail.From = new MailAddress("noreply@intellectsoftsol.com");
                    mail.Subject = request.Subject;
                    mail.Body = htmlContent;
                    mail.IsBodyHtml = true;
                    //mail.Attachments.Add(new System.Net.Mail.Attachment(request.AttachmentPath));
                    SmtpClient smtpClient = new SmtpClient();
                    //smtpClient.Host = "SMTP.office365.com";
                    //smtpClient.Host = "smtp.mediu.edu.my";
                    smtpClient.Host = _AppSettings.MailSettings.Host;
                    // set timeout to 10 minuts
                    smtpClient.Timeout = 600000;
                    smtpClient.EnableSsl = _AppSettings.MailSettings.EnableSSL;
                    smtpClient.Port = _AppSettings.MailSettings.Port;
                    smtpClient.UseDefaultCredentials = false;
                    //smtpClient.Credentials = new NetworkCredential(_AppSettings.MailSettings.EmailAddress, "Gate123456#", "zowarah.com");
                    //smtpClient.Credentials = new NetworkCredential(_AppSettings.MailSettings.EmailAddress, "TraininG2019*", "training@mediu.edu.my");
                    //smtpClient.Credentials = new NetworkCredential("noreply@intellectsoftsol.com", "Intellect$2048*", "noreply@intellectsoftsol.com");
                    smtpClient.Credentials = new NetworkCredential(_AppSettings.MailSettings.UserName, _AppSettings.MailSettings.Password);
                    smtpClient.Send(mail);

                    var response = new SendMailNotificationResponse
                    {
                        Status = true,
                        ErrorMessage = ""
                    };
                    return response;
                }
                catch (Exception ex)
                {

                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        private async void SendGrid(SendMailNotificationRequest request)
        {
            // var apiKey = Environment.GetEnvironmentVariable("SENDGRID_APIKEY");
            var client = new SendGridClient(_AppSettings.MailSettings.MailApi);
            var from = new EmailAddress(_AppSettings.MailSettings.EmailAddress, _AppSettings.MailSettings.DisplayName);
            var subject = request.Subject;
            var to = new EmailAddress(request.To, request.DisplayName);
            var plainTextContent = request.Content;
            var template = await _mediator.Send(new GetMailTemplateRequest() { Id = _AppSettings.MailSettings.MainTemplateId });
            var htmlContent = template.Template.Replace("$$title$$", request.Subject).Replace("$$planText$$", request.Content).Replace("$$mail$$", request.To);
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var responsemail = await client.SendEmailAsync(msg);
        }
    }
}