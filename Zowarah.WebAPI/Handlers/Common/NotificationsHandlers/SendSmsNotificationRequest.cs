﻿using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.WebAPI.Config.AppSettings;

namespace Zowarah.WebAPI.Handlers.Common.NotificationsHandlers
{

    public class SendSmsNotificationRequest : IRequest<SendSmsNotificationResponse>
    {
        public string Mobile { get; set; }
        public string Content { get; set; }

    }
    public class SendSmsNotificationResponse
    {
        public Boolean Status { get; set; }
        public string ErrorMessage { get; set; }
    }
    public class SendSmsNotificationHandler : IRequestHandler<SendSmsNotificationRequest, SendSmsNotificationResponse>
    {

        private readonly IMediator _mediator;
        private readonly AppSettings _AppSettings;

        public SendSmsNotificationHandler(IMediator mediator, IOptions<AppSettings> settings)
        {
            _mediator = mediator;
            _AppSettings = settings.Value;
        }
            public async Task<SendSmsNotificationResponse> Handle(SendSmsNotificationRequest request, CancellationToken cancellationToken)
        {
            Dictionary<string, string> Params = new Dictionary<string, string>();
            Params.Add("content", request.Content);
            Params.Add("to", request.Mobile);

            var responseSMs = await SendSMS(Params);
            dynamic jsonResponse = JObject.Parse(responseSMs);
            // JsonConvert.DeserializeObject(responseSMs);
            var response = new SendSmsNotificationResponse
            {
                Status = jsonResponse.messages[0].Accepted,
                ErrorMessage = ""
            };
            return response;
        }

        //This function is in charge of converting the data into a json array and sending it to the rest sending controller.
        private async Task<string> SendSMS(Dictionary<string, string> Params)
        {
            Params["to"] = CreateRecipientList(Params["to"]);
            string JsonArray = JsonConvert.SerializeObject(Params, Formatting.None);
            JsonArray = JsonArray.Replace("\\\"", "\"").Replace("\"[", "[").Replace("]\"", "]");
            return await Post(JsonArray);
        }
        //This function converts the recipients list into an array string so it can be parsed correctly by the json array.

        string CreateRecipientList(string to)
        {
            string[] tmp = to.Split(',');
            to = "[\"";
            to = to + string.Join("\",\"", tmp);
            to = to + "\"]";
            return to;
        }
        async Task<string> Post(string json)
        {
            string Token = _AppSettings.SMSSettings.ApiKey;
            // "pEQaoY_xRuuwK_B7P8dBVw==";
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(_AppSettings.SMSSettings.ApiLink);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Accept = "application/json";
            httpWebRequest.PreAuthenticate = true;
            httpWebRequest.Headers.Add("Authorization", Token);

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = await httpWebRequest.GetResponseAsync();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                return result;
            }
        }
    }
}

