﻿using MediatR;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Zowarah.WebAPI.Handlers.Common.NotificationsHandlers
{
    public class GetMailTemplateRequest : IRequest<GetMailTemplateResponse>
    {
        public string Id { get; set; }
    }
    public class GetMailTemplateResponse
    {
        public string Template { get; set; }
    }
    public class GetMailTemplateRequestHandler : IRequestHandler<GetMailTemplateRequest, GetMailTemplateResponse>
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public GetMailTemplateRequestHandler(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;

        }
        public async Task<GetMailTemplateResponse> Handle(GetMailTemplateRequest request, CancellationToken cancellationToken)
        {
            var htmlContent = await File.ReadAllTextAsync($"{this._hostingEnvironment.WebRootPath }/Template/{request.Id}.html");
            var response = new GetMailTemplateResponse
            {
                Template = htmlContent
            };
            return response;
        }

    }
}
