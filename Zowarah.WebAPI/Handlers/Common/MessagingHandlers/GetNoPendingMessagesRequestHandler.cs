﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Common
{
    public class GetNoPendingMessagesRequest: IRequest<int>
    {
        public long UserId { get; set; }
        public RoleTypes RoleType { get; set; }
        public long ServiceId { get; set; }

    }
 
    public class GetNoPendingMessagesRequestHandler : IRequestHandler<GetNoPendingMessagesRequest, int>
    {
        private readonly IMessagingService repo;

        public GetNoPendingMessagesRequestHandler(IMessagingService _repo)
        {
            repo = _repo;
        }

        public async Task<int> Handle(GetNoPendingMessagesRequest request, CancellationToken cancellationToken)
        {
            var result = await repo.GetNoPendingMessages( request.UserId, request.RoleType, request.ServiceId);
            return result;
        }
    }

}