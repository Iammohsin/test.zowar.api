﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Common
{
    public class MarkReadRequest : IRequest<Boolean>
    {
        public string MessageId { get; set; }
        public long UserId { get; set; }
    }

    public class MarkReadRequestHandler : IRequestHandler<MarkReadRequest, Boolean>
    {
        private readonly IMessagingService repo;

        public MarkReadRequestHandler(IMessagingService _repo)
        {
            repo = _repo;
        }

        public async Task<Boolean> Handle(MarkReadRequest request, CancellationToken cancellationToken)
        {
             await repo.MarkRead(request.UserId, request.MessageId);
            return true;
        }
    }
}