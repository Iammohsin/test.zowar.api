﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Services.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Common
{
    public class ComposeMessageRequest:  IRequest<ComposeMessageResponse>
    {
        public long SenderId { get; set; }
        public long RecieverId { get; set; }
        public RoleTypes SenderRole { get; set; }
        public RoleTypes RecieverRole { get; set; }
        public int TransactionTypeId { get; set; }
        public string ReferenceId { get; set; }
        public DateTime Date { get; set; }
        public int MessageCategoryId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public Boolean IsClosed { get; set; }
        public DateTime CloseDate { get; set; }
        public virtual ICollection<MessageTrail> MessageTrails { get; set; }
        public long UserId { get; set; }
        public bool SendByProvider { get; set; }
    }
    public class ComposeMessageResponse
    {
        public string MessageId { get; set; }
    }

    public class ComposeMessageRequestHandler : IRequestHandler<ComposeMessageRequest, ComposeMessageResponse>
    {
        private readonly IMessagingService repo;
        private readonly IServiceService _repoService;

        public ComposeMessageRequestHandler(IMessagingService _repo,IServiceService repoService)
        {
            repo = _repo;
            _repoService = repoService;
        }

        public async Task<ComposeMessageResponse> Handle(ComposeMessageRequest request, CancellationToken cancellationToken)
        {
            if (request.SendByProvider) {
                var service =await _repoService.GetById(request.SenderId);
                if (service.ProviderId != request.UserId)
                    throw new Exception("Invalid Hotel!");                
            }

            var id = await repo.Compose(new Message(){
                                                        SenderId = request.SenderId,
                                                        RecieverId = request.RecieverId,
                                                        SenderRole = request.SenderRole,
                                                        ReceiverRole = request.RecieverRole,
                                                        Title = request.Title,
                                                        Body = request.Body,
                                                        TransactionTypeId = request.TransactionTypeId,
                                                        ReferenceId = request.ReferenceId,
                                                        Date = DateTime.Now,
                                                        MessageCategoryId = request.MessageCategoryId,
                                                        IsClosed = request.IsClosed,
                                                        CloseDate = request.CloseDate
                                                     }
                                       );
            return new ComposeMessageResponse { MessageId = id };
        }
    }
}