﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Common
{
    public class StartProcessingRequest: IRequest<Boolean>
    {
        public string MessageId { get; set; }
        public long UserId { get; set; }
    }
 
    public class StartProcessingRequestHandler : IRequestHandler<StartProcessingRequest, Boolean>
    {
        private readonly IMessagingService repo;

        public StartProcessingRequestHandler(IMessagingService _repo)
        {
            repo = _repo;
        }

        public async Task<Boolean> Handle(StartProcessingRequest request, CancellationToken cancellationToken)
        {
            var result = await repo.StartProcessing(request.MessageId, request.UserId);
            return result;
        }
    }

}