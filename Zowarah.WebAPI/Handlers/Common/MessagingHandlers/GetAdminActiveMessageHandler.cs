﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Queries.Common;

namespace Zowarah.WebAPI.Handlers.Common {

    public class GetAdminActiveMessageRequest : IRequest<GetAdminActiveMessageResponse> {
        public long UserId { get; set; }
        public long ServiceId { get; set; }
    }

    public class GetAdminActiveMessageResponse {
        public ICollection<MessageQuery> Messages { get; set; }
    }
    public class GetAdminActiveMessageHandler : IRequestHandler<GetAdminActiveMessageRequest, GetAdminActiveMessageResponse> {
        private readonly IMessagingService _service;

        public GetAdminActiveMessageHandler(IMessagingService service) {
            _service = service;
        }
        public async Task<GetAdminActiveMessageResponse> Handle(GetAdminActiveMessageRequest request, CancellationToken cancellationToken) {
            return new GetAdminActiveMessageResponse {
                Messages = await _service.GetMessages(true, null, null, RoleTypes.Administrator, request.UserId, request.ServiceId, false, true, false,true)
            };
        }
    }
}
