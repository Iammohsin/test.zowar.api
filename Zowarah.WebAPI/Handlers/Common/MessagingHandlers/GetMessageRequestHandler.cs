﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Queries.Common;

namespace Zowarah.WebAPI.Handlers.Common
{
    public class GetMessageRequest : IRequest<GetMessageResponse>
    {
        public string MessageId { get; set; }
        public long UserId { get; set; }
    }

    public class GetMessageResponse
    {
        public MessageQuery Message { get; set; }
    }
    public class GetMessageRequestHandler : IRequestHandler<GetMessageRequest, GetMessageResponse>
    {
        private readonly IMessagingService repo;

        public GetMessageRequestHandler(IMessagingService _repo)
        {
            repo = _repo;
        }

        public async Task<GetMessageResponse> Handle(GetMessageRequest request, CancellationToken cancellationToken)
        {
            var result = await repo.GetMessage(request.MessageId,request.UserId);
            return new GetMessageResponse()
            {
                Message = result
            };
        }
    }
}