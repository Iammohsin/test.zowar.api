﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Interfaces;
using Zowarah.Domain.Queries.Common;

namespace Zowarah.WebAPI.Handlers.Common
{
    public class GetMessagesRequest: IRequest<GetMessagesResponse>
    {
        public Boolean IsAll { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public long UserId { get; set; }
        public RoleTypes RoleType { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public long ServiceId { get; set; }
    }

    public class GetMessagesResponse
    {
        public IList<MessageQuery> Messages { get; set; }
        public int TotalPages { get; set; }

    }

    public class GetMessagesReportsRequestHandler : IRequestHandler<GetMessagesRequest, GetMessagesResponse>
    {
        private readonly IMessagingService repo;

        public GetMessagesReportsRequestHandler(IMessagingService _repo)
        {
            repo = _repo;
        }

        public async Task<GetMessagesResponse> Handle(GetMessagesRequest request, CancellationToken cancellationToken)
        {
            var result = await repo.GetMessages(request.IsAll,request.FromDate,request.ToDate,request.RoleType,request.UserId, request.ServiceId,true, true,null,true);

            int count = result.Count();
            int totalPages = (int)Math.Ceiling(count / (double)request.PageSize);
            return new GetMessagesResponse() {
                Messages = result.ToList(), //result.Skip((request.Page - 1) * request.PageSize).Take(request.PageSize).ToList(),
                TotalPages = totalPages };
        }
    }
}