﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Common
{
    public class CloseMessageRequest: IRequest<Boolean>
    {
        public string MessageId { get; set; }
    }
 
    public class CloseMessageRequestHandler : IRequestHandler<CloseMessageRequest, Boolean>
    {
        private readonly IMessagingService repo;

        public CloseMessageRequestHandler(IMessagingService _repo)
        {
            repo = _repo;
        }

        public async Task<Boolean> Handle(CloseMessageRequest request, CancellationToken cancellationToken)
        {
            var result = await repo.CloseMessage(request.MessageId);
            return result;
        }
    }
}