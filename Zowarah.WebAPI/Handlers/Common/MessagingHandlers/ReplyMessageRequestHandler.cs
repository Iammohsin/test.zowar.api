﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Common
{
    public class ReplyMessageRequest: IRequest<ReplyMessageResponse>
    {
        public string MessageId { get; set; }
        public long SenderId { get; set; }
        public DateTime Date { get; set; }
        public string Body { get; set; }
    }
    public class ReplyMessageResponse
    {
        public string MessageTrailId { get; set; }

    }
    public class ReplyMessageRequestHandler : IRequestHandler<ReplyMessageRequest, ReplyMessageResponse>
    {
        private readonly IMessagingService repo;

        public ReplyMessageRequestHandler(IMessagingService _repo)
        {
            repo = _repo;
        }

        public async Task<ReplyMessageResponse> Handle(ReplyMessageRequest request, CancellationToken cancellationToken)
        {
            var result = await repo.Reply(new MessageTrail()
            {
                MessageId = request.MessageId,
                SenderId = request.SenderId,
                Body = request.Body,
                Date = DateTime.Now
            });
            return new ReplyMessageResponse() { MessageTrailId = result };
        }
    }
}