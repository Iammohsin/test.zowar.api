﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Common
{
    public class DeleteMessageRequest: IRequest<Boolean>
    {
        public string MessageId { get; set; }
        public long SenderId { get; set; }
        public long UserId { get; set; }
        public bool SendByProvider { get; set; }
    }
 
    public class DeleteMessageRequestHandler : IRequestHandler<DeleteMessageRequest, Boolean>
    {
        private readonly IMessagingService repo;
        private readonly IServiceService _repoService;
        public DeleteMessageRequestHandler(IMessagingService _repo, IServiceService repoService)
        {
            repo = _repo;
            _repoService = repoService;
        }

        public async Task<Boolean> Handle(DeleteMessageRequest request, CancellationToken cancellationToken)
        {
            if (request.SendByProvider)
            {
                var service = await _repoService.GetById(request.SenderId);
                if (service.ProviderId != request.UserId)
                    throw new Exception("Invalid Hotel!");
            }
            await repo.DeleteMessage(request.UserId, request.MessageId);
            return true;
        }
    }

}