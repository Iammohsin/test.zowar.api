﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Queries.Common;

namespace Zowarah.WebAPI.Handlers.Common
{
    public class GetAdminNewMessagesRequest : IRequest<GetAdminNewMessagesResponse>
    {

    }

    public class GetAdminNewMessagesResponse
    {
        public ICollection<MessageQuery> Messages { get; set; }
    }
    public class GetAdminNewMessagesHandler : IRequestHandler<GetAdminNewMessagesRequest, GetAdminNewMessagesResponse>
    {
        private readonly IMessagingService _service;

        public GetAdminNewMessagesHandler(IMessagingService service)
        {
            _service = service;
        }
        public async Task<GetAdminNewMessagesResponse> Handle(GetAdminNewMessagesRequest request, CancellationToken cancellationToken)
        {
            return new GetAdminNewMessagesResponse()
            {
                Messages = await _service.GetMessages(true, null, null, RoleTypes.Administrator, 0, 0, false, true, false, false)
            };
        }
    }
}


