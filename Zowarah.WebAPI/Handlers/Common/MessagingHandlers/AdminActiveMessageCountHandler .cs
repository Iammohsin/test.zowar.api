﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Queries.Common;

namespace Zowarah.WebAPI.Handlers.Common
{
   
    public class AdminActiveMessageCountRequest : IRequest<int> {
        public long UserId { get; set; }
    } 
    public class AdminActiveMessageCountHandler : IRequestHandler<AdminActiveMessageCountRequest, int> {
        private readonly IMessagingService _service;

        public AdminActiveMessageCountHandler(IMessagingService service) {
            _service = service;
        }
        public async Task<int> Handle(AdminActiveMessageCountRequest request, CancellationToken cancellationToken) {
            return await _service.AdminActiveMessageCount(request.UserId);
        }
    }
}
