﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Common
{
    public class IsValidTransactionRequest: IRequest<long>
    {
        public RoleTypes RoleType  { get; set; }
        public MessagingTransTypes TransTypeId { get; set; }
        public string TransId { get; set; }
        public long UserId { get; set; }
    }
 
    public class IsValidTransactionRequestHandler : IRequestHandler<IsValidTransactionRequest, long>
    {
        private readonly IMessagingService repo;

        public IsValidTransactionRequestHandler(IMessagingService _repo)
        {
            repo = _repo;
        }

        public async Task<long> Handle(IsValidTransactionRequest request, CancellationToken cancellationToken)
        {
            var result = await repo.IsValidTransaction(request.UserId,request.RoleType, request.TransTypeId, request.TransId);
            return result;
        }
    }

}