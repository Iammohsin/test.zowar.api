﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Queries.Common;

namespace Zowarah.WebAPI.Handlers.Common
{
   
    public class AdminNewMessageCountRequest : IRequest<int> {

    } 
    public class AdminNewMessageCountHandler : IRequestHandler<AdminNewMessageCountRequest, int> {
        private readonly IMessagingService _service;

        public AdminNewMessageCountHandler(IMessagingService service) {
            _service = service;
        }
        public async Task<int> Handle(AdminNewMessageCountRequest request, CancellationToken cancellationToken) {
            return await _service.AdminNewMessageCount();
        }
    }
}
