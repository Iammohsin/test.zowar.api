﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;
using static Zowarah.WebAPI.Handlers.Common.GetLocalizationListResponse;

namespace Zowarah.WebAPI.Handlers.Common {
    public class GetLocalizationListRequest : IRequest<GetLocalizationListResponse> {
        public string LanguageId { get; set; }
    }
    public class GetLocalizationListResponse {
        public GetLocalizationListResponse() {
            list = new List<LocalizationQuery>();
        }

        public ICollection<LocalizationQuery> list { get; set; }
        public class LocalizationQuery {
            public string Id { get; set; }
            public string LocText { get; set; }
        }
    }

    public class GetLocalizationListRequestHandler : IRequestHandler<GetLocalizationListRequest, GetLocalizationListResponse> {
        private readonly IRepository<Localization> repo;
        private readonly IUnitOfWork uow;

        public GetLocalizationListRequestHandler(IRepository<Localization> _repo, IUnitOfWork _uow) {
            repo = _repo;
            uow = _uow;
        }

        public async Task<GetLocalizationListResponse> Handle(GetLocalizationListRequest request, CancellationToken cancellationToken) {
            var lst = await repo.FindBy(x => x.LanguageId == request.LanguageId).ToListAsync();
            if (lst == null)
                return null;

            return new GetLocalizationListResponse {
                list = Mapper.Map<IList<LocalizationQuery>>(lst)
            };
        }
    }
}