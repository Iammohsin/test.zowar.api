﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Common
{
    public class AddLocalizationRequest : IRequest<Boolean>
    {
        public string Id { get; set; }
        public string LocText { get; set; }
    }
   

    public class AddLocalizationRequestHandler : IRequestHandler<AddLocalizationRequest, Boolean>
    {
        private readonly IRepository<Localization> repo;
        private readonly IUnitOfWork uow;

        public AddLocalizationRequestHandler(IRepository<Localization> _repo, IUnitOfWork _uow)
        {
            repo = _repo;
            uow = _uow;
        }

        public async Task<Boolean> Handle(AddLocalizationRequest request, CancellationToken cancellationToken)
        {

            var localization = new Localization();
            localization.Id = request.Id;
            localization.LanguageId = "en";
            localization.LocText = request.LocText;

            repo.Create(localization);
            await uow.SaveChangesAsync();
            return true;
        }
    }
}