﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Common
{
    public class UpdateLocalizationTextRequest : IRequest<Boolean>
    {
        public string Id { get; set; }
        public string LanguageId { get; set; }
        public string LocText { get; set; }

    }
   
    public class UpdateLocalizationTextRequestHandler : IRequestHandler<UpdateLocalizationTextRequest, Boolean>
    {
        private readonly IRepository<Localization> repo;
        private readonly IUnitOfWork uow;

        public UpdateLocalizationTextRequestHandler(IRepository<Localization> _repo, IUnitOfWork _uow)
        {
            repo = _repo;
            uow = _uow;
        }

        public async Task<Boolean> Handle(UpdateLocalizationTextRequest request, CancellationToken cancellationToken)
        {
            var localization = await repo.FindBy(x => x.Id == request.Id && x.LanguageId == request.LanguageId).FirstOrDefaultAsync();
            if (localization == null) // Add New
            {
                localization = new Localization();
                localization.Id = request.Id;
                localization.LocText = request.LocText;
                localization.LanguageId = request.LanguageId;
                repo.Create(localization);
            }
            else
            {
                localization.LocText = request.LocText;
                repo.Update(localization);
            }
            await uow.SaveChangesAsync();
            return true;
        }
    }
}