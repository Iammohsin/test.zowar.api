﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Common {

    public class CreateOrUpdateLookUpRequest : IRequest<CreateOrUpdateLookUpResponse> {
        public string Name { get; set; }
        public JObject JObject { get; set; }
    }

    public class CreateOrUpdateLookUpResponse {

    }
    public class CreateOrUpdateLookUpHandler : IRequestHandler<CreateOrUpdateLookUpRequest, CreateOrUpdateLookUpResponse> {


        private readonly ApplicationDbContext db;

        public CreateOrUpdateLookUpHandler(ApplicationDbContext db) {
            this.db = db;
        }
        public async Task<CreateOrUpdateLookUpResponse> Handle(CreateOrUpdateLookUpRequest request, CancellationToken cancellationToken) {
            var res = new CreateOrUpdateLookUpResponse();
            var lookup = await db.Set<LookUpTable>().FindAsync(request.Name);
            if (lookup != null) {
                //try {
                Type type = GetType(request.Name);
                dynamic entity = request.JObject.ToObject(type);
                var dbEntity = await db.FindAsync(type, entity.Id);
                if (dbEntity != null) {
                    db.Entry(dbEntity).State = EntityState.Detached;
                    var updatedEntity = Mapper.Map(entity, dbEntity);
                    db.Update(updatedEntity);
                } else {
                    if (entity.Id is int)
                        entity.Id = 0;
                    db.Entry(entity).State = EntityState.Added;
                }
                await db.SaveChangesAsync();
            }
            //} catch (Exception ex) { }
            return res;
        }


        public Type GetType(string typeName) {
            var type = Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies().Where(s => s.FullName.StartsWith("Zowarah.Domain"))) {
                type = a.GetTypes().Where(s => s.IsClass && s.FullName.EndsWith("." + typeName)).FirstOrDefault();
                if (type != null)
                    return type;
            }
            return null;
        }
    }
}
