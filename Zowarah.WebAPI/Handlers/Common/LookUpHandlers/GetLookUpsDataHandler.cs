﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Reviews;

namespace Zowarah.WebAPI.Handlers.Common
{
    
    public class GetLookUpsDataRequest : IRequest<GetLookUpsDataResponse>
    {
        public string Name { get; set; }
    }

    public class GetLookUpsDataResponse
    {
        public dynamic List { get; set; }
    }
    public class GetLookUpsDataHandler : IRequestHandler<GetLookUpsDataRequest, GetLookUpsDataResponse>
    {
        private readonly ApplicationDbContext db;

        public GetLookUpsDataHandler(ApplicationDbContext db)
        {
            this.db = db;
        }
        public async Task<GetLookUpsDataResponse> Handle(GetLookUpsDataRequest request, CancellationToken cancellationToken)
        {
            var res = new GetLookUpsDataResponse();
            switch (request.Name.ToLower())
            {
                case "lookupmodel":
                    res.List = await GetAll<LookUpModel>();
                    break;
                case "lookuptable":
                    res.List = await GetAll<LookUpTable>();
                    break;
                case "setting":
                    res.List = await GetAll<Setting>();
                    break;
                case "currency":
                    res.List = await GetAll<Currency>();
                    break;
                case "city":
                    res.List = await GetAll<City>();
                    break;
                case "country":
                    res.List = await GetAll<Country>();
                    break;
                case "creditcard":
                    res.List = await GetAll<CreditCard>();
                    break;
                case "servicenearbytype":
                    res.List = await GetAll<ServiceNearbyType>();
                    break;
                case "servicenearbycategory":
                    res.List = await GetAll<ServiceNearbyCategory>();
                    break;
                case "breakfastoption":
                    res.List = await GetAll<BreakFastOption>();
                    break;
                case "breakfasttype":
                    res.List = await GetAll<BreakFastType>();
                    break;
                case "cancellationfee":
                    res.List = await GetAll<CancellationFee>();
                    break;
                case "cancellationpolicy":
                    res.List = await GetAll<CancellationPolicy>();
                    break;
                case "facilitycategory":
                    res.List = await GetAll<FacilityCategory>();
                    break;
                case "facility":
                    res.List = await GetAll<Facility>();
                    break;
                case "internetoption":
                    res.List = await GetAll<InternetOption>();
                    break;
                case "parkingoption":
                    res.List = await GetAll<ParkingOption>();
                    break;
                case "parkingreservation":
                    res.List = await GetAll<ParkingReservation>();
                    break;
                case "parkingsite":
                    res.List = await GetAll<ParkingSite>();
                    break;
                case "parkingtype":
                    res.List = await GetAll<ParkingType>();
                    break;
                case "propertytype":
                    res.List = await GetAll<PropertyType>();
                    break;
                case "roomamenitycategory":
                    res.List = await GetAll<RoomAmenityCategory>();
                    break;
                case "roomamenity":
                    res.List = await GetAll<RoomAmenity>();
                    break;
                case "roomtype":
                    res.List = await GetAll<RoomType>();
                    break;
                case "roomtypename":
                    res.List = await GetAll<RoomTypeName>();
                    break;
                case "smokingoption":
                    res.List = await GetAll<SmokingOption>();
                    break;
                case "reviewfeaturetype":
                    res.List = await GetAll<ReviewFeatureType>();
                    break;
            }
            return res;
        }

        private async Task<IList<T>> GetAll<T>() where T : class
        {
            return await db.Set<T>().ToListAsync();
        }
    }
}
