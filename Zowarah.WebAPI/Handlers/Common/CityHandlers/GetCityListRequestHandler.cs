﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Common
{
    public class GetCityListRequest : IRequest<GetCityListResponse>
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetCityResponse {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetCityListResponse
    {
        public GetCityListResponse()
        {
            CityList = new List<GetCityResponse>();
        }

        public List<GetCityResponse> CityList;
    }
    public class GetCityListRequestHandler : IRequestHandler<GetCityListRequest, GetCityListResponse>
    {
        private readonly IRepository<City> _CityRepo;
        private readonly IUnitOfWork _uow;

        public GetCityListRequestHandler(IRepository<City> CityRepo, IUnitOfWork uow)
        {
            _CityRepo = CityRepo;
            _uow = uow;
        }

        public async Task<GetCityListResponse> Handle(GetCityListRequest request, CancellationToken cancellationToken)
        {
               var lst = await _CityRepo.GetAll();
                if (lst == null)
                    return null;

            return new GetCityListResponse {
                CityList = Mapper.Map<List<GetCityResponse>>(lst)
            };
        }
    }
}