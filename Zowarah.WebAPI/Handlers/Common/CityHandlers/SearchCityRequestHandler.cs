﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Common
{
    public class SearchCityRequest :IRequest<SearchCityResponse>
    {
        public string Term { get; set; }
    }
    public class SearchCityResponse
    {
        public IList<String> Cities;
    }
    public class SearchCityRequestHandler : IRequestHandler<SearchCityRequest, SearchCityResponse>
    {
        private readonly ICityService _cityRepo;
        private readonly IUnitOfWork _uow;

        public SearchCityRequestHandler(ICityService cityRepo, IUnitOfWork uow)
        {
            _cityRepo = cityRepo;
            _uow = uow;
        }

        public async Task<SearchCityResponse> Handle(SearchCityRequest request, CancellationToken cancellationToken)
        {
            var cities = await _cityRepo.Search(request.Term);
            var response = new SearchCityResponse
            {
                Cities = cities
            };
            return response;
        }
    }
}
