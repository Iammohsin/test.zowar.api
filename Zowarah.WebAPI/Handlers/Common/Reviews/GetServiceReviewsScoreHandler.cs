﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Reviews
{
  
    public class GetServiceReviewsScoreRequest : IRequest<GetServiceReviewsScoreResponse>
    {

        public long ServiceId { get; set; }
    }

    public class GetServiceReviewsScoreResponse
    {
        public decimal TotalReviews { get; set; }
        public float ReviewsScore { get; set; }
        public ICollection<ServiceReviewFeaturesScore> Features { get; set; } = new HashSet<ServiceReviewFeaturesScore>();
        public ICollection<ServiceReviewAveragesScore> Averages { get; set; } = new HashSet<ServiceReviewAveragesScore>();
    }
    public class GetServiceReviewsScoreHandler : IRequestHandler<GetServiceReviewsScoreRequest, GetServiceReviewsScoreResponse>
    {
        private readonly IReviewService repo;
        private readonly IRepository<Service> serviceRepo;
        public GetServiceReviewsScoreHandler(IReviewService _repo,IRepository<Service> _serviceRepo)
        {
            repo = _repo;
            serviceRepo = _serviceRepo;
        }
        public async Task<GetServiceReviewsScoreResponse> Handle(GetServiceReviewsScoreRequest request, CancellationToken cancellationToken)
        {
            var service = await serviceRepo.GetById(request.ServiceId);
            if (service == null)
                throw new Exception("Invalid Service !!");

            var featuresScores = await repo.GetServiceFeaturesScore(request.ServiceId);
            var averageScores = await repo.GetServiceAveragesScore(request.ServiceId);
            return new GetServiceReviewsScoreResponse
            {
                TotalReviews = service.TotalReviews,
                ReviewsScore = service.ReviewScore,
                Features = featuresScores,
                Averages = averageScores
            };
        }
    }
}
