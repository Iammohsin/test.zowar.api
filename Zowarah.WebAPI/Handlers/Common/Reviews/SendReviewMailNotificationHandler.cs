﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Application.Interfaces.Users;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Reviews;
using Zowarah.Domain.Interfaces;
namespace Zowarah.WebAPI.Handlers.Reviews
{
    public class ReplyReviewMailNotification
    {
        public static string TemplateId => "ReplyReview";
        public static string Subject => " Review Reply";
    }
    public class ProviderReviewMailNotification
    {
        public static string TemplateId => "ProviderReview";
    }


    public class SendReviewMailNotificationRequest : IRequest<SendReviewMailNotificationResponse>
    {
        public Review Review { get; set; }
        public bool IsReply { get; set; }
    }
    public class SendReviewMailNotificationResponse {
    }

    public class SendReviewMailNotificationHandler : IRequestHandler<SendReviewMailNotificationRequest, SendReviewMailNotificationResponse>
    {
        private readonly IUserService userRepo;
        private readonly IRepository<Service> serv;
        private readonly INotificationService notifSer;

        public SendReviewMailNotificationHandler(INotificationService _notifSer, IUserService _userRepo, IRepository<Service> _serv)
        {
            userRepo = _userRepo;
            serv = _serv;
            notifSer = _notifSer;
        }
        public async Task<SendReviewMailNotificationResponse> Handle(SendReviewMailNotificationRequest request, CancellationToken cancellationToken)
        {
            if (request.IsReply)
            {
                await SendClientNotification(request.Review);
            }
            else
            {
                await SendProviderNotification(request.Review);
            }
            return new SendReviewMailNotificationResponse();
        }

        private async Task SendClientNotification(Review review)
        {
            var client = await userRepo.GetUserById(review.ClientId);
            var service = await serv.GetById(review.ServiceId);
            var html = notifSer.GetTemplate(ReplyReviewMailNotification.TemplateId);
            html = html.Replace("$$GuestName$$", client.FirstName)
                .Replace("$$HotelName$$", service.Name)
                .Replace("$$Title$$", review.Title)
                .Replace("$$Comment$$", review.Comment)
                .Replace("$$Rate$$", review.Score + "")
                .Replace("$$ReplyComment$$", review.ReplyComment)
                .Replace("$$ReplyDate$$", review.ReplyDate.ToString("MM-dd-yyyy"));
            await notifSer.SendBySmtp(client.Email, service.Name + ReplyReviewMailNotification.Subject, client.FirstName, html);
        }
        private async Task SendProviderNotification(Review review)
        {
            var client = await userRepo.GetUserById(review.ClientId);
            var service = await serv.GetById(review.ServiceId);
            var provider = await userRepo.GetUserById(service.ProviderId);
            var html = notifSer.GetTemplate(ProviderReviewMailNotification.TemplateId);
            html = html.Replace("$$GuestName$$", provider.FirstName)
                .Replace("$$ProviderName$$", provider.FirstName)
                .Replace("$$HotelName$$", service.Name)
                .Replace("$$Title$$", review.Title)
                .Replace("$$Comment$$", review.Comment)
                .Replace("$$Rate$$", review.Score + "")
                .Replace("$$Date$$", review.Date.ToString("MM-dd-yyyy"));            
            await notifSer.SendBySmtp(provider.Email, $"{client.FirstName} Reviewed {service.Name}" , service.Name, html);
        }
    }
}
