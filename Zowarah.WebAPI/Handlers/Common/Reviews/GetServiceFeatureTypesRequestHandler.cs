﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Services.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Reviews;

namespace Zowarah.WebAPI.Handlers.Reviews
{

    public class GetServiceFeatureTypesRequest : IRequest<GetServiceFeatureTypesResponse>
    {
        public ServiceTypeEnum ServiceTypeId { get; set; }

    }

    public class GetServiceFeatureTypesResponse
    {
        public ICollection<ReviewFeatureType> FeatureTypes { get; set; } = new HashSet<ReviewFeatureType>();
    }
    public class GetServiceFeatureTypesHandler : IRequestHandler<GetServiceFeatureTypesRequest, GetServiceFeatureTypesResponse>
    {
        private readonly IReviewService repo;

        public GetServiceFeatureTypesHandler(IReviewService _repo)
        {
            repo = _repo;
        }
        public async Task<GetServiceFeatureTypesResponse> Handle(GetServiceFeatureTypesRequest request, CancellationToken cancellationToken)
        {
            var featureTypes = await repo.GetServiceFeatureTypes(request.ServiceTypeId);
            return new GetServiceFeatureTypesResponse
            {
                FeatureTypes = featureTypes
            };
        }
    }
}
