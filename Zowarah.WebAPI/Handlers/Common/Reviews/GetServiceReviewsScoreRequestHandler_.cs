﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Services.Common;

namespace Zowarah.WebAPI.Handlers.Reviews
{

    public class GetServiceReviewsScoreRequestRequest : IRequest<GetServiceReviewsScoreRequestResponse>
    {
        public long ServiceId { get; set; }
        public int Page { get; set; }
        public int Count { get; set; }
        public ReviewOrderbyEnum OrderBy { get; set; }
    }

    public class GetServiceReviewsScoreRequestResponse
    {
        public ICollection<ReviewQuery> Reviews { get; set; } = new HashSet<ReviewQuery>();
    }
    public class GetServiceReviewsScoreRequestHandler : IRequestHandler<GetServiceReviewsScoreRequestRequest, GetServiceReviewsScoreRequestResponse>
    {
        private readonly IReviewService repo;

        public GetServiceReviewsScoreRequestHandler(IReviewService _repo)
        {
            repo = _repo;
        }
        public async Task<GetServiceReviewsScoreRequestResponse> Handle(GetServiceReviewsScoreRequestRequest request, CancellationToken cancellationToken)
        {
            var reviews = await repo.GetByServiceId(request.ServiceId, request.Page, request.Count, request.OrderBy);
            return new GetServiceReviewsScoreRequestResponse
            {
                Reviews = reviews
            };
        }
    }
}
