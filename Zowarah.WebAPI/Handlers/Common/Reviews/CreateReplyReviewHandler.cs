﻿using Hangfire;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Services.Common;
using Zowarah.Domain.Interfaces;
using Zowarah.WebAPI.Handlers.Invoices;

namespace Zowarah.WebAPI.Handlers.Reviews
{
    public class CreateReplyReviewRequest : IRequest<CreateReplyReviewResponse>
    {
        public string Id { get; set; }
        public string Comment { get; set; }
    }

    public class CreateReplyReviewResponse
    {

    }
    public class CreateReplyReviewHandler : IRequestHandler<CreateReplyReviewRequest, CreateReplyReviewResponse>
    {
        private readonly IUnitOfWork uow;
        private readonly IReviewService repo;
        private readonly INotificationService notificationService;
        private readonly IMediator mediator;
        public CreateReplyReviewHandler(IReviewService _Repo, IUnitOfWork _uow, INotificationService _notificationService, IMediator _mediator)
        {
            repo = _Repo;
            uow = _uow;
            notificationService = _notificationService;
            mediator = _mediator;
        }
        public async Task<CreateReplyReviewResponse> Handle(CreateReplyReviewRequest request, CancellationToken cancellationToken)
        {
            var review = await repo.GetById(request.Id);
           
            if (review != null && string.IsNullOrEmpty(review.ReplyComment) && !string.IsNullOrEmpty(review.Comment))
            {
                review.ReplyComment = request.Comment;
                review.ReplyDate = DateTime.Now;
                repo.Update(review);
                await uow.SaveChangesAsync();
                var reviewEmail = new SendReviewMailNotificationRequest { IsReply = true, Review = review };
                BackgroundJob.Enqueue(() => SendMailNotification(reviewEmail));
                return new CreateReplyReviewResponse();
            }
            return null;
        }

        public async Task SendMailNotification(SendReviewMailNotificationRequest request)
        {
            await mediator.Send(request);
        }
    }
}
