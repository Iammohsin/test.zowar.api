﻿

using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;

namespace Zowarah.WebAPI.Handlers.Reviews
{
    public class UpdatePropertyReviewRateRequest : IRequest<UpdatePropertyReviewRateResponse>
    {
        public long ServiceId { get; set; }
        public float Rate { get; set; }
    }

    public class UpdatePropertyReviewRateResponse
    {

    }
    public class UpdatePropertyReviewRateHandler : IRequestHandler<UpdatePropertyReviewRateRequest, UpdatePropertyReviewRateResponse>
    {
        private readonly IReviewService repo;
        public UpdatePropertyReviewRateHandler(IReviewService _Repo)
        {
            repo = _Repo;
        }

        
        public async Task<UpdatePropertyReviewRateResponse> Handle(UpdatePropertyReviewRateRequest request, CancellationToken cancellationToken)
        {
            await repo.UpdateServiceReviewRate(request.ServiceId, request.Rate);
            return null;
        }
    }
}
