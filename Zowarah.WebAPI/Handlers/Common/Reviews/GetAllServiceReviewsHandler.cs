﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Services.Common;

namespace Zowarah.WebAPI.Handlers.Reviews
{

    public class GetAllServiceReviewsRequest : IRequest<GetAllServiceReviewsResponse>
    {
        public long ServiceId { get; set; }
        public int Page { get; set; }
        public int Count { get; set; }
        public ReviewOrderbyEnum OrderBy { get; set; }
    }

    public class GetAllServiceReviewsResponse
    {
        public ICollection<ReviewQuery> Reviews { get; set; } = new HashSet<ReviewQuery>();
    }
    public class GetAllServiceReviewsHandler : IRequestHandler<GetAllServiceReviewsRequest, GetAllServiceReviewsResponse>
    {
        private readonly IReviewService repo;

        public GetAllServiceReviewsHandler(IReviewService _repo)
        {
            repo = _repo;
        }
        public async Task<GetAllServiceReviewsResponse> Handle(GetAllServiceReviewsRequest request, CancellationToken cancellationToken)
        {
            var reviews = await repo.GetByServiceId(request.ServiceId, request.Page, request.Count, request.OrderBy);
            return new GetAllServiceReviewsResponse
            {
                Reviews = reviews
            };
        }
    }
}
