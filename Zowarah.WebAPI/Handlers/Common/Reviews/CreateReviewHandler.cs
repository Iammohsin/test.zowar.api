﻿using Hangfire;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Interfaces.Hotels;
using Zowarah.Application.Interfaces.Transactions;
using Zowarah.Domain.Entities.Reviews;
using Zowarah.Domain.Interfaces;
using Zowarah.Infrastructure.Config;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Reviews
{

    public class CreateReviewRequest : IRequest<CreateReviewResponse>
    {
        public long ServiceId { get; set; }
        public string TransactionId { get; set; }
        public string Title { get; set; }
        public string Comment { get; set; }
        public long ClientId { get; set; }
        public List<ReviewFeature> ReviewFeatures { get; set; } = new List<ReviewFeature>();
    }

    public class CreateReviewResponse
    {
        public string Id { get; set; }
    }
    public class CreateReviewHandler : IRequestHandler<CreateReviewRequest, CreateReviewResponse>
    {
        private readonly IReviewService repo;
        private readonly IBookingService repoBooking;
        private readonly ITransactionService repoTransactions;
        private readonly IUnitOfWork uow;
        private readonly IMediator mediator;

        public CreateReviewHandler(IReviewService _Repo, IUnitOfWork _uow, IBookingService _repoBooking, ITransactionService _repoTransactions, IMediator _mediator)
        {
            repo = _Repo;
            repoBooking = _repoBooking;
            repoTransactions = _repoTransactions;
            uow = _uow;
            mediator = _mediator;
        }
        public async Task<CreateReviewResponse> Handle(CreateReviewRequest request, CancellationToken cancellationToken)
        {
            var hasReview = await repo.IsExist(request.ClientId, request.TransactionId);
           var isClientShowUp = await repoBooking.IsClientShowUp(request.TransactionId);           
            if (!isClientShowUp)
                throw new CustomException("Client has not show Up!");

            if (!hasReview && isClientShowUp)
            {
                var review = new Review();
                review = review.FromRequest(request);
                review.Id = Guid.NewGuid().ToString();
                var transaction = await repoTransactions.GetById(request.TransactionId);
                review.ServiceId = transaction.ServiceId;
                review.Tags = await repo.GetReviewTags(request.TransactionId);
                var total = await repo.TotalReviewFeatureTypesScore();
                review.Score = float.Parse(Math.Round(review.ReviewFeatures.Sum(s => s.Score) / total, 1).ToString());
                repo.Create(review);
                await uow.SaveChangesAsync();

                // Update Aggregationss
                BackgroundJob.Enqueue(() => repo.UpdateServiceReviewRate(review.ServiceId, review.Score));
                BackgroundJob.Enqueue(() => repo.UpdateServiceReviewFeaturesRate(review.ServiceId, review.ReviewFeatures));

                var reviewEmail = new SendReviewMailNotificationRequest { IsReply = false, Review = review };
                BackgroundJob.Enqueue(() => SendMailNotification(reviewEmail));
                return new CreateReviewResponse() { Id = review.Id };
            }
            return null;
        }
        public async Task SendMailNotification(SendReviewMailNotificationRequest request)
        {
            await mediator.Send(request);
        }
    }
}
