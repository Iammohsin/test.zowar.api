﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Services.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Reviews;
using Zowarah.Domain.Interfaces;
using Zowarah.WebAPI.Config.AutoMapping;

namespace Zowarah.WebAPI.Handlers.Reviews {

    public class GetFeatureTypesRequest : IRequest<GetFeatureTypesResponse> {
        public ServiceTypeEnum ServiceTypeId { get; set; }

    }

    public class GetFeatureTypesResponse {
        public List<ReviewFeatureTypeQuery> FeatureTypes { get; set; } = new List<ReviewFeatureTypeQuery>();
    }

    public class ReviewFeatureTypeQuery {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetFeatureTypesHandler : IRequestHandler<GetFeatureTypesRequest, GetFeatureTypesResponse> {
        private readonly IRepository<ReviewFeatureType> repo;

        public GetFeatureTypesHandler(IRepository<ReviewFeatureType> _repo) {
            repo = _repo;
        }

        public async Task<GetFeatureTypesResponse> Handle(GetFeatureTypesRequest request, CancellationToken cancellationToken) {
            var featureTypes = await repo.GetAll();
            return new GetFeatureTypesResponse {
                FeatureTypes = Mapper.Map<List<ReviewFeatureTypeQuery>>(featureTypes)
            };
        }
    }
}
