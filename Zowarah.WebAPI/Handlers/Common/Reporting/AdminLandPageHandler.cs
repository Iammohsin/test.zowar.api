﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Interfaces.Transactions;
using Zowarah.Application.Interfaces.Users;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.WebAPI.Handlers.Common.Reporting
{

    public class GetAdminDashboardRequest : IRequest<GetAdminDashboardResponse>
    {

    }

    public class GetAdminDashboardResponse
    {
        public GetAdminTransactionDashboardResponse Transaction { get; set; }
        public GetAdminProviderDashboardResponse Provider { get; set; }
        public GetAdminClientDashboardResponse Client { get; set; }
        

        public class GetAdminTransactionDashboardResponse
        {
            public int Confirmed { get; set; }
            public int Cancelled { get; set; }
            public int NoShow { get; set; }
        }
        public class GetAdminProviderDashboardResponse
        {
            public int ServiceRegistered { get; set; }
            public int ServiceActivated { get; set; }
            public int ServicePending { get; set; }
            public int ServiceUnResolvedComplaints { get; set; }
        }
        public class GetAdminClientDashboardResponse {
            public int Registered { get; set; }
            public int Activated { get; set; }
            public int UnresolvedComplaints { get; set; }
        }
    }
    public class GetAdminDashboardHandler : IRequestHandler<GetAdminDashboardRequest, GetAdminDashboardResponse>
    {
        private readonly ITransactionService repoTrans;
        private readonly IReportingService repoService;
        private readonly IUserService userService;

        public GetAdminDashboardHandler(ITransactionService _repoTrans, IReportingService repoService, IUserService userService)
        {
            repoTrans = _repoTrans;
            this.repoService = repoService;
            this.userService = userService;
        }
        public async Task<GetAdminDashboardResponse> Handle(GetAdminDashboardRequest request, CancellationToken cancellationToken)
        {
            var res = new GetAdminDashboardResponse();
            res.Transaction = new GetAdminDashboardResponse.GetAdminTransactionDashboardResponse();
            res.Provider = new GetAdminDashboardResponse.GetAdminProviderDashboardResponse();
            res.Client = new GetAdminDashboardResponse.GetAdminClientDashboardResponse();
            var tasks = new Task<int>[8];
            tasks[0] = repoTrans.GetTotalConfirmed();
            tasks[1] = repoTrans.GetTotalCancelled();
            tasks[2] = repoTrans.GetTotalNoShow();

            tasks[3] = repoService.GetTotalServiceRegistered();
            tasks[4] = repoService.GetTotalServiceActived();
            tasks[5] = repoService.GetTotalServicePendding();

            tasks[6] = userService.TotalUserByRoleName("Client");
            tasks[7] = userService.TotalUserByRoleName("Client", true);

            var results = await Task.WhenAll(tasks);
            res.Transaction.Confirmed = results[0];
            res.Transaction.Cancelled = results[1];
            res.Transaction.NoShow = results[2];

            res.Provider.ServiceRegistered = results[3];
            res.Provider.ServiceActivated = results[4];
            res.Provider.ServicePending = results[5];
            res.Provider.ServiceUnResolvedComplaints = 0;

            res.Client.Registered = results[6];
            res.Client.Activated = results[7];
            res.Client.UnresolvedComplaints = 0;

            return res;
        }
    }
}
