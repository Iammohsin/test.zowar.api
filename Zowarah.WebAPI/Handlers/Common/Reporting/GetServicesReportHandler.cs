﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Interfaces.Users;
using Zowarah.Application.Services.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Common.Reporting
{

    public class GetServicesReportRequest : IRequest<GetServicesReportResponse>
    {
        public int? City { get; set; }
        public ServiceTypeEnum ServiceType { get; set; }
        public ServiceStatusEnum Status { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        public string InvoiceId { get; set; }
        public long ServiceId { get; set; }
        public long ProviderId { get; set; }
    }

    public class GetServicesReportResponse
    {
        public ICollection<ServiceReport> ServiceReports { get; set; } = new HashSet<ServiceReport>();
    }
    public class GetServicesReportHandler : IRequestHandler<GetServicesReportRequest, GetServicesReportResponse>
    {
        private readonly IServiceService repoService;
        public GetServicesReportHandler(IServiceService _repoService)
        {
            repoService = _repoService;
        }
        public async Task<GetServicesReportResponse> Handle(GetServicesReportRequest request, CancellationToken cancellationToken)
        {
            return new GetServicesReportResponse
            {
                ServiceReports = await repoService.GetServiceReport(request.City, request.ServiceType, request.Status, request.From, request.To, request.ServiceId, request.ProviderId)
            };
        }
    }

}
