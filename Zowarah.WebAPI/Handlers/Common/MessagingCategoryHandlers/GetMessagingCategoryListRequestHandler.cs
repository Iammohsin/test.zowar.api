﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Common {
    public class GetMessagingCategoryListRequest : IRequest<GetMessagingCategoryListResponse> {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class GetMessagingCategoryListResponse {
        public GetMessagingCategoryListResponse() {
            MessagesCategories = new List<GetMessagingCategoryResponse>();
        }

        public ICollection<GetMessagingCategoryResponse> MessagesCategories;
    }


    public class GetMessagingCategoryResponse {
        public string Id { get; set; }
        public string Description { get; set; }
    }
    public class GetMessagingCategoryListRequestHandler : IRequestHandler<GetMessagingCategoryListRequest, GetMessagingCategoryListResponse> {
        private readonly IRepository<MessageCategory> repo;

        public GetMessagingCategoryListRequestHandler(IRepository<MessageCategory> _repo, IUnitOfWork uow) {
            repo = _repo;
        }

        public async Task<GetMessagingCategoryListResponse> Handle(GetMessagingCategoryListRequest request, CancellationToken cancellationToken) {
            var lst = await repo.GetAll();
            if (lst == null)
                return null;

                GetMessagingCategoryListResponse response = new GetMessagingCategoryListResponse();
                var responseLst = Mapper.Map<ICollection<MessageCategory>, ICollection<GetMessagingCategoryResponse>>(lst);
                response.MessagesCategories = responseLst;
                return  response;
        }
    }
}