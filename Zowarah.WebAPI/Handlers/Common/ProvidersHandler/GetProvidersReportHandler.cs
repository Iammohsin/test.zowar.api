﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Interfaces.Users;

namespace Zowarah.WebAPI.Handlers.Common
{
   
    public class GetProvidersReportRequest : IRequest<GetProvidersReportResponse>
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }

    public class GetProvidersReportResponse
    {
        public int Registered { get; set; }
        public int Activated { get; set; }

    }
    public class GetProvidersReportHandler : IRequestHandler<GetProvidersReportRequest, GetProvidersReportResponse>
    {
        public IUserService _userService { get; set; }
        public GetProvidersReportHandler(IUserService userService)
        {            
            _userService = userService;
        }
        public async Task<GetProvidersReportResponse> Handle(GetProvidersReportRequest request, CancellationToken cancellationToken)
        {
            var res =new  GetProvidersReportResponse();
            res.Registered =await _userService.TotalUserByRoleName("Provider",null,request.From,request.To);
            res.Activated =await _userService.TotalUserByRoleName("Provider", true, request.From, request.To);
            return res;
        }
    }
}
