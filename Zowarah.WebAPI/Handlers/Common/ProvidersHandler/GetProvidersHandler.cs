﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Application.Interfaces.Common;
using Zowarah.Application.Services.Common;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Common
{

    public class GetProvidersRequest : IRequest<GetProvidersResponse>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }

    public class GetProvidersResponse : ProviderPage
    {

    }
    public class GetProvidersHandler : IRequestHandler<GetProvidersRequest, GetProvidersResponse>
    {
        public IServiceService _serviceRepo { get; set; }
        public GetProvidersHandler(IServiceService serviceRepo)
        {
            _serviceRepo = serviceRepo;
        }
        public async Task<GetProvidersResponse> Handle(GetProvidersRequest request, CancellationToken cancellationToken)
        {
            var result = await _serviceRepo.GetProviders(request.PageNumber,
              request.PageSize,
              request.From,
              request.To);
            var res = new GetProvidersResponse();
            result.ToResponse(res);
            return res;
        }
    }
}
