﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;
using Zowarah.WebAPI.Extensions;

namespace Zowarah.WebAPI.Handlers.Common {
    public class GetCountryListRequest : IRequest<GetCountryListResponse> {
        public int Id { get; set; }
        public string Description { get; set; }
    }

    public class GetCountryResponse {
        public string Id { get; set; }
        public string Description { get; set; }
        public string CurrencyId { get; set; }
        public string IsoCode { get; set; }
    }

    public class GetCountryListResponse {
        public GetCountryListResponse() {
            CountryList = new List<GetCountryResponse>();
        }
        public List<GetCountryResponse> CountryList;
    }
    public class GetCountryListRequestHandler : IRequestHandler<GetCountryListRequest, GetCountryListResponse> {
        private readonly IRepository<Country> _CountryRepo;
        private readonly IUnitOfWork _uow;

        public GetCountryListRequestHandler(IRepository<Country> CountryRepo, IUnitOfWork uow) {
            _CountryRepo = CountryRepo;
            _uow = uow;
        }

        public async Task<GetCountryListResponse> Handle(GetCountryListRequest request, CancellationToken cancellationToken) {
            var lst = await _CountryRepo.GetAll();
            if (lst == null)
                return null;          
            
            return new GetCountryListResponse {
                CountryList = Mapper.Map<List<GetCountryResponse>>(lst)
            };
        }
    }

}
