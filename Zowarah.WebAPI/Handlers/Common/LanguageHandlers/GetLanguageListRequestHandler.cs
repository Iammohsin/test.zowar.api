﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Common {
    public class GetLanguageListRequest : IRequest<GetLanguageListResponse> {
        public int Id { get; set; }
        public string Description { get; set; }
    }

    public class GetLanguageResponse {
        public string Id { get; set; }
        public string Name { get; set; }
        public string NativeName { get; set; }
    }
    public class GetLanguageListResponse {
        public GetLanguageListResponse() {
            LanguageList = new List<GetLanguageResponse>();
        }

        public List<GetLanguageResponse> LanguageList;
    }
    public class GetLanguageListRequestHandler : IRequestHandler<GetLanguageListRequest, GetLanguageListResponse> {
        private readonly IRepository<Language> _LanguageRepo;
        private readonly IUnitOfWork _uow;

        public GetLanguageListRequestHandler(IRepository<Language> LanguageRepo, IUnitOfWork uow) {
            _LanguageRepo = LanguageRepo;
            _uow = uow;
        }

        public async Task<GetLanguageListResponse> Handle(GetLanguageListRequest request, CancellationToken cancellationToken) {
            var lst = await _LanguageRepo.GetAll();
            if (lst == null)
                return null;

            return new GetLanguageListResponse {
                LanguageList = Mapper.Map<List<GetLanguageResponse>>(lst)
            };
        }
    }
}