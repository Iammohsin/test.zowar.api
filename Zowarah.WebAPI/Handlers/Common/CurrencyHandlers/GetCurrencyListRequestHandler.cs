﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Interfaces;

namespace Zowarah.WebAPI.Handlers.Common
{
    public class GetCurrencyListRequest : IRequest<GetCurrencyListResponse>
    {
        //public int Id { get; set; }
        //public string Description { get; set; }
    }

    public class GetCurrencyResponse {
        public string Id { get; set; }
        public string Symbol { get; set; }
        public string Name { get; set; }
        public decimal ExchangeRate { get; set; }
    }

    public class GetCurrencyListResponse
    {
        public GetCurrencyListResponse()
        {
            CurrencyList = new List<GetCurrencyResponse>();
        }

        public List<GetCurrencyResponse> CurrencyList;
    }
    public class GetCurrencyListRequestHandler : IRequestHandler<GetCurrencyListRequest, GetCurrencyListResponse>
    {
        private readonly IRepository<Currency> _CurrencyRepo;
        private readonly IUnitOfWork _uow;

        public GetCurrencyListRequestHandler(IRepository<Currency> CurrencyRepo, IUnitOfWork uow)
        {
            _CurrencyRepo = CurrencyRepo;
            _uow = uow;
        }

        public async Task<GetCurrencyListResponse> Handle(GetCurrencyListRequest request, CancellationToken cancellationToken)
        {
            var lst = await _CurrencyRepo.GetAll();
            if (lst == null)
                return null;
            return new GetCurrencyListResponse {
                CurrencyList = Mapper.Map<List<GetCurrencyResponse>>(lst)
            };           

        }
    }

}
