﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Zowarah.WebAPI.Extensions
{

    public static class IncludeExtension
    {
        public static IQueryable<TEntity> Include<TEntity>(this DbSet<TEntity> dbSet,
                                                           params Expression<Func<TEntity, object>>[] includes)
                                                           where TEntity : class
        {
            IQueryable<TEntity> query = null;
            foreach (var include in includes)
            {
                query = dbSet.Include(include);
            }
            return query ?? dbSet;
        }
    }
    public static class BaseEntityExtension
    {
        public static TEntity FromRequest<TEntity, TRequest>(this TEntity entity, TRequest request)
        {
            return Mapper.Map<TRequest, TEntity>(request);
        }
        public static void ToResponse<TEntity, TResponse>(this TEntity entity, TResponse response)
        {
            Mapper.Map<TEntity, TResponse>(entity, response);
        }
    }
}
