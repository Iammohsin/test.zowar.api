﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zowarah.WebAPI.Config.AutoMapping;

namespace Zowarah.WebAPI {
    public class RegisterAutoMapper {
        private static object _thisLock = new object();
        private static bool _initialized = false;
        public static void Register(IServiceCollection services) {
            if (!_initialized) {
                _initialized = true;
                Mapper.Reset();
                ServiceCollectionExtensions.UseStaticRegistration = true;
                services.AddAutoMapper();
                services.AddSingleton(provider => new MapperConfiguration(cfg => cfg.AddProfile<ApiMapper>()));
            }
        }
    }
}
