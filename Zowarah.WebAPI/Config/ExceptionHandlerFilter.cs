﻿using log4net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Zowarah.Infrastructure.Config;

namespace Zowarah.WebAPI.Config
{
    public class ExceptionHandlerFilter : IExceptionFilter
    {
        //private readonly ILogger logger;
        private readonly ILog logger;
        public ExceptionHandlerFilter()
        {
           //logger = _logger;
            logger = LogManager.GetLogger(typeof(ExceptionHandlerFilter));
        }

        public void OnException(ExceptionContext context)
        {
            HttpStatusCode status;
            String message = String.Empty;
            System.Diagnostics.Debug.WriteLine("{0}::{1}==={2}##{3}",
                context.HttpContext.Request.Path,
                context.Exception.InnerException,
                context.Exception.Message,context.Exception.Source);
            var exceptionType = context.Exception.GetType();
            status = HttpStatusCode.BadRequest;
            if (exceptionType == typeof(UnauthorizedAccessException))
                message = "Unauthorized Access";
            /*  else if (exceptionType == typeof(NotImplementedException))
                message = "A server error occurred.";
            */
            else if (exceptionType == typeof(CustomException))
                message = context.Exception.Message;
            else if (exceptionType == typeof(DbUpdateException))
            {
                SqlException innerException = context.Exception.InnerException as SqlException;
                if (innerException.Number == 2627)
                    message = "Duplicate Entry !!";
                else
                    message = innerException.Message;
            }
            else
            {
                // Log It 
                if (context.Exception.InnerException!=null)
                    logger.Info(context.Exception.InnerException.Message);
                else
                    logger.Info(context.Exception.Message);
                //message = "Unexpected Error!";
                message = context.Exception.Message;
            }
            //logger.Info("Custom Exception");
            //logger.Debug(message);

            context.ExceptionHandled = true;
            HttpResponse response = context.HttpContext.Response;
            response.StatusCode = (int)status;
            response.ContentType = "application/json";
            var err = message;
            response.WriteAsync(err);
        }
    }
}
