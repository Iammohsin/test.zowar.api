﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zowarah.WebAPI.Config.AppSettings
{
    public class AppSettings
    {
        public GeneralSettings GeneralSettings { get; set; }
        public MailSettings MailSettings { get; set; }
        public SMSSettings SMSSettings { get; set; }

    }

    public class GeneralSettings
    {
        public string Url { get; set; }
    }
    public class MailSettings
    {
        public string MailApi { get; set; }
        public string EmailAddress { get; set; }
        public string DisplayName { get; set; }
        public string MainTemplateId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public Boolean EnableSSL { get; set; }



    }
    public class SMSSettings
    {
        public string ApiLink { get; set; }
        public string ApiKey { get; set; }
    }
}
