﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zowarah.WebAPI.Config.Notification
{
    public class CancelBookingTemplate
    {
        public static string ClientTemplateId => "ClinetCancelBooking";
        public static string ProviderTemplateId => "ProviderCancelBooking";
        public static string FreeMessage => " free of charge";
        public static string Subject => "Cancel Booking at $$HotelName$$";
    }
}
