﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zowarah.WebAPI.Config.Notification
{
    public class ConfirmBooking
    {
        public static string TemplateId => "confirmBooking";
        public static string Subject => "Booking at ";
        public static string SMSMessage => "Your booking is now confirmed. for more details please visit zowar.com.sa";
        public static string AttachTemplateId => "AttachBookingConfirmation";

    }
}
