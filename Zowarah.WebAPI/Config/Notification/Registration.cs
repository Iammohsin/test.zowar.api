﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zowarah.WebAPI.Config.Notification
{
    public class Registration
    {
        public static string TemplateId => "UserRegistration";
        public static string Subject => "Registration By Admin";     
    }
}
