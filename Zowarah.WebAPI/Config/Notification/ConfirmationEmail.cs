﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zowarah.WebAPI.Config.Notification
{
    public static class ConfirmationEmail
    {
        public static string TemplateId => "confirmEmail";
        public static string Subject => "Confirmation Email Account";
        public static string ConfirmDomain => "http://alzowar.com:5050/home/confirm";
    }
}
