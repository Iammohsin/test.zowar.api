﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zowarah.WebAPI.Config.Notification
{
    public class ForgotPassword
    {
        public static string TemplateId => "ForgotPassword";
        public static string Subject => "Reset Password";
        public static string ConfirmDomain => "/home/resetpassword";
    }
}