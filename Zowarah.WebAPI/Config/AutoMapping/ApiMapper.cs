﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zowarah.Application.Services.Common;
using Zowarah.Application.Services.Hotels;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Invoices;
using Zowarah.Domain.Entities.Reviews;
using Zowarah.Domain.Entities.Transactions;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Queries.Common;
using Zowarah.WebAPI.Handlers.Common;
using Zowarah.WebAPI.Handlers.Hotels;
using Zowarah.WebAPI.Handlers.Hotels.ServiceProfilesHandler;
using Zowarah.WebAPI.Handlers.Payments;
using Zowarah.WebAPI.Handlers.Reviews;
using Zowarah.WebAPI.Handlers.Transactions.TransactionHandlers;
using Zowarah.WebAPI.Handlers.Users.CreditCard;
using static Zowarah.WebAPI.Handlers.Common.GetLocalizationListResponse;
using static Zowarah.WebAPI.Handlers.Hotels.GetPromotionsByServiceIdResponse;

namespace Zowarah.WebAPI.Config.AutoMapping
{
    public class ApiMapper : Profile
    {
        public ApplicationDbContext dbContext;
        public ApiMapper(ApplicationDbContext _dbContext)
        {
            dbContext = _dbContext;
        }
        public ApiMapper()
        {
            /* ToDo : Enhance by injection */
            dbContext = new ApplicationDbContext();

            /* Hotel Query */
            CreateMap<HotelQuery, HotelDetails>();

            /*PropertyBasicInfo*/
            CreateMap<CreatePropertyBasicInfoRequest, Hotel>();

            CreateMap<CreatePropertyBasicInfoRequest, Service>()
                .ForMember(t => t.CityId, opt => opt.MapFrom(s => s.CityId))
                .ForMember(t => t.ProviderId, opt => opt.MapFrom(s => s.ProviderId))
                .ForMember(t => t.Name, opt => opt.MapFrom(s => s.Name))
                .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<UpdatePropertyBasicInfoRequest, Hotel>();
            CreateMap<Hotel, GetPropertyBasicInfoResponse>();

            CreateMap<CreateBookingRequest, CreateTransactionRequest>();
            CreateMap<CreateTransactionRequest, Transaction>();

            CreateMap<ConfirmInvoicePaymentRequest, Payment>();

            CreateMap<CreateServiceProfileRequest, ServiceProfile>();
            CreateMap<UpdateServiceProfileRequest, ServiceProfile>();

            CreateMap<ServiceNearbyQuery, ServiceNearby>();

            CreateMap<CreateReviewRequest, Review>();

            //Property Details Handlers
            CreateMap<UpdatePropertyDetailsRequest, Hotel>();
            CreateMap<Hotel, GetPropertyDetailsResponse>()
                     .ForMember(vr => vr.Facilities, opt => opt.MapFrom(v => v.Facilities.Select(vf => vf.FacilityId)));

            CreateMap<Hotel, GetServicesResponse>()
                .ForMember(vr => vr.Languages, opt => opt.MapFrom(v => v.Languages.Select(vf => vf.LanguageId)))
                .ForMember(vr => vr.Facilities, opt => opt.MapFrom(v => v.Facilities.Select(vf => vf.FacilityId)))
                .ForMember(vr => vr.Meals, opt => opt.MapFrom(v => v.HotelMeals))
                .ForMember(vr => vr.IsFoodServed, opt => opt.MapFrom(v => v.IsFoodServed))
                .ForAllOtherMembers(opt => opt.Ignore());

            // Payment Policy 
            CreateMap<Hotel, GetPaymentPolicyResponse>()
                        .ForMember(vr => vr.CreditCards, opt => opt.MapFrom(v => v.CreditCards.Select(vf => vf.CreditCardId)));

            CreateMap<UpdatePaymentPolicyRequest, Hotel>()
            .ForMember(v => v.CreditCards, opt => opt.Ignore())
            .AfterMap((vr, v) =>
            {

                // Remove unselected features
                var removedCards = v.CreditCards.Where(f => !vr.CreditCards.Contains(f.CreditCardId)).ToList();
                foreach (var f in removedCards)
                    v.CreditCards.Remove(f);

                // Add New Features
                var addedFeatures = vr.CreditCards.Where(id => !v.CreditCards.Any(f => f.CreditCardId == id))
                .Select(id => new HotelCreditCard { CreditCardId = id, HotelId = v.Id }).ToList();
                foreach (var f in addedFeatures)
                    v.CreditCards.Add(f);
            });


            // Get Promotions By ServiceId
            CreateMap<HotelPromotion, GetPromotionResponse>()
               .ForMember(v => v.Rooms, opt => opt.MapFrom(vr =>
                  (from r in dbContext.Rooms
                   join pr in vr.HotelPromotionRooms
                   on r.Id equals pr.RoomId
                   select new Room
                   {
                       Id = r.Id,
                       RoomTypeNameId = r.RoomTypeNameId
                   })
                    )
                );


            // Hotel Promotion 
            CreateMap<AddPromotionRequest, HotelPromotion>()
                .ForMember(v => v.HotelPromotionRooms, opt => opt.Ignore())
                .AfterMap((vr, v) =>
                {

                    // Add New Features
                    var addedRooms = vr.Rooms.Where(x => !v.HotelPromotionRooms.Any(f => f.RoomId == x.Id))
                    .Select(x => new HotelPromotionRoom { HotelPromotionId = v.Id, RoomId = x.Id }).ToList();
                    foreach (var f in addedRooms)
                        v.HotelPromotionRooms.Add(f);
                }
              );



            CreateMap<UpdatePromotionRequest, HotelPromotion>()
            .ForMember(v => v.HotelPromotionRooms, opt => opt.Ignore())
            .AfterMap((vr, v) =>
            {

                // Remove Rooms 
                var removedRooms = v.HotelPromotionRooms.Where(x => !vr.Rooms.Select(r => r.Id).Contains(x.RoomId)).ToList();
                foreach (var f in removedRooms)
                    v.HotelPromotionRooms.Remove(f);

                // Add New Features
                var addedRooms = vr.Rooms.Where(x => !v.HotelPromotionRooms.Any(f => f.RoomId == x.Id))
                .Select(x => new HotelPromotionRoom { HotelPromotionId = v.Id, RoomId = x.Id }).ToList();
                foreach (var f in addedRooms)
                    v.HotelPromotionRooms.Add(f);
            }
                );


            //  CreateMap<Hotel, getpromotio>()
            //  .ForMember(vr => vr.CreditCards, opt => opt.MapFrom(v => v.CreditCards.Select(vf => vf.CreditCardId)));

            //CreateBooking
            CreateMap<CreateBookingRequest, Booking>();

            //CreateMap<CreateBookingRequest, Booking>();

            /*
            CreateMap<CreatePaymentPolicyRequest, Hotel>()
              .ForMember(v => v.CreditCards, opt => opt.Ignore())
        .AfterMap((vr, v) =>
        {
                        // Remove unselected features 
                        var removedCards = v.CreditCards.Where(f => !vr.cre.Contains(f.creditCardId)).ToList();
            foreach (var f in removedCards)
                v.creditCards.Remove(f);

                        // Add New Features
                        var addedFeatures = vr.creditCards.Where(id => !v.creditCards.Any(f => f.creditCardId == id)).Select(id => new HotelCreditCard { creditCardId = id, hotelId = v.hotelId }).ToList();
            foreach (var f in addedFeatures)
                v.creditCards.Add(f);
        });*/

            // Hotel
            /*  CreateMap<Hotel, HotelDetails>()
                  .ForMember(vr => vr.Facilities, opt => opt.MapFrom(v => v.Facilities.Select(vf => vf.FacilityId)))
                  .ForMember(vr => vr.Rooms, opt => opt.MapFrom (v=> new RoomDetails { id }))
                  */

            CreateMap<UpdatePropertyDetailsRequest, Hotel>()
    .ForMember(v => v.Facilities, opt => opt.Ignore())
    .AfterMap((vr, v) =>
    {
        // Remove unselected features 
        var removedFeatures = v.Facilities.Where(f => !vr.Facilities.Contains(f.FacilityId)).ToList();
        foreach (var f in removedFeatures)
            v.Facilities.Remove(f);

        // Add New Features
        var addedFeatures = vr.Facilities.Where(id => !v.Facilities.Any(f => f.FacilityId == id)).Select(id => new HotelFacility { FacilityId = id, HotelId = v.Id }).ToList();
        foreach (var f in addedFeatures)
            v.Facilities.Add(f);
    });


            CreateMap<UpdateServicesRequest, Hotel>()

           .ForMember(v => v.Facilities, opt => opt.Ignore())
           .AfterMap((vr, v) =>
           {
               // Remove unselected features 
               var removedFeatures = v.Facilities.Where(f => !vr.Facilities.Contains(f.FacilityId)).ToList();
               foreach (var f in removedFeatures)
                   v.Facilities.Remove(f);

               // Add New Features
               var addedFeatures = vr.Facilities.Where(id => !v.Facilities.Any(f => f.FacilityId == id)).Select(id => new HotelFacility { FacilityId = id, HotelId = v.Id }).ToList();
               foreach (var f in addedFeatures)
                   v.Facilities.Add(f);
           })
           .ForMember(v => v.Languages, opt => opt.Ignore())
           .AfterMap((vr, v) =>
           {
               var removedLanguages = v.Languages.Where(f => !vr.Languages.Contains(f.LanguageId)).ToList();
               foreach (var f in removedLanguages)
                   v.Languages.Remove(f);

               // Add New Features
               var addedLanguages = vr.Languages.Where(id => !v.Languages.Any(f => f.LanguageId == id)).Select(id => new HotelLanguage { LanguageId = id, HotelId = v.Id }).ToList();
               foreach (var f in addedLanguages)
                   v.Languages.Add(f);

           })
           .ForMember(v => v.HotelMeals, opt => opt.Ignore())
           .AfterMap((vr, v) =>
           {
               if (v.HotelMeals == null)
               {
                   v.HotelMeals = new HotelMeals()
                   {
                       HotelId = v.Id,
                       BreakFast = vr.Meals.BreakFast,
                       BreakFastRate = vr.Meals.BreakFastRate,
                       Lunch = vr.Meals.Lunch,
                       LunchRate = vr.Meals.LunchRate,
                       Dinner = vr.Meals.Dinner,
                       DinnerRate = vr.Meals.DinnerRate
                   };
               }
               else
               {
                   v.HotelMeals.HotelId = v.Id;
                   v.HotelMeals.BreakFast = vr.Meals.BreakFast;
                   v.HotelMeals.BreakFastRate = vr.Meals.BreakFastRate;
                   v.HotelMeals.Lunch = vr.Meals.Lunch;
                   v.HotelMeals.LunchRate = vr.Meals.LunchRate;
                   v.HotelMeals.Dinner = vr.Meals.Dinner;
                   v.HotelMeals.DinnerRate = vr.Meals.DinnerRate;
               }
           });


            // Hotel Queries 
            //CreateMap<HotelQuery, PropertyDetails>();


            // Rooms 
            CreateMap<CreateRoomsRequest, Room>();
            CreateMap<Room, GetRoomsResponse>();
            CreateMap<UpdateRoomsRequest, Room>()
                .ForMember(s => s.Id, opt => opt.Ignore());


            CreateMap<Hotel, GetHotelConfigurationStateResponse>().ForMember(s => s.IsCalendarConfigured, opt => opt.Ignore());

            //Hotel Facilities
            CreateMap<CreateHotelFacilityTypeRequest, Hotel>();

            CreateMap<GetPropertyTypeRequest, PropertyType>();
            CreateMap<PropertyType, GetPropertyTypeResponse>();

            CreateMap<GetSmokingOptionRequest, SmokingOption>();
            CreateMap<SmokingOption, GetSmokingOptionResponse>();

            CreateMap<UpdateRoomAmenitiesRequest, Hotel>()
            .ForMember(v => v.RoomAmenities, opt => opt.Ignore())
            .AfterMap((vr, v) =>
            {
                // Removed features 
                var removedAmenities = v.RoomAmenities.Where(f => !vr.RoomAmenities.Any(r => r.RoomAmenityId == f.RoomAmenityId && r.RoomId == f.RoomId)).ToList();
                foreach (var f in removedAmenities)
                    v.RoomAmenities.Remove(f);

                // New Features
                var addedAmenities = vr.RoomAmenities.Where(r => !v.RoomAmenities.Any(f => f.RoomAmenityId == r.RoomAmenityId && f.RoomId == r.RoomId))
                .Select(r => new HotelRoomAmenity { RoomAmenityId = r.RoomAmenityId, RoomId = r.RoomId, HotelId = v.Id }).ToList();
                foreach (var f in addedAmenities)
                    v.RoomAmenities.Add(f);
            });


            // register list
            CreateMap<Country, GetCountryResponse>();
            CreateMap<Currency, GetCurrencyResponse>();
            CreateMap<City, GetCityResponse>();
            CreateMap<Localization, LocalizationQuery>();
            CreateMap<CreditCard, GetCreditCardResponse>();
            CreateMap<ReviewFeature, ReviewFeatureTypeQuery>();
            CreateMap<Language, GetLanguageResponse>();
            CreateMap<CancellationFee, GetCancellationFeeResponse>();
            CreateMap<CancellationPolicy, GetCancellationPolicyResponse>();
            CreateMap<InternetOption, GetInternetOptionsResponse>();
            CreateMap<ParkingOption, GetParkingOptionsResponse>();
            CreateMap<FacilityCategory, GetFacilityCategoryResponse>();
            CreateMap<HotelPhoto, GetPhotoResponse>();
            CreateMap<Facility, GetFacilitiesResponse>();
            CreateMap<RoomAmenity, GetRoomAmenityCategoryResponse>();
            CreateMap<RoomTypeName, GetRoomTypeNameResponse>();

            CreateMap<MessageCategory, GetMessagingCategoryResponse>();
            CreateMap<Message, MessageQuery>();

            CreateMap<AddNewCreditCardRequest, CreditCardInfo>();

        }
    }
}

