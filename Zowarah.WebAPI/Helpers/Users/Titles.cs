﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zowarah.WebAPI.Helpers.Users
{
    public enum Titles
    {
        MR = 1,
        MRS = 2
    }
}
