﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.Application.Services.Transactions;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Transactions;
using Zowarah.WebAPI.Handlers.Transactions.TransactionHandlers;

namespace Zowarah.WebAPI.Test.Transactions {
    public class TransactionControllerTest : BaseTesting {
        public string ApiUrl => "/api/Transactions/Transaction";
        public TransactionControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Get_ById() {
            var response = await Client.GetAsync($"{ApiUrl}/GetById/{1}");

            // OK
            response.EnsureSuccessStatusCode();

            // return 
            var content = await response.Content.ReadAsAsync<GetTransactionResponse>();
            content.Should().NotBeNull();
        }


        [Fact]
        public async Task Test_Get_ClientTransactions() {
            var response = await Client.GetAsync($"{ApiUrl}/GetClientTransactions/{3}");

            // OK
            response.EnsureSuccessStatusCode();

            // return 
            var content = await response.Content.ReadAsAsync<List<GetServiceTransaction>>();
            content.Should().NotBeNullOrEmpty();
        }


        [Theory]
        [InlineData(TransactionStatus.Cancelled,null,0,0,0,true,0)]
        [InlineData(TransactionStatus.Confirmed,null,0,0,0,true, 0)]
        [InlineData(TransactionStatus.NoShow, null,0,0,0,true, 0)]
        [InlineData(TransactionStatus.NoShow, null,0,0,0,true, 1)]
        [InlineData(TransactionStatus.NoShow, null,1,0,0,true, 1)]
        [InlineData(TransactionStatus.NoShow, null,1,1,0,true, 1)]
        [InlineData(TransactionStatus.NoShow, null, 1,1,3,true, 1)]        
        [InlineData(TransactionStatus.Confirmed, null, 0, 0, 0, true, 1)]
        [InlineData(TransactionStatus.Confirmed, null, 1, 0, 0, true, 1)]
        [InlineData(TransactionStatus.Confirmed, null, 1, 1, 0, true, 1)]
        [InlineData(TransactionStatus.Confirmed, null, 1, 1, 3, true, 1)]
        public async Task Test_Get_AllBookingTransactions(TransactionStatus status,string invoiceId, int roomTypeId,int city, long clientId, bool isShowup, long serviceId) {
            var response = await Client.PostAsync($"{ApiUrl}/GetAllBookingTransactions", new StringContent(
                JsonConvert.SerializeObject(
                            new GetAllBookingTransactionsRequest() {
                                From = new DateTime(2018, 01, 01),
                                To = new DateTime(2018, 12, 01),
                                InvoiceId=invoiceId,
                                RoomTypeId=roomTypeId,
                                ServiceCity=city,
                                Status = status,
                                ClientId = clientId,
                                IsShowUp = isShowup,
                                ServiceId = serviceId
                            }), Encoding.UTF8, "application/json"));

            // OK
            response.EnsureSuccessStatusCode();

            // return 
            var content = await response.Content.ReadAsAsync<ICollection<BookingTransaction>>();
            content.Should().NotBeNullOrEmpty();
        }


        [Theory]
        [InlineData(0,null, 0, 0, ServiceTypeEnum.All, TransactionStatus.Cancelled)]
        [InlineData(0,null, 0, 0, ServiceTypeEnum.All, TransactionStatus.Confirmed)]
        [InlineData(0,null, 0, 0, ServiceTypeEnum.All, TransactionStatus.NoShow)]
        [InlineData(0,null, 0, 1, ServiceTypeEnum.All, TransactionStatus.Confirmed)]
        [InlineData(0,null, 1, 1, ServiceTypeEnum.All, TransactionStatus.Confirmed)]
        [InlineData(3,null, 1, 1, ServiceTypeEnum.All, TransactionStatus.Confirmed)]
        public async Task Test_Get_AllTransactionsByFilter(long clientId,string invoiceId,long providerId,long serviceId, ServiceTypeEnum serviceType, TransactionStatus status) {
            var response = await Client.PostAsync($"{ApiUrl}/GetAllTransactionsByFilter/{1}/{10}", new StringContent(
                JsonConvert.SerializeObject(
                            new GetTransactionsByFilterRequest() {
                                From = new DateTime(2018, 01, 01),
                                To = new DateTime(2018, 12, 01),
                                ClientId = clientId,
                                InvoiceId = invoiceId,
                                ProviderId=providerId,
                                ServiceId=serviceId,
                                ServiceType=serviceType,
                                Status=status
                            }), Encoding.UTF8, "application/json"));

            // OK
            response.EnsureSuccessStatusCode();

            // return 
            var content = await response.Content.ReadAsAsync<GetTransactionsByFilterResponse>();
            content.Should().NotBeNull();
        }

        
             [Theory]   
        [InlineData(0, null, 0, 0, ServiceTypeEnum.All, TransactionStatus.Confirmed)]
       // [InlineData(0, null, 0, 0, ServiceTypeEnum.All, TransactionStatus.NoShow)]
        [InlineData(0, null, 0, 1, ServiceTypeEnum.All, TransactionStatus.Confirmed)]
        [InlineData(0, null, 1, 1, ServiceTypeEnum.All, TransactionStatus.Confirmed)]
        [InlineData(3, null, 1, 1, ServiceTypeEnum.All, TransactionStatus.Confirmed)]
        public async Task Test_Get_TranactionsReportByFilter(long clientId, string invoiceId, long providerId, long serviceId, ServiceTypeEnum serviceType, TransactionStatus status) {
            var response = await Client.PostAsync($"{ApiUrl}/GetTranactionsReportByFilter", new StringContent(
                JsonConvert.SerializeObject(
                            new GetTranactionsReportByFilterRequest() {
                                From = new DateTime(2018, 01, 01),
                                To = new DateTime(2018, 12, 01),
                                ClientId = clientId,
                                InvoiceId = invoiceId,
                                ProviderId = providerId,
                                ServiceId = serviceId,
                                ServiceType = serviceType,
                                Status = status
                            }), Encoding.UTF8, "application/json"));

            // OK
            response.EnsureSuccessStatusCode();

            // return 
            var content = await response.Content.ReadAsAsync<ICollection<TransactionReport>>();
            content.Should().NotBeNullOrEmpty();
        }


        [Theory]
       // [InlineData("1",TransactionStatus.Cancelled)]
        [InlineData("3",TransactionStatus.NoShow)]
        public async Task Test_Update_TranascationStatus(string id,TransactionStatus status) {
            var response = await Client.PostAsync($"{ApiUrl}/UpdateTranascationStatus", new StringContent(
                JsonConvert.SerializeObject(
                            new UpdateTransactionSatusRequest() {
                               Id=id,
                               Comment="done",
                                Status = status
                            }), Encoding.UTF8, "application/json"));

            // OK
            response.EnsureSuccessStatusCode();

            // return 
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNull();

            // ToDo: replace ownerId with id in transactions table
            // ToDo: add all status
        }

        [Fact]
        public async Task Test_Get_ProviderTransactions() {
            var response = await Client.GetAsync($"{ApiUrl}/GetProviderTransactions/{1}");

            // OK
            response.EnsureSuccessStatusCode();

            // return 
            var content = await response.Content.ReadAsAsync<List<GetServiceTransaction>>();
            content.Should().NotBeNullOrEmpty();

            // ToDo: remove this function; no need anymore
        }


        [Fact]
        public async Task Test_Cancel_Transaction() {
            var response = await Client.DeleteAsync($"{ApiUrl}/CancelTransaction/{2}");

            // OK
            response.EnsureSuccessStatusCode();

           
        }

    }
}
