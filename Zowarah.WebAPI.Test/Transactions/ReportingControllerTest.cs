﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.Application.Enums.Hotels;
using Zowarah.WebAPI.Handlers.Transactions;

namespace Zowarah.WebAPI.Test.Transactions
{
    public class ReportingControllerTest : BaseTesting {
        public string ApiUrl => "/api/Transactions/Reporting";
        public ReportingControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Get_Transactions() {
            var response = await Client.PostAsync($"{ApiUrl}/GetTransactions", new StringContent(
                 JsonConvert.SerializeObject(
                             new GetSalesReportRequest() {
                                City=0,
                                ClientId=0,
                                From=new DateTime(2018,01,01),
                                To= new DateTime(2018, 08, 01),
                                ServiceType=0,
                                View=ReportView.years
                             }), Encoding.UTF8, "application/json"));

            // OK
            response.EnsureSuccessStatusCode();

            // return 
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }


        [Fact]
        public async Task Test_Get_TransactionDetails() {
            var response = await Client.PostAsync($"{ApiUrl}/TransactionDetails/2018-01-01", new StringContent(
                 JsonConvert.SerializeObject(
                             new GetSalesReportDetailsRequest() {                                
                                 City = 0,
                                 ClientId = 0,
                                 From = new DateTime(2018, 01, 01),
                                 To = new DateTime(2018, 08, 01),
                                 ServiceType = 0,
                                 View = ReportView.years
                             }), Encoding.UTF8, "application/json"));

            // OK
            response.EnsureSuccessStatusCode();

            // return 
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }



    }
}
