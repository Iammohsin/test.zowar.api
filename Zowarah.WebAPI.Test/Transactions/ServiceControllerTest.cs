﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.Domain.Entities.Common;
using Zowarah.WebAPI.Handlers.Transactions;

namespace Zowarah.WebAPI.Test.Transactions
{
    public class ServiceControllerTest : BaseTesting {
        public string ApiUrl => "/api/Transactions/Service";
        public ServiceControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Get_PendingRequests() {
            // get Pending and Approve count
            var response = await Client.GetAsync($"{ApiUrl}/GetPendingRequests");

            // OK
            response.EnsureSuccessStatusCode();

            // return 
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Get_Requests() {
            // new GetRequestsRequest {}
            var response = await Client.GetAsync($"{ApiUrl}/GetRequests?CityId={0}&From=2018-01-01&To=2018-12-31&PageNumber={1}&ServiceType={1}");

            // OK
            response.EnsureSuccessStatusCode();

            // return 
            var content = await response.Content.ReadAsAsync<GetRequestsResponse>();
            content.Should().NotBeNull();
        }

        [Fact]
        public async Task Test_Services() {
            // new GetRequestsRequest {}
            var response = await Client.PostAsync($"{ApiUrl}/GetServices/1/10",
                new StringContent(
                JsonConvert.SerializeObject(
                            new GetServicesRequest() {
                               City=0,
                               From=new DateTime(2018,01,01),
                               To=new DateTime(2018,12,01),
                               Status=0,
                               ServiceType=0,
                               ProviderId=0,
                               ServiceId=0
                            }), Encoding.UTF8, "application/json")
                );

            // OK
            response.EnsureSuccessStatusCode();

            // return 
            var content = await response.Content.ReadAsAsync<GetRequestsResponse>();
            content.Should().NotBeNull();
        }


        [Theory]
        [InlineData(ServiceStatusEnum.Submitted)]
        [InlineData(ServiceStatusEnum.InProgress)]
        [InlineData(ServiceStatusEnum.Rejected)]
        [InlineData(ServiceStatusEnum.Suspended)]
        [InlineData(ServiceStatusEnum.UnderReview)]
        [InlineData(ServiceStatusEnum.Approved)]
        public async Task Test_Update_RequestStatus(ServiceStatusEnum status) {
            // new GetRequestsRequest {}
            var response = await Client.PutAsync($"{ApiUrl}/UpdateRequestStatus",
                new StringContent(
                JsonConvert.SerializeObject(
                            new UpdateRequestStatusRequest() {
                              ServiceId=1,
                              Comments="hi",
                              ServiceStatus=status
                            }), Encoding.UTF8, "application/json")
                );

            // OK
            response.EnsureSuccessStatusCode();

            
        }
    }
}
