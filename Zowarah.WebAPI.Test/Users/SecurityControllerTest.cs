﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.WebAPI.Handlers.Users;
using Zowarah.WebAPI.Handlers.Users.Security;

namespace Zowarah.WebAPI.Test.Users {
    public class SecurityControllerTest : BaseTesting {
        public string ApiUrl => "/api/Users/Security";
        public SecurityControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Change_Password() {
            var response = await Client.PostAsync($"{ApiUrl}/ChangePassword", new StringContent(
                 JsonConvert.SerializeObject(
                    new ChangePasswordRequest {
                        CurrentPassword = "Hatem@123",
                        NewPassword = "Hate@3456",
                        UserId = "1"
                    }), Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Test_Forgot_Password() {
            var response = await Client.PostAsync($"{ApiUrl}/ForgotPassword", new StringContent(
                 JsonConvert.SerializeObject(
                    new ForgotPasswordRequest {                       
                        Email = "hatem.alhassani@mediu.my"
                    }), Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Rest_Password() {
            var response = await Client.PutAsync($"{ApiUrl}/ForgotPassword", new StringContent(
                 JsonConvert.SerializeObject(
                    new ResetPasswordRequest {
                        NewPassword = "Hatem#$%1111",
                        UserId="1",
                        UserToken="token"
                    }), Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }
    }
}
