﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.WebAPI.Handlers.Users;

namespace Zowarah.WebAPI.Test.Users
{
    public class SignupControllerTest : BaseTesting {
        public string ApiUrl => "/api/Users/Signup";
        public SignupControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Signup() {
            var response = await Client.PostAsync($"{ApiUrl}/Signup", new StringContent(
                 JsonConvert.SerializeObject(
                    new SignupUserRequest {    
                        Email="hatem.alhassani@mediu.edu.my",
                        Password = "Hate@3456",
                        CountryCode="965",
                        FirstName="test",
                        LastName="hello",
                        Role=Domain.Entities.Users.RoleTypes.Client                        
                    }), Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();
        }


        [Fact]
        public async Task Test_ReSendConfirmEmail() {
            var response = await Client.PostAsync($"{ApiUrl}/ReSendConfirmEmail/{1}/{"token"}", new StringContent(
                 "", Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();
        }



        [Fact]
        public async Task Test_ConfirmEmail() {
            var response = await Client.PostAsync($"{ApiUrl}/ReSendConfirmEmail/{1}", new StringContent(
                 "", Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();
        }

    }
}
