﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.WebAPI.Handlers.Users;
using static Zowarah.Identity.Api.Handlers.UserHandlers.GetProfile;

namespace Zowarah.WebAPI.Test.Users
{
    public class ProfilesControllerTest : BaseTesting {
        public string ApiUrl => "/api/Users/profiles";
        public ProfilesControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Get_Profile_Currency() {
            var response = await Client.GetAsync($"{ApiUrl}/GetCurrency/{1}");
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Get_Profile() {
            var response = await Client.GetAsync($"{ApiUrl}/Get/{1}");
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetProfileResponse>();
            content.Should().NotBeNull();
        }

        [Fact]
        public async Task Test_Get_Preferences() {
            var response = await Client.GetAsync($"{ApiUrl}/GetPreferences/{1}");
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetPreferencesResponse>();
            content.Should().NotBeNull();
        }
    }
}
