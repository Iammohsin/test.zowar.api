﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.Domain.Entities.Users;
using Zowarah.WebAPI.Handlers.Users.Users;

namespace Zowarah.WebAPI.Test.Users {
    public class UsersControllerTest : BaseTesting {
        public string ApiUrl => "/api/Users";
        public UsersControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Get_Users() {
            var response = await Client.GetAsync($"{ApiUrl}/Get");
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<List<GetUsersResponseItem>>();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Get_User_ById() {
            var response = await Client.GetAsync($"{ApiUrl}/GetUserById/{1}");
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetUserByIdResponse>();
            content.Should().NotBeNull();
        }


        [Fact]
        public async Task Test_Is_User_In_Role() {
            var response = await Client.GetAsync($"{ApiUrl}/IsInRole?Id={1}&Role={RoleTypes.Administrator}");
            // Assert             
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Test_Create_User() {
            var response = await Client.PostAsync($"{ApiUrl}/CreateUser",
                new StringContent(
                 JsonConvert.SerializeObject(
                    new CreateUserRequest {
                        Email = "hatem.alhassani@mediu.my",
                        FirstName="test",
                        LastName="test",
                        Role=RoleTypes.Agent,
                        Permissions=new List<long>()
                    }), Encoding.UTF8, "application/json")
                );
            // Assert             
            response.EnsureSuccessStatusCode();
        }


        [Fact]
        public async Task Test_Update_User() {
            var response = await Client.PutAsync($"{ApiUrl}/UpdateUser",
                new StringContent(
                 JsonConvert.SerializeObject(
                    new UpdateUserRequest {
                        Id=1,
                        Email = "hatem.alhassani@mediu.my",
                        FirstName = "test",
                        LastName = "test",                        
                        Permissions = new List<long>()
                    }), Encoding.UTF8, "application/json")
                );
            // Assert             
            response.EnsureSuccessStatusCode();
        }


        [Fact]
        public async Task Test_ChangeStatus() {
            var response = await Client.PostAsync($"{ApiUrl}/ChangeStatus",
                new StringContent(
                 JsonConvert.SerializeObject(
                    new ChangeStatusRequest {
                      Id="1",
                      Status=true
                    }), Encoding.UTF8, "application/json")
                );
            // Assert             
            response.EnsureSuccessStatusCode();
        }


        [Fact]
        public async Task Test_Get_Admin_Permissions() {
            var response = await Client.GetAsync($"{ApiUrl}/GetAdminPermissions");
            // Assert             
            response.EnsureSuccessStatusCode();
        }


    }
}
