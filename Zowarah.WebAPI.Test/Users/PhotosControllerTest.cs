﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Zowarah.WebAPI.Test.Users
{
    public class PhotosControllerTest : BaseTesting {
        public string ApiUrl => "/api/Users/Photos";
        public PhotosControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Get_Profile_Photo() {
            var response = await Client.GetAsync($"{ApiUrl}/Get/{1}");
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Add_Profile_Photo() {
            throw new NotImplementedException();
        }
    }
}
