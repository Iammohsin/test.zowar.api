﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Zowarah.WebAPI.Test.Users
{
    public class RolesControllerTest : BaseTesting {
        public string ApiUrl => "/api/Users/Roles";
        public RolesControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Create_Role() {
            var response = await Client.PostAsync($"{ApiUrl}/{"RoleName"}", new StringContent("", Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();          
        }
    }
}

