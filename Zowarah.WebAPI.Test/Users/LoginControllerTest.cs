﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.Domain.Entities.Users;
using Zowarah.WebAPI.Handlers.Common;

namespace Zowarah.WebAPI.Test.Users {
    public class LoginControllerTest : BaseTesting {
        public string ApiUrl => "/api/Users/Login";
        public LoginControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Login() {
            var response = await Client.PostAsync($"{ApiUrl}/Login", new StringContent(
                JsonConvert.SerializeObject(
                            new LoginUserRequest() {
                              Email="hatem.alhassani@mediu.my",
                              Password="Hatem@123",
                              Role=RoleTypes.Administrator
                            }), Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }
    }
}
