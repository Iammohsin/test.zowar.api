﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentAssertions;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using Newtonsoft.Json;
using Zowarah.WebAPI.Handlers.Payments;

namespace Zowarah.WebAPI.Test.Payments {
    public class PaymentsControllerTest : BaseTesting {
        public string ApiUrl => "/api/payments/Payments";
        public PaymentsControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Confirm_InvoicePayment() {
            var response = await Client.PostAsync($"{ApiUrl}/ConfirmInvoicePayment", new StringContent(
                JsonConvert.SerializeObject(
                            new ConfirmInvoicePaymentRequest() {
                                AccountNo = "1234",
                                Bank = "test",
                                Date = DateTime.Now,
                                InvoiceId = "1",
                                Reference = "1"
                            }), Encoding.UTF8, "application/json"));

            // OK
            response.EnsureSuccessStatusCode();

            // return 
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }
    }
}
