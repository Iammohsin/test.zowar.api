﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Xunit;


namespace Zowarah.WebAPI.Test {
    public class BaseTesting : IClassFixture<CustomWebApplicationFactory<Startup>> {
        private readonly HttpClient _client;
        public HttpClient Client {
            get {
                // Mapper.Reset();
                //ServiceCollectionExtensions.UseStaticRegistration = false;
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + JwtToken());
                return _client;
            }
        }
        public CustomWebApplicationFactory<Startup> _factory { get; private set; }

        public BaseTesting(CustomWebApplicationFactory<Startup> factory) {
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions {
                AllowAutoRedirect = false
            });
            
            _factory = factory;
        }
        public int RandomInt(int min, int max) {
            return new Random().Next(min, max);
        }

        public string RandomText(int maxLetter = 100) {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, maxLetter).Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public string GetCurrency() {
            return "MR";
        }
        public long providerId() {
            return 2;
        }

        public string JwtToken() {
            var claims = new[] {
                new Claim("id", "1"),
                new Claim("email", "hatem.alhassani@mediu.my"),
                new Claim("role", "Administrator"),
                new Claim("currency", "SA"),
                new Claim("name","Admin"),
                new Claim("culture",  "en" ),
                new Claim("photo", "user-placeholder.jpg" ),
                new Claim("privs", "")
            };

            var _configuration = _factory.Configuration;

            var token = new JwtSecurityToken
                    (
                        issuer: _configuration.GetValue<string>("Token:Issuer"),
                        audience: _configuration.GetValue<string>("Token:Audience"),
                        claims: claims,
                        expires: DateTime.UtcNow.AddDays(60),
                        notBefore: DateTime.UtcNow,
                        signingCredentials: new SigningCredentials(new SymmetricSecurityKey
                        (Encoding.UTF8.GetBytes(_configuration.GetValue<string>("Token:Key"))),
                        SecurityAlgorithms.HmacSha256)
                      );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
