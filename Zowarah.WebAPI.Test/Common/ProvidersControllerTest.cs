﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.WebAPI.Handlers.Common;

namespace Zowarah.WebAPI.Test.Common
{
    public class ProvidersControllerTest : BaseTesting {
        public string ApiUrl => "/api/Common/Providers";
        public ProvidersControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }


       [Fact]
        public async Task Test_GetAll_Providers() {
            var response = await Client.GetAsync($"{ApiUrl}/getAll/{1}/{10}" +
                $"/{DateTime.Now.ToString("yyyy-01-01")}/{DateTime.Now.ToString("yyyy-12-31")}");
            // Assert
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetProvidersResponse>();
            content.Total.Should().BeGreaterThan(0);
            content.providers.Should().NotBeNullOrEmpty();
        }


        [Fact]
        public async Task Test_Get_ProvidersReport() {
            var response = await Client.GetAsync($"{ApiUrl}/ProvidersReport" +
                $"/{DateTime.Now.ToString("yyyy-01-01")}/{DateTime.Now.ToString("yyyy-12-31")}");
            // Assert
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetProvidersReportResponse>();
            content.Should().NotBeNull();
        }
    }
}
