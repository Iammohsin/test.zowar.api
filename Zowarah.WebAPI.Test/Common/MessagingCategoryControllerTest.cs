﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.WebAPI.Handlers.Common;

namespace Zowarah.WebAPI.Test.Common {
    public class MessagingCategoryControllerTest : BaseTesting {
        public string ApiUrl => "/api/Common/MessagingCategory";
        public MessagingCategoryControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_MessageCategory_GetAll() {
            var response = await Client.GetAsync($"{ApiUrl}/GetAll");
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<IList<GetMessagingCategoryResponse>>();
            content.Should().NotBeNullOrEmpty();
        }
    }
}
