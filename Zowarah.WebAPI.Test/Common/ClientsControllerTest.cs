﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.Domain.Enums;
using Zowarah.WebAPI.Handlers.Users.Clients;

namespace Zowarah.WebAPI.Test.Common
{
    public class ClientsControllerTest : BaseTesting {
        public string ApiUrl => "/api/Users/Clients";
        public ClientsControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }


        [Fact]
        public async Task Test_Get_Clients() {
            /*
             &" +
                $"FromDate={DateTime.Now.AddYears(-1).ToString("yyyy-MM-dd")}&" +
                $"ToDate={DateTime.Now.AddYears(1).ToString("yyyy-MM-dd")}
             */
            var response = await Client.GetAsync($"{ApiUrl}/GetOverview?PageNumber={1}&PageSize={10}" +
                $"&Country={1}&period={PeriodEnum.thisYear}");
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetClientsOverviewResponse>();
            content.Should().NotBeNull();
        }
    }
}
