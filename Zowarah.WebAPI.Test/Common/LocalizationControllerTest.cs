﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.WebAPI.Handlers.Common;
using static Zowarah.WebAPI.Handlers.Common.GetLocalizationListResponse;

namespace Zowarah.WebAPI.Test.Common {
    public class LocalizationControllerTest : BaseTesting {
        public string ApiUrl => "/api/Common/Localization";
        public LocalizationControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Get_Language_Localizations() {
            var response = await Client.GetAsync($"{ApiUrl}/getAll/{"en"}");
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<ICollection<LocalizationQuery>>();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Add_Language_Localization() {
            var response = await Client.PostAsync($"{ApiUrl}/add", new StringContent(
                JsonConvert.SerializeObject(
                            new AddLocalizationRequest() {
                               Id="test22",
                               LocText="test"
                            }), Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<bool>();
            content.Should().BeTrue();
        }

        [Fact]
        public async Task Test_Update_Text_Language_Localization() {
            var response = await Client.PutAsync($"{ApiUrl}/UpdateText", new StringContent(
                JsonConvert.SerializeObject(
                            new UpdateLocalizationTextRequest() {
                                Id = "test",
                                LanguageId="en",
                                LocText = "test"
                            }), Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<bool>();
            content.Should().BeTrue();
        }
        
       




    }
}
