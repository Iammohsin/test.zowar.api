﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.Application.Services.Common;
using Zowarah.Domain.Entities.Reviews;
using Zowarah.WebAPI.Handlers.Reviews;

namespace Zowarah.WebAPI.Test.Common {
    public class ReviewsControllerTest : BaseTesting {
        public string ApiUrl => "/api/Common/Reviews";
        public ReviewsControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }


        [Fact]
        public async Task Test_Get_Review_FeatureTypes() {
            var response = await Client.GetAsync($"{ApiUrl}/GetFeatureTypes");
            // Assert
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<List<ReviewFeatureTypeQuery>>();
            content.Should().NotBeNullOrEmpty();
        }


        [Fact]
        public async Task Test_Get_Review_ByServiceId() {
            var response = await Client.GetAsync($"{ApiUrl}/GetByServiceId/{1}/{1}/{10}/{ReviewOrderbyEnum.Most_recent}");
            // Assert
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<List<ReviewQuery>>();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Get_Service_Reviews_Score() {
            var response = await Client.GetAsync($"{ApiUrl}/GetServiceReviewsScore/{1}");
            // Assert
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetServiceReviewsScoreResponse>();
            content.ReviewsScore.Should().BeGreaterThan(0);
            content.TotalReviews.Should().BeGreaterThan(0);
            content.Features.Should().NotBeNullOrEmpty();
            content.Averages.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Reply_Review() {
            var response = await Client.PostAsync($"{ApiUrl}/ReplyReview",
                 new StringContent(
                JsonConvert.SerializeObject(
                            new CreateReplyReviewRequest() {
                                Id = "1",
                                Comment = "test"
                            }), Encoding.UTF8, "application/json"));
            // Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Test_Create_Review() {
            var response = await Client.PostAsync($"{ApiUrl}/CreateReview",
                 new StringContent(
                JsonConvert.SerializeObject(
                            new CreateReviewRequest() {
                                ClientId = 3,
                                Comment = "test",
                                ServiceId = 1,
                                TransactionId = "1",
                                Title = "good",
                                ReviewFeatures = new List<ReviewFeature> {
                                    new ReviewFeature{
                                        ReviewFeatureTypeId =1,
                                        ReviewId ="1",
                                        Score =5
                                    }
                                }
                            }), Encoding.UTF8, "application/json"));
            // Assert
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }


    }
}
