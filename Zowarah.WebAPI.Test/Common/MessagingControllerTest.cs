﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Users;
using Zowarah.Domain.Queries.Common;
using Zowarah.WebAPI.Handlers.Common;

namespace Zowarah.WebAPI.Test.Common {
    public class MessagingControllerTest : BaseTesting {
        public string ApiUrl => "/api/Common/Messaging";
        public MessagingControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Get() {
            var response = await Client.GetAsync($"{ApiUrl}/Get/{"1"}");
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetMessageResponse>();
            content.Should().NotBeNull();
        }


        [Fact]
        public async Task Test_Compose() {
            var response = await Client.PostAsync($"{ApiUrl}/Compose", new StringContent(
                JsonConvert.SerializeObject(
                            new ComposeMessageRequest() {
                                Body = "hi test!, we compose the message from test method",
                                MessageCategoryId = 1,
                                SenderId=1,
                                RecieverId=0,
                                RecieverRole=Domain.Entities.Users.RoleTypes.Administrator,
                                ReferenceId="1",
                                TransactionTypeId=1,
                                Title="hi bro!, Are you fine? please reply on my message",
                                MessageTrails = new List<MessageTrail> {
                                   new MessageTrail{
                                       SenderId=1,
                                       Body ="hi test!, we compose the message from test method"
                                   }
                               }
                            }), Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Reply() {
            var response = await Client.PostAsync($"{ApiUrl}/reply", new StringContent(
                JsonConvert.SerializeObject(
                            new ReplyMessageRequest() {
                                MessageId="1",
                                Body = "hi test!, i am sorry that i make you wait for a while, please stop sending any message",
                               SenderId=1
                            }), Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Close() {
            var response = await Client.PostAsync($"{ApiUrl}/Close/{"1"}", new StringContent(
               "", Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<Boolean>();
            content.Should().BeTrue();
        }

        [Fact]
        public async Task Test_Delete() {
            var response = await Client.DeleteAsync($"{ApiUrl}/Delete/{1}/{"1"}");
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<Boolean>();
            content.Should().BeTrue();
        }


        [Fact]
        public async Task Test_Get_NoPendingMessages() {
            var response = await Client.GetAsync($"{ApiUrl}/GetNoPendingMessages/{1}/{RoleTypes.Administrator}");
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<int>();
            content.Should().BeGreaterOrEqualTo(0);
        }


        [Fact]
        public async Task Test_AdminNewMessagesCount() {
            var response = await Client.GetAsync($"{ApiUrl}/AdminNewMessagesCount");
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<int>();
            content.Should().BeGreaterOrEqualTo(0);
        }


        [Fact]
        public async Task Test_AdminActiveMessagesCount() {
            var response = await Client.GetAsync($"{ApiUrl}/AdminActiveMessagesCount");
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<int>();
            content.Should().BeGreaterOrEqualTo(0);
        }

        [Fact]
        public async Task Test_Get_AdminNewMessages() {
            var response = await Client.GetAsync($"{ApiUrl}/GetAdminNewMessages");
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<List<MessageQuery>>();
            content.Should().NotBeNullOrEmpty();
        }


        [Fact]
        public async Task Test_Get_AdminActiveMessages() {
            var response = await Client.GetAsync($"{ApiUrl}/GetAdminActiveMessages");
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<List<MessageQuery>>();
            content.Should().NotBeNullOrEmpty();
        }


        [Fact]
        public async Task Test_StartProcessing() {
            var response = await Client.PostAsync($"{ApiUrl}/StartProcessing?messageId={"1"}&UserId{1}", new StringContent(
                "", Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<Boolean>();
            content.Should().BeTrue();
        }

        [Fact]
        public async Task Test_MarkRead() {
            var response = await Client.PostAsync($"{ApiUrl}/MarkRead" , new StringContent(
                JsonConvert.SerializeObject(
                            new MarkReadRequest() {
                               MessageId="1",
                               UserId=1
                            }), Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<Boolean>();
            content.Should().BeTrue();
        }


        [Fact]
        public async Task Test_GetMessages() {
            var response = await Client.PostAsync($"{ApiUrl}/GetMessages", new StringContent(
                JsonConvert.SerializeObject(
                            new GetMessagesRequest() {
                                FromDate=DateTime.Now.AddDays(-10),
                                IsAll=true,
                                Page=1,
                                PageSize=10,
                                RoleType=RoleTypes.Administrator,
                                ToDate=DateTime.Now
                            }), Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetMessagesResponse>();
            content.Should().NotBeNull();
        }


        [Fact]
        public async Task Test_IsValidTransaction() {
            var response = await Client.PostAsync($"{ApiUrl}/IsValidTransaction", new StringContent(
                JsonConvert.SerializeObject(
                            new IsValidTransactionRequest() {
                                RoleType = RoleTypes.Client,
                                TransId = "3",
                                TransTypeId = MessagingTransTypes.Booking,
                                UserId =3
                            }), Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<Boolean>();
            content.Should().BeTrue();
        }
    }
}
