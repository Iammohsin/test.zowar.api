﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.WebAPI.Handlers.Common;

namespace Zowarah.WebAPI.Test.Common {
    public class LookUpControllerTest : BaseTesting {
        public string ApiUrl => "/api/Common/LookUp";
        public LookUpControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Theory]
        [InlineData("LookUpTable")]
        [InlineData("LookUpModel")]
        [InlineData("BreakFastOption")]
        public async Task Test_GetAll_ByName(string Name) {
            var response = await Client.GetAsync($"{ApiUrl}/get/{Name}");
            // Assert
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<List<dynamic>>();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Create() {
            var response = await Client.PostAsync($"{ApiUrl}/Update/{"BreakFastOption"}", new StringContent(
                JsonConvert.SerializeObject(
                            new BreakFastOption() {                             
                              Description="test"
                            }), Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();           
        }

        [Fact]
        public async Task Test_Update() {
            var response = await Client.PostAsync($"{ApiUrl}/Update/{"BreakFastOption"}", new StringContent(
                JsonConvert.SerializeObject(
                            new BreakFastOption() {
                                Id =1,
                                Description = "test"
                            }), Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();
        }
    }
}
