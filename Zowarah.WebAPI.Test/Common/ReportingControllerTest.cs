﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.Application.Services.Common;
using Zowarah.Domain.Entities.Common;
using Zowarah.WebAPI.Handlers.Common.Reporting;

namespace Zowarah.WebAPI.Test.Common {
    public class ReportingControllerTest : BaseTesting {
        public string ApiUrl => "/api/Common/Reporting";
        public ReportingControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }


        [Fact]
        public async Task Test_Get_Admin_Landing_Page_Dashboard() {
            var response = await Client.GetAsync($"{ApiUrl}/GetAdminLandingPageDashboard");
            // Assert
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetAdminDashboardResponse>();
            content.Should().NotBeNull();
        }


        [Theory]
        [InlineData(0, "", 0, 0, ServiceStatusEnum.Approved)]
        [InlineData(0, "", 0, 0, ServiceStatusEnum.All)]
        [InlineData(0, "1", 0, 0, ServiceStatusEnum.All)]
        [InlineData(1, "1", 0, 0, ServiceStatusEnum.All)]
        [InlineData(0, "1", 1, 0, ServiceStatusEnum.All)]
        [InlineData(0, "1", 0, 1, ServiceStatusEnum.All)]
        public async Task Test_Get_ServicesReport(int cityId, string invoiceId, int providerId, long serviceId, ServiceStatusEnum status) {
            var response = await Client.PostAsync($"{ApiUrl}/GetServicesReport", new StringContent(
                JsonConvert.SerializeObject(
                            new GetServicesReportRequest() {
                                City = cityId,
                                ServiceType = Domain.Entities.Common.ServiceTypeEnum.Hotel,
                                From = DateTime.Now,
                                To = DateTime.Now,
                                InvoiceId = invoiceId,
                                ProviderId = providerId,
                                ServiceId = serviceId,
                                Status = status
                            }), Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<ICollection<ServiceReport>>();
            content.Should().NotBeNullOrEmpty();
        }

    }
}
