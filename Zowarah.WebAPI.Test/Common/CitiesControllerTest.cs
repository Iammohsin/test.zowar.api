﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.WebAPI.Handlers.Common;

namespace Zowarah.WebAPI.Test.Common {
    public class CitiesControllerTest : BaseTesting {
        public string ApiUrl => "/api/Common/City";
        public CitiesControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Search_Cities() {
            var response = await Client.PostAsync($"{ApiUrl}/Search", new StringContent(
                JsonConvert.SerializeObject(
                            new SearchCityRequest() {
                                Term = "jed"
                            }), Encoding.UTF8, "application/json"));
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<IList<string>>();
            content.Should().NotBeNullOrEmpty();
        }
        [Fact]
        public async Task Test_GetAll_Cities() {
            var response = await Client.GetAsync($"{ApiUrl}/getAll");
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<List<GetCityResponse>>();
            content.Should().NotBeNullOrEmpty();
        }
    }
}
