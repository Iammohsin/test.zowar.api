﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.WebAPI.Handlers.Common;

namespace Zowarah.WebAPI.Test.Common
{
    public class CountryControllerTest : BaseTesting {
        public string ApiUrl => "/api/Common/country";
        public CountryControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_GetAll_Countries() {
            var response = await Client.GetAsync($"{ApiUrl}/getAll");
            // Assert             
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<List<GetCountryResponse>>();
            content.Should().NotBeNullOrEmpty();
        }


    }
}
