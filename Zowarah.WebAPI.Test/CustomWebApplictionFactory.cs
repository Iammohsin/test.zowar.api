﻿using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Entities.Invoices;
using Zowarah.Domain.Entities.Reviews;
using Zowarah.Domain.Entities.Transactions;

namespace Zowarah.WebAPI.Test
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<Startup>
    {
        public IConfiguration Configuration { get; set; }
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                ConfigrationSQlServer(services);

            })
            .UseEnvironment("Testing");
        }

        private void ConfigrationSQlServer(IServiceCollection services)
        {
            // Create a new service provider.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Add a database context (ApplicationDbContext) using an in-memory 
            // database for testing.
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseInMemoryDatabase("Identity");
                options.UseInternalServiceProvider(serviceProvider);
            });

            // Build the service provider.
            var sp = services.BuildServiceProvider();

            // Create a scope to obtain a reference to the database
            // context (ApplicationDbContext).
            using (var scope = sp.CreateScope())
            {
                var scopedServices = scope.ServiceProvider;
                var db = scopedServices.GetRequiredService<ApplicationDbContext>();

                Configuration = scopedServices.GetService<IConfiguration>();

                // Ensure the database is created.
                db.Database.EnsureCreated();
                DbInitializer dbInitializer = new DbInitializer(db);
                dbInitializer.Seed().GetAwaiter();
                Seed(db);
            }

        }



        private static void Seed(ApplicationDbContext db)
        {
            // add data to any table here
            // ToDo: Move cities to Seed Data
            if (!db.Cities.AnyAsync().GetAwaiter().GetResult())
            {
                db.Cities.Add(new City
                {
                    Description = "Jeddah"
                });
            }
            if (!db.Countries.AnyAsync().GetAwaiter().GetResult())
            {
                db.Countries.Add(new Country
                {
                    Description = "Sudia Arabia",
                    CurrencyId = "sa",
                    DialingCode = "00966"
                });
            }
            if (!db.Currencies.AnyAsync().GetAwaiter().GetResult())
            {
                db.Currencies.Add(new Currency
                {
                    Id = "sar",
                    ExchangeRate = 1,
                    Name = "sa",
                    Symbol = "SAR"
                });
            }
            if (!db.Languages.AnyAsync().GetAwaiter().GetResult())
            {
                db.Languages.Add(new Language
                {
                    Id = "en",
                    Name = "English",
                    NativeName = "English",
                });
            }
            if (!db.Localization.AnyAsync().GetAwaiter().GetResult())
            {
                db.Localization.Add(new Localization
                {
                    Id = "en",
                    LanguageId = "en",
                    LocText = "English"
                });
            }



            // ToDo: move FacilityCategories to Seed Data
            if (!db.FacilityCategories.AnyAsync().GetAwaiter().GetResult())
            {
                db.FacilityCategories.Add(new FacilityCategory
                {
                    Description = "Jeddah"
                });
            }


            if (!db.Hotels.AnyAsync().GetAwaiter().GetResult())
            {
                db.Hotels.Add(new Hotel
                {
                    Name = "Hotel Name",
                    noOfRooms = 100,
                    CityId = 1,
                    IsBasicInfoConfigured = true,
                    IsPropertyDetailsConfigured = true,
                    IsRoomTypesConfigured = true,
                    IsPhotosConfigured = true,
                    IsPaymentPolicyConfigured = true,
                    ProviderId = 1,
                    CancellationPolicyId = 1
                });
            }
            if (!db.Services.AnyAsync().GetAwaiter().GetResult())
            {
                db.Services.Add(new Service
                {
                    Name = "Hotel Name",
                    Status = ServiceStatusEnum.Approved,
                    CityId = 1,
                    ProviderId = 1,
                    ServiceType = ServiceTypeEnum.Hotel,
                    CreatedDate = DateTime.Now,
                    ReviewScore = 1,
                    TotalScore = 1,
                    TotalReviews = 1
                });
            }
            if (!db.Rooms.AnyAsync().GetAwaiter().GetResult())
            {
                db.Rooms.Add(new Room
                {
                    HotelId = 1,
                    NoOfRooms = 10,
                    NoOfPersons = 1,
                    NoOfBathrooms = 1,
                    IsLivingRoom = false,
                    RatePerNight = 100,
                    RoomTypeNameId = 1,
                    SmokingOptionId = 1,
                    IsKitchen = false
                });
            }
            if (!db.ServiceNearbies.AnyAsync().GetAwaiter().GetResult())
            {
                db.ServiceNearbies.Add(new ServiceNearby
                {
                    Distance = 100,
                    ServiceId = 1,
                    Name = "Service",
                    ServiceNearbyCategoryId = 1,
                    Unit = "meters"
                });
            }
            if (!db.ServiceProfiles.AnyAsync().GetAwaiter().GetResult())
            {
                db.ServiceProfiles.Add(new ServiceProfile
                {
                    AboutNeighborhood = "ServiceProfiles",
                    AboutProperty = "ServiceProfiles",
                    AboutStaff = "ServiceProfiles",
                    Language = "en",
                    PropertyName = "ServiceProfiles",
                    ServiceId = 1
                });
            }
            if (!db.HotelPromotions.AnyAsync().GetAwaiter().GetResult())
            {
                db.HotelPromotions.Add(new HotelPromotion
                {
                    ServiceId = 1,
                    AudienceId = AudienceEnum.Public,
                    PromotionType = PromotionType.Basic,
                    FreeNights = 0,
                    MinimumStay = 0,
                    BookingDaysCondition = 0,
                    Date = new DateTime(2018, 07, 01),
                    Discount = 10,
                    FromDate = DateTime.Now.AddYears(-1),
                    ToDate = DateTime.Now.AddYears(1),
                    Name = "promotion",
                    IsActive = true,
                    HotelPromotionRooms = new List<HotelPromotionRoom> {
                        new HotelPromotionRoom{
                           RoomId=1
                        }
                    }
                });
            }
            if (!db.HotelPhotos.AnyAsync().GetAwaiter().GetResult())
            {
                db.HotelPhotos.Add(new HotelPhoto
                {
                    FileName = "test.jpj",
                    HotelId = 1,
                    IsMain = true
                });
            }
            if (!db.HotelLanguages.AnyAsync().GetAwaiter().GetResult())
            {
                db.HotelLanguages.Add(new HotelLanguage
                {
                    HotelId = 1,
                    LanguageId = "en"
                });
            }


            if (!db.HotelRoomAmenities.AnyAsync().GetAwaiter().GetResult())
            {
                db.HotelRoomAmenities.Add(new HotelRoomAmenity
                {
                    HotelId = 1,
                    RoomId = 1
                });
            }

            if (!db.Booking.AnyAsync().GetAwaiter().GetResult())
            {

                db.Booking.Add(new Booking
                {
                    Id = "1",
                    ArrivalDate = DateTime.Now.AddDays(-6),
                    CheckInDate = DateTime.Now.AddDays(-6),
                    CheckOutDate = DateTime.Now.AddDays(-3),
                    BaseRate = 10,
                    Discount = 0,
                    IsPersonal = true,
                    isPaymentHotel = true,
                    RatePerNight = 100,
                    RoomId = 1,
                    RefereeEmail = "Hatem@aa.aaa",
                    RefereeName = "test",
                    Rooms = 1,
                    SpecialRequest = "",
                    TransactionId = "1",
                    Transaction = new Transaction
                    {
                        Id = "1",
                        TransactionType = TransactionType.BookingHotel,
                        BaseAmount = 100,
                        ServiceId = 1,
                        ClientAmount = 100,
                        ProviderAmount = 100,
                        ClientId = 3,
                        Status = TransactionStatus.Confirmed,
                        Date = DateTime.Now,
                        InvoiceId = "1"
                    }
                });

                // this booking to show in provider list 
                db.Booking.Add(new Booking
                {
                    Id = "2",
                    ArrivalDate = DateTime.Now.AddDays(1),
                    CheckInDate = DateTime.Now.AddDays(1),
                    CheckOutDate = DateTime.Now.AddDays(3),
                    BaseRate = 10,
                    Discount = 0,
                    IsPersonal = true,
                    isPaymentHotel = true,
                    RatePerNight = 100,
                    RoomId = 1,
                    RefereeEmail = "Hatem@aa.aaa",
                    RefereeName = "test",
                    Rooms = 1,
                    SpecialRequest = "",
                    TransactionId = "2",
                    Transaction = new Transaction
                    {
                        Id = "2",
                        TransactionType = TransactionType.BookingHotel,
                        BaseAmount = 100,
                        ServiceId = 1,
                        ClientAmount = 100,
                        ProviderAmount = 100,
                        ClientId = 3,
                        Status = TransactionStatus.Confirmed,
                        Date = DateTime.Now,
                        InvoiceId = "1"
                    }
                });
                // this booking to show in update status list 
                db.Booking.Add(new Booking
                {
                    Id = "3",
                    ArrivalDate = DateTime.Now.AddDays(-1),
                    CheckInDate = DateTime.Now.AddDays(-1),
                    CheckOutDate = DateTime.Now.AddDays(1),
                    BaseRate = 10,
                    Discount = 0,
                    IsPersonal = true,
                    isPaymentHotel = true,
                    RatePerNight = 100,
                    RoomId = 1,
                    RefereeEmail = "Hatem@aa.aaa",
                    RefereeName = "test",
                    Rooms = 1,
                    SpecialRequest = "",
                    TransactionId = "3",
                    Transaction = new Transaction
                    {
                        Id = "3",
                        TransactionType = TransactionType.BookingHotel,
                        BaseAmount = 100,
                        ServiceId = 1,
                        ClientAmount = 100,
                        ProviderAmount = 100,
                        ClientId = 3,
                        Status = TransactionStatus.Confirmed,
                        Date = DateTime.Now,
                        InvoiceId = "1"
                    }
                });
            }

            if (!db.Reviews.AnyAsync().GetAwaiter().GetResult())
            {
                db.Reviews.Add(new Review
                {
                    Id = "1",
                    ClientId = 3,
                    Comment = "test",
                    CreatedDate = DateTime.Now,
                    Date = DateTime.Now,
                    IsPuplished = true,
                    IsRead = true,
                    ServiceId = 1,
                    Score = 5,
                    Tags = "test",
                    TransactionId = "3",
                    Title = "good",
                    ReviewFeatures = new List<ReviewFeature> {
                      new ReviewFeature{
                          ReviewFeatureTypeId=1,
                          ReviewId="1",
                          Score=5
                      }
                  }
                });
            }
            if (!db.ServiceReviewFeaturesScores.AnyAsync().GetAwaiter().GetResult())
            {
                db.ServiceReviewFeaturesScores.Add(new ServiceReviewFeaturesScore
                {
                    ReviewFeatureTypeId = 1,
                    ServiceId = 1,
                    TotalReviews = 1,
                    TotalScore = 1,
                    Score = 1
                });
            }

            if (!db.Calendar.AnyAsync().GetAwaiter().GetResult())
            {
                db.Calendar.Add(new Calendar
                {
                    HotelId = 1,
                    OpenFrom = new DateTime(2018, 01, 01),
                    OpenTo = new DateTime(2019, 01, 01),
                    CloseFrom = new DateTime(2016, 01, 01),
                    CloseTo = new DateTime(2016, 01, 01)
                });
            }

            if (!db.Invoices.AnyAsync().GetAwaiter().GetResult())
            {
                db.Invoices.Add(new Invoice
                {
                    Id = "1",
                    ServiceId = 1,
                    Amount = 1,
                    Date = DateTime.Now,
                    DueDate = DateTime.Now,
                    Status = InvoiceStatus.Pending
                });
            }

            if (!db.Messages.AnyAsync().GetAwaiter().GetResult())
            {
                db.Messages.Add(new Message
                {
                    Id = "1",
                    Date = DateTime.Now,
                    IsDeletedByReciever = false,
                    IsDeletedBySender = false,
                    IsRead = false,
                    MessageCategoryId = 1,
                    ReceiverRole = Domain.Entities.Users.RoleTypes.Administrator,
                    RecieverId = 0,
                    SenderId = 1,
                    ReferenceId = "",
                    TransactionTypeId = 1,
                    Title = "hi test!",
                    MessageTrails = new List<MessageTrail> {
                        new MessageTrail{
                            Id="1",
                            SenderId=1,
                            Body="hi test!",
                            CreatedDate=DateTime.Now,
                            Date=DateTime.Now
                        }
                    },
                    CreatedDate = DateTime.Now,
                    Body = "hi test!"
                });
                // active message
                db.Messages.Add(new Message
                {
                    Id = "2321",
                    Date = DateTime.Now,
                    IsDeletedByReciever = false,
                    IsDeletedBySender = false,
                    IsRead = false,
                    MessageCategoryId = 1,
                    ReceiverRole = Domain.Entities.Users.RoleTypes.Administrator,
                    RecieverId = 1,
                    SenderId = 1,
                    ReferenceId = "",
                    TransactionTypeId = 1,
                    Title = "hi test!",
                    MessageTrails = new List<MessageTrail> {
                        new MessageTrail{
                            Id="2241",
                            SenderId=1,
                            Body="hi test!",
                            CreatedDate=DateTime.Now,
                            Date=DateTime.Now
                        }
                    },
                    CreatedDate = DateTime.Now,
                    Body = "hi test!"
                });
            }

            var date = new DateTime(2018, 1, 1);
            var end = new DateTime(DateTime.Now.Year, 12, 31);
            do
            {
                db.DimCalendars.Add(new DimCalendar { Date = date.Date });
                date = date.AddDays(1);
            } while (date <= end);

            db.SaveChanges();
        }

    }
}
