﻿using System;
using System.Collections.Generic;
using System.Text;
using Zowarah.Domain.Entities.Users;

namespace Zowarah.WebAPI.Test
{
    public static class TestingService
    {
        public static AppUser Admin { get; set; }
        public static AppUser Provider { get; set; }
        public static AppUser client { get; set; }
    }
}
