﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Test.Hotels
{
    public class ParkingOptionsControllerTest : BaseTesting {
        public string ApiUrl => "/api/Hotels/ParkingOptions";
        public ParkingOptionsControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }
        
       
        [Fact]
        public async Task Test_GetAll_ParkingOptions() {
            var response = await Client.GetAsync($"{ApiUrl}/getAll");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<IList<GetParkingOptionsResponse>>();
            content.Should().NotBeNullOrEmpty();
            // lookup repository  error

        }

    }
}
