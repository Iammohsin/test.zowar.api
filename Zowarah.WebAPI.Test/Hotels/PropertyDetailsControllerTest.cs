﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Test.Hotels
{
    public class PropertyDetailsControllerTest : BaseTesting {
        public string ApiHotelUrl => "/api/Hotels/PropertyDetails";
        public PropertyDetailsControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }


        [Fact]
        public async Task Test_Get_PropertyDetails() {
            var response = await Client.GetAsync($"{ApiHotelUrl}/Get/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetPropertyBasicInfoResponse>();
            content.Should().BeOfType<GetPropertyBasicInfoResponse>();
        }
        [Fact]
        public async Task Test_Update_PropertyDetails() {

            // Act            
            var response = await Client.PutAsync($"{ApiHotelUrl}/Update", new StringContent(
                JsonConvert.SerializeObject(
                            new UpdatePropertyDetailsRequest() {
                                Id = 1,
                                checkInFrom = 10,
                                checkInTo = 4,
                                checkOutFrom = 12,
                                checkOutTo = 6,
                                Facilities = new int[] { 1, 2, 3 },
                                internetOptionId = 1,
                                isChildrenAllowed=false,
                                isPetsAllowed=false,
                                parkingOptionId=1
                            }
                            ), Encoding.UTF8, "application/json"));

            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();

            // return int
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNull();
        }
    }
}
