﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Test.Hotels
{
    public class ServicesControllerTest : BaseTesting {
        public string ApiUrl => "/api/Hotels/Services";
        public ServicesControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Get_Service() {
            var response = await Client.GetAsync($"{ApiUrl}/get/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Update_PropertyProfile() {
            // Act
            var response = await Client.PutAsync($"{ApiUrl}/update", new StringContent(
                JsonConvert.SerializeObject(
                            new UpdateServicesRequest() {                               
                                Id=1,
                                Facilities=new int[] { 1,2},
                                IsFoodServed=true,
                                Languages=new string []{ "en","ar"},
                                Meals= new UpdateServicesRequest.MealsInfo() {
                                    BreakFast=true,
                                    BreakFastRate=100,
                                    Dinner=false,
                                    DinnerRate=0,
                                    Lunch=true,
                                    LunchRate=80
                                } 

                            }
                            ), Encoding.UTF8, "application/json"));

            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();

            // return int
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }
    }
}
