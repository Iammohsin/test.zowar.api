﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Test.Hotels
{
    public class PaymentPolicyControllerTest : BaseTesting {
        public string ApiUrl => "/api/Hotels/PaymentPolicy";
        public PaymentPolicyControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Get_PaymentPolicy() {
            // Arrange       

            // Act
            var response = await Client.GetAsync($"{ApiUrl}/Get/{1}");

            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();

            response.Content.ReadAsAsync<GetPaymentPolicyResponse>().Should().NotBeNull();

        }


        [Fact]
        public async Task Test_Update_PaymentPolicy() {
            // Arrange       

            // Act
            var response = await Client.PutAsync($"{ApiUrl}/Update/{1}", new StringContent(
                JsonConvert.SerializeObject(
                            new UpdatePaymentPolicyRequest {
                                Id=1,
                                CancellationFeeId=1,
                                CancellationPolicyId=1,
                                InvoicingAddress="test",
                                InvoicingCityId=1,
                                InvoicingCountryId=1,
                                InvoicingPostalAddress="1234",
                                InvoicingRecipientName="test",
                                IsCreditCardAccepted=true,
                                IsInvoicingHotelAddress=true,
                                CreditCards=new int[] { }
                                
                            }), Encoding.UTF8, "application/json"));

            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();

        }
    }
}
