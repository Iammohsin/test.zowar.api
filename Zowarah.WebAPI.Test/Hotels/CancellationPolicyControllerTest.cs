﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Test.Hotels
{
    public class CancellationPolicyControllerTest : BaseTesting {
        public string ApiUrl => "/api/Hotels/CancellationPolicy";
        public CancellationPolicyControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

       

        [Fact]
        public async Task Test_GetAll_CancellationPolicy() {
            var response = await Client.GetAsync($"{ApiUrl}/getAll");
            // OK status
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<IList<GetCancellationFeeResponse>>();
            content.Should().NotBeNullOrEmpty();
        }
    }
}
