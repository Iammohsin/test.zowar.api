﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.Application.Services.Hotels;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Test.Hotels {
    public class BookingControllerTest : BaseTesting {
        public string ApiUrl => "/api/Hotels/Booking";
        public BookingControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }
                
        [Fact]
        public async Task Test_Get_PropertyBookingParams() {
            var response = await Client.GetAsync($"{ApiUrl}/GetPropertyBookingParams/{1}");
            // OK status
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetPropertyBookingParametersResponse>();
            content.Should().NotBeNull();
        }

        [Fact]
        public async Task Test_Get_BookingDetails() {
            var response = await Client.GetAsync($"{ApiUrl}/GetBookingDetails/{1}");
            // OK status
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetBookingDetailsResponse>();
            content.Should().NotBeNull();
        }


        [Fact]
        public async Task Test_Get_NetBooking() {
            var response = await Client.GetAsync($"{ApiUrl}/GetNetBooking/{1}/2018-08-01/2018-08-03");
            // OK status
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<ICollection<NetBookingDetails>>();
            content.Should().NotBeNull();
        }
               
        [Fact]
        public async Task Test_Get_AvailableRooms() {
            var response = await Client.GetAsync($"{ApiUrl}/AvailableRooms/{1}/2018-08-01/2018-08-03/{1}/{1}/{0}");
            // OK status
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<ICollection<AvailableRoom>>();
            content.Should().NotBeNullOrEmpty();
        }

    }
}