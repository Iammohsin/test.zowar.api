﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Test.Hotels {
    public class CalendarControllerTest : BaseTesting {
        public string ApiUrl => "/api/Hotels/Calendar";
        public CalendarControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }



        [Fact]
        public async Task Test_Get_Calendar() {
            var response = await Client.GetAsync($"{ApiUrl}/GetCalendar/{1}");
            // OK status
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetCalendarResponse>();
            content.Should().NotBeNull();
        }



        [Fact]
        public async Task Test_Get_CalendarManual() {
            var response = await Client.GetAsync($"{ApiUrl}/GetCalendarManual/{1}");
            // OK status
            response.EnsureSuccessStatusCode();
            // return      
            //var content = await response.Content.ReadAsAsync<GetCalendarResponse>();
            //content.Should().NotBeNull();
        }



        [Fact]
        public async Task Test_Set_Calendar() {
            // Arrange       

            // Act
            var response = await Client.PostAsync($"{ApiUrl}/SetCalendar", new StringContent(
                JsonConvert.SerializeObject(
                            new SetCalendarRequest() {
                                Id = 1,
                                OpenFrom = new DateTime(2018, 01, 01),
                                OpenTo = new DateTime(2019, 01, 01),
                                Sat = false
                            }), Encoding.UTF8, "application/json"));

            // OK
            response.EnsureSuccessStatusCode();

            // return 
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Close_Rooms() {
            // Arrange       

            // Act
            var response = await Client.PostAsync($"{ApiUrl}/CloseRooms", new StringContent(
                JsonConvert.SerializeObject(
                            new CloseRoomsRequest() {
                                Id = 1,
                                CloseFrom = new DateTime(2018, 01, 01),
                                CloseTo = new DateTime(2019, 01, 01),
                                Sat = false
                            }), Encoding.UTF8, "application/json"));

            // OK
            response.EnsureSuccessStatusCode();

            // return 
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }


        [Fact]
        public async Task Test_Set_CalendarStatus() {
            // Arrange       

            // Act
            var response = await Client.PostAsync($"{ApiUrl}/SetCalendarStatus", new StringContent(
                JsonConvert.SerializeObject(
                            new SetCalendarStatusRequest() {
                                HotelId = 1,
                                RoomId = 1,
                                Status = true,
                                Date = new DateTime(2018, 08, 1)
                            }), Encoding.UTF8, "application/json"));

            // OK
            response.EnsureSuccessStatusCode();

            // return 
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }


        [Fact]
        public async Task Test_Set_CalendarRooms() {
            // Arrange       

            // Act
            var response = await Client.PostAsync($"{ApiUrl}/SetCalendarRooms", new StringContent(
                JsonConvert.SerializeObject(
                            new SetCalendarStatusRequest() {
                                HotelId = 1,
                                RoomId = 1,
                                Status = true,
                                Date = new DateTime(2018, 08, 1)
                            }), Encoding.UTF8, "application/json"));

            // OK
            response.EnsureSuccessStatusCode();

            // return 
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }



    }
}
