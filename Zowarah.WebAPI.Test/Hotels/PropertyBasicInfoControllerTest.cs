﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.Data.Contexts;
using Zowarah.Domain.Entities.Common;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Test.Hotels
{
    public class PropertyBasicInfoControllerTest : BaseTesting {
        public string ApiHotelUrl => "/api/Hotels/PropertyBasicInfo";
        public PropertyBasicInfoControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Create() {
            // Arrange       

            // Act
            var response = await Client.PostAsync($"{ApiHotelUrl}/Create", new StringContent(
                JsonConvert.SerializeObject(
                            new CreatePropertyBasicInfoRequest() {
                                Address = RandomText(20),
                                CityId = RandomInt(1, 100),
                                Contact1 = RandomInt(100000, 10000000).ToString(),
                                ContactName = RandomText(5),
                                CurrencyId = GetCurrency(),
                                Name = RandomText(20),
                                NoOfRooms = RandomInt(10, 100),
                                LocLat = RandomInt(25, 50),
                                LocLng = RandomInt(25, 50),
                                ProviderId = providerId() + "",
                                Rating = RandomInt(1,5),
                            //mohsin
                                TaxP = RandomInt(1, 5),
                                Vat = RandomInt(1, 5),

                                PropertyTypeId = (int)ServiceTypeEnum.Hotel,
                                SmokingOptionId=1
                            }
                            ), Encoding.UTF8, "application/json"));

            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();

            // return int
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Get() {
            var response = await Client.GetAsync($"{ApiHotelUrl}/Get/"+1);
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetPropertyBasicInfoResponse>();
            content.Should().BeOfType<GetPropertyBasicInfoResponse>();
        }

        [Fact]
        public async Task Test_Update() {

            // Act
            string newName = "Updated Name Hotel";
            var response = await Client.PostAsync($"{ApiHotelUrl}/Create", new StringContent(
                JsonConvert.SerializeObject(
                            new UpdatePropertyBasicInfoRequest() {
                                Id=1,
                                ServiceId=1,
                                Address = RandomText(20),
                                CityId = RandomInt(1, 100),
                                Contact1 = RandomInt(100000, 10000000).ToString(),
                                ContactName = RandomText(5),                               
                                Name = newName,
                                NoOfRooms = RandomInt(10, 100),
                                LocLat = RandomInt(25, 50),
                                LocLng = RandomInt(25, 50),                               
                                Rating = RandomInt(1, 5),
                               // mohsin
                               TaxP = RandomInt(1, 5),
                                Vat = RandomInt(1, 5),

                                PropertyTypeId = (int)ServiceTypeEnum.Hotel,
                                SmokingOptionId = 1
                            }
                            ), Encoding.UTF8, "application/json"));

            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();

            // return int
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNull();
        }
    }
}
