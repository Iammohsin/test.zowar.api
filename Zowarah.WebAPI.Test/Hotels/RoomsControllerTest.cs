﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Test.Hotels {
    public class RoomsControllerTest : BaseTesting {
        public string ApiUrl => "/api/Hotels/Rooms";
        public RoomsControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Get_Room() {
            var response = await Client.GetAsync($"{ApiUrl}/Get/" + 1);
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetRoomsResponse>();
            content.Should().NotBeNull();
        }

        [Fact]
        public async Task Test_Create_Room() {
            // Act
            var response = await Client.PostAsync($"{ApiUrl}/Create", new StringContent(
                JsonConvert.SerializeObject(
                            new CreateRoomsRequest() {
                                HotelId = 1,
                                IsKitchen = false,
                                NoOfPathrooms = 1,
                                NoOfPersons = 10,
                                IsLivingRoom = true,
                                RatePerNight = 100,
                                RoomTypeId = 1,
                                RoomTypeNameId = 1,
                                NoOfRooms = 10,
                                SmokingOptionId = 1
                            }
                            ), Encoding.UTF8, "application/json"));

            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();

            // return int
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Delete_Room() {
            var response = await Client.DeleteAsync($"{ApiUrl}/Delete/" + 1);
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }


        [Fact]
        public async Task Test_Get_Rooms_ByHotelId() {
            var response = await Client.GetAsync($"{ApiUrl}/GetByHotelId/" + 1);
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<List<RoomsDetails>>();
            content.Should().NotBeNullOrEmpty();
        }


        [Fact]
        public async Task Test_Update_Room() {
            // Act
            var response = await Client.PutAsync($"{ApiUrl}/Update", new StringContent(
                JsonConvert.SerializeObject(
                            new UpdateRoomsRequest() {
                                // ToDo: remove HotelId 
                                HotelId = 1,
                                Id = 1,
                                IsKitchen = false,
                                NoOfPathrooms = 1,
                                NoOfPersons = 10,
                                IsLivingRoom = true,
                                RatePerNight = 100,
                                RoomTypeId = 1,
                                RoomTypeNameId = 1,
                                NoOfRooms = 10,
                                SmokingOptionId = 1
                            }
                            ), Encoding.UTF8, "application/json"));

            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();

            // return int
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }

    }
}
