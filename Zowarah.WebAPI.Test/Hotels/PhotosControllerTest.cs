﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Test.Hotels {
    public class PhotosControllerTest : BaseTesting {
        public string ApiUrl => "/api/Hotels/Photos";
        public PhotosControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }


        [Fact]
        public async Task Test_GetAll_Photos() {
            var response = await Client.GetAsync($"{ApiUrl}/getAll/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return
            var content = await response.Content.ReadAsAsync<IList<GetPhotoResponse>>();
            content.Should().NotBeNullOrEmpty();
        }



        [Fact]
        public async Task Test_Add_Photo() {
            throw new NotImplementedException();
        }


        [Fact]
        public async Task Test_Delete_Photo() {
            throw new NotImplementedException();
        }

        [Fact]
        public async Task Test_Set_Main_Photo() {
            // Arrange       

            // act
            var response = await Client.PostAsync($"{ApiUrl}/SetMain/{1}",
                new StringContent("", Encoding.UTF8, "application/json"));

            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }

    }
}
