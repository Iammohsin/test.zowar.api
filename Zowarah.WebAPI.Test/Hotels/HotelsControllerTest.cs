﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.Application.Services.Hotels;
using Zowarah.Domain.Queries.Hotels;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Test.Hotels {
    public class HotelsControllerTest : BaseTesting {
        public string ApiUrl => "/api/Hotels/hotels";
        public HotelsControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Get_Hotel() {
            var response = await Client.GetAsync($"{ApiUrl}/get/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNull();
            // no seed for cities
        }


        [Fact]
        public async Task Test_Search() {
            var response = await Client.PostAsync($"{ApiUrl}/Search",
                 new StringContent(
                JsonConvert.SerializeObject(
                            new SearchHotelRequest {
                                CheckInDate = new DateTime(2018, 08, 01),
                                CheckOutDate = new DateTime(2018, 08, 02),
                                Location = "Jeddah",
                                NoOfChildren = 0,
                                NoOfPersons = 1,
                                NoOfRooms = 1,
                                Page = 1,
                                PageSize = 10,
                                Filters=new int[4][] { new int[] { }, new int[] { }, new int[] { }, new int[] { } } 
                            }), Encoding.UTF8, "application/json"));
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<ICollection<HotelSearchResult>>();
            content.Should().NotBeNull();
        }


        [Fact]
        public async Task Test_Get_SpokenLanguages() {
            var response = await Client.GetAsync($"{ApiUrl}/GetSpokenLanguages/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<ICollection<string>>();
            content.Should().NotBeNullOrEmpty();
        }


        [Fact]
        public async Task Test_Get_ByProviderId() {
            var response = await Client.GetAsync($"{ApiUrl}/GetByProviderId/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<IList<HotelDetails>>();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Get_ActiveBookings() {
            var response = await Client.GetAsync($"{ApiUrl}/GetActiveBookings/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<ICollection<HotelBooking>>();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Search_BookingTransactions() {
            var response = await Client.PostAsync($"{ApiUrl}/SearchBookingTransactions",
                new StringContent(
                JsonConvert.SerializeObject(
                            new SearchBookingListRequest {
                                FromDate = new DateTime(2018, 08, 01),
                                ToDate = new DateTime(2018, 08, 02),
                                ServiceId = 1,
                                Status = 0
                            }), Encoding.UTF8, "application/json"));
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<ICollection<HotelBooking>>();
            content.Should().NotBeNull();
        }

        [Fact]
        public async Task Test_Get_RoomAmenities() {
            var response = await Client.GetAsync($"{ApiUrl}/GetRoomAmenities/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<ICollection<HotelRoomAmenityDetails>>();
            content.Should().NotBeNullOrEmpty();
        }

       

        [Fact]
        public async Task Test_Update_RoomAmenities() {
            var response = await Client.PutAsync($"{ApiUrl}/UpdateRoomAmenities/{1}",
                new StringContent(
                JsonConvert.SerializeObject(
                            new List<HotelRoomAmenityDetails> {
                                new HotelRoomAmenityDetails{
                                    RoomAmenityId=1,
                                    RoomId=1
                                }
                            }), Encoding.UTF8, "application/json"));
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<UpdateRoomAmenitiesResponse>();
            content.Should().NotBeNull();            
        }


        [Fact]
        public async Task Test_Delete() {
            var response = await Client.DeleteAsync($"{ApiUrl}/Delete/{1}");
            // Assert 
            response.EnsureSuccessStatusCode();            
        }

        [Fact]
        public async Task Test_Get_Hotel_Room() {
            var response = await Client.GetAsync($"{ApiUrl}/GetHotelRoom/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetHotelRoomResponse>();
            content.Should().NotBeNull();
        }
        [Fact]
        public async Task Test_Get_ConfigFlags() {
            var response = await Client.GetAsync($"{ApiUrl}/GetConfigFlags/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetConfigFlagsResponse>();
            content.Should().NotBeNull();
        }

        [Fact]
        public async Task Test_Get_ConfigurationState() {
            var response = await Client.GetAsync($"{ApiUrl}/GetHotelConfigurationState/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetHotelConfigurationStateResponse>();
            content.Should().NotBeNull();
        }

        [Fact]
        public async Task Test_Get_SearchParameters() {
            var response = await Client.GetAsync($"{ApiUrl}/GetHotelSearchParameters?CheckInDate=2018-08-01&CheckOutDate=2018-08-03&HotelId=1");
           
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetHotelSearchParametersResponse>();
            content.Should().NotBeNull();
        }

        [Fact]
        public async Task Test_Get_RoomSearchParameters() {
            var response = await Client.GetAsync($"{ApiUrl}/GetRoomSearchParameters?CheckInDate=2018-08-01&CheckOutDate=2018-08-03&RoomId=1");
           
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetHotelSearchParametersResponse>();
            content.Should().NotBeNull();
        }

        [Fact]
        public async Task Test_SubmitDetails() {
            var response = await Client.PostAsync($"{ApiUrl}/SubmitDetails/{1}",
                new StringContent(""));
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNull();
        }



    }
}
