﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Test.Hotels
{
    public class FacilityCategoryControllerTest : BaseTesting {
        public string ApiUrl => "/api/Hotels/FacilityCategory";
        public FacilityCategoryControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

       
        [Fact]
        public async Task Test_GetAll_FacilityCategory() {
            var response = await Client.GetAsync($"{ApiUrl}/getAll");
            // OK status
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<List<GetFacilityCategoryResponse>>();
            content.Should().NotBeNullOrEmpty();
        }
    }
}
