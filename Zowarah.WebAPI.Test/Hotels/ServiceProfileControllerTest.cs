﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.Application.Services.Common;
using Zowarah.WebAPI.Handlers.Hotels.ServiceProfilesHandler;

namespace Zowarah.WebAPI.Test.Hotels
{
    public class ServiceProfileControllerTest : BaseTesting {
        public string ApiUrl => "/api/Hotels/ServiceProfile";
        public ServiceProfileControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Get_PropertyProfile() {
            var response = await Client.GetAsync($"{ApiUrl}/GetPropertyProfile/{"en"}/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }
        [Fact]
        public async Task Test_GetAll_PropertyProfiles() {
            var response = await Client.GetAsync($"{ApiUrl}/GetAll/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<ICollection<GetServiceProfile>>();
            content.Should().NotBeNullOrEmpty();
        }


        [Fact]
        public async Task Test_Create_PropertyProfile() {
            // Act
            var response = await Client.PostAsync($"{ApiUrl}/Create", new StringContent(
                JsonConvert.SerializeObject(
                            new CreateServiceProfileRequest() {
                                AboutNeighborhood ="neighbor",
                                AboutProperty="about",
                                AboutStaff="staff",
                                Language="ar",
                                PropertyName="name",
                                ServiceId=1
                                
                            }
                            ), Encoding.UTF8, "application/json"));

            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();

            // return int
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }


        [Fact]
        public async Task Test_Update_PropertyProfile() {
            // Act
            var response = await Client.PutAsync($"{ApiUrl}/update", new StringContent(
                JsonConvert.SerializeObject(
                            new UpdateServiceProfileRequest() {
                                AboutNeighborhood = "neighbor",
                                AboutProperty = "about",
                                AboutStaff = "staff",
                                Language = "en",
                                PropertyName = "name",
                                ServiceId = 1,
                                Id=1

                            }
                            ), Encoding.UTF8, "application/json"));

            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();

            // return int
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }


        [Fact]
        public async Task Test_Delete_PropertyProfile() {
            var response = await Client.DeleteAsync($"{ApiUrl}/Delete/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }
    }
}
