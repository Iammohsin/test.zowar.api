﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.Domain.Entities.Hotels;
using Zowarah.Domain.Queries.Hotels;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Test.Hotels {
    public class PromotionsControllerTest : BaseTesting {
        public string ApiUrl => "/api/Hotels/Promotions";
        public PromotionsControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }


        [Fact]
        public async Task Test_Get_Promotion() {
            var response = await Client.GetAsync($"{ApiUrl}/get/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Get_Promotion_byServiceId() {
            var response = await Client.GetAsync($"{ApiUrl}/GetByServiceId/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<ICollection<GetPromotionResponse>>();
            content.Should().NotBeNullOrEmpty();
        }



        [Fact]
        public async Task Test_Get_Promotion_Rooms() {

            var response = await Client.GetAsync($"{ApiUrl}/GetRoomsPromotions?ServiceId={1}&IsMember=true&CheckInDate=2018-8-8&CheckOutDate=2018-8-10");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<ICollection<GetPromotionResponse>>();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Get_Room_Promotion() {

            var response = await Client.GetAsync($"{ApiUrl}/GetRoomPromotion?RoomId={1}&IsMember=true&CheckInDate={DateTime.Now.AddDays(-20).ToString("yyyy-MM-dd")}&CheckOutDate={DateTime.Now.AddDays(-10).ToString("yyyy-MM-dd")}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<Promotion>();
            content.Should().NotBeNull();
        }


        [Fact]
        public async Task Test_Create_Promotion() {
            // Arrange       

            // Act
            var response = await Client.PostAsync($"{ApiUrl}/Add", new StringContent(
                JsonConvert.SerializeObject(
                            new AddPromotionRequest() {
                                ServiceId = 1,
                                BookingDaysCondition=3,
                                PromotionType=PromotionType.LastMinute,
                                AudienceId = AudienceEnum.Public,
                                Date = new DateTime(2018, 07, 01),
                                Discount = 10,
                                FromDate = new DateTime(2018, 08, 01),
                                ToDate = new DateTime(2018, 09, 01),
                                Name = "promotion",
                                MinimumStay = 2,
                                Rooms = new List<Room> {
                                    new Room(){
                                        Id=1
                                    }
                                }
                            }), Encoding.UTF8, "application/json"));

            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();

            // return int
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }


        [Fact]
        public async Task Test_Update_Promotion() {
            // Arrange       

            // Act
            var response = await Client.PutAsync($"{ApiUrl}/Update", new StringContent(
                JsonConvert.SerializeObject(
                            new UpdatePromotionRequest() {
                                Id=1,
                                AudienceId = AudienceEnum.Public,
                                PromotionType=PromotionType.FreeNights,
                                FreeNights=1,
                                Date = new DateTime(2018, 07, 01),
                                Discount = 10,
                                FromDate = new DateTime(2018, 08, 01),
                                ToDate = new DateTime(2018, 09, 01),
                                Name = "promotion",
                                MinimumStay = 2,
                                Rooms = new List<Room> {
                                    new Room(){
                                        Id=1
                                    }
                                }
                            }), Encoding.UTF8, "application/json"));

            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();

            // return int
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }


        [Fact]
        public async Task Test_Delete_Promotion() {
            var response = await Client.DeleteAsync($"{ApiUrl}/Delete/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNull();
        }

        [Fact]
        public async Task Test_Set_Status_Promotion() {

            var response = await Client.GetAsync($"{ApiUrl}/SetStatus/{1}/{true}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsStringAsync();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Get_Top_Promotions() {
            var response = await Client.GetAsync($"{ApiUrl}/GetTopPromotions/{10}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<ICollection<TopPromotion>>();
            content.Should().NotBeNullOrEmpty();
        }
    }
}
