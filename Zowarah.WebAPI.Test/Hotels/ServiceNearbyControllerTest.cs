﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.Application.Services.Common;

namespace Zowarah.WebAPI.Test.Hotels {
    public class ServiceNearbyControllerTest : BaseTesting {
        public string ApiUrl => "/api/Hotels/ServiceNearby";
        public ServiceNearbyControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Get_ServiceNearByTypes() {
            var response = await Client.GetAsync($"{ApiUrl}/GetServiceNearByTypes");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<ICollection<ServiceNearbyCategoryQuery>>();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_GetAll_ServiceNearby() {
            var response = await Client.GetAsync($"{ApiUrl}/GetAllServiceNearby/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<ICollection<ServiceNearbyQuery>>();
            content.Should().NotBeNullOrEmpty();
        }


        [Fact]
        public async Task Test_Update_ServiceNearby() {
            // Act
            var response = await Client.PostAsync($"{ApiUrl}/UpdateServiceNearby/{1}", new StringContent(
                JsonConvert.SerializeObject(
                            new List<ServiceNearbyQuery> {
                                new ServiceNearbyQuery{
                                    Distance=100,
                                    Id=1,
                                    Name="test",
                                    Unit="meters"
                            },
                                 new ServiceNearbyQuery{
                                    Distance=100,
                                    Name="test",
                                    Unit="feet"
                            }
                            }), Encoding.UTF8, "application/json"));

            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();

            // return int
            var content = await response.Content.ReadAsAsync<List<ServiceNearbyQuery>>();
            content.Should().NotBeNullOrEmpty();
        }
    }
}
