﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Test.Hotels
{
    public class PropertyTypesControllerTest : BaseTesting {
        public string ApiHotelUrl => "/api/Hotels/PropertyTypes";
        public PropertyTypesControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }       

        [Fact]
        public async Task Test_GetAll_PropertyTypeListRequest() {
            var response = await Client.GetAsync($"{ApiHotelUrl}/GetAll/");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<List<GetPropertyTypeResponse>>();
            content.Should().NotBeNullOrEmpty();

        }

    }
}
