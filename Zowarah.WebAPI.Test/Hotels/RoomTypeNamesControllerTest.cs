﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Test.Hotels
{
    public class RoomTypeNamesControllerTest : BaseTesting {
        public string ApiUrl => "/api/Hotels/RoomTypeNames";
        public RoomTypeNamesControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_GetAll_RoomTypesNames() {
            var response = await Client.GetAsync($"{ApiUrl}/GetAll");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<List<GetRoomTypeNameResponse>>();
            content.Should().NotBeNullOrEmpty();
        }
    }
}
