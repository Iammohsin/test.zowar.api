﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.WebAPI.Handlers.Hotels;

namespace Zowarah.WebAPI.Test.Hotels
{
    public class InternetOptionsControllerTest : BaseTesting {
        public string ApiUrl => "/api/Hotels/InternetOptions";
        public InternetOptionsControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }


       

        [Fact]
        public async Task Test_GetAll_InternetOptions() {
            var response = await Client.GetAsync($"{ApiUrl}/GetAll/");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<IList<GetInternetOptionsResponse>>();
            content.Should().NotBeNullOrEmpty();
            // lookup repository error 
        }
    }
}
