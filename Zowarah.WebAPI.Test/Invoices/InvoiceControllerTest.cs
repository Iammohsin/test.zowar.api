﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zowarah.Application.Enums.Hotels;
using Zowarah.Application.Services.Invoices;
using Zowarah.Domain.Entities.Common;
using Zowarah.Domain.Entities.Invoices;
using Zowarah.WebAPI.Handlers.Invoices;

namespace Zowarah.WebAPI.Test.Invoices {
    public class InvoiceControllerTest : BaseTesting {
        public string ApiUrl => "/api/Invoices/Invoice";
        public InvoiceControllerTest(CustomWebApplicationFactory<Startup> factory) : base(factory) {
        }

        [Fact]
        public async Task Test_Get_Invoice() {
            var response = await Client.GetAsync($"{ApiUrl}/GetInvoice/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetInvoiceResponse>();
            content.Should().NotBeNull();
        }


        [Theory]
        [InlineData(1, 0, "", 0, 0, InvoiceStatus.Pending)]
        [InlineData(0, 0, "", 0, 0, InvoiceStatus.Pending)]
        [InlineData(0, 0, "1", 0, 0, InvoiceStatus.Pending)]
        [InlineData(0, 1, "1", 0, 0, InvoiceStatus.Pending)]
        [InlineData(0, 0, "1", 1, 0, InvoiceStatus.Pending)]
        [InlineData(0, 0, "1", 0, 1, InvoiceStatus.Pending)]
        public async Task Test_GetAll_Invoices(int clientId, int cityId, string invoiceId, int providerId, long serviceId, InvoiceStatus status) {
            var response = await Client.PostAsync($"{ApiUrl}/GetAllInvoices", new StringContent(
                JsonConvert.SerializeObject(
                            new GetAllInvoicesByFilterRequest() {
                                ClientId = clientId,
                                City = cityId,
                                From = DateTime.Now,
                                To = DateTime.Now,
                                InvoiceId = invoiceId,
                                ProviderId = providerId,
                                ServiceId = serviceId,
                                Status = status
                            }), Encoding.UTF8, "application/json"));
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<ICollection<InvoiceQuery>>();
            content.Should().NotBeNullOrEmpty();
        }

        [Theory]
        [InlineData(0, ReportView.years, 0, 0, InvoiceStatus.Pending)]
        [InlineData(0, ReportView.months, 0, 0, InvoiceStatus.Pending)]
        [InlineData(0, ReportView.days, 0, 0, InvoiceStatus.Pending)]
        [InlineData(1, ReportView.years, 0, 0, InvoiceStatus.Pending)]
        [InlineData(0, ReportView.years, 1, 0, InvoiceStatus.Pending)]
        [InlineData(0, ReportView.years, 0, 1, InvoiceStatus.Pending)]
        public async Task Test_Get_InvoicesReport(int cityId, ReportView reportView, int providerId, long serviceId, InvoiceStatus status) {
            var response = await Client.PostAsync($"{ApiUrl}/GetInvoicesReport", new StringContent(
                JsonConvert.SerializeObject(
                            new GetInvoicesReportsRequest() {
                                View = reportView,
                                City = cityId,
                                From = DateTime.Now,
                                To = DateTime.Now,
                                ServiceType = ServiceTypeEnum.Hotel,
                                ProviderId = providerId,
                                ServiceId = serviceId,
                                Status = status
                            }), Encoding.UTF8, "application/json"));
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<ICollection<InvoiceReport>>();
            content.Should().NotBeNullOrEmpty();
        }


        [Theory]
        [InlineData(0, ReportView.days, 0, 0, InvoiceStatus.Pending)]
        [InlineData(1, ReportView.days, 0, 0, InvoiceStatus.Pending)]
        [InlineData(0, ReportView.days, 1, 0, InvoiceStatus.Pending)]
        [InlineData(0, ReportView.days, 0, 1, InvoiceStatus.Pending)]
        public async Task Test_Get_InvoicesReportDetails(int cityId, ReportView reportView, int providerId, long serviceId, InvoiceStatus status) {
            var response = await Client.PostAsync($"{ApiUrl}/GetInvoicesReportDetails/{DateTime.Now.ToString("yyyy-MM-dd")}", new StringContent(
                JsonConvert.SerializeObject(
                            new GetInvoicesReportDetailsRequest() {
                                View = reportView,
                                City = cityId,
                                From = DateTime.Now,
                                To = DateTime.Now,
                                ServiceType = ServiceTypeEnum.Hotel,
                                ProviderId = providerId,
                                ServiceId = serviceId,
                                Status = status
                            }), Encoding.UTF8, "application/json"));
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetInvoicesReportDetailsResponse>();
            content.Should().NotBeNull();
        }


        [Fact]
        public async Task Test_GetAll_InvoicesTransactions() {
            var response = await Client.GetAsync($"{ApiUrl}/GetAllInvoicesTransactions/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<ICollection<InvoiceTransaction>>();
            content.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task Test_Get_TransactionInvoiceDetails() {
            var response = await Client.GetAsync($"{ApiUrl}/GetTransactionInvoiceDetails/{1}");
            // Assert 
            // return result Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            response.EnsureSuccessStatusCode();
            // return      
            var content = await response.Content.ReadAsAsync<GetTransactionInvoiceDetailsResponse>();
            content.Should().NotBeNull();
        }
    }
}
